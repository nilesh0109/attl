<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function CreateSideBars() {

  register_sidebar(array(
    'name' => HOME_WIDGET_PLACEHOLDER_NAME,
    'id' => HOME_WIDGET_PLACEHOLDER_ID,
  ));
}

add_action('widgets_init', 'CreateSideBars');

/**
 * Spotlight widget for the Pages other than Home Page
 *
 */
class Spotlight extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  public function __construct() {

    parent::__construct(
            SPOTLIGHT_WIDGET_NAME, // Base ID
            SPOTLIGHT_WIDGET_ID, // Name
            array('description' => __('Widget to dispaly spotlights'))// Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget($args, $instance) {
    global $spotlightSection;
// outputs the content of the widget
    if ($instance[spotlight_id] == 0 || $instance[spotlight_id] == "") {
      return;
    }
    $obSpotlight = get_post($instance[spotlight_id]);
    $arPostMeta = get_post_meta($obSpotlight->ID);
    $image = wp_get_attachment_image_src($arPostMeta[_thumbnail_id][0], 'full');
    $alt_text = get_post_meta($arPostMeta[_thumbnail_id][0], '_wp_attachment_image_alt', true);
    if (empty($alt_text)) {
      $alt_text = $obSpotlight->post_title;
    }
    if (isset($arPostMeta[CTA_TARGET_ATTRIBUTE][0]) && $arPostMeta[CTA_TARGET_ATTRIBUTE][0] == 1) {
      $target = 'target ="_blank"';
    } else {

      $target = '';
    }
    if (class_exists('Utility')) {
      global $obUtility;
      $cta_url = $obUtility->getCtaUrl($arPostMeta[CTA_URL_ATTRIBUTE][0], $arPostMeta[CTA_URL_TYPE][0]);
    }
    if (class_exists('ContainerTag')) {
      global $obContainerTag;
      $container_tag_string = $obContainerTag->getTagsForCta($obSpotlight->ID, $arPostMeta[CTA_URL_TYPE][0], $obSpotlight->post_title, SPOTLIGHT_CUSTOM_POST);
      //modifying the spotlight id for container tag for clicking on spotlight image
      $container_tag_string_image = $obContainerTag->getTagsForCta('image_' . $obSpotlight->ID, $arPostMeta[CTA_URL_TYPE][0], $obSpotlight->post_title, SPOTLIGHT_CUSTOM_POST);
    }
    $cta_lable = isset($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) ? $arPostMeta[CTA_LABEL_ATTRIBUTE][0] : '';
    $cta_lable = esc_attr($cta_lable);
    ?> 

    <?php
    switch ($args['id']) {

      case HOME_WIDGET_PLACEHOLDER_ID:
        $class_name = "span4";
        break;
      default:
        $class_name = "";
        break;
    }
    if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_YOUTUBE) {
      $video_id = $args['widget_id'];
      if (empty($image)) {
        $image_url = YOUTUBE_IMAGE_PREFIX . $arPostMeta[CTA_URL_ATTRIBUTE][0] . YOUTUBE_IMAGE_SUFFIX;
      } else {
        $image_url = $image[0];
      }
      ?>
      <article class="<?php echo $class_name; ?>">
          <div class="teaser3 video spotlight-<?php echo $obSpotlight->ID ?>">
              <?php if (!empty($image_url)) { ?>
                <figure>
                    <a <?php echo $container_tag_string_image; ?> href="#<?php echo $video_id ?>" role="button" class="btn-link" data-toggle="my-modal">
                        <img src="<?php
                        echo $image_url;
                        ?>" alt="<?php echo esc_attr($alt_text); ?>" title="<?php echo $obSpotlight->post_title ?>" />
                    </a>
                </figure>
              <?php } ?>
              <?php if (!empty($obSpotlight->post_title) || !empty($obSpotlight->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                <div class="caption">
                    <?php if (!empty($obSpotlight->post_title)) { ?><h3><?php echo $obSpotlight->post_title ?></h3><?php } ?>
                    <?php if (!empty($obSpotlight->post_content)) { ?><p><?php echo $obSpotlight->post_content ?></p><?php } ?>
                    <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                      <a <?php echo $container_tag_string; ?> href="#<?php echo $video_id ?>" role="button" class="btn btn-small" title="<?php
                      if (!empty($obSpotlight->post_title)) {
                        echo $obSpotlight->post_title;
                      } else {
                        echo $cta_lable;
                      }
                      ?>" data-toggle="my-modal"><?php echo $cta_lable; ?></a>
                <?php } ?>
                </div>
      <?php } ?>
              <!-- /.caption -->

              <div class="panel"></div>
              <!-- Modal -->
              <div id="<?php echo $video_id ?>" class="my-modal hide" tabindex="-1" role="dialog">
                  <div class="my-modal-header">
                      <button type="button" class="close" data-dismiss="my-modal">x</button>
                      <h3><?php echo $obSpotlight->post_title ?></h3>
                  </div>
                  <div class="my-modal-body">
                      <span class="model-iframe" data-href="<?php echo $cta_url ?>" data-width="100%" data-height="349"></span>
                  </div>

              </div>
          </div>
          <!-- /.teaser4 -->
      </article>
      <!-- /.span4 -->


      <?php
    } else if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_VIDEO) {
      $video_id = $args['widget_id'];
      $image_url = $image[0];
      ?>
      <article class="<?php echo $class_name; ?>">
          <div class="teaser3 video spotlight-<?php echo $obSpotlight->ID ?>">
      <?php if (!empty($image_url)) { ?>
                <figure>
                    <a <?php echo $container_tag_string_image; ?> href="#<?php echo $video_id ?>" role="button" class="btn-link" data-toggle="my-modal">
                        <img src="<?php
                echo $image_url;
                ?>" alt="<?php echo esc_attr($alt_text); ?>" title="<?php echo $obSpotlight->post_title ?>" />
                    </a>
                </figure>
                  <?php } ?>
                  <?php if (!empty($obSpotlight->post_title) || !empty($obSpotlight->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                <div class="caption">
                    <?php if (!empty($obSpotlight->post_title)) { ?><h3><?php echo $obSpotlight->post_title ?></h3><?php } ?>
                    <?php if (!empty($obSpotlight->post_content)) { ?><p><?php echo $obSpotlight->post_content ?></p><?php } ?>
                    <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                      <a <?php echo $container_tag_string; ?> href="#<?php echo $video_id ?>" role="button" class="btn btn-small" title="<?php
                      if (!empty($obSpotlight->post_title)) {
                        echo $obSpotlight->post_title;
                      } else {
                        echo $cta_lable;
                      }
                      ?>" data-toggle="my-modal"  video-href="<?php echo $arPostMeta[CTA_URL_ATTRIBUTE][0] ?>"><?php echo $cta_lable; ?></a>
        <?php } ?>
                </div>
      <?php } ?>
              <!-- /.caption -->

              <div class="panel"></div>
              <!-- Modal -->
              <div id="<?php echo $video_id ?>" class="my-modal hide" tabindex="-1" role="dialog">
                  <div class="my-modal-header">
                      <button type="button" class="close" data-dismiss="my-modal">x</button>
                      <h3><?php echo $obSpotlight->post_title ?></h3>
                  </div>
                  <div class="my-modal-body">
                      <span class="model-iframe" data-href="<?php echo $cta_url ?>" data-width="100%" data-height="349"></span>
                  </div>

              </div>
          </div>
          <!-- /.teaser4 -->
      </article>
      <!-- /.span4 -->

            <?php } else {
              ?>
     
        <article class="col-sm-4">
          <div class="teasers">
                  <?php
                  if (!empty($image)) {
                   
                      //when some action is selected then the link will work  
                        ?>
                      <figure>
                          <img src="<?php
                      if ($image) {
                        echo $image[0];
                      }
                      ?>" alt="<?php echo $alt_text; ?>" title="<?php echo $obSpotlight->post_title ?>" />
                      </figure>

                        
                      
               
                        <?php 
                  }
      ?>        
        <div class="teaser-caption">
            <h4><?php if (!empty($obSpotlight->post_title)) { ?><?php echo $obSpotlight->post_title ?><?php } ?></h4>
            <p><?php echo $obSpotlight->post_content ?></p>
               <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                                <a <?php echo $container_tag_string; ?><?php echo $target; ?> href="<?php echo $cta_url ?>" class="small-yellow-button" title="<?php
                            if (!empty($cta_lable)) {
                              echo $cta_lable;
                            } else {
                              echo $obSpotlight->post_title;
                            }
                            ?>"><?php echo $cta_lable; ?></a>
              <?php } ?>
        </div>

              <!-- /.caption -->
          </div>
          <!-- /.teaser3 -->
      </article>
    
   
      <!-- /.span3 -->
      <?php
    }
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   *
   * @return string Back-end widget form
   */
  public function form($instance) {

    $selectedSpotlight = isset($instance['spotlight_id']) ? $instance['spotlight_id'] : '';
    $args = array('post_type' => SPOTLIGHT_CUSTOM_POST, 'posts_per_page' => -1);
    $arSpotlights = get_posts($args);
// If no menus exists, direct the user to go and create some.
    if (!$arSpotlights) {
      _e('No SpotLight have been created yet.');
      return;
    }
    ?>

    <p>
        <label for="<?php echo $this->get_field_id('spotlight_id'); ?>"><?php _e('Select Spotlight:'); ?></label>
        <select id="<?php echo $this->get_field_id('spotlight_id'); ?>" name="<?php echo $this->get_field_name('spotlight_id'); ?>">
    <?php
    foreach ($arSpotlights as $obSpotlights) {
      echo '<option value="' . $obSpotlights->ID . '"'
      . selected($selectedSpotlight, $obSpotlights->ID, false)
      . '>' . $obSpotlights->post_title . '</option>';
    }
    ?>
        </select>
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   *
   * @see WP_Widget::update()
   */
  public function update($new_instance, $old_instance) {
// processes widget options to be saved
    $instance['spotlight_id'] = (int) $new_instance['spotlight_id'];
    return $instance;
  }

}

add_action('widgets_init', function() {
  register_widget('Spotlight');
});

