<?php

class SiteSetup {

    public $footer_menu_id;
    public $sitemap_info_menu_id;
    public $sitemap_search_menu_id;
    public $top_header_menu_id;
    public $header_menu_id;
    public $left_menu_article_id;
    public $left_menu_product_id;
    Public $article_landing_page_id;
    Public $product_landing_page_id;
    Public $recipes_landing_page_id;
    public $contact_us_page_id;
    public $about_us_page_id;
    public $sitemap_page_id;
    public $sitemap_xml_page_id;
    public $header_page_id;
    public $footer_page_id;
    Public $menu_item_ids;
    Public $page_ids;

    function __construct() {

        add_action('after_switch_theme', array($this, 'doAction'), 10, 2);
    }

    function doAction() {

        $this->menu_item_ids = get_option(SITE_SETUP_MENU_ITEMS_IDS);
        $this->page_ids = get_option(SITE_SETUP_PAGE_IDS);
        $this->createBasicTaxonomyTerms();
        $this->createBasicSitePages();
        $this->registerNavMenus(); // DO NOT CHANGE THE ORDER OF THE FUNCTION THEY ARE CALLED. THIS FUNCTION HAS DEPENDENCY ON createBasicSitePages
        $this->removeSamplePosts();
        $this->updateMenuIds();
        $this->setThumbnailSize();
        $this->setupRewriteRules();
        $this->flushRewriteRules();
    }

    // registers nav menu place holders for the header ,footer and leftnav
    function registerNavMenus() {


        register_nav_menus(
                array(
                    CUSTOM_HEADER_MENU_LOCATION => CUSTOM_HEADER_MENU_NAME,
                    CUSTOM_LEFT_NAVIGATION_ARTICLE_LOCATION => CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME,
                    CUSTOM_LEFT_NAVIGATION_PRODUCT_LOCATION => CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME,
                    CUSTOM_FOOTER_MENU_LOCATION => CUSTOM_FOOTER_MENU_NAME,
                    CUSTOM_SITEMAP_INFO_MENU_LOCATION => CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME,
                    CUSTOM_SITEMAP_SEARCH_MENU_LOCATION => CUSTOM_SITEMAP_INFO_MENU_NAME,
                    CUSTOM_SITEMAP_SEARCH_MENU_LOCATION => CUSTOM_SITEMAP_SEARCH_MENU_NAME,
                    CUSTOM_TOP_HEADER_MENU_LOCATION => CUSTOM_TOP_HEADER_MENU_NAME,
                    CUSTOM_SITEMAP_STATIC_PAGES_LOCATION => CUSTOM_SITEMAP_STATIC_PAGES,
                )
        );
        $arLocations = get_theme_mod('nav_menu_locations');
        $obTerm = wp_get_nav_menu_object(CUSTOM_HEADER_MENU_NAME);
        $this->header_menu_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME);
        $this->left_menu_article_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME);
        $this->left_menu_product_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_FOOTER_MENU_NAME);
        $this->footer_menu_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_SITEMAP_INFO_MENU_NAME);
        $this->sitemap_info_menu_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_SITEMAP_SEARCH_MENU_NAME);
        $this->sitemap_search_menu_id = $obTerm->term_id;
        $obTerm = wp_get_nav_menu_object(CUSTOM_TOP_HEADER_MENU_NAME);
        $this->top_header_menu_id = $obTerm->term_id;
        //now see if the main navigation menu is there - if not, create it.
        if (!wp_get_nav_menu_object(CUSTOM_HEADER_MENU_NAME)) {
            $this->header_menu_id = wp_create_nav_menu(CUSTOM_HEADER_MENU_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_HEADER_MENU_LOCATION] = $this->header_menu_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME)) {
            $this->left_menu_article_id = wp_create_nav_menu(CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_LEFT_NAVIGATION_ARTICLE_LOCATION] = $this->left_menu_article_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME)) {
            $this->left_menu_product_id = wp_create_nav_menu(CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_LEFT_NAVIGATION_PRODUCT_LOCATION] = $this->left_menu_product_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_FOOTER_MENU_NAME)) {

            $this->footer_menu_id = wp_create_nav_menu(CUSTOM_FOOTER_MENU_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_FOOTER_MENU_LOCATION] = $this->footer_menu_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_SITEMAP_INFO_MENU_NAME)) {

            $this->sitemap_info_menu_id = wp_create_nav_menu(CUSTOM_SITEMAP_INFO_MENU_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_SITEMAP_INFO_MENU_LOCATION] = $this->sitemap_info_menu_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_SITEMAP_STATIC_PAGES)) {

            $this->sitemap_info_menu_id = wp_create_nav_menu(CUSTOM_SITEMAP_STATIC_PAGES); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_SITEMAP_STATIC_PAGES_LOCATION] = $this->sitemap_info_menu_id; //set our new menu to be the main nav
        }
         if (!wp_get_nav_menu_object(CUSTOM_TOP_HEADER_MENU_NAME)) {

            $this->sitemap_search_menu_id = wp_create_nav_menu(CUSTOM_TOP_HEADER_MENU_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_SITEMAP_INFO_MENU_LOCATION] = $this->sitemap_search_menu_id; //set our new menu to be the main nav
        }
        if (!wp_get_nav_menu_object(CUSTOM_TOP_HEADER_MENU_NAME)) {
            $this->top_header_menu_id = wp_create_nav_menu(CUSTOM_TOP_HEADER_MENU_NAME); //create the menu
            //get the menu locations
            $arLocations[CUSTOM_TOP_HEADER_MENU_LOCATION] = $this->top_header_menu_id; //set our new menu to be the main nav
        }
        set_theme_mod('nav_menu_locations', $arLocations); //update

        $this->addHeaderMenuItems();
        $this->addFooterMenuItems();
    }

    // add default menu items for the footer menu
    public function addFooterMenuItems() {
        $menu_id = $this->footer_menu_id;
        $menu_name = CUSTOM_FOOTER_MENU_NAME;
        $menu_location = CUSTOM_FOOTER_MENU_LOCATION;
        $arMenuItems = wp_get_nav_menu_items($menu_id);
        $arFooterOption = $this->menu_item_ids;



        // Set up default Mizkan footer links and add them to the menu.
        if (empty($arFooterOption) || !get_post($arFooterOption['about_us'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->about_us_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(ABOUT_US_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arFooterOption['about_us'] = $id;
        }
        if (empty($arFooterOption) || !get_post($arFooterOption['contact_us'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->contact_us_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(CONTACT_US_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arFooterOption['contact_us'] = $id;
        }
        if (empty($arFooterOption) || !get_post($arFooterOption['sitemap'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->sitemap_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(SITEMAP_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arFooterOption['sitemap'] = $id;
        }
        $filePath = WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/footer_menu.php';
        if (!file_exists($file_path)) {
            require_once($filePath);
        }
        $this->menu_item_ids = $arFooterOption;
    }

    // add default menu items for the header menu
    public function addHeaderMenuItems() {

        $menu_id = $this->header_menu_id;
        $menu_name = CUSTOM_HEADER_MENU_NAME;
        $menu_location = CUSTOM_HEADER_MENU_LOCATION;

        $arMenuItems = wp_get_nav_menu_items($menu_id);
        $arHeaderOption = $this->menu_item_ids;

        // Set up default Mizkan Header links and add them to the menu.
        if (empty($arHeaderOption) || !get_post($arHeaderOption['home'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object' => 'custom',
                'menu-item-title' => __(HOME_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => get_site_url('', '', 'http'),
                'menu-item-status' => 'publish'));
            $arHeaderOption['home'] = $id;
        }
        if (empty($arHeaderOption) || !get_post($arHeaderOption['product_landing'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->product_landing_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(PRODUCT_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arHeaderOption['product_landing'] = $id;
        }
        if (empty($arHeaderOption) || !get_post($arHeaderOption['article_landing'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->article_landing_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(ARTICLE_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arHeaderOption['article_landing'] = $id;
        }
        if (empty($arHeaderOption) || !get_post($arHeaderOption['recipes_landing'])) {
            $id = wp_update_nav_menu_item($menu_id, 0, array(
                'menu-item-object-id' => $this->recipes_landing_page_id,
                'menu-item-object' => 'page',
                'menu-item-type' => 'post_type',
                'menu-item-title' => __(RECIPES_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME),
                'menu-item-classes' => '',
                'menu-item-url' => '',
                'menu-item-status' => 'publish'));
            $arHeaderOption['recipes_landing'] = $id;
        }
        $this->menu_item_ids = $arHeaderOption;
    }

    // Create Basic Term Taxonomies for Home Page/ Product Landing Page/ Article Landing Page
    public function createBasicTaxonomyTerms() {
        // Create Home Page Carousel Term
        $arParameter['slug'] = CAROUSEL_HOME_PAGE;
        wp_insert_term(__('Home Page'), CAROUSEL_TAXONOMY, $arParameter);

        // Create Product Landing Page Carousel Term

        $arParameter['slug'] = CAROUSEL_PRODUCT_LANDING;
        wp_insert_term(__('Product Landing Page'), CAROUSEL_TAXONOMY, $arParameter);

        // Create Article Landing Page Carousel Term
        $arParameter['slug'] = CAROUSEL_ARTICLE_LANDING;
        wp_insert_term(__('Article Landing Page'), CAROUSEL_TAXONOMY, $arParameter);
        
        // Create Recipes Landing Page Carousel Term
        $arParameter['slug'] = CAROUSEL_RECIPES_LANDING;
        wp_insert_term(__('Recipes Landing Page'), CAROUSEL_TAXONOMY, $arParameter);

        // Create Home Page Teaser Carousel Term
        $arParameter['slug'] = TEASER_CAROUSEL_HOME_PAGE;
        wp_insert_term(__('Home Page'), TEASER_TAXONOMY, $arParameter);
        
        $arParameter['slug'] = TEASER_CAROUSEL_RECIPES_PAGE;
        wp_insert_term(__('Recipes Page'), TEASER_TAXONOMY, $arParameter);

        // Create Product Landing Page Promo2 Term
        $arParameter['slug'] = PROMO2_COL_PRODUCT_LANDING;
        wp_insert_term(__('Product Landing Page'), PROMO_COL_TAXONOMY, $arParameter);

        // Create Product Landing Page Promo3 Term
        $arParameter['slug'] = PROMO3_COL_PRODUCT_LANDING;
        wp_insert_term(__('Product Landing Page'), PROMO_COL_TAXONOMY, $arParameter);
        
        // Create Recipes Landing Page Promo2 Term
        $arParameter['slug'] = PROMO2_COL_RECIPES_LANDING;
        wp_insert_term(__('Recipes Landing Page'), PROMO_COL_TAXONOMY, $arParameter);
        
         $arParameter['slug'] = PROMO3_COL_RECIPES_LANDING;
        wp_insert_term(__('Recipes Landing Page'), PROMO_COL_TAXONOMY, $arParameter);
        
        
        // Create Recipes Landing Page Promo2 Term
        $arParameter['slug'] = PROMO2_COL_ARTICLE_LANDING;
        wp_insert_term(__('Article Landing Page'), PROMO_COL_TAXONOMY, $arParameter);
        
         $arParameter['slug'] = PROMO3_COL_ARTICLE_LANDING;
        wp_insert_term(__('Article Landing Page'), PROMO_COL_TAXONOMY, $arParameter);
    }

    public function createBasicSitePages() {

        $arDefaultPages = $this->page_ids;

        $arPageAttribute = array(
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => PAGE_CREATOR_ID,
            'post_type' => 'page',
            'post_category' => array(0)
        );
        // Create Product Landing Page
        $this->product_landing_page_id = $arDefaultPages['product_landing'];
        if (empty($this->product_landing_page_id) || !get_post($this->product_landing_page_id)) {
            $arPageAttribute['post_title'] = __(PRODUCT_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(PRODUCT_LANDING_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->product_landing_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->product_landing_page_id, '_wp_page_template', PRODUCT_LANDING_PAGE_TEMPLATE);
            $arDefaultPages['product_landing'] = $this->product_landing_page_id;
        }
        // Create Article Landing Page
        $this->article_landing_page_id = $arDefaultPages['article_landing'];
        if (empty($this->article_landing_page_id) || !get_post($this->article_landing_page_id)) {
            $arPageAttribute['post_title'] = __(ARTICLE_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(ARTICLE_LANDING_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->article_landing_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->article_landing_page_id, '_wp_page_template', ARTICLE_LANDING_PAGE_TEMPLATE);
            $arDefaultPages['article_landing'] = $this->article_landing_page_id;
        }
        // Create Contact us Page
        $this->contact_us_page_id = $arDefaultPages['contact_us'];
        if (empty($this->contact_us_page_id) || !get_post($this->contact_us_page_id)) {

            $arPageAttribute['post_title'] = __(CONTACT_US_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(CONTACT_US_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->contact_us_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->contact_us_page_id, '_wp_page_template', CONTACT_US_TEMPLATE);
            $arDefaultPages['contact_us'] = $this->contact_us_page_id;
        }
        //create About us Page
        $this->about_us_page_id = $arDefaultPages['about_us'];
        if (empty($this->about_us_page_id) || !get_post($this->about_us_page_id)) {
            $arPageAttribute['post_title'] = __(ABOUT_US_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(ABOUT_US_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->about_us_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->about_us_page_id, '_wp_page_template', ABOUT_US_TEMPLATE);
            $arDefaultPages['about_us'] = $this->about_us_page_id;
        }
        //create Sitemap Page
        $this->sitemap_page_id = $arDefaultPages['site_map'];
        if (empty($this->sitemap_page_id) || !get_post($this->sitemap_page_id)) {
            $arPageAttribute['post_title'] = __(SITEMAP_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(SITEMAP_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->sitemap_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->sitemap_page_id, '_wp_page_template', SITEMAP_TEMPLATE);
            $arDefaultPages['site_map'] = $this->sitemap_page_id;
        }

        //create Sitemap xml Page
        $this->sitemap_xml_page_id = $arDefaultPages['site_map_xml'];
        if (empty($this->sitemap_xml_page_id) || !get_post($this->sitemap_xml_page_id)) {
            $arPageAttribute['post_title'] = SITEMAP_XML_PAGE_TITLE;
            $arPageAttribute['post_name'] = SITEMAP_XML_SLUG;
            $this->sitemap_xml_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->sitemap_xml_page_id, '_wp_page_template', SITEMAP_XML_TEMPLATE);
            $arDefaultPages['site_map_xml'] = $this->sitemap_xml_page_id;
        }
        //create Header Tempalte Page
        $this->header_page_id = $arDefaultPages['header_page'];
        if (empty($this->header_page_id) || !get_post($this->header_page_id)) {
            $arPageAttribute['post_title'] = HEADER_PAGE_TITLE;
            $arPageAttribute['post_name'] = HEADER_SLUG;
            $this->header_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->header_page_id, '_wp_page_template', HEADER_TEMPLATE);
            $arDefaultPages['header_page'] = $this->header_page_id;
        }
        //create Footer Tempalte Page
        $this->footer_page_id = $arDefaultPages['footer_page'];
        if (empty($this->footer_page_id) || !get_post($this->footer_page_id)) {
            $arPageAttribute['post_title'] = FOOTER_PAGE_TITLE;
            $arPageAttribute['post_name'] = FOOTER_SLUG;
            $this->footer_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->footer_page_id, '_wp_page_template', FOOTER_TEMPLATE);
            $arDefaultPages['footer_page'] = $this->footer_page_id;
        }
        // Create Recipes Landing Page
        $this->recipes_landing_page_id = $arDefaultPages['recipes_landing'];
        if (empty($this->recipes_landing_page_id) || !get_post($this->recipes_landing_page_id)) {
            $arPageAttribute['post_title'] = __(RECIPES_LANDING_PAGE_TITLE, LANGUAGE_DOMAIN_NAME);
            $arPageAttribute['post_name'] = __(RECIPES_LANDING_SLUG, LANGUAGE_DOMAIN_NAME);
            $this->recipes_landing_page_id = wp_insert_post($arPageAttribute);
            update_post_meta($this->recipes_landing_page_id, '_wp_page_template', RECIPES_LANDING_PAGE_TEMPLATE);
            $arDefaultPages['recipes_landing'] = $this->recipes_landing_page_id;
        }
        update_option(SITE_SETUP_PAGE_IDS, $arDefaultPages); // Update Site_option table with Default Page Ids
    }

    function checkPageExists($slug) {
        $arPage = array('name' => $slug, 'post_type' => 'page', 'post_status' => 'publish', 'posts_per_page' => 1);
        $arPostDetail = get_posts($arPage);
        if (count($arPostDetail) > 0) {
            return $arPostDetail[0]->ID;
        }
        return false;
    }

    function updateMenuIds() {

        update_option(SITE_SETUP_MENU_ITEMS_IDS, $this->menu_item_ids);
    }

    // deletes sample page and post created by wordpress on installation

    function removeSamplePosts() {
        wp_delete_post(1, true);
        wp_delete_post(2, true);
    }

    function setThumbnailSize() {
        update_option('thumbnail_size_w', THUMBNAIL_IMAGE_WIDTH);
        update_option('thumbnail_size_h', THUMBNAIL_IMAGE_HEIGHT);
        update_option('thumbnail_crop', THUMBNAIL_CROP_IMAGE);
    }

    /**
     * Flush your rewrite rules
     */
    function flushRewriteRules() {
        flush_rewrite_rules();
    }
    /**
     * To update the permalink of the site
     * 
     * @global type $wp_rewrite to get the rewrite object
     */
    public function setupRewriteRules() {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure('/%postname%');
    }

}

new SiteSetup();
?>