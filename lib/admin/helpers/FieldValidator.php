<?php

/**
 * This document set is the property of Mizkan, and contains confidential and trade secret
 * information.
 * It cannot be transferred from the custody or control of Mizkan except as
 * authorized in writing by an officer of Mizkan. Neither this item nor the information it
 * contains can be used, transferred, reproduced, published, or disclosed, in whole or in part,
 * directly or indirectly, except as expressly authorized by an officer of Mizkan, pursuant to
 * written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * @category FieldValidation
 * @package Mizkan
 * @author Manish Tanwar <mtanwar@sapient.com>
 */

/**
 * Validates input for form fields.
 */
abstract class FieldValidator {

    /**
     * Validates image field.
     *
     * @param File   $file      Reference to the uploaded file
     * @param string $fieldName Name of the field on which validation is happening.
     * @param array  $settings  Validation settings overrides for default values.
     *
     * @return boolean True, if image is valid.
     */
    public static function ValidateImage($file, $fieldName, $settings = array()) {
        // Default values for image validation.
        $file_types = FileTypes::$IMAGE;
        $max_file_size = 2120000;

        // Check if image validation defaults have been overwritten.
        if (count($settings) > 0) {
            if (isset($settings['filetypes'])) {
                $file_types = $settings['filetypes'];
            }

            if (isset($settings['maxfilesize'])) {
                $max_file_size = $settings['maxfilesize'];
            }
        }

        // If file is an uploaded one.
        if ($file != null && is_array($file)) {
            $size = $file['size'];
            if ($size >= $max_file_size) {
                add_settings_error($fieldName, 'Image_Big_Size', __('Please enter a valid size image'), 'error');

                return false;
            } else {
                if ($file['error'] !== UPLOAD_ERR_OK) {
                    add_settings_error($fieldName, 'Image_Upload_Failed', __('File upload failed. Please try again.'), 'error');

                    return false;
                } else {
                    $arImageData = getimagesize($file['tmp_name']);
                    if (!in_array($arImageData[2], $file_types)) {
                        add_settings_error($fieldName, 'Image_Invalid_Size', __('Please enter a valid Image Format'), 'error');

                        return false;
                    }
                }
            }
        }

        return true;
    }

}

/**
 * Defines different file types supported by the Mizkan system.
 *
 * @category FieldValidation
 * @package Mizkan
 * @author Manish Tanwar <mtanwar@sapient.com>
 */
abstract class FileTypes {

    /**
     * Image formats supported by system.
     * @var type array
     */
    public static $IMAGE = array(
        IMAGETYPE_JPEG,
        IMAGETYPE_GIF,
        IMAGETYPE_PNG
    );

}

?>