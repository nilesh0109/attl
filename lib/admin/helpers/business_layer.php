<?php

namespace AdminTheme\Helper;

/**
 * CSS helper
 * Purpose :perform replacement in tpl
 */
class CSSHelper {

    /**
     * function:getclassArray
     * @return array of classname,tpl
     */
    static function getClassArray() {
        $arIni = parse_ini_file(get_template_directory() . '/admin/data/tpls/labels/ClassToBusinessMapping.ini');
        return $arIni;
    }

    static function getUpdatedSelectorArray() {
        if (file_exists(get_template_directory() . '/admin/data/tpls/labels/Selectors.ini')) {
            $arIni = parse_ini_file(get_template_directory() . '/admin/data/tpls/labels/Selectors.ini');
            return $arIni;
        }
    }

    /**
     *
     * @param type $tpl :final result
     * @param type $css_filtered_array
     * @param type $class_name
     * @param string $attribute
     * @param type $existing
     * @return type
     */
    static function mapLabeltoAttribute($tpl, $css_filtered_array, $class_name, $attribute, $existing, $arrSelectors) {

        if ($attribute == 'background-rgba') {
            $attribute = "background";
        }
        //check if css attribute exists in both
        if (array_key_exists($class_name, $existing) && array_key_exists($class_name, $css_filtered_array)) {
            if (array_key_exists($attribute, $existing[$class_name]) && (array_key_exists($attribute, $css_filtered_array[$class_name]))) {
                $tpl = self::attributeExistsinFiltered($tpl, $css_filtered_array, $class_name, $attribute, $existing);
            }
            //check if css attribute exists in default
            elseif (array_key_exists($attribute, $existing[$class_name]) && (!array_key_exists($attribute, $css_filtered_array[$class_name]))) {
                $tpl = self::attributeExistsinExisting($tpl, $css_filtered_array, $class_name, $attribute, $existing);
            }
            //check if css attribute exists in child
            elseif (!array_key_exists($attribute, $existing[$class_name]) && (array_key_exists($attribute, $css_filtered_array[$class_name]))) {

                $tpl = self::attributeExistsinFiltered($tpl, $css_filtered_array, $class_name, $attribute, $existing);
            }
            //check if css attribute exists in none
            else {

                $tpl = self::notattributeExistsinboth($tpl, $css_filtered_array, $class_name, $attribute, $existing);
            }
        }
        /*
         * Repeat above for classes/selectors in css
         */
        if (!array_key_exists($class_name, $existing) && (array_key_exists($class_name, $css_filtered_array))) {

            $tpl = self::attributeExistsinFiltered($tpl, $css_filtered_array, $class_name, $attribute, $existing);
        } elseif (array_key_exists($class_name, $existing) && (!array_key_exists($class_name, $css_filtered_array))) {

            if (sizeof($arrSelectors) >= 1) {
                $value = array_search($class_name, $arrSelectors);
                if ($value != false) {

                    $class_name = $value;
                    if (array_key_exists($value, $css_filtered_array)) {
                        $tpl = self::attributeExistsinFiltered($tpl, $css_filtered_array, $class_name, $attribute, $existing);
                    } else {
                        $tpl = self::attributeExistsinExisting($tpl, $css_filtered_array, $class_name, $attribute, $existing);
                    }
                } else {
                    $tpl = self::attributeExistsinExisting($tpl, $css_filtered_array, $class_name, $attribute, $existing);
                }
            } else {
                $tpl = self::attributeExistsinExisting($tpl, $css_filtered_array, $class_name, $attribute, $existing);
            }
        } elseif (!array_key_exists($class_name, $existing) && (!array_key_exists($class_name, $css_filtered_array))) {

            $tpl = self::notattributeExistsinboth($tpl, $css_filtered_array, $class_name, $attribute, $existing);
        }


        return $tpl;
    }

    /**
     *
     * @param type $tpl
     * @param type $css_filtered_array
     * @param type $class_name
     * @param type $attribute
     * @param type $existing
     * @return type
     */
    static function attributeExistsinExisting($tpl, $css_filtered_array, $class_name, $attribute, $existing) {
        if (strpos($tpl, "gradient") !== false) {
            $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);

            if (isset($css_filtered_array[$class_name]["background"]) || isset($css_filtered_array[$class_name]["background-color"]) || isset($css_filtered_array[$class_name]["background-image"])) {
                $tpl = str_replace("{+startcolor+}", "#ffffff", $tpl);
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endcolor+}", "#ffffff", $tpl);
            } elseif (strpos($existing[$class_name][$attribute], "gradient") !== false) {
                $arCustom1 = explode(",", $existing[$class_name][$attribute]);
                $tpl = str_replace('value="grad-color"', 'value="grad-color" checked="checked"', $tpl);
                $arCustom1[1] = str_replace(" 0%", "", $arCustom1[1]);
                $arCustom1[2] = str_replace(" 100%", "", $arCustom1[2]);
                $arCustom1[1] = str_replace(" ", "", $arCustom1[1]);
                $arCustom1[2] = str_replace(" ", "", $arCustom1[2]);
                $tpl = str_replace("{+startcolor+}", $arCustom1[1], $tpl);
                $tpl = str_replace("{+startvalue+}", $arCustom1[1], $tpl);
                $tpl = str_replace("{+endvalue+}", str_replace(")", '', $arCustom1[2]), $tpl);
                $tpl = str_replace("{+endcolor+}", str_replace(")", '', $arCustom1[2]), $tpl);
                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);

                if (strpos($tpl, "grad-color") !== false && strpos($css_filtered_array[$class_name]["background"], "transparent") === false && !isset($css_filtered_array[$class_name]["background-color"])) {

                    $tpl = str_replace("hide-default", " ", $tpl);
                }
            } elseif (strpos($existing[$class_name]["background"], "transparent") !== false) {
                $tpl = str_replace('value="transparent"', 'value="transparent"' . ' checked="checked"', $tpl);
                $tpl = str_replace('data-default=""', 'data-default="' . $existing[$class_name]["background-color"] . '"', $tpl);
                $tpl = str_replace("{+startcolor+}", $existing[$class_name]["background-color"], $tpl);
                $tpl = str_replace("{+startvalue+}", $existing[$class_name]["background-color"], $tpl);
                $tpl = str_replace("{+endvalue+}", $existing[$class_name]["background-color"], $tpl);
                $tpl = str_replace("{+endcolor+}", $existing[$class_name]["background-color"], $tpl);
                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);
            } else {
                if ((strpos($tpl, "bg-color") !== false) && (isset($existing[$class_name][$attribute]))) {
                    if (!isset($css_filtered_array[$class_name]["background-image"]) && !(isset($css_filtered_array[$class_name]["background"]))) {
                        $tpl = str_replace("hide-default", " ", $tpl);
                    }
                }
                $tpl = str_replace('value="bg-color"', 'value="bg-color"' . ' checked="checked"', $tpl);
                $tpl = str_replace('value="#ffffff"', 'value="' . $existing[$class_name]["background-color"] . '"', $tpl);
                $tpl = str_replace('data-default=""', 'data-default="' . $existing[$class_name]["background-color"] . '"', $tpl);
                $tpl = str_replace("{+startcolor+}", "#ffffff", $tpl);
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endcolor+}", "#ffffff", $tpl);
            }
        } elseif (strpos($tpl, "<!--rgba-->") !== false) {

            if (strpos($existing[$class_name]["background"], "transparent") !== false) {
                $tpl = str_replace('value="transparent-color"', 'value="transparent"' . ' checked="checked"', $tpl);
            } else {
                $tpl = str_replace('value="bg-color"', 'value="bg-color"' . ' checked="checked"', $tpl);
                $hexcode = $existing[$class_name]["background"];
                preg_match('/\(([^\)]*)\)/', $hexcode, $matches);
                $rgb = explode(",", $matches[1]);
                $value = self::RgbToHex($rgb);
                $tpl = str_replace('value=""', 'value="' . $value . '"', $tpl);
            }
        } else {

            if (strpos($tpl, "text") !== false) {
                $tpl = str_replace('value=""', 'value="' . $existing[$class_name][$attribute] . '"', $tpl);
                $tpl = str_replace('data-default=""', 'data-default="' . $existing[$class_name][$attribute] . '"', $tpl);
            }
            if (strpos($tpl, "</select>") !== false) {
                $tpl = str_replace('value="' . $existing[$class_name][$attribute] . '"', 'value=""' . ' selected="selected"', $tpl);
            }
            if (strpos($tpl, "radio") !== false) {
                $tpl = str_replace('value="' . $existing[$class_name][$attribute] . '"', 'value="' . $existing[$class_name][$attribute] . '"' . 'checked="checked"', $tpl);
                $tpl = str_replace('data-default=""', 'data-default="' . $existing[$class_name][$attribute] . '"', $tpl);
            }
        }
        return $tpl;
    }

    /**
     *
     * @param type $tpl
     * @param type $css_filtered_array
     * @param type $class_name
     * @param type $attribute
     * @param type $existing
     * @return type
     */
    static function attributeExistsinFiltered($tpl, $css_filtered_array, $class_name, $attribute, $existing) {

        if (strpos($tpl, "gradient") !== false) {

            if (strpos($css_filtered_array[$class_name][$attribute], "gradient") !== false) {
                $arCustom1 = explode(",", $css_filtered_array[$class_name][$attribute]);
                $tpl = str_replace('value="grad-color"', 'value="grad-color" checked="checked"', $tpl);
                $arCustom1[1] = str_replace(" 0%", "", $arCustom1[1]);
                $arCustom1[2] = str_replace(" 100%", "", $arCustom1[2]);
                $arCustom1[1] = str_replace(" ", "", $arCustom1[1]);
                $arCustom1[2] = str_replace(" ", "", $arCustom1[2]);
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+startcolor+}", $arCustom1[1], $tpl);
                $tpl = str_replace("{+endcolor+}", str_replace(")", '', $arCustom1[2]), $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);
                if (strpos($tpl, "grad-color") !== false) {

                    $tpl = str_replace("hide-default", " ", $tpl);
                }
            } elseif (strpos($css_filtered_array[$class_name]["background"], "transparent") !== false) {


                $tpl = str_replace("hide-default", " ", $tpl);
                $tpl = str_replace('value="transparent"', 'value="transparent"' . ' checked="checked"', $tpl);
                $tpl = str_replace("{+startcolor+}", "#ffffff", $tpl);
                $tpl = str_replace('value=""', 'value="' . $css_filtered_array[$class_name][$attribute] . '"', $tpl);
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endcolor+}", "#ffffff", $tpl);

                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);
            } elseif (isset($css_filtered_array[$class_name]["background-color"])) {

                $tpl = str_replace("{+startcolor+}", "#ffffff", $tpl);
                $tpl = str_replace('value=""', 'value="' . $css_filtered_array[$class_name][$attribute] . '"', $tpl);
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endcolor+}", "#ffffff", $tpl);


                if ((strpos($tpl, "bg-color") !== false)) {

                    $tpl = str_replace("hide-default", " ", $tpl);
                    $tpl = str_replace('value="bg-color"', 'value="bg-color"' . ' checked="checked"', $tpl);
                }
            } else {
                $tpl = str_replace("{+startvalue+}", "#ffffff", $tpl);
                $tpl = str_replace("{+startcolor+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endcolor+}", "#ffffff", $tpl);
                $tpl = str_replace("{+endvalue+}", "#ffffff", $tpl);
                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);
            }
        } elseif (strpos($tpl, "<!--rgba-->") !== false) {

            if (strpos($css_filtered_array[$class_name]["background"], "transparent") !== false) {
                $tpl = str_replace('value="transparent-color"', 'value="transparent"' . ' checked="checked"', $tpl);
            } else {
                $tpl = str_replace('value="bg-color"', 'value="bg-color"' . ' checked="checked"', $tpl);
                $hexcode = $css_filtered_array[$class_name]["background"];
                preg_match('/\(([^\)]*)\)/', $hexcode, $matches);
                $rgb = explode(",", $matches[1]);
                $value = self::RgbToHex($rgb);
                $tpl = str_replace('value=""', 'value="' . $value . '"', $tpl);
            }
        } else {
            if ((strpos($tpl, "text") !== false) && (strpos($tpl, "file") == false)) {

                $tpl = str_replace('value=""', 'value="' . $css_filtered_array[$class_name][$attribute] . '"', $tpl);
            }
            if (strpos($tpl, "select") !== false) {
                if (strpos($css_filtered_array[$class_name][$attribute], '"') !== false) {
                    $tpl = str_replace("value='" . $css_filtered_array[$class_name][$attribute] . "'", "value='" . $css_filtered_array[$class_name][$attribute] . "'" . ' selected = "selected"', $tpl);
                } else {
                    $tpl = str_replace('value="' . $css_filtered_array[$class_name][$attribute] . '"', 'value="' . $css_filtered_array[$class_name][$attribute] . '"' . ' selected = "selected"', $tpl);
                }
            }

            if (strpos($tpl, "radio") !== false) {

                $tpl = str_replace('value="' . $css_filtered_array[$class_name][$attribute] . '"', 'value="' . $css_filtered_array[$class_name][$attribute] . '"' . 'checked="checked"', $tpl);
            }
            if (strpos($tpl, "file") !== false) {
                if (strpos($css_filtered_array[$class_name][$attribute], "gradient") === false) {
                    $tpl = str_replace('src=""', 'src="' . substr($css_filtered_array[$class_name][$attribute], 5, strlen($css_filtered_array[$class_name][$attribute]) - 7) . '"', $tpl);
                    $tpl = str_replace('value=""', 'value="' . substr($css_filtered_array[$class_name][$attribute], 5, strlen($css_filtered_array[$class_name][$attribute]) - 7) . '"', $tpl);
                }
            }
        }
        return $tpl;
    }

    /*
     * make null in case attribute do not exist
     */

    static function notattributeExistsinboth($tpl, $css_filtered_array, $class_name, $attribute, $existing) {
        if (strpos($tpl, "gradient") !== false) { {
                //echo $attribute;
                $tpl = str_replace("{+startcolor+}", "#f5f5f5", $tpl);
                $tpl = str_replace("{+startvalue+}", "#f5f5f5", $tpl);
                $tpl = str_replace("{+endvalue+}", "#f5f5f5", $tpl);
                $tpl = str_replace("{+endcolor+}", "#f5f5f5", $tpl);
                $tpl = str_replace('value=""', 'value="#ffffff"', $tpl);
            }
        }
        return $tpl;
    }

    /**
     * Converts RGB to #color code
     * @param type $rgb
     * @return type
     */
    static function RgbToHex($rgb) {
        $hex = "#";
        $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

        return $hex; // returns the hex value including the number sign (#)
    }

}

?>
