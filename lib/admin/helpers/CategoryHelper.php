<?php

/**
 * Block deletion of certain categories, if they have child elements.
 *
 * @category CategoryHelper
 * @package CategoryHelper
 * @author Manish Tanwar <mtanwar@sapient.com>
 */
$category_helper = new CategoryHelper();

/**
 * Block deletion of certain categories, if they have child elements.
 */
class CategoryHelper {

    /**
     * Checks if request for deleting a category.
     *
     * @return bool True, if request for deleting a category.
     */
    private function isCategoryDeleteRequest() {
        $isRegDeleteRequest =
                empty($_REQUEST['taxonomy']) || empty($_REQUEST['action']) || !($_REQUEST['taxonomy'] === 'category' || $_REQUEST['taxonomy'] === 'product-category' || $_REQUEST['taxonomy'] === 'article-category') || !( $_REQUEST['action'] === 'delete' || $_REQUEST['action'] === 'delete-tag');

        return !$isRegDeleteRequest;
    }

    /**
     * Checks if category deletion request is for bulk action.
     *
     * @return bool True, if request for deleting a category.
     */
    private function isBulkDeleteRequest() {
        $isRegDeleteRequest =
                empty($_REQUEST['taxonomy']) || empty($_REQUEST['action']) || !($_REQUEST['taxonomy'] === 'category' || $_REQUEST['taxonomy'] === 'product-category' || $_REQUEST['taxonomy'] === 'article-category') || !( $_REQUEST['action'] === 'delete' || $_REQUEST['action'] === 'delete-tag');

        $isCatDeleteRequest = !$isRegDeleteRequest;

        return $isCatDeleteRequest;
    }

    /**
     * Initializes the instance of CategoryHelper class
     */
    public function __construct() {
        add_filter('check_admin_referer', array($this, 'check_AdminReferrer'), 10, 2);
        add_filter('check_ajax_referer', array($this, 'check_referrer'), 10, 2);
    }

    /**
     * Checks if category is deletable or not.
     *
     * @param string $categoryID Category ID to check for.
     * 
     * @return bool True, if category doesn't contain any child item/category.
     */
    private function isUndeletable($categoryID) {
        $taxonomy = $_REQUEST['taxonomy'];
        $category = get_term_by('id', $categoryID, $taxonomy);

        $has_products = $category->count > 0;
        $has_subcategories = false;

        // If category doesn't have any product then look if it contains any sub-category.
        if (!$has_products) {
            $args = array(
                'orderby' => 'name',
                'parent' => $categoryID,
                'taxonomy' => $taxonomy,
                'pad_counts' => true,
                'hide_empty' => false
            );

            $subcategories = get_categories($args);
            $has_subcategories = count($subcategories) > 0;
        }

        return ($has_products || $has_subcategories);
    }

    /**
     * Ajax call hook.
     * @param type $action
     * @param type $result
     * @return type
     */
    public function check_referrer($action, $result) {
        if (!$this->isCategoryDeleteRequest()) {
            return;
        }

        $prefix = 'delete-tag_';
        if (strpos($action, $prefix) !== 0) {
            return;
        }
        $actionID = substr($action, strlen($prefix));
        $categoryID = max(0, (int) $actionID);
        if ($this->isUndeletable($categoryID)) {
            wp_die('This category is blocked for deletion.');
        }
    }

    public function check_AdminReferrer($action, $result) {
        if (!$this->isBulkDeleteRequest()) {
            return;
        }

        $categoryIDs = $_REQUEST['delete_tags'];

        if ($categoryIDs !== NULL) {
            $undeletedCategories = array();

            foreach ($categoryIDs as $categoryId) {
                if ($this->isUndeletable($categoryId)) {
                    array_push($undeletedCategories, $categoryId);
                }
            }

            if (count($undeletedCategories) > 0) {
                wp_die('This category is blocked for deletion.');
            }
        }
    }

}
