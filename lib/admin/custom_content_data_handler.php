<?php

/*
 *
 * Author  : PHP DEV
 * Purpose : Define library to save custom meta data for Custom Content Type/ Custom Taxonomy.
 *
 */

class Custom_Content_Data_Handler {

    private $custom_msg_id = '';

    /**
     * This function Will Display custom Messages on saving a post
     */
    function displayCustomErrorMessages($location, $post_id) {
        return $location = add_query_arg('message', $this->custom_msg_id, get_edit_post_link($post_id, 'url'));
    }

    /**
     * This function save all the Custom Meta Data Defined for Custom Post Types into the Database
     */
    function saveMetaData() {
        global $post;
        switch ($post->post_type) {
            case SPOTLIGHT_CUSTOM_POST:
            case CAROUSEL_SLIDE_CUSTOM_POST:
                $this->saveCallToAction();
                break;
            case TEASER_CUSTOM_POST:
                $this->saveCallToAction();
                break;
            case ARTICLE_POST_TYPE:
                $this->saveSEOMetaData();
                $this->saveArticleProductAssociation();
                //$this->savePrimaryCategory();
                break;
            case 'page':
                $this->saveSEOMetaData();
                $this->savePromoBoxAssociation(PROMO_TYPE_2);
                $this->savePromoBoxAssociation(PROMO_TYPE_3);
                $this->saveResponsiveIframeSettings();
                break;
        }
        if (isset($this->custom_msg_id) && $this->custom_msg_id != '') {
            add_filter('redirect_post_location', array($this, 'displayCustomErrorMessages'), 10, 2);
        }
    }

    /**
     * This Method Save SEO Meta data (Title, Description, Keyword, H1 Text) Associated with Different Custom Post Type/ Pages
     */
    function saveSEOMetaData() {
        global $post;

        // if this fails, check_admin_referer() will automatically print a "failed" page and die.
        if (!empty($_POST['mz_update_meta_box']) && check_admin_referer('createSEOMetaBox', 'mz_update_meta_box')) {
            update_post_meta($post->ID, SEO_TITLE_ATTRIBUTE, sanitize_text_field($_POST['seo_meta_title']));
            update_post_meta($post->ID, SEO_DESCRIPTION_ATTRIBUTE, sanitize_text_field($_POST['seo_meta_description']));
            update_post_meta($post->ID, SEO_KEYWORDS_ATTRIBUTE, sanitize_text_field($_POST['seo_meta_keywords']));
            update_post_meta($post->ID, SEO_H1_ATTRIBUTE, sanitize_text_field($_POST['seo_meta_h1text']));
        }
    }


    /**
     * This function save meta data For Responsive Iframe
     */
    function saveResponsiveIframeSettings() {
        global $post;
        if (!empty($_POST['mz_update_meta_box_responsive_iframe']) && check_admin_referer('createResponsiveIframeMetaBox', 'mz_update_meta_box_responsive_iframe')) {
            update_post_meta($post->ID, RESPONSIVE_IFRAME_ATTRIBUTE, $_POST[RESPONSIVE_IFRAME_ATTRIBUTE]);
        }
    }


    /**
     * This Method Save Call To Action Data (Label, URL, Open in New Window) Associated with Different Modules (Slide, Teaser, Slide, Spotlight, Promo Col)
     * All these Modules are Custom Post Types in Entity Relationship Arcitecture
     */
    function saveCallToAction() {
        global $post;
        if (!empty($_POST['mz_update_meta_box_action']) && check_admin_referer('createCallToActionBox', 'mz_update_meta_box_action')) {
            update_post_meta($post->ID, CTA_LABEL_ATTRIBUTE, sanitize_text_field($_POST['cta_label']));
            if ($_POST['cta_target_new']) {
                update_post_meta($post->ID, CTA_TARGET_ATTRIBUTE, $_POST['cta_target_new']);
            } else {
                update_post_meta($post->ID, CTA_TARGET_ATTRIBUTE, 0);
            }
            $cta_url = '';

            if ($_POST['cta_type'] == CTA_URL_TYPE_EXTERNAL) {
                $cta_url = sanitize_text_field($_POST['cta_url']);
            } else if ($_POST['cta_type'] == CTA_URL_TYPE_YOUTUBE) {
                $cta_url = sanitize_text_field($_POST['cta_youtube']);
            } else if (isset($_POST['found_post_id'])) {
                $cta_url = $_POST['found_post_id'];
            } else if ($_POST['cta_type'] == CTA_URL_TYPE_VIDEO) {
                $cta_url = sanitize_text_field($_POST['cta_video']);
            }
            if (isset($_POST['cta_type']) && ((isset($cta_url) && $cta_url != '') )) {
                update_post_meta($post->ID, CTA_URL_TYPE, $_POST['cta_type']);
                update_post_meta($post->ID, CTA_URL_ATTRIBUTE, $cta_url);
            } else if ($_POST['cta_type'] == 'none') {
                delete_post_meta($post->ID, CTA_URL_TYPE);
                delete_post_meta($post->ID, CTA_URL_ATTRIBUTE);
            } else if ($_POST['cta_type'] != $_POST['current_cta_type'] && $cta_url == '') {
                $this->custom_msg_id = 102;
            }
        }
    }

    /**
     * This Method Save Custom Data assigned to Custom Taxonomy (Product Category, Article Category Page)
     */
    function saveTaxonomyData($term_id) {
        switch ($_POST['taxonomy']) {

            case ARTICLE_TAXONOMY:
                $obTerm = get_term_by('id', $term_id, ARTICLE_TAXONOMY);
                $this->saveTaxonomyMetaData($obTerm->term_taxonomy_id);
                break;
            case TEASER_TAXONOMY:
                $obTerm = get_term_by('id', $term_id, TEASER_TAXONOMY);
                $this->saveSlideSortPriority($obTerm->term_taxonomy_id, TEASER_CUSTOM_POST);
                wp_update_term_count_now(array($obTerm->term_taxonomy_id), TEASER_TAXONOMY);

                break;
            case CAROUSEL_TAXONOMY:
                $obTerm = get_term_by('id', $term_id, CAROUSEL_TAXONOMY);
                $this->saveSlideSortPriority($obTerm->term_taxonomy_id, CAROUSEL_SLIDE_CUSTOM_POST);
                $this->saveCarouselSettings($obTerm->term_taxonomy_id, CAROUSEL_SLIDE_CUSTOM_POST);
                wp_update_term_count_now(array($obTerm->term_taxonomy_id), CAROUSEL_TAXONOMY);
                break;
        }
    }

    /**
     * This Method Save Sort Priorty of Custom Post (Teaser, Slides)/Custom Taxonomy Relationship (Teaser Carousel/ Carousel)
     */
    function saveSlideSortPriority($term_taxonomy_id, $item_type) {
        global $wpdb;

        $queryDelete = 'DELETE FROM ' . $wpdb->term_relationships . ' where term_taxonomy_id =' . $term_taxonomy_id;
        $wpdb->query($wpdb->prepare($queryDelete, array()));
        $queryInsert = 'INSERT INTO ' . $wpdb->term_relationships . ' VALUES ';
        $i = 1;
        foreach ($_POST[$item_type . '_items'] as $slide_id) {
            $arQuery[] .= '(' . $slide_id . ',' . $term_taxonomy_id . ',' . $i . ')';
            $i++;
        }
        $queryInsert = $queryInsert . implode(',', $arQuery);
        $wpdb->query($wpdb->prepare($queryInsert, array()));
    }

    /**
     * This Method Save SEO Meta data (Title, Description, Keyword, H1 Text) Associated with Different Custom Taxonomy 
     */
    function saveTaxonomyMetaData($tt_id) {
        if ($_POST['taxonomy'] == 'category') {
            $tag_extra_fields = get_option(CUSTOM_CATEGORY_FIELDS);
            $tag_extra_fields[$tt_id][SEO_TITLE_ATTRIBUTE] = sanitize_text_field($_POST['meta_title']);
            $tag_extra_fields[$tt_id][SEO_DESCRIPTION_ATTRIBUTE] = sanitize_text_field($_POST['meta_description']);
            $tag_extra_fields[$tt_id][SEO_KEYWORDS_ATTRIBUTE] = sanitize_text_field($_POST['meta_keywords']);
            $tag_extra_fields[$tt_id][SEO_H1_ATTRIBUTE] = sanitize_text_field($_POST['meta_h1text']);
            $tag_extra_fields[$tt_id][LEFT_NAV_OPTION] = sanitize_text_field($_POST['meta_left_nav']);
            update_option(CUSTOM_CATEGORY_FIELDS, $tag_extra_fields);
        }
    }

    /**
     * This Method Save Carousel Transition settings (Navigation Type, Auto Transition, Pause Play, Time Interval) Associated with Taxonomy 'Carousel'
     */
    function saveCarouselSettings($tt_id) {
        if ($_POST['taxonomy'] == CAROUSEL_TAXONOMY) {
            $carousel_extra_fields = get_option(CAROUSEL_CUSTOM_FIELD_PREFIX . $tt_id);
            $carousel_extra_fields[$tt_id][CAROUSEL_NAVIGATION_SETTING] = sanitize_text_field($_POST['carousel_nav_options']);
            $carousel_extra_fields[$tt_id][CAROUSEL_AUTO_TRANSITION] = sanitize_text_field($_POST['carousel_auto_transition']);
            $carousel_extra_fields[$tt_id][CAROUSEL_PAUSE_PLAY_FEATURE] = sanitize_text_field($_POST['carousel_pause_play_feature']);
            $carousel_extra_fields[$tt_id][CAROUSEL_TRANSITION_TIME_INTERVAL] = sanitize_text_field($_POST['carousel-transition-time-interval']);
            update_option(CAROUSEL_CUSTOM_FIELD_PREFIX . $tt_id, $carousel_extra_fields);
        }
    }

    /**
     * This function Automatically Create Custom Taxonomy on creation of a Product Category/ Article Category in the system
     * Promo Col Placeholder are Custom Taxonomy in Entity Relationship Arcitecture
     */
    function createTermAssociatedBuildingBlocks($term_id, $tt_id, $taxonomy) {
        switch ($taxonomy) {
            case ARTICLE_TAXONOMY:
                $obArticleCat = get_term_by('id', $term_id, ARTICLE_TAXONOMY);
                // Create Carousel Taxonomy on Creation of Article Category
                $arParameter['cat_name'] = 'Article Category ' . $obArticleCat->name;
                $arParameter['category_nicename'] = CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX . $tt_id;
                $arParameter['taxonomy'] = CAROUSEL_TAXONOMY;
                wp_insert_category($arParameter);
                break;
            case RECIPES_TAXONOMY:
                $obArticleCat = get_term_by('id', $term_id, RECIPES_TAXONOMY);
                // Create Carousel Taxonomy on Creation of Article Category
                $arParameter['cat_name'] = 'Recipes Category ' . $obArticleCat->name;
                $arParameter['category_nicename'] = CAROUSEL_RECIPES_CATEGORY_SLUG_PREFIX . $tt_id;
                $arParameter['taxonomy'] = CAROUSEL_TAXONOMY;
                wp_insert_category($arParameter);
                break;  
            case PRODUCT_TAXONOMY:
                $obProductCat = get_term_by('id', $term_id, PRODUCT_TAXONOMY);
                // Create Carousel Taxonomy on Creation of Product Category

                $arParameter['cat_name'] = 'Product Category ' . $obProductCat->name;
                $arParameter['category_nicename'] = CAROUSEL_PRODUCT_CATEGORY_SLUG_PREFIX . $tt_id;
                $arParameter['taxonomy'] = CAROUSEL_TAXONOMY;
                wp_insert_category($arParameter);
                // Create Promo2 Col Taxonomy on Creation of Product Category

                $arParameter['cat_name'] = 'Product Category ' . $obProductCat->name . ' Promo Small';
                $arParameter['category_nicename'] = PROMO2_COL_PRODUCT_CATEGORY_SLUG_PREFIX . $tt_id;
                $arParameter['taxonomy'] = PROMO_COL_TAXONOMY;
                wp_insert_category($arParameter);
                // Create Promo3 Col Taxonomy on Creation of Product Category

                $arParameter['cat_name'] = 'Product Category ' . $obProductCat->name . ' Promo Large';
                $arParameter['category_nicename'] = PROMO3_COL_PRODUCT_CATEGORY_SLUG_PREFIX . $tt_id;
                $arParameter['taxonomy'] = PROMO_COL_TAXONOMY;
                wp_insert_category($arParameter);
                break;
        }
    }
    /**
     * This function Automatically Create Promo2 Col Placeholder/ Promo3 Col Placeholder on creation of a Product in the system
     * Promo Col Placeholder are Custom Taxonomy in Entity Relationship Arcitecture
     */
    function createBuildingBlockForPostType($data, $postarr) {
        global $post;
        switch ($data['post_type']) {
            case PRODUCT_CUSTOM_POST:
                if ($data['post_status'] != 'auto-draft' && $post) {
                    $arParameter['cat_name'] = 'Product ' . $data['post_title'] . ' Promo Small';
                    $arParameter['category_nicename'] = PROMO2_COL_TAXONOMY_SLUG_PREFIX . $post->ID;
                    $arParameter['taxonomy'] = PROMO_COL_TAXONOMY;
                    wp_insert_category($arParameter);
                    $arParameter['cat_name'] = 'Product ' . $data['post_title'] . ' Promo Large';
                    $arParameter['category_nicename'] = PROMO3_COL_TAXONOMY_SLUG_PREFIX . $post->ID;
                    $arParameter['taxonomy'] = PROMO_COL_TAXONOMY;
                    wp_insert_category($arParameter);
                }
                break;
        }
        return $data;
    }
    /**
     * This function Delete Promo2 Col Placeholder/ Promo3 Col Placeholder/ Carousel on deletion of a Product Category/Article Category in the system
     * Promo Col Placeholder are Custom Taxonomy in Entity Relationship Arcitecture
     */
    function removeTermAssociatedBuildingBlocks($term, $tt_id, $taxonomy, $deleted_term) {

        switch ($taxonomy) {
            case ARTICLE_TAXONOMY:
                $obCarousel = get_term_by('slug', CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX . $tt_id, CAROUSEL_TAXONOMY);
                wp_delete_term($obCarousel->term_id, CAROUSEL_TAXONOMY); // Delete Carousel Taxonomy for Article Category Page
                break;
        }
    }

    /**
     * This function updates post meta table to add associated products with article
     * @global type $post
     * @return null
     */
    function saveArticleProductAssociation() {
        global $post;
        if ($post->post_type != ARTICLE_POST_TYPE) {
            return false;
        }
    }
}

?>
