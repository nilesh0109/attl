<?php
/*
 *
 * Author  : PHP DEV
 * Purpose : Define UI Interface for adding Custom Meta Data in Admin Section for Custom Content Type/ Custom Taxonomy.
 *
 */

class Custom_Content_UI_Handler {

    function __construct() {

    }

    public function mzOptionsScripts($args) {

        $output = '<script>';
        $output.= 'jQuery( "#helpvideo" ).click(function(){
                     jQuery("#helpvideotext").toggle();
                     })';
        $output.= '</script>';

        echo $output;
    }

    /**
     * This function Register all the required Custom Meta boxes Required for Custom Post Types designed in the systems
     */
    function manageMetaBoxes() {
        global $current_user;
        
        
        // added by Paritosh Gautam
        // Modifed By Madhu Sudan
        $postVariationMetabox = array(
                array( // Repeatable & Sortable Text inputs
                       'label'    => 'Repeatable', // <label>
                       'desc'    => 'Products variants required for Store Locator', // description
                       'id'        => $prefix.'repeatable', // field id and name
                       'type'    => 'repeatable', // type of field
                       'sanitizer' => array( // array of sanitizers with matching kets to next array
                                   'featured' => 'meta_box_santitize_boolean',
                                   'title' => 'sanitize_text_field',
                                   'desc' => 'wp_kses_data'
                       ),
                   ),
               );
        if(file_exists(get_theme_root() . '/library/metaboxes_framework/loader.php')) {
            include(get_theme_root() . '/library/metaboxes_framework/loader.php');
        }
        // END by Paritosh Gautam
        
        
        
		// Enqueue Custom JS Script which will handle thickbox used to assign Promo2 Col/ Promo3 Col to the Product Pages
        wp_enqueue_script("associate_items", content_url() . "/" . LIB_NAME . "/js/find_post.js");

        // add some custom js to the head of the page
        add_action('admin_footer', array($this, 'mzOptionsScripts'));

        
        add_meta_box('post_category', __('Article Category'), array(&$this, 'postCategoriesMetaBox'), ARTICLE_POST_TYPE, 'side', 'low');

        if ($_GET['action'] == 'edit') { // Add this meta box only on edit page
            //add_meta_box('primary_category', __('Choose Primary Category'), array( &$this, 'createPrimaryCategoryBox' ), PRODUCT_CUSTOM_POST, 'side', 'low');
            //add_meta_box('primary_category', __('Choose Primary Category'), array( &$this, 'createPrimaryCategoryBox' ), ARTICLE_POST_TYPE, 'side', 'low');
        }
        add_meta_box('seo_meta_id', __('SEO Meta'), array(&$this, 'createSEOMetaBox'), ARTICLE_POST_TYPE, 'normal', 'low');
        add_meta_box('seo_meta_id', __('SEO Meta'), array(&$this, 'createSEOMetaBox'), 'page', 'normal', 'low');

        add_meta_box('call_to_action', __('Call To Action'), array(&$this, 'createCallToActionBox'), SPOTLIGHT_CUSTOM_POST, 'normal', 'low');
        add_meta_box('call_to_action', __('Call To Action'), array(&$this, 'createCallToActionBox'), TEASER_CUSTOM_POST, 'normal', 'low');
        add_meta_box('call_to_action', __('Call To Action'), array(&$this, 'createCallToActionBox'), CAROUSEL_SLIDE_CUSTOM_POST, 'normal', 'low');

        if (array_key_exists("administrator", $current_user->caps)) {
            add_meta_box('responsive_iframe', __('Responsive Iframe'), array(&$this, 'createResponsiveIframeMetaBox'), 'page', 'side', 'low');
        }
        global $arBrandSiteSettings; // Defined in lib/loader.php
        if ($arBrandSiteSettings[BRAND_SITE_POST_ADDITIONAL_DETAIL] == 1) {
            add_meta_box('product_additional_details', __('Product Additional Details'), array(&$this, 'createProductAdditionalDetailMetaBox'), PRODUCT_CUSTOM_POST, 'normal', 'low');
        }
        // Remove Non Required Default Meta Boxes from the add/Edit default Post (Articles) Screen
        remove_meta_box('commentstatusdiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('commentsdiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('trackbacksdiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('postcustom', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('formatdiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('revisionsdiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('authordiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box('categorydiv', ARTICLE_POST_TYPE, 'normal');
        remove_meta_box(CAROUSEL_TAXONOMY . 'div', CAROUSEL_SLIDE_CUSTOM_POST, 'normal');
        remove_meta_box(TEASER_TAXONOMY . 'div', TEASER_CUSTOM_POST, 'normal');



        // Remove Non Required Default Meta Boxes from the add/Edit Page Screen
        remove_meta_box('commentstatusdiv', 'page', 'normal');
        remove_meta_box('commentsdiv', 'page', 'normal');
        remove_meta_box('postcustom', 'page', 'normal');
        remove_meta_box('revisionsdiv', 'page', 'normal');
        remove_meta_box('authordiv', 'page', 'normal');
    }

    /**
     * This function load CSS used to invoke Thick Box
     */
    function addFindPostStyle() {
        global $post;
        if ((isset($post)) || get_current_screen()->id == "edit-" . TEASER_TAXONOMY || get_current_screen()->id == "edit-" . CAROUSEL_TAXONOMY) {
            wp_enqueue_style('thickbox'); // needed for find posts div
        }
    }

    /**
     * This function load JS Files used to invoke Thick Box
     */
    function addFindPostScripts() {
        global $post;
        if ((isset($post) && ($post->post_type == 'page' || $post->post_type == SPOTLIGHT_CUSTOM_POST || $post->post_type == TEASER_CUSTOM_POST || $post->post_type == CAROUSEL_SLIDE_CUSTOM_POST)) || get_current_screen()->id == "edit-" . TEASER_TAXONOMY || get_current_screen()->id == "edit-" . CAROUSEL_TAXONOMY) {
            wp_enqueue_script('thickbox'); // needed for find posts div
            wp_enqueue_script('associate_items');
            wp_enqueue_script('wp-ajax-response');
            wp_enqueue_script('jquery-ui-draggable');
            wp_localize_script('associate_items', 'data_elements', $arData);
        }
        // Enque Sortable Library for Teaser CarouselTaxonomy/ Carousel Taxonomy Edit Page
        if (get_current_screen()->id == "edit-" . TEASER_TAXONOMY || get_current_screen()->id == "edit-" . CAROUSEL_TAXONOMY) { // Add JS Files in order to implement Sortable Order List
            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-sortable');
        }
    }

    /**
     * This function define UI part of Call To Action Data (Label, URL, Open in New Window) Associated with Different Modules (Slide, Teaser, Slide, Spotlight, Promo Col)
     * All these Modules are Custom Post Types in Entity Relationship Arcitecture
     */
    function createCallToActionBox() {
        global $post, $wp_taxonomies, $wp_post_types;
        $cta_label = get_post_meta($post->ID, CTA_LABEL_ATTRIBUTE, true);
        $cta_url_type = get_post_meta($post->ID, CTA_URL_TYPE, true);
        $cta_url_id = get_post_meta($post->ID, CTA_URL_ATTRIBUTE, true);
        if ($cta_url_type == CTA_URL_TYPE_EXTERNAL) {
            $cta_url = $cta_url_id;
        } else if ($cta_url_type == CTA_URL_TYPE_YOUTUBE) {
            $cta_youtube = $cta_url_id;
        } else if ($cta_url_type == CTA_URL_TYPE_VIDEO) {
            $cta_video = $cta_url_id;
        }
        $cta_target_new = get_post_meta($post->ID, CTA_TARGET_ATTRIBUTE, true);
        $checked_yes = "";
        $checked_no = "";
        if ($cta_target_new) {
            $checked_yes = "checked";
        } else {
            $checked_no = "checked";
        }
        if ($cta_url_type == "") {
            $cta_url_type = 'none';
        }
        ?>
        <div id="titlediv">
            <label><b><?php _e("Call To Action Label") ?></b></label>
            <input id="cta_label" class="widefat" name="cta_label" type="text" value="<?php echo $cta_label; ?>" /><br><br>
            <label><b><?php _e("Call To Action Association") ?></b></label><br>
            <input type="hidden" name="current_cta_type" value="<?php echo $cta_url_type; ?>" />
            <?php
            // Add an nonce field so we can check for it later.
            wp_nonce_field('createCallToActionBox', 'mz_update_meta_box_action');
            ?>
            <?php
            $arCTAType = array('none', ARTICLE_TAXONOMY, ARTICLE_POST_TYPE, 'page', CTA_URL_TYPE_EXTERNAL);
            //if ($post->post_type == SPOTLIGHT_CUSTOM_POST) {
            //$arCTAType[] = CTA_URL_TYPE_YOUTUBE;
            if ($post->post_type != TEASER_CUSTOM_POST) {
                $arCTAType[] = CTA_URL_TYPE_YOUTUBE;
                $arCTAType[] = CTA_URL_TYPE_VIDEO;
            }
            foreach ($arCTAType as $cta_type) {

                $checked_type = "";
                $selected_title = "";
                switch ($cta_type) {
                    case ARTICLE_TAXONOMY:
                    case ARTICLE_POST_TYPE:
                    case CTA_URL_TYPE_EXTERNAL:
                        $singular_name = __('External');
                        $cta_association_type = "external";
                        break;
                    case CTA_URL_TYPE_YOUTUBE:
                        $singular_name = __('YouTube');
                        $cta_association_type = "youtube";
                        break;
                    case CTA_URL_TYPE_VIDEO:
                        $singular_name = __('Video');
                        $cta_association_type = "video";
                        break;
                    default:
                        $singular_name = __('None');
                        $cta_association_type = "none";
                        break;
                }
                if ($cta_type == $cta_url_type) {
                    $checked_type = "checked";
                }
                /* 	else if($cta_type == "none" && $cta_url_id == "")
                  {
                  $checked_type = "checked";
                  } */
                ?>
                <br/><input type="radio" name="cta_type" <?php echo $checked_type; ?> value="<?php echo $cta_type; ?>"> <?php echo $singular_name; ?>
                <?php if ($cta_association_type == "taxonomy_type" || $cta_association_type == "post_type") { ?>
                    <span class="cta_association">&nbsp;&nbsp;<a id="lms_findposts" onclick="findItems.open('<?php echo $cta_association_type; ?>', '<?php echo $cta_type; ?>');
                                            return false;" href="#"><?php
                                                                     _e('Associate');
                                                                     echo " " . $singular_name;
                                                                     ?></a>
                        <b> <?php echo $selected_title; ?></b>
                    </span>
                <?php } else if ($cta_association_type == "external") { ?>
                    <span class="cta_association"><input id="cta_url" class="widefat" name="cta_url" rows ="10" type="text" value="<?php echo $cta_url; ?>" /></span>
                    <?php
                } else if ($cta_association_type == "youtube") {
                    ?>
                    <span class="cta_association"><br>http://youtube.com/watch?v=<input id="cta_url" class="widefat" name="cta_youtube" rows ="10" type="text" value="<?php echo $cta_youtube; ?>" /></span>
                    <?php
                } else if ($cta_association_type == "video") {
                    ?>
                    <span class="cta_association"><br><input id="cta_url" class="widefat" name="cta_video" rows ="10" type="text" value="<?php echo $cta_video; ?>" />
                        <br /><br />

                        <label><b>Video providers supported URLs format:</b></label>
                        <ul>
                            <li>http://www.tudou.com/programs/view/ExBQjh5gB-k/ </li>
                            <li>http://v.youku.com/v_show/id_XNjE1OTU2MjM2.html </li>
                            <li>http://www.iqiyi.com/v_19rrhand40.html</li>
                        </ul>
                    </span>
                    <?php
                }
            }
            ?>
            <br><label><b><?php _e("Open in New Window") ?></b></label>&nbsp;
            <input id="cta_target_new" name="cta_target_new" type="radio" <?php echo $checked_no; ?> value="0" />&nbsp; No
            <input id="cta_target_new" name="cta_target_new" type="radio" <?php echo $checked_yes; ?> value="1" />&nbsp; Yes
        </div>
        <style>
            .cta_association{ display:none;}
        </style>
        <script>
                        jQuery('input[name="cta_type"]').each(function()
                        {
                            if (jQuery(this).is(":checked"))
                            {
                                jQuery(this).next().show();
                            }
                        });
                        jQuery('input[name="cta_type"]').click(function()
                        {
                            jQuery(".cta_association").hide();
                            jQuery(this).next().show();
                        });
        </script>

        <?php
    }

    /**
     * This function define UI part SEO Meta data (Title, Description, Keyword) Associated with Different Custom Post Type/ Pages
     */
    function createSEOMetaBox() {
        global $post;
        $seo_meta_title = get_post_meta($post->ID, SEO_TITLE_ATTRIBUTE, true);
        $seo_meta_description = get_post_meta($post->ID, SEO_DESCRIPTION_ATTRIBUTE, true);
        $seo_meta_keywords = get_post_meta($post->ID, SEO_KEYWORDS_ATTRIBUTE, true);
        $seo_meta_h1text = get_post_meta($post->ID, SEO_H1_ATTRIBUTE, true); // H1 Field is added for Unique H1 of each product
        ?>
        <div id="titlediv">
            <label><?php _e("Meta Title") ?></label>
            <input id="seo_meta_title" class="widefat" name="seo_meta_title" type="text" value="<?php echo esc_html($seo_meta_title); ?>" /><br>
            <label><?php _e("Meta Description") ?></label>
            <textarea id="seo_meta_description" class="widefat" name="seo_meta_description" rows ="10" type="text" /><?php echo esc_textarea($seo_meta_description); ?></textarea><br>
            <label><?php _e("Meta Keywords") ?></label>
            <input id="seo_meta_keywords" class="widefat" name="seo_meta_keywords" type="text" value="<?php echo esc_html($seo_meta_keywords); ?>" />
            <label><?php _e("H1 Text") ?></label>
            <input id="seo_meta_h1text" class="widefat" name="seo_meta_h1text" type="text" value="<?php echo esc_html($seo_meta_h1text); ?>" />
            <?php
            // Add an nonce field so we can check for it later.
            wp_nonce_field('createSEOMetaBox', 'mz_update_meta_box');
            ?>
        </div>
        <?php
    }
    /**
     * Add Custom Field For Responsive Iframe
     */
    function createResponsiveIframeMetaBox() {
        global $post;
        $is_responsive_iframe = get_post_meta($post->ID, RESPONSIVE_IFRAME_ATTRIBUTE, true);
        $checked = "";
        if ($is_responsive_iframe) {
            $checked = "checked";
        }
        ?>
        <div id="titlediv">
            <label><?php _e("Responsive Iframe") ?></label>
            <input id="<?php echo RESPONSIVE_IFRAME_ATTRIBUTE; ?>" class="widefat" name="<?php echo RESPONSIVE_IFRAME_ATTRIBUTE; ?>" <?php echo $checked; ?> type="checkbox" value="1" />
            <?php
            // Add an nonce field so we can check for it later.
            wp_nonce_field('createResponsiveIframeMetaBox', 'mz_update_meta_box_responsive_iframe');
            ?>
        </div>

        <?php
    }

    /**
     * This function provide find post box
     */
    function addFindPostBox() {
        global $post;
        if ($post->post_type == PRODUCT_CUSTOM_POST || $post->post_type == "page" || $post->post_type == ARTICLE_POST_TYPE || $post->post_type == RECIPES_CUSTOM_POST) {
            $this->find_custom_posts_div('checkbox'); // Used to Create Thickbox Popup having list of Promo2 Col or Promo3 Col with Search and select Functionality
        } else {
            $this->find_custom_posts_div('radio');
        }

        if ($this->isError()) {
            ?>
            <script type="text/javascript">
                // <![CDATA[
                jQuery('#message').attr('class', 'error');
                // ]]>
            </script>
            <?php
        }
    }

    private function isError() {
        return $_REQUEST['message'] > 100;
    }


    /**
     * This function is used to add up Custom Fields on edit Taxonomy Screen Page (e.g. edit Category/ Edit Product Category)
     * This function add form Elements for entering SEO Meta data (Title, Description, Keyword and H1 Text)
     */
    function editTaxonomyForm($tag) {

        $arCustomField = get_option(CUSTOM_CATEGORY_FIELDS);
        ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top"><label for="meta_title"><?php _e('Page Meta Title'); ?></label></th>
                <td><input name="meta_title" id="meta_title" type="text" size="40" value="<?php echo $arCustomField[$tag->term_taxonomy_id][SEO_TITLE_ATTRIBUTE]; ?>" />
                    <p class="description"><?php _e('The meta title will be used for Meta Title of the Category Page.'); ?></p></td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="meta_description"><?php _e('Page Meta Description'); ?></label></th>
                <td><input name="meta_description" id="meta_description" type="text" size="40" value="<?php echo $arCustomField[$tag->term_taxonomy_id][SEO_DESCRIPTION_ATTRIBUTE]; ?>" />
                    <p class="description"><?php _e('The meta description will be used for Meta Description of the Category Page.'); ?></p></td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="meta_keywords"><?php _e('Page Meta Keywords'); ?></label></th>
                <td><input name="meta_keywords" id="meta_keywords" type="text" size="40" value="<?php echo $arCustomField[$tag->term_taxonomy_id][SEO_KEYWORDS_ATTRIBUTE]; ?>" />
                    <p class="description"><?php _e('The meta keywords will be used for Meta Keywords of the Category Page.'); ?></p></td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="meta_h1text"><?php _e('Page H1 Text'); ?></label></th>
                <td><input name="meta_h1text" id="meta_h1text" type="text" size="40" value="<?php echo $arCustomField[$tag->term_taxonomy_id][SEO_H1_ATTRIBUTE]; ?>" />
                    <p class="description"><?php _e('The meta H1 Text will be used for H1 Text of the Category Page.'); ?></p></td>
            </tr>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="meta_left_nav"><?php _e('Left Navigation Option'); ?></label></th>
                <td align='left'>
                    <label for='meta_left_nav_enable'>Enable</label><input class="carousel-radio" name="meta_left_nav" id="meta_left_nav_enable" type="radio"   value="enable" <?php echo ($arCustomField[$tag->term_taxonomy_id][LEFT_NAV_OPTION] == 'enable') ? 'checked' : ''; ?> />
                    <label for='meta_left_nav_disable'>Disable</label><input class="carousel-radio" name="meta_left_nav" id="meta_left_nav_disable" type="radio"   value="disable" <?php echo ($arCustomField[$tag->term_taxonomy_id][LEFT_NAV_OPTION] == 'disable') ? 'checked' : ''; ?> />
                    <p class="description"><?php _e('Left navigation on/off option for this category'); ?></p>
                </td>
            </tr>
            <?php if ($tag->taxonomy == PRODUCT_TAXONOMY) { ?>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="meta_related_product_title"><?php _e('Related Product Title Option'); ?></label></th>
                    <td align='left'>
                        <label for='meta_related_product_title_enable'>Enable</label><input class="carousel-radio" name="meta_related_product_title" id="meta_related_product_title_enable" type="radio"   value="enable" <?php echo ($arCustomField[$tag->term_taxonomy_id][RELATED_PRODUCT_TITLE_OPTION] == 'enable') ? 'checked' : ''; ?> />
                        <label for='meta_related_product_title_disable'>Disable</label><input class="carousel-radio" name="meta_related_product_title" id="meta_related_product_title_disable" type="radio"   value="disable" <?php echo ($arCustomField[$tag->term_taxonomy_id][RELATED_PRODUCT_TITLE_OPTION] == 'disable') ? 'checked' : ''; ?> />
                        <p class="description"><?php _e('Related Product Title on/off option for this category'); ?></p>
                    </td>
                </tr>  <?php } ?>
        </table>
        <?php if ($tag->taxonomy == PRODUCT_TAXONOMY) { ?>
            <table>
                <tr>
                    <th scope="row" valign="top" align="left" width="220"><label><?php _e('Associate Small Promo'); ?></label></th>
                    <td>
                        <?php
                        $promo2_col_slug = PROMO2_COL_PRODUCT_CATEGORY_SLUG_PREFIX . $tag->term_taxonomy_id;
                        $promo3_col_slug = PROMO3_COL_PRODUCT_CATEGORY_SLUG_PREFIX . $tag->term_taxonomy_id;


                        $this->find_custom_posts_div('checkbox'); // Used to Create Thickbox Popup having list of Promo2 Col or Promo3 Col with Search and select Functionality
                        ?>
                        <a id="lms_findposts" onclick="findItems.open('post_type', '<?php echo PROMO_COL_CUSTOM_POST; ?>', '<?php echo PROMO_TYPE; ?>', '<?php echo PROMO_TYPE_2; ?>');
                                return false;" href="#"><?php _e('Associate Small Promo'); ?></a>
                           <?php
                           $promo2_term = get_term_by('slug', $promo2_col_slug, PROMO_COL_TAXONOMY);
                           ?>
                    </td>
                </tr>
                <?php
                $this->addSortableItems($promo2_term, PROMO_TYPE_2);
                ?>
                <tr class="form-field">
                    <th scope="row" valign="top" align="left" width="220"><label><?php _e('Associate Big Promo'); ?></label></th>
                    <td>
                        <a id="lms_findposts" onclick="findItems.open('post_type', '<?php echo PROMO_COL_CUSTOM_POST; ?>', '<?php echo PROMO_TYPE; ?>', '<?php echo PROMO_TYPE_3; ?>');
                                return false;" href="#"><?php _e('Associate Big Promo'); ?></a>
                           <?php
                           $promo3_term = get_term_by('slug', $promo3_col_slug, PROMO_COL_TAXONOMY);
                           ?>
                    </td>
                </tr>
                <?php
                $this->addSortableItems($promo3_term, PROMO_TYPE_3);
                $this->addSortableJS();
            }
            ?>
        </table>
        <?php
    }

    /**
     * This function provide an interface to associated slides to Carousel
     *
     */
    function addAssociateSlideBox($tag) {
        switch ($tag->taxonomy) {
            case TEASER_TAXONOMY:
                $custom_post_type = TEASER_CUSTOM_POST;
                break;
            case CAROUSEL_TAXONOMY:
                $custom_post_type = CAROUSEL_SLIDE_CUSTOM_POST;
                break;
        }
        ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top"><label><?php _e("Associate Slides") ?></label></th>
                <td><a id="" onclick="findItems.open('post_type', '<?php echo esc_js($custom_post_type); ?>');
                            return false;" href="#"><?php _e('Associate Slides'); ?></a>
                </td>
            </tr>
            <?php
            $this->addSortableItems($tag);
            ?>
        </table>
        <?php
        $this->find_custom_posts_div('checkbox'); // Used to Create Thickbox Popup
        $this->addSortableJS();
    }

    /**
     * This function is for carousel transition settings
     * 	First parameter is array with Tag Id
     */
    function addCarouselSettings($tag) {
        $carouselCustomField = get_option(CAROUSEL_CUSTOM_FIELD_PREFIX . $tag->term_taxonomy_id);
        if (!isset($carouselCustomField[$tag->term_taxonomy_id])) {
            $carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_NAVIGATION_SETTING] = CAROUSEL_NAVIGATION_DEFAULT_VALUE;
            $carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_AUTO_TRANSITION] = CAROUSEL_AUTO_TRANSITION_DEFAULT_VALUE;
            $carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_PAUSE_PLAY_FEATURE] = CAROUSEL_PAUSE_PLAY_FEATURE_DEFAULT_VALUE;
            $carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_TRANSITION_TIME_INTERVAL] = CAROUSEL_TRANSITION_TIME_INTERVAL_DEFAULT_VALUE;
        }
        ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top"><label><?php _e('Carousel Settings', LANGUAGE_DOMAIN_NAME); ?></label></th>
            </tr>
            <tr class="form-field">
                <td><?php _e('Navigation Options', LANGUAGE_DOMAIN_NAME); ?></td>
                <td align='left'>
                    <input class="carousel-radio" name="carousel_nav_options" id="carousel_nav_options" type="radio" value="<?php echo CAROUSEL_NAVIGATION_ARROWS; ?>" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_NAVIGATION_SETTING] == CAROUSEL_NAVIGATION_ARROWS) ? 'checked' : ''; ?> /><label for='carousel_nav_options'><?php _e('Arrows', LANGUAGE_DOMAIN_NAME); ?></label>
                    <input class="carousel-radio" name="carousel_nav_options" id="carousel_nav_options" type="radio" value="<?php echo CAROUSEL_NAVIGATION_BULLETS; ?>" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_NAVIGATION_SETTING] == CAROUSEL_NAVIGATION_BULLETS) ? 'checked' : ''; ?> /><label for='carousel_nav_options'><?php _e('Bullets', LANGUAGE_DOMAIN_NAME); ?></label>
                    <input class="carousel-radio" name="carousel_nav_options" id="carousel_nav_options" type="radio" value="<?php echo CAROUSEL_NAVIGATION_BOTH; ?>" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_NAVIGATION_SETTING] == CAROUSEL_NAVIGATION_BOTH) ? 'checked' : ''; ?> /><label for='carousel_nav_options'><?php _e('Both', LANGUAGE_DOMAIN_NAME); ?></label>
                </td>
            </tr>
            <tr class="form-field">
                <td><?php _e('Auto Transition', LANGUAGE_DOMAIN_NAME); ?></td>
                <td align='left'>
                    <input class="carousel-radio" name="carousel_auto_transition" id="carousel_auto_transition" type="radio" value="1" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_AUTO_TRANSITION] == '1') ? 'checked' : ''; ?> /><label for='carousel_auto_transition'><?php _e('Enable', LANGUAGE_DOMAIN_NAME); ?></label>
                    <input class="carousel-radio" name="carousel_auto_transition" id="carousel_auto_transition" type="radio" value="0" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_AUTO_TRANSITION] == '0') ? 'checked' : ''; ?> /><label for='carousel_auto_transition'><?php _e('Disable', LANGUAGE_DOMAIN_NAME); ?></label>

                </td>
            </tr>

            <tr class="form-field">
                <td><?php _e('Pause-Play Feature', LANGUAGE_DOMAIN_NAME); ?></td>
                <td align='left'>
                    <input class="carousel-radio" name="carousel_pause_play_feature" id="carousel_pause_play_feature" type="radio" value="1" <?php echo ($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_PAUSE_PLAY_FEATURE] == '1') ? 'checked' : ''; ?> /><label for='carousel_pause_play_feature'><?php _e('Enable', LANGUAGE_DOMAIN_NAME); ?></label>
                    <input class="carousel-radio" name="carousel_pause_play_feature" id="carousel_pause_play_feature" type="radio" value="0" <?php echo (empty($carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_PAUSE_PLAY_FEATURE])) ? 'checked' : ''; ?> /><label for='carousel_pause_play_feature'><?php _e('Disable', LANGUAGE_DOMAIN_NAME); ?></label>

                </td>
            </tr>
            <tr class="form-field">
                <td><?php _e('Transition Time Interval', LANGUAGE_DOMAIN_NAME); ?></td>
                <td align = 'left'>
                    <?php
                    echo ("<select class='postform' name='carousel-transition-time-interval'>");
                    $i = 3;
                    for ($i = 3; $i <= 10; $i++) {
                        $selected = '';
                        echo $carouselCustomField[$tag->term_taxonomy_id][CAROUSEL_TRANSITION_TIME_INTERVAL] == $i ? $selected = " selected='selected'" : "";
                        echo"<option value ='{$i}'" . $selected . ">" . $i . "</option>";
                    }
                    echo("</select>");
                    ?> <?php _e('Seconds', LANGUAGE_DOMAIN_NAME); ?></td>
            </tr>
        </table>
        <?php
    }

    /**
     * This function Sort Array of Post Object with respect to another Post Id array
     * 	First parameter is array with Post Ids
     * Second parameter is array of post object that need to sort
     */
    function sortPostObjectArray($arSorted, $arToSort) {
        $maxOrder = 1000;
        foreach ($arToSort as $key => $obTeaser) {
            $arData[$key] = (array) $obTeaser;
            $arData[$key]['sort'] = array_search($obTeaser->ID, $arSorted);

            $arSortPriority[] = $arData[$key]['sort'];
        }
        array_multisort($arSortPriority, SORT_ASC, $arData);
        return $arData;
    }

    /**
     * This function provide an interface to provide sort order by drag drop Slides associated to Carousel/ Teaser Carousel
     *
     */
    function addSortableItems($tag, $item_type = '') {
        global $wpdb;
        $queryTeaser = "SELECT object_id,term_order FROM " . $wpdb->term_relationships . " where term_taxonomy_id =" . $tag->term_taxonomy_id . " order by term_order ASC";
        $arTeaserID = $wpdb->get_results($wpdb->prepare($queryTeaser, array()));

        $maxOrder = 1000;
        foreach ($arTeaserID as $obTeaser) {
            if ($obTeaser->term_order) {
                $arId[$obTeaser->term_order] = $obTeaser->object_id;
            } else {
                $arId[$maxOrder] = $obTeaser->object_id;
                $maxOrder++;
            }
        }
        switch ($tag->taxonomy) {
            case TEASER_TAXONOMY:
                $custom_post_type = TEASER_CUSTOM_POST;
                break;
            case CAROUSEL_TAXONOMY:
                $custom_post_type = CAROUSEL_SLIDE_CUSTOM_POST;
                break;
        }
        if (!$item_type) {
            $item_type = $custom_post_type;
        }
        $arTeasersQuery = array('post_type' => $custom_post_type, $tag->taxonomy => $tag->slug, 'posts_per_page' => -1);
        $arTeasers = get_posts($arTeasersQuery);
        if (count($arTeasers) > 0 && count($arId) > 0) {
            $arSortedTeaser = $this->sortPostObjectArray($arId, $arTeasers);
        }
        ?>
        <table>
            <tr class="form-field">
                <td><label><b><?php _e("Sort Items") ?></b></label></td>
            </tr>
            <tr class="form-field">
                <td colspan="2">
                    <ul id="custom-order-list-<?php echo $item_type; ?>" class="item_order_list">
                        <input type="hidden" id="modified-item-<?php echo $item_type; ?>" name="modified-item-<?php echo $item_type; ?>" value="0" />
                        <?php
                        if (count($arSortedTeaser) > 0) {
                            foreach ($arSortedTeaser as $arTeaser) {

                                $arPostMeta = get_post_meta($arTeaser['ID']);
                                $arImage = wp_get_attachment_image_src($arPostMeta['_thumbnail_id'][0], 'thumbnail');
                                ?>
                                <li id="associated_item_<?php echo $arTeaser['ID'] ?>" class="menu-item-handle"><span class="item-title">
                                        <?php
                                        if (count($arImage) > 0) {
                                            ?>
                                            <img height="28" width="28" align="middle" src="<?php echo$arImage['0']; ?>" />&nbsp; &nbsp;
                                            <?php
                                        }
                                        ?>
                                <?php echo $arTeaser['post_title'] ?></span>
                                    <input type="hidden" name="<?php echo $item_type; ?>_items[]" value="<?php echo $arTeaser['ID'] ?>" />
                                </li>
                                <?php
                            }
                        } else {
                            echo "<p>";
                            _e("No Item associated");
                            echo "</p>";
                        }
                        ?>
                    </ul>
                </td>
            </tr>

            <?php
        }

        /**
         * This function provide Js to provide sort order by drag drop Slides
         *
         */
        function addSortableJS() {
            ?>
            <script type="text/javascript">
                // <![CDATA[
                function customtaxorderAddLoadEvent() {
                    jQuery(".item_order_list").sortable({
                        placeholder: "sortable-placeholder",
                        revert: false,
                        tolerance: "pointer"
                    });
                }
                ;
                addLoadEvent(customtaxorderAddLoadEvent);
                // ]]>
            </script>
            <?php
        }

        /**
         * This function is modified version of Word press's find_posts.
         * 	Find post was not giving the provision to get list of post on the basis of post type or meta values
         * This function is used to display list of posts in thick box.
         * We are using it for Associating Promo Col to the different entities
         */
        function custom_find_posts() {
            global $wpdb, $wp_taxonomies, $wp_post_types;

            //check_ajax_referer( 'find-custom-posts' );
            $item_type_name = $_POST['item_type_name'];
            $item_type_value = $_POST['item_type_value'];
            $meta_key = $_POST['meta_key'];
            $meta_value = $_POST['meta_value'];

            if (!isset($item_type_name)) {
                $post_types = get_post_types(array('public' => true), 'objects');
                unset($post_types['attachment']);
                $post_type = array_keys($post_types);
            }

            $s = stripslashes($_POST['ps']);
            $searchand = $search = '';
            if ('' !== $s)
                $args['s'] = $s;

            if ($item_type_name == "taxonomy_type") {
                $args['search'] = $s;
                $args['hide_empty'] = false;
                $arTaxonomies = array($item_type_value);
                $posts = get_terms($arTaxonomies, $args);
                $arTaxonomyImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS);
            } else {
                $args = array(
                    'post_type' => $item_type_value,
                    'post_status' => 'any',
                    'posts_per_page' => 50,
                );
                $s = stripslashes($_POST['ps']);
                if ('' !== $s)
                    $args['s'] = $s;

                if (isset($meta_key) && $meta_key != "" && isset($meta_value) && $meta_value != "") {
                    $args['meta_query'] = array(
                        array(
                            'key' => $meta_key,
                            'value' => $meta_value,
                        )
                    );
                    $item_type = $meta_value;
                } else {
                    $item_type = $item_type_value;
                }
                $posts = get_posts($args);
            }

            if (!$posts)
                wp_die(__('No items found.'));

            $html = '<table class="widefat" cellspacing="0"><thead><tr><th class="found-radio"><br /></th><th>' . __('Title') . '</th><th class="no-break">' . __('Type') . '</th><th class="no-break">' . __('Image') . '</th></tr></thead><tbody>';
            foreach ($posts as $post) {
                if ($item_type_name == "taxonomy_type") {
                    $title = trim($post->name) ? $post->name : __('(no title)');
                    $post_type_name = $wp_taxonomies[$item_type_value]->labels->singular_name;
                    $arImage = wp_get_attachment_image_src($arTaxonomyImages[$post->term_taxonomy_id]['image_id'], 'thumbnail');
                    $found_item_id = $post->term_id;
                } else {
                    $title = trim($post->post_title) ? $post->post_title : __('(no title)');
                    $post_type_name = $wp_post_types[$item_type_value]->labels->singular_name;
                    $arPostMeta = get_post_meta($post->ID);
                    $arImage = wp_get_attachment_image_src($arPostMeta['_thumbnail_id'][0], 'thumbnail');
                    $found_item_id = $post->ID;
                }
                $html .= '<tr class="found-items"><td class="found-items"><input type="' . $_POST['found_action'] . '" id="found-' . $found_item_id . '" name="found_post_id" value="' . esc_attr($found_item_id) . '" label="' . esc_html($title) . '"></td>';
                $html .= '<td><label for="found-' . $found_item_id . '">' . esc_html($title) . '</label></td><td class="no-break">' . $post_type_name . '</td><td class="no-break"><img id="post-image-' . $found_item_id . '" width="28" height="28" src="' . $arImage[0] . '" /></td></tr>' . "\n\n";
            }
            $html .= '</tbody></table>';
            if ($_POST['found_action'] == 'checkbox') {
                $html .= '<script>
			jQuery("#custom-order-list-' . $item_type . ' input").each( function () {
				var item_id = jQuery(this).val();

				if(jQuery("#found-"+item_id))
				{
					jQuery("#found-"+item_id).attr("checked",true);
				}

			});
			jQuery(".found-items input").click( function() {

				jQuery("#modified-item-' . $item_type . '").val(1);
				var post_id = jQuery(this).val();
				var post_name = jQuery(this).attr("label");
				if(jQuery(this).is(":checked"))
				{
					if(jQuery("#custom-order-list-' . $item_type . ' p"))
					{
						jQuery("#custom-order-list-' . $item_type . ' p").remove();
					}
					var img_src = jQuery("#post-image-"+post_id).attr("src");
					li_html= "<li id=\"associated_item_"+post_id+"\" class=\"menu-item-handle\"><span class=\"item-title\"><img height=\"28\" width=\"28\" align=\"middle\" src=\""+img_src+"\">&nbsp; &nbsp;"+post_name+"</span><input type= \"hidden\" value=\""+post_id+"\" name=\"' . $item_type . '_items[]\"></li>"
					jQuery("#custom-order-list-' . $item_type . '").append(li_html);
				}
				else
				{
					jQuery("#associated_item_"+post_id).remove();
				}

			})
			</script>';
            }
            $x = new WP_Ajax_Response();
            $x->add(array(
                'data' => $html
            ));
            $x->send();
        }

        // ADD NEW COLUMN
        function postColumnHead($defaults) {
            global $post;
            if ($post->post_type == SPOTLIGHT_CUSTOM_POST) {
                $defaults['description'] = 'Description';
            }
            return $defaults;
        }

        // SHOW THE FEATURED IMAGE
        function postColumnContent($column_name, $post_ID) {

            if ($column_name == 'description') {
                $description = get_the_content($post_ID);
                echo $description;
            }
        }

        /**
         * Purpose of this function is to add custom categories meta box on Article and Product Page
         *
         */
        function postCategoriesMetaBox($post, $box) {
            $defaults = array('taxonomy' => 'category', 'options' => 'radio');
            if (!isset($box['args']) || !is_array($box['args']))
                $args = array();
            else
                $args = $box['args'];
            extract(wp_parse_args($args, $defaults), EXTR_SKIP);
            $tax = get_taxonomy($taxonomy);
            ?>
            <div id="taxonomy-<?php echo $taxonomy; ?>" class="categorydiv">
                <ul id="<?php echo $taxonomy; ?>-tabs" class="category-tabs">
                    <li class="tabs"><a href="#<?php echo $taxonomy; ?>-all" tabindex="3"><?php echo $tax->labels->all_items; ?></a></li>
                </ul>

                <div id="<?php echo $taxonomy; ?>-all" class="tabs-panel">
                    <?php
                    $name = ( $taxonomy == 'category' ) ? 'post_category' : 'tax_input[' . $taxonomy . ']';
                    echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
                    ?>
                    <ul id="<?php echo $taxonomy; ?>checklist" class="list:<?php echo $taxonomy ?> categorychecklist form-no-clear">

                        <?php
                        // this is PHP patch for converting Checkbox to Radio button for Category Listing
                        ob_start();
                        wp_terms_checklist($post->ID, array('taxonomy' => $taxonomy, 'checked_ontop' => false));
                        $html = ob_get_clean();
                        $html = str_replace('"checkbox"', $options, $html);
                        echo $html;
                        ?>
                    </ul>
                </div>

            </div>
            <?php
        }

        function find_custom_posts_div($found_action = '') {
            ?>
            <div id="find-posts" class="find-box" style="display:none;">
                <div id="find-posts-head" class="find-box-head"><?php _e('Search'); ?></div>
                <div class="find-box-inside">
                    <div class="find-box-search">
                        <?php if ($found_action) { ?>
                            <input type="hidden" id="found_action" name="found_action" value="<?php echo esc_attr($found_action); ?>" />
                        <?php } ?>
                        <input type="hidden" name="item_type" id="item_type" value="" />
                        <input type="hidden" name="item_attribute" id="item_attribute" value="" />
                        <?php wp_nonce_field('find-posts', '_ajax_nonce', false); ?>
                        <label class="screen-reader-text" for="find-posts-input"><?php _e('Search'); ?></label>
                        <input type="text" id="find-posts-input" name="ps" value="" />
                        <span class="spinner"></span>
                        <input type="button" id="find-posts-search" value="<?php esc_attr_e('Search'); ?>" class="button" />
                    </div>
                    <div id="find-posts-response"></div>
                </div>
                <div class="find-box-buttons">
                    <input id="find-posts-close" type="button" class="button alignright " value="<?php esc_attr_e('Close'); ?>" />
                    <?php if ($found_action == "radio") { ?>
                        <?php submit_button(__('Select'), 'button-primary alignleft', 'find-posts-submit', false); ?>
                    <?php } ?>
                </div>
            </div>
            <?php
        }

        }
        ?>
