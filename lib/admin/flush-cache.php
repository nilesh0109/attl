<?php

/**
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 * @author  : Rupinder Kaur
 * @copyright (c) 2013, Mizkan
 *
 * Purpose : Flush Page Cache on Required Events
 *
 */
class flushCache {

	function __construct()
	{
		add_action('update_option_sidebars_widgets',array($this,'widgetAssociation'),10,2);
		add_filter('edited_terms', array($this, 'flushTaxonomyPages'));
		add_action('save_post', array($this, 'flushPostPages'));
	}
	function flushPostPages()
	{
		global $post;
        switch ($post->post_type) {			
			case ARTICLE_POST_TYPE:
				$this->flushAllTaxonomyPages(ARTICLE_TAXONOMY);
				$this->flushPostPages(ARTICLE_POST_TYPE);
			break;
			case PRODUCT_CUSTOM_POST:
				$this->flushAllTaxonomyPages(PRODUCT_TAXONOMY);
				$this->flushPostPages(PRODUCT_CUSTOM_POST);
			break;
		}
	}
	function flushTaxonomyPages($term_id)
	{
		switch ($_POST['taxonomy']) {

            case ARTICLE_TAXONOMY:
				$this->flushAllTaxonomyPages(ARTICLE_TAXONOMY);
				$this->flushAllPostPages(ARTICLE_POST_TYPE);
            break;
            case PRODUCT_TAXONOMY:
				$this->flushAllTaxonomyPages(PRODUCT_TAXONOMY);
				$this->flushAllPostPages(PRODUCT_CUSTOM_POST);
            break;
            case TEASER_TAXONOMY:               
            case CAROUSEL_TAXONOMY:
                $obTerm = get_term_by('id', $term_id, $_POST['taxonomy']);
			     $this->flushCarouselPage($obTerm->slug);
            break;
        }
	}
    function __construct11($item_type='',$item_id='')
	{
		switch($item_type)
		{
			// Associate image with a Article Category
			case 'article-category-image-assocoateion':
			break;
			// Associate image with a Product Category
			case 'product-category-image-assocoateion':
			break;
			// Associate image with a Article
			case 'article-image-assocoateion':
			break;
			// Associate image with a Product
			case 'product-image-assocoateion':
			break;						
		}				
	}
	function widgetAssociation($oldvalue,$_newvalue)
	{		
		foreach($oldvalue as $sidebar_name => $arSideBar)
		{		
			if($sidebar_name == 'array_version')
			{
				continue;
			}
			if(
			(count($oldvalue[$sidebar_name]) == 0 && count($_newvalue[$sidebar_name]) > 0) 
			|| (count($_newvalue[$sidebar_name])==0 && count($oldvalue[$sidebar_name]) > 0) 
			|| array_diff_assoc($oldvalue[$sidebar_name],$_newvalue[$sidebar_name])
			)
			{
				switch($sidebar_name)
				{
					case HOME_WIDGET_PLACEHOLDER_ID:									
						echo "Flush Home Page";					
					break;
					case PRODUCT_LANDING_WIDGET_PLACEHOLDER_ID:											
						echo "Flush Product Landing Page";									
					break;
					case ARTICLE_LANDING_WIDGET_PLACEHOLDER_ID:
							echo "Flush Article Landing Page";									
					break;
					case ARTICLE_CATEGORY_WIDGET_PLACEHOLDER_ID:
						echo "Flush All Article Categories Pages";									
					break;
					case PRODUCT_CATEGORY_WIDGET_PLACEHOLDER_ID:
						echo "// Flush All Product Categories Pages";											
					break;
					case PRDUCT_DETAIL_WIDGET_PLACEHOLDER_ID:
						echo "// Flush all Product Detail Pages";											
					break;
					case CONTACTUS_WIDGET_PLACEHOLDER_ID:
						echo "// flush Contact us page";
					break;
				}
			}
		}		
		exit();
	}
	/**
     * This function Will flush all the Taxonomy Pages (Taxonomy Landing Page, Taxonomy Pages)
     */
	function flushAllTaxonomyPages($taxonomy)
	{
	// Coding
	}
	/**
     * This function Will flush all Post Detail Pages
	 * 
     */
	function flushAllPostPages($post_type)
	{
	// Coding
	}
	/**
     * This function Will flush single Post Detail Pages
     */
	function flushSinglPost($post_id)
	{
	// Coding
	}
	/**
     * This function Will flush all pages
     */
	function flushAllPages()
	{
	// Coding
	}	
	/**
     * This function Will flush corresponding page with which a carousel/teaser slide has been associated
     */
	 function flushCarouselPage($slug)
	 {
		switch($slug)
		{
			case CAROUSEL_HOME_PAGE:
			case TEASER_CAROUSEL_HOME_PAGE:
			echo 'Flush Home Page';
			// Coding
			break;
			case CAROUSEL_PRODUCT_LANDING:
			echo 'Flush Product Landing Page';
			// Coding
			break;
			case CAROUSEL_ARTICLE_LANDING:
			echo 'Flush Article Landing Page';
			// Coding
			break;
			default:
				$pos = strpos($slug, CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX);
				if($pos !== false)
				{
					// get term_taxonomy_id for the term page needs to be flushed
					$term_taxonomy_id = str_replace(CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX, '' , $slug);					
				}
				else
				{
					$pos = strpos($slug, CAROUSEL_PRODUCT_CATEGORY_SLUG_PREFIX);
					if($pos !== false)
					{
						// get term_taxonomy_id for the term page needs to be flushed
						$term_taxonomy_id = str_replace(CAROUSEL_PRODUCT_CATEGORY_SLUG_PREFIX, '' , $slug);
					}
				}				
				// Get Term URL from term_taxonomy_id and Flush the Page
				echo $term_taxonomy_id;
				// Coding
			break;
		}		
	 }
}
new flushCache();
?>