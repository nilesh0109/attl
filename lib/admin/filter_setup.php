<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Rupinder Kaur
 * Purpose : Implement functionality on some Common Filters/Actions
 *
 */

class FilterSetup {

    public static $youtubeRegex = '@^\s*https?://(www\.)?(?:(?:youtube.com/watch\?)|(?:youtu.be/))([^\s"]+)\s*$@im';

    function __construct() {

        add_filter('the_content', array($this, 'parseYoutubeVideo'), 1); // Implement Embed Youtube video on copy/pasting of link
        add_filter('tiny_mce_before_init', array($this, 'customAnchorTagSettings')); // Custom Anchor tag setting in Default editor for CT Tags
        add_filter('mce_buttons_2', array($this, 'addSuperSubScript'));
        add_action('init', array($this, 'allowCtTagsContent'));
        add_action('rss2_item', array($this, 'customRSSFeed'));
    }

    /**
     * Add Super Script/ Sub Script Buttons in Text Editor
     */
    function addSuperSubScript($buttons) {

        $buttons[] = 'sup';
        $buttons[] = 'sub';
        return $buttons;
    }

    /**
     * This function allow to add custom attribute in anchor Tag needed for Container Tags
     */
    function customAnchorTagSettings($initArray) {


        $ext = 'a[href|target|id|class|DATA-CT-IDENTIFIER|DATA-CT-CATEGORY|DATA-CT-ACTION|DATA-CT-INFORMATION|DATA-CT-FUNNEL|DATA-CT-ENABLED|DATA-CT-ATTR|rel|title]';
        if (isset($initArray['extended_valid_elements'])) {
            $initArray['extended_valid_elements'] .= ',' . $ext;
        } else {
            $initArray['extended_valid_elements'] = $ext;
        }
        return $initArray;
    }

    /**
     * This function parse the post content and find youtube URL using Regular Expression
     */
    function parseYoutubeVideo($content) {
        $content = preg_replace_callback(self::$youtubeRegex, array($this, 'addYoutubeVideo'), $content);
        return $content;
    }

    /**
     * This function Generate array of Key/Value
     */
    public static function generateKeyValue($qry, $includev) {
        $arQueryVars = explode('&', $qry);
        $arKeyValue = array();
        foreach ($arQueryVars as $key => $value) {
            $arKeyPair = explode('=', $value);
            if (count($arKeyPair) == 2 && ($includev || strtolower($arKeyPair[0]) != 'v')) {
                $arKeyValue[$arKeyPair[0]] = $arKeyPair[1];
            }
        }
        return $arKeyValue;
    }

    /**
     * This function return Youtube URL on the basis of Youtube ID
     */
    function addYoutubeVideo($content) {
        $link = trim(preg_replace('/&amp;/i', '&', $content[0]));
        $link = preg_replace('/\s/', '', $link);
        $arURL = explode('?', $link);
        /* Code for explode video link parameter with text data- */
        $video_params ='';
        if(isset($arURL[1])){
            $params = explode('&', $arURL[1]);
            foreach ($params as &$val) {
                $val = " data-" . $val;
            }
            $video_params = implode('', $params);
        } 
        /* End code for explode video link parameter with text data- */
        $arURLAttr = self::generateKeyValue($arURL[1], true);

        if (strpos($arURL[0], 'youtu.be') !== false && !$arURLAttr['v']) {
            $vtemp = explode('/', $arURLAttrtemp[0]);
            $arURLAttr['v'] = array_pop($vtemp);
        }
        /* New Video Overlay
         * $video_overlay_div to append video in overlay
         * $overlay global variable
         * $html_video to return video or video value
         */
        global $overlay;
        $video_overlay_div = "<div id='video-overlay' class='modal hide fade video-overlay' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-body'></div></div>";
        if ($overlay != 1) {
            $html_video = $video_overlay_div . '<div class="videoAppend" id="' . $arURLAttr['v'] . '-' . rand('1', '1000') . '"><a class="model-video"' . $video_params . ' href="//www.youtube.com/embed/' . $arURLAttr['v'] . '" title="' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '">' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '</a></div>';
            $overlay = 1;
        } else {
            $html_video = '<div class="videoAppend" id="' . $arURLAttr['v'] . '-' . rand('1', '1000') . '"><a class="model-video"' . $video_params . ' href="//www.youtube.com/embed/' . $arURLAttr['v'] . '" title="' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '">' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '</a></div>';
        }
        return $html_video;
        //return '<div class="videoAppend" id="' . $arURLAttr['v'] . '-' . rand('1', '1000') . '"><a class="model-video" '.$all_params.' href="//www.youtube.com/embed/' . $arURLAttr['v'] . '" title="' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '">' . __('Watch Video', LANGUAGE_DOMAIN_NAME) . '</a></div>';
        //	return '<section class="span7 offset1"><iframe width="100%" height="349" src="http://www.youtube.com/embed/'.$arURLAttr['v'].'" frameborder="0" allowfullscreen></iframe></section>';
    }

    function allowCtTagsContent() {
        global $allowedposttags;

        $tags = array('a');
        $new_attributes = array(
            'data-ct-action' => array(),
            'data-ct-category' => array(),
            'data-ct-identifier' => array(),
            'data-ct-information' => array(),
            'data-ct-funnel' => array(),
            'data-ct-enabled' => array(),
            'data-ct-attr' => array()
        );

        foreach ($tags as $tag) {
            if (isset($allowedposttags[$tag]) && is_array($allowedposttags[$tag]))
                $allowedposttags[$tag] = array_merge($allowedposttags[$tag], $new_attributes);
        }
    }

    function customRSSFeed() {
        global $post;
        $output = '';
        $thumbnail_id = get_post_thumbnail_id($post->ID);
        $arThumbnail = wp_get_attachment_image_src($thumbnail_id, 'full');
        $media_type = get_post_mime_type($thumbnail_id);
        $size = filesize(get_attached_file($thumbnail_id));
        $terms = get_the_terms($post->ID, PRODUCT_TAXONOMY);
        if (sizeof($terms) > 0) {
            foreach ($terms as $term) {
                $category_name = $term->name;
                $output .= '<category><![CDATA[' . @html_entity_decode($category_name, ENT_COMPAT, get_option('blog_charset')) . ']]></category>';
            }
        }
        if (!empty($arThumbnail [0])) {
            $output .= "<enclosure url='" . $arThumbnail [0] . "' length='" . $size . "' type='" . $media_type . "'/>";
        }
        echo $output;
    }

}

$obFilterSetup = new FilterSetup();
?>