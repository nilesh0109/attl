<?php

/**
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * @author  : "Atul Gupta <agupta164@sapient.com>"
 * @copyright (c) 2013, Mizkan
 *
 * Purpose : Cache handling
 *
 */

/**
 * Description of update_cache
 *
 * @author agu136
 * @category Cache
 *
 */
class updateCache {

    function __construct() {

    }

    public function addflushPage() {
        add_menu_page('Empty All Cache', __('Empty All Cache', LANGUAGE_DOMAIN_NAME), THEME_OPTION_CAPABILITY, MZ_FLUSH_ALL, array('updateCache', 'flushAll'));
    }

    function flushAll() {

        if (function_exists('w3tc_pgcache_flush')) {
            w3tc_pgcache_flush();
            _e('All caches successfully emptied', LANGUAGE_DOMAIN_NAME);
        } else {
            _e('Cache is not activated', LANGUAGE_DOMAIN_NAME);
        }
    }

}

if (function_exists('w3tc_pgcache_flush')) {
    add_action('admin_menu', function() {
                updateCache::addflushPage();
            });
    add_action('admin_init', function() {
                new updateCache();
            });
}
?>
