<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Paritosh Gautam
 * Purpose : Site settings for Mizkan.
 *
 */

class MzHomeOption {

    public $arMzOptions;

    function __construct() {

        $this->arMzOptions = get_option(MZ_HOME_OPTIONS);

        $this->registerSettingsAndFields();
    }

    /*
     * Adding a Menu Page
     */

    public function addMenuPage() {
        add_theme_page("MZ Home Options", __("Home Page Settings"), THEME_OPTION_CAPABILITY, MZ_HOME_OPTION_SLUG, array('MzHomeOption', 'displayOptionsPage'));
    }

    public function displayOptionsPage() {
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2>
                <?php echo __('Home Page Settings') ?>
            </h2>
            <form action="options.php" method="post" enctype="multipart/form-data">
                <?php settings_fields(MZ_HOME_OPTIONS); ?>
                <?php do_settings_sections(MZ_HOME_OPTION_SLUG) ?>
                <p class="submit">
                    <input type="submit" name="submit" class="button-primary"
                           value="<?php _e("Save Changes"); ?>" />
                </p>
            </form>
        </div>
        <?php
    }

    public function registerSettingsAndFields() {
        register_setting(MZ_HOME_OPTIONS, MZ_HOME_OPTIONS);
        add_settings_section("mz_seo_section", __("Seo Section"), array($this, "MzMainSectionCb"), MZ_HOME_OPTION_SLUG);

        //Seo
        add_settings_field(MZ_SEO_TITLE, __("Title for SEO"), array($this, 'mzSeoTitleSetting'), MZ_HOME_OPTION_SLUG, 'mz_seo_section');
        add_settings_field(MZ_SEO_KEYWORDS, __("SEO Keywords"), array($this, 'mzSeoKeywordSetting'), MZ_HOME_OPTION_SLUG, 'mz_seo_section');
        add_settings_field(MZ_SEO_DESCRIPTION, __("SEO Description"), array($this, 'mzSeoDescriptionSetting'), MZ_HOME_OPTION_SLUG, 'mz_seo_section');
    }

    /*
     * input
     */

    public function mzSeoTitleSetting() {
        echo ("<input name='" . MZ_HOME_OPTIONS . "[" . MZ_SEO_TITLE . "]' type='text' value='{$this->arMzOptions[MZ_SEO_TITLE]}'/>");
    }

    public function mzSeoKeywordSetting() {
        echo ("<input name='" . MZ_HOME_OPTIONS . "[" . MZ_SEO_KEYWORDS . "]' type='text' value='{$this->arMzOptions[MZ_SEO_KEYWORDS]}'/>");
    }

    public function mzSeoDescriptionSetting() {
        echo ("<input name='" . MZ_HOME_OPTIONS . "[" . MZ_SEO_DESCRIPTION . "]' type='text' value='{$this->arMzOptions[MZ_SEO_DESCRIPTION]}'/>");
    }

    /* callback
     */

    public function MzMainSectionCb() {

    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            MzHomeOption::addMenuPage();
        });
add_action('admin_init', function() {
            new MzHomeOption();
        });
?>