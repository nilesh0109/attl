<?php
/**
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains
 * can be used, transferred, reproduced, published, or disclosed, in whole
 * or in part, directly or indirectly, except as expressly authorized by an
 * officer of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author : Paritosh Gautam
 * Purpose : Site Appearance Settings for Mizkan.
 *
 * @category FooterOptions
 * @package Mizkan
 * @author Paritosh Gautam <pgautam2@sapient.com>
 */
require_once 'helpers/FieldValidator.php';

// Add the submenu page in appearance admin menu.
add_action('admin_menu', function() {
            MzThemeOption::add_menu_page();
        });

add_action('admin_init', function() {
            $settings = new MzThemeOption();
            $settings->register_settings_and_fields();
        });

/**
 * Handles Mizkan theme options.
 */
class MzThemeOption {

    private $arMzOptions;
    private $option_group;
    private $option_name;

    /**
     * Initailizes new instance of class MzThemeOption
     */
    function __construct() {
        if (is_admin() && (isset($_GET['page']) && $_GET['page'] == 'update_theme_options')) {
            wp_enqueue_media();
            wp_enqueue_script(
                    'script-upload', content_url() . '/' . LIB_NAME . '/js/admin/script.js', array('jquery', 'media-upload'), time(), true
            );
        }
        $this->option_group = MZ_APPEARANCE_OPTION_SLUG;
        $this->option_name = MZ_APPEARANCE_OPTIONS;

        $this->arMzOptions = get_option($this->option_name);

        // If site options doesn't exist, then set to empty array.
        // This case arises for any site which is fresh set-up.
        if ($this->arMzOptions === false) {
            $this->arMzOptions = array();
            add_option($this->option_name);
        }
    }

    /**
     * Adds Admin Menu for MZ Appearance settings.
     *
     * @return string|bool The resulting page's hook_suffix, or false if the user does not have the capability required.
     */
    public function add_menu_page() {
        // Generates Admin Menu under Appearance.
        add_theme_page(__('MZ Footer Options'), __('Site Footer Options'), THEME_OPTION_CAPABILITY, MZ_APPEARANCE_OPTION_SLUG, 'MzThemeOption::displayAppearanceOptions');
    }

    /**
     * Regsiters Setting sections and fields for MZ
     *  Appearance Admin Menu.
     *
     * @return void
     */
    public function register_settings_and_fields() {
        $page = MZ_APPEARANCE_OPTION_SLUG;
        $section_id = 'mz_footer_section';
        $disclaimer_section = 'mz_footer_disclaimer_section';
        $add_choice_section_id = 'mz_add_choice_section';
        register_setting($this->option_group, $this->option_name, array($this, 'mz_validate_setting'));

        add_settings_section($section_id, __('Footer Section'), array($this, 'MzMainSectionCb'), $page);
        add_settings_section($disclaimer_section, __('Footer Disclaimer Section'), array($this, 'MzMainSectionCb'), $page);
        //add_settings_section($add_choice_section_id, __("Ad Choice / Cookie Policy Section"), array($this, 'MzMainSectionCb'), $page);
        // Footer fields
        add_settings_field(MZ_FOOTER_LOGO, __('Logo'), array($this, 'mzFooterLogoSetting'), $page, $section_id);
        add_settings_field(MZ_FOOTER_TEXT, __('Copyright'), array($this, 'mzFooterDisclaimerSetting'), $page, $section_id);
        add_settings_field(MZ_FOOTER_MORE_TEXT, __('Disclaimer'), array($this, 'mzFooterAdditionalTextSetting'), $page, $section_id);

        add_settings_field(MZ_FOOTER_HOME_PAGE_TEXT, __('Home Page Disclaimer'), array($this, 'mzFooterHomePageTextSetting'), $page, $disclaimer_section);
        add_settings_field(MZ_FOOTER_PRODUCT_PAGE_TEXT, __('Product Page Disclaimer'), array($this, 'mzFooterProductPageTextSetting'), $page, $disclaimer_section);
        add_settings_field(MZ_FOOTER_ARTICLE_PAGE_TEXT, __('Article Page Disclaimer'), array($this, 'mzFooterArticlePageTextSetting'), $page, $disclaimer_section);
        add_settings_field(MZ_FOOTER_HELP_PAGE_TEXT, __('Help Center Disclaimer'), array($this, 'mzFooterHelpPageTextSetting'), $page, $disclaimer_section);

        //add choice
        /* add_settings_field(MZ_FOOTER_ADD_CHOICE_HTML, __("Ad Choice HTML"), array($this, 'addChoiceHtmlSetting'), $page, $add_choice_section_id);
          add_settings_field(MZ_FOOTER_ADD_CHOICE_JS, __("Ad Choice Javascript With Script Tags"), array($this, 'addChoiceJsSetting'), $page, $add_choice_section_id);
          add_settings_field(MZ_FOOTER_COOKIE_JS, __("Cookie Javascript With Script Tags"), array($this, 'CookieJsSetting'), $page, $add_choice_section_id);
         */
    }

    /**
     * Validates footer options
     *
     * @param array $arSiteOptions Array of site options which will contain valid fields only.
     *
     * @return type
     */
    public function mz_validate_setting($arSiteOptions) {
        //$arSiteOptions = $this->validate_image(MZ_FOOTER_LOGO, $arSiteOptions);
        return $arSiteOptions;
    }

    /**
     * Validates uploaded image.
     *
     * @param string $attr          Name of the form field to validate.
     * @param array  $arSiteOptions Array of site options which will contain valid fields only.
     *
     * @return type
     */
    private function validate_image($attr, $arSiteOptions) {
        if (!empty($_FILES[$attr]['tmp_name'])) {
            if (FieldValidator::ValidateImage($_FILES[$attr], $attr)) {
                $overrides = array('test_form' => false);
                $arFile = wp_handle_upload($_FILES[$attr], $overrides);

                $arSiteOptions[$attr] = $arFile['url'];
            } else {
                $arSiteOptions[$attr] = $this->arMzOptions[$attr];
            }
        } else {
            $arSiteOptions[$attr] = $this->arMzOptions[$attr];
        }

        return $arSiteOptions;
    }

    public function mzFooterProductPageTextSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_PRODUCT_PAGE_TEXT], $this->option_name . '[' . MZ_FOOTER_PRODUCT_PAGE_TEXT . ']', $this->getWpEditorArgs());
    }

    public function mzFooterArticlePageTextSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_ARTICLE_PAGE_TEXT], $this->option_name . '[' . MZ_FOOTER_ARTICLE_PAGE_TEXT . ']', $this->getWpEditorArgs());
    }

    public function mzFooterHomePageTextSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_HOME_PAGE_TEXT], $this->option_name . '[' . MZ_FOOTER_HOME_PAGE_TEXT . ']', $this->getWpEditorArgs());
    }

    public function mzFooterHelpPageTextSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_HELP_PAGE_TEXT], $this->option_name . '[' . MZ_FOOTER_HELP_PAGE_TEXT . ']', $this->getWpEditorArgs());
    }

    public function mzFooterAdditionalTextSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_MORE_TEXT], $this->option_name . '[' . MZ_FOOTER_MORE_TEXT . ']', $this->getWpEditorArgs());
    }

    public function mzFooterDisclaimerSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_TEXT], $this->option_name . '[' . MZ_FOOTER_TEXT . ']', $this->getWpEditorArgs());
    }

    public function addChoiceHtmlSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_ADD_CHOICE_HTML], $this->option_name . '[' . MZ_FOOTER_ADD_CHOICE_HTML . ']', $this->getWpEditorArgs());
    }

    public function addChoiceJsSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_ADD_CHOICE_JS], $this->option_name . '[' . MZ_FOOTER_ADD_CHOICE_JS . ']', $this->getWpEditorArgs());
    }

    public function CookieJsSetting() {
        wp_editor($this->arMzOptions[MZ_FOOTER_COOKIE_JS], $this->option_name . '[' . MZ_FOOTER_COOKIE_JS . ']', $this->getWpEditorArgs());
    }

    public function mzFooterLogoSetting() {

        if (!empty($this->arMzOptions[MZ_FOOTER_LOGO])) {
            $image_footer = wp_get_attachment_image_src(($this->arMzOptions[MZ_FOOTER_LOGO]), 'full');
            $image_footer = $image_footer[0];
        }
        ?>
        <div class="widget_image" style="clear:both">
            <input id="<?php echo MZ_APPEARANCE_OPTIONS . "[" . MZ_FOOTER_LOGO . "]"; ?>" name="<?php echo MZ_APPEARANCE_OPTIONS . "[" . MZ_FOOTER_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions[MZ_FOOTER_LOGO]) ?>" />
            <img src="<?php echo esc_attr($image_footer); ?>" class="widget_preview_image" alt="" /><br>
            <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions[MZ_FOOTER_LOGO] ?>">Choose Image</a>
            <?php
            if (!empty($this->arMzOptions[MZ_FOOTER_LOGO])) {
                ?>
                <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
            <?php } ?>
        </div>
        <?php
    }

    public function MzMainSectionCb() {

    }

    private function getWpEditorArgs() {
        return array('media_buttons' => false);
    }

    /**
     * Displays MZ Lite Appearance Options.
     *
     * @return void
     */
    public function displayAppearanceOptions() {
        if (!current_user_can(THEME_OPTION_CAPABILITY)) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2>
                <?php echo __('Site Footer Options'); ?>
            </h2>
            <form action="options.php" method="post" enctype="multipart/form-data">
                <?php settings_errors(); ?>
                <?php settings_fields(MZ_APPEARANCE_OPTION_SLUG); ?>
                <?php do_settings_sections(MZ_APPEARANCE_OPTION_SLUG) ?>
                <p class="submit">
                    <input type="submit" name="submit" class="button-primary"
                           value="<?php _e('Save Changes'); ?>" />
                </p>
            </form>
        </div>

        <?php
    }

}
?>
