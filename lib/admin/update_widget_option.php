<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Sanchit Baveja
 * Purpose : Twitter/FB Widget.
 *
 */

class MzWidgetLiteOption {

    public $arMzWidgetOptions;

    function __construct() {
        $this->arMzWidgetOptions = get_option(MZ_SITE_WIDGET_OPTIONS);
        $this->registerSettingsAndFields();
        wp_enqueue_script('colorbox', content_url() . "/" . LIB_NAME . '/js/admin/colorpicker.js');
        wp_enqueue_style('colorboxcs', content_url() . "/" . LIB_NAME . '/css/admin/colorpicker.css');
    }

    /*
     * Adding a Menu Page
     */

    public function addMenuPage() {
        add_theme_page("MZ Social Widget Options", __("Social Widgets Options"), THEME_OPTION_CAPABILITY, MZ_WIDGET_EDIT_OPTION_SLUG, array('MzWidgetLiteOption', 'displayOptionsPage'));
    }

    public function displayOptionsPage() {
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2><?php _e('Social Widgets Options') ?> </h2>
            <?php settings_errors(); ?>
            <!--For Resetting Form Values-->
            <script>
                function clear_form_elements(ele) {


                    tags = ele.getElementsByTagName('input');
                    for (i = 0; i < tags.length; i++) {
                        switch (tags[i].type) {
                            case 'password':
                            case 'text':
                                tags[i].value = '';
                                var twitterLinkColor = '#87c2ed';

                                var colorPicker = function(sel) {
                                    jQuery(sel).each(function() {
                                        jQuery(this).css('background-color', jQuery(this).val());
                                        var _this = this;

                                        jQuery(this).ColorPicker({
                                            color: jQuery(_this).val(twitterLinkColor),
                                        });

                                    });
                                };
                                colorPicker('input.pickerfield');
                                break;
                            case 'checkbox':
                            case 'radio':
                                tags[i].checked = false;
                                break;
                        }
                    }

                    tags = ele.getElementsByTagName('select');
                    for (i = 0; i < tags.length; i++) {
                        if (tags[i].type == 'select-one') {
                            tags[i].selectedIndex = 0;
                        }
                        else {
                            for (j = 0; j < tags[i].options.length; j++) {
                                tags[i].options[j].selected = false;
                            }
                        }
                    }

                    tags = ele.getElementsByTagName('textarea');
                    for (i = 0; i < tags.length; i++) {
                        tags[i].value = '';
                    }

                }
            </script>
            <!--For Resetting Form Values-->
            <form action ="options.php" method="post" enctype="multipart/form-data">
                <?php settings_fields(MZ_SITE_WIDGET_OPTIONS); ?>
                <?php do_settings_sections(MZ_WIDGET_EDIT_OPTION_SLUG) ?>
                <p class ="submit">
                    <input type="submit" name="submit" class="button-primary" value="<?php echo(__("Save Changes")); ?>"/>
                    <input type="button" value="Reset Values" class="button-primary" onclick="clear_form_elements(this.form)" />
                </p>
                <script type="text/javascript">
                var colorPicker = function(sel) {
                    jQuery(sel).each(function() {
                        jQuery(this).css('background-color', jQuery(this).val());
                        var _this = this;

                        jQuery(this).ColorPicker({
                            color: jQuery(_this).val(),
                            onShow: function(colpkr) {
                                jQuery(colpkr).fadeIn(500);
                                return false;
                            },
                            onHide: function(colpkr) {
                                jQuery(colpkr).fadeOut(500);
                                return false;
                            },
                            onChange: function(hsb, hex, rgb) {
                                jQuery(_this).val("#" + hex);
                                jQuery(_this).css('background-color', '#' + hex);
                            }
                        });

                    });
                };
                colorPicker('input.pickerfield');
                </script>
                <?php
                // Add an nonce field so we can check for it later.
                wp_nonce_field('displayOptionsPage', 'mz_social_widget_setting');
                ?>
            </form>
        </div>
        <?php
    }

    public function registerSettingsAndFields() {
        register_setting(MZ_SITE_WIDGET_OPTIONS, MZ_SITE_WIDGET_OPTIONS, array($this, 'mzLiteValidateSetting')); //3rd param is callback
        // ALL Sections
        add_settings_section("mz_lite_twitter_section", __("Twitter Section"), array($this, 'MzWidgetsSectioncb'), MZ_WIDGET_EDIT_OPTION_SLUG);
        add_settings_section("mz_lite_facebook_section", __("Facebook Section"), array($this, 'MzWidgetsSectioncb'), MZ_WIDGET_EDIT_OPTION_SLUG);
        //twitter Settings
        add_settings_field(MZ_TWITTER_WIDGET_ID, __("Twitter Widget ID"), array($this, 'mzTwitterWidgetIdSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');
        add_settings_field(MZ_TWITTER_ID, __("Twitter ID"), array($this, 'mzTwitterIdSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');
        add_settings_field(MZ_TWITTER_LINKCOLOR, __("Twitter Link Color"), array($this, 'mzTwitterLinkColorSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');
        add_settings_field(MZ_TWITTER_HEIGHT, __("Twitter Height"), array($this, 'mzTwitterHeightSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');
        /* removing twitter count on BL-328 */
        //add_settings_field(MZ_TWITTER_TWEETCOUNT, __("Twitter Count"), array($this, 'mzTwitterCountSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');
        add_settings_field(MZ_TWITTER_SCHEME, __("Twitter Color Scheme"), array($this, 'mzTwitterSchemeSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');

        add_settings_field(MZ_TWITTER_CHROME, __("Twitter Chrome"), array($this, 'mzTwitterChromeSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_twitter_section');

        //FB settings
        add_settings_field(MZ_FACEBOOK_ID, __("Facebook ID"), array($this, 'mzFacebookIdSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_facebook_section');
        add_settings_field(MZ_FACEBOOK_URL, __("Site URL"), array($this, 'mzFacebookUrlSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_facebook_section');
        add_settings_field(MZ_FACEBOOK_HEIGHT, __("Facebook Widget Height"), array($this, 'mzheightsetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_facebook_section');
        add_settings_field(MZ_FACEBOOK_FONT, __("Facebook Font"), array($this, 'mzFacebookFontSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_facebook_section');
        add_settings_field(MZ_FACEBOOK_COLOR_SCHEME, __("Facebook Color Scheme"), array($this, 'mzFacebookColorSchemeSetting'), MZ_WIDGET_EDIT_OPTION_SLUG, 'mz_lite_facebook_section');
    }

    /**
     * Generates fields for BIN Base URL setting.
     */
    /*
     * validate settings
     */

    public function mzLiteValidateSetting($arSiteOptions) {


        // if this fails, check_admin_referer() will automatically print a "failed" page and die.
        if (!empty($_POST['mz_social_widget_setting']) && check_admin_referer('displayOptionsPage', 'mz_social_widget_setting')) {
            if (!empty($arSiteOptions[MZ_TWITTER_WIDGET_ID])) {
                $arSiteOptions[MZ_TWITTER_WIDGET_ID] = sanitize_text_field($arSiteOptions[MZ_TWITTER_WIDGET_ID]);
            }
            if (!empty($arSiteOptions[MZ_TWITTER_ID])) {
                $arSiteOptions[MZ_TWITTER_ID] = sanitize_text_field($arSiteOptions[MZ_TWITTER_ID]);
            }
            if (!empty($arSiteOptions[MZ_TWITTER_LINKCOLOR])) {
                $arSiteOptions[MZ_TWITTER_LINKCOLOR] = sanitize_text_field($arSiteOptions[MZ_TWITTER_LINKCOLOR]);
            }
            if (!is_numeric($arSiteOptions[MZ_TWITTER_HEIGHT]) && !empty($arSiteOptions[MZ_TWITTER_HEIGHT])) {
                add_settings_error(
                        'MZ_TWITTER_HEIGHT', esc_attr('settings_updated'), __('Please fill the Twitter Height Correctly.'), 'error');
                $arSiteOptions[MZ_TWITTER_HEIGHT] = '';
            } else {
                $arSiteOptions[MZ_TWITTER_HEIGHT] = sanitize_text_field($arSiteOptions[MZ_TWITTER_HEIGHT]);
            }
            if (!empty($arSiteOptions[MZ_FACEBOOK_ID])) {
                $arSiteOptions[MZ_FACEBOOK_ID] = sanitize_text_field($arSiteOptions[MZ_FACEBOOK_ID]);
            }
            if (!empty($arSiteOptions[MZ_FACEBOOK_URL])) {
                $arSiteOptions[MZ_FACEBOOK_URL] = sanitize_text_field($arSiteOptions[MZ_FACEBOOK_URL]);
            }
            if (!is_numeric($arSiteOptions[MZ_FACEBOOK_HEIGHT]) && !empty($arSiteOptions[MZ_FACEBOOK_HEIGHT])) {
                add_settings_error(
                        'MZ_FACEBOOK_HEIGHT', esc_attr('settings_updated'), __('Please fill the Facebook Height Correctly.'), 'error');
                $arSiteOptions[MZ_FACEBOOK_HEIGHT] = '';
            } else {
                $arSiteOptions[MZ_FACEBOOK_HEIGHT] = sanitize_text_field($arSiteOptions[MZ_FACEBOOK_HEIGHT]);
            }
        }

        return $arSiteOptions;
    }

    /*
     * input
     */

    public function mzTwitterWidgetIdSetting() {
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_WIDGET_ID . "]' type='text' value='{$this->arMzWidgetOptions[MZ_TWITTER_WIDGET_ID]}'/>");
    }

    public function mzTwitterIdSetting() {
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_ID . "]' type='text' value='{$this->arMzWidgetOptions[MZ_TWITTER_ID]}'/>");
    }

    public function mzTwitterHeightSetting() {
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_HEIGHT . "]' type='text' value='{$this->arMzWidgetOptions[MZ_TWITTER_HEIGHT]}' maxlength='3'/> ");
    }

    public function mzTwitterLinkColorSetting() {

        if (empty($this->arMzWidgetOptions[MZ_TWITTER_LINKCOLOR])) {
            $linkColor = '#87c2ed';
        } else {
            $linkColor = $this->arMzWidgetOptions[MZ_TWITTER_LINKCOLOR];
        }
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_LINKCOLOR . "]' type='text' value={$linkColor} maxlength='6' class='colorpickerField1 pickerfield' />");
    }

    /* removing twitter count on BL-328 */

    public function mzTwitterCountSetting() {
        //echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_TWEETCOUNT . "]' type='text' value='{$this->arMzWidgetOptions[MZ_TWITTER_TWEETCOUNT]}' maxlength='2'/> ");
    }

    public function mzTwitterSchemeSetting() {
        $ar_scheme = array("light" => ("Light"), "dark" => ("Dark"));
        echo ("<select name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_SCHEME . "]'>");
        foreach ($ar_scheme as $theme => $value) {
            $selected = '';
            $this->arMzWidgetOptions[MZ_TWITTER_SCHEME] == $theme ? $selected = " selected='selected'" : "";
            echo"<option value ='{$theme}'" . $selected . ">" . $value . "</option>";
        }
        echo("</select>");
    }

    public function mzTwitterChromeSetting() {
        $count = 0;
        $ar_chromeOptions = array('noheader' => __("No Header"), 'nofooter' => __("No Footer"), 'noscrollbar' => __("No Scroll Bar"), 'noborders' => __("No Borders"), 'transparent' => __("Transparent"));
        echo"<table>";
        foreach ($ar_chromeOptions as $chrome => $value) {
            echo"<tr>";
            $checked = '';
            $this->arMzWidgetOptions[MZ_TWITTER_CHROME][$count] === $chrome ? $checked = " checked='checked'" : "";
            echo "<td><label>" . $value . "</label></td><td><input type='checkbox' name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_TWITTER_CHROME . "][{$count}]' value='{$chrome}'. $checked/></td>";
            $count = $count + 1;
            echo"</tr>";
        }
        echo"</table>";
    }

    public function mzFacebookIdSetting() {
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_FACEBOOK_ID . "]' type='text' value='{$this->arMzWidgetOptions[MZ_FACEBOOK_ID]}'/>");
    }

    public function mzFacebookUrlSetting() {
        echo ("<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_FACEBOOK_URL . "]' type='text' value='{$this->arMzWidgetOptions[MZ_FACEBOOK_URL]}'/>");
    }

    public function mzheightsetting() {
        echo ( "<input name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_FACEBOOK_HEIGHT . "]' type='text' value='{$this->arMzWidgetOptions[MZ_FACEBOOK_HEIGHT]}' maxlength='3' />");
    }

    public function mzFacebookColorSchemeSetting() {
        $ar_scheme = array("light" => __("Light"), "dark" => __("Dark"));
        echo ("<select name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_FACEBOOK_COLOR_SCHEME . "]'>");
        foreach ($ar_scheme as $theme => $value) {
            $selected = '';
            $this->arMzWidgetOptions[MZ_FACEBOOK_COLOR_SCHEME] == $theme ? $selected = " selected='selected'" : "";
            echo"<option value ='{$theme}'" . $selected . ">" . $value . "</option>";
        }
        echo("</select>");
    }

    public function mzFacebookFontSetting() {
        $ar_scheme = array("arial" => __("Arial"), "lucida grande" => __("Lucida Grande"), "tahoma" => __("Tahoma"), "trebuchet ms" => __("Trebuchet MS"), "verdana" => __("Verdana"));
        echo ("<select name='" . MZ_SITE_WIDGET_OPTIONS . "[" . MZ_FACEBOOK_FONT . "]'>");
        foreach ($ar_scheme as $theme => $value) {
            $selected = '';
            $this->arMzWidgetOptions[MZ_FACEBOOK_FONT] == $theme ? $selected = " selected='selected'" : "";
            echo"<option value ='{$theme}'" . $selected . ">" . $value . "</option>";
        }
        echo("</select>");
    }

    /*
     * input
     */

    public function MzWidgetsSectioncb() {

    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            MzWidgetLiteOption::addMenuPage();
        });
add_action('admin_init', function() {
            new MzWidgetLiteOption();
        });
?>