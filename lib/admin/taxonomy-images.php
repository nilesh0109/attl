<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Rupinder Kaur
 * Purpose : Functionality to upload images for taxonomies for Mizkan.
 *
 */

class TaxonomyImages {

    public $arTaxonomyImages;

    function __construct() {
        add_action('admin_init', array($this, 'initilize'));
        add_action('wp_ajax_taxonomy_image_association', array($this, 'taxonomyImageAssociation'));
        add_action('wp_ajax_remove_taxonomy_image', array($this, 'removeTaxonomyImage'));

        $this->arTaxonomyImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS); //initilize array with taxonomy image relationship data
    }

    /**
     * Initilize actions for required taxonomies
     */
    function initilize() {
        // Enqueue Js which handle taxonomy association/remove operation
        wp_enqueue_script("taxonomy_images", content_url() . "/" . LIB_NAME . "/js/taxonomy_images.js");

        $arTaxonomies = get_taxonomies();
        if (is_array($arTaxonomies)) {

            $arAllowedTaxonomies = array(ARTICLE_TAXONOMY, PRODUCT_TAXONOMY);

            foreach ($arTaxonomies as $taxonomy) {
                if (in_array($taxonomy, $arAllowedTaxonomies)) {
                    add_filter('manage_edit-' . $taxonomy . '_columns', array($this, 'taxonomyImageColumns'));
                    add_filter('manage_' . $taxonomy . '_custom_column', array($this, 'taxonomyImageRows'), 10, 3);
                }
            }
        }
    }

    /**
     * Edit Term Columns.
     */
    function taxonomyImageColumns($original_columns) {

        wp_enqueue_script('taxonomy_images'); // needed for find posts div
        wp_enqueue_media();
        $new_columns = $original_columns;
        array_splice($new_columns, 1);
        $new_columns['taxonomy_image'] = __('Image');
        return array_merge($new_columns, $original_columns);
    }

    /**
     * Edit Term Rows.
     */
    function taxonomyImageRows($row, $column_name, $term_id) {

        if ('taxonomy_image' === $column_name) {
            global $taxonomy;
            return $row . $this->taxonomyImageActions($term_id, $taxonomy);
        }
        return $row;
    }

    /**
     * Show associate/remove Image Control on each row.
     */
    function taxonomyImageActions($term_id, $taxonomy) {

        $term = get_term($term_id, $taxonomy);
        $term_taxonomy_id = 0;
        if (isset($term->term_taxonomy_id)) {
            $term_taxonomy_id = (int) $term->term_taxonomy_id;
        }
        $taxonomy_image_src = $this->getTaxonomyImageThumbnail($term_taxonomy_id);
        $show_remove = 'style="display:"';
        if (!$taxonomy_image_src) {
            $show_remove = 'style="display:none"';
            $taxonomy_image_src = DEFAULT_TAXONOMY_IMAGE;
        }
        ?>
        <div id="<?php echo 'taxonomy-image-control-' . $term_taxonomy_id; ?>" class="taxonomy-image-control hide-if-no-js">
            <input type="hidden" id="default_image" value="<?php echo DEFAULT_TAXONOMY_IMAGE ?>">
            <img height="75" width="75" id="<?php echo 'taxonomy_image_path_' . $term_taxonomy_id ?>" src="<?php echo $taxonomy_image_src; ?>" alt="" />
            <br><a id="<?php echo 'add-image-' . $term_taxonomy_id; ?>" href="#" class="add_taxonomy_image" data="<?php echo $term_taxonomy_id; ?>"><?php _e('Add') ?></a>
            <a id="<?php echo 'remove-image-' . $term_taxonomy_id; ?>" <?php echo $show_remove; ?> href="#" class="remove_taxonomy_image" data="<?php echo $term_taxonomy_id; ?>"><?php _e('Remove') ?></a>
        </div>
        <?php
    }

    /**
     * Produce JSON Respose.
     * Terminates script execution.
     */
    function taxonomyImageJsonResponse($args) {
        /* translators: An ajax request has failed for an unknown reason. */
        $response = wp_parse_args($args, array(
            'status' => 'false',
            'msg' => __('Unknown error encountered')
                ));
        header('Content-type: application/jsonrequest');
        print json_encode($response);
        exit;
    }

    /**
     * Handle Taxonomy/Image Association action
     */
    function taxonomyImageAssociation() {

        $tt_id = absint($_POST['tt_id']);
        if (!isset($tt_id)) {
            $this->$this->taxonomyImageJsonResponse(array(
                'status' => 'false',
                'msg' => __('Not a Valid Term'),
            ));
        }
        $image_id = absint($_POST['attachment_id']);
        if (!isset($image_id)) {
            $this->taxonomyImageJsonResponse(array(
                'status' => 'false',
                'msg' => __('Attach a Valid Image')
            ));
        }
        if ($this->updateTaxonomyImage($tt_id, $image_id)) {
            $this->taxonomyImageJsonResponse(array(
                'status' => 'true',
                'msg' => __('Image has been associated successfully')
            ));
        } else {
            $this->taxonomyImageJsonResponse(array(
                'status' => 'false',
                'msg' => __('Association could not be created')
            ));
        }
    }

    /**
     * Get Image Associated with a taxonomy
     */
    function getTaxonomyImageThumbnail($tt_id) {
        $attachment_id = $this->arTaxonomyImages[$tt_id]['image_id'];
        if ($attachment_id) {
            $arImage = wp_get_attachment_image_src($attachment_id, 'thumbnail');
            return $arImage[0];
        }
        return false;
    }

    /**
     * Set Image Associated with a taxonomy
     */
    function updateTaxonomyImage($tt_id, $attachment_id) {
        $arTaxonomyImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS);
        $arTaxonomyImages[$tt_id][TAXONOMY_IMAGE_ATTRIBUTE] = $attachment_id;
        if (update_option(CUSTOM_TAXONOMY_IMAGE_FIELDS, $arTaxonomyImages)) {
            return true;
        }
        return false;
    }

    /**
     * Remove Image Associated with a taxonomy
     */
    function removeTaxonomyImage($tt_id) {
        $tt_id = absint($_POST['tt_id']);
        $arTaxonomyImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS);
        unset($arTaxonomyImages[$tt_id]);
        if (update_option(CUSTOM_TAXONOMY_IMAGE_FIELDS, $arTaxonomyImages)) {
            $this->taxonomyImageJsonResponse(array(
                'status' => 'true',
                'msg' => __('Association has been removed')
            ));
        } else {
            $this->taxonomyImageJsonResponse(array(
                'status' => 'false',
                'msg' => __('Association could not be removed')
            ));
        }
    }

}

$obTaxonomyImages = new TaxonomyImages();
?>