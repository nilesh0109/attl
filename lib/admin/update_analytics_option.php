<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Sanchit Baveja
 * Purpose : Capture Analytics Info.
 *
 */

class MzAnalyticsOption {

    public $arMzAnalyticsOptions;

    function __construct() {
        $this->arMzAnalyticsOptions = get_option(MZ_ANALYTICS_OPTIONS);
        $this->registerSettingsAndFields();
    }

    /*
     * Adding a Menu Page
     */

    public function addMenuPage() {
        add_theme_page("MZ Analytics Options", __("Analytics Settings"), THEME_OPTION_CAPABILITY, MZ_ANALYTICS_EDIT_OPTION_SLUG, array('MzAnalyticsOption', 'displayOptionsPage'));
    }

    public function displayOptionsPage() {
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2><?php _e('Analytics Settings'); ?> </h2>
            <?php settings_errors(); ?>
            <form action ="options.php" method="post" enctype="multipart/form-data">
                <?php settings_fields(MZ_ANALYTICS_OPTIONS); ?>
                <?php do_settings_sections(MZ_ANALYTICS_EDIT_OPTION_SLUG) ?>
                <p class ="submit">
                    <input type="submit" name="submit" class="button-primary" value="<?php echo(__("Save Changes")); ?>"/>
                </p>
                <?php
                // Add an nonce field so we can check for it later.
                wp_nonce_field('displayOptionsPage', 'mz_analytics_section');
                ?>
            </form>
        </div>
        <?php
    }

    public function registerSettingsAndFields() {
        register_setting(MZ_ANALYTICS_OPTIONS, MZ_ANALYTICS_OPTIONS, array($this, 'mzLiteValidateSetting')); //3rd param is callback
        // ALL Sections
        add_settings_section("mz_lite_analytics_section", __("Analytics Section"), array($this, 'MzAnalyticsSectionCb'), MZ_ANALYTICS_EDIT_OPTION_SLUG);
        add_settings_field(MZ_BRAND_NAME, __("Global Brand Name"), array($this, 'mzBrandNameSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        //add_settings_field(MZ_TAGGING_LOCAL_BRAND, __("Local Brand Name"), array($this, 'mzLocalBrandNameSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        //add_settings_field(MZ_CT_SERVER_URL, __("Container tag URL "), array($this, 'mzCTServerUrlSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_TAGGING_LOCAL_BRAND, __("Local Brand Name"), array($this, 'mzLocalBrandNameSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
       // add_settings_field(MZ_TAGGING_CATEGORY, __("Tagging Category"), array($this, 'mzTaggingCategorySetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        //add_settings_field(MZ_TAGGING_CHANNEL, __("Tagging Channel"), array($this, 'mzTaggingChannelSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        //add_settings_field(MZ_TAGGING_MOBILE_CHANNEL, __("Tagging Channel For Mobile"), array($this, 'mzTaggingMobileChannelSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_TAGGING_GID, __("GA ID"), array($this, 'mzTaggingGidSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_TAGGING_GOOGLE_ANALYTICS, __("ROLLUP ID"), array($this, 'mzTaggingGAASetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_TAGGING_COUNTRY, __("Country"), array($this, 'mzcountrySetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_DOMNAME, __("Site Domain"), array($this, 'mzGlobalDomNameSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        add_settings_field(MZ_SITE_TYPE, __("Site Type"), array($this, 'mzSiteTypeSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_analytics_section');
        //add_settings_section("mz_lite_webmaster_section", __("Webmaster Section"), array($this, 'MZ_WebmasterSectionCb'), MZ_ANALYTICS_EDIT_OPTION_SLUG);
        //add_settings_field(MZ_GOOGLE_WEBMASTER_CODE, __("Google Code"), array($this, 'mzGoogleCodeSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_webmaster_section');
        //add_settings_field(MZ_BING_WEBMASTER_CODE, __("Bing Code"), array($this, 'mzBingCodeSetting'), MZ_ANALYTICS_EDIT_OPTION_SLUG, 'mz_lite_webmaster_section');
    }

    /*
     * input
     */

    public function mzcountrySetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_COUNTRY . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_COUNTRY]}'/>");
    }

    public function mzLocalBrandNameSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_LOCAL_BRAND . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_LOCAL_BRAND]}'/>");
    }

    public function mzTaggingGAASetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_GOOGLE_ANALYTICS . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_GOOGLE_ANALYTICS]}'/>");
    }

    public function mzTaggingGidSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_GID . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_GID]}'/>");
    }

    public function mzTaggingChannelSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_CHANNEL . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_CHANNEL]}'/>");
    }

    public function mzTaggingMobileChannelSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_MOBILE_CHANNEL . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_MOBILE_CHANNEL]}'/>");
    }

    public function mzTaggingCategorySetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_TAGGING_CATEGORY . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_TAGGING_CATEGORY]}'/>");
    }

    public function mzBrandNameSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_BRAND_NAME . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_BRAND_NAME]}'/>");
    }

    public function mzCTServerUrlSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_CT_SERVER_URL . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_CT_SERVER_URL]}'/>");
    }

    public function mzGlobalBrandNameSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_GLOBAL_BRANDNAME . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_GLOBAL_BRANDNAME]}' />");
    }

    public function mzGlobalDomNameSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_DOMNAME . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_DOMNAME]}' />");
    }

    public function mzSiteTypeSetting() {
        $site_setting = isset($this->arMzAnalyticsOptions[MZ_SITE_TYPE]) ? $this->arMzAnalyticsOptions[MZ_SITE_TYPE] : MZ_SITE_TYPE_DEFAULT_VALUE;
        $readonly = "";
        if(!is_super_admin())
        {
           $readonly = "readonly"; // This field is Readonly for users other than super Admin.
        }
        echo ("<input ".$readonly." name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_SITE_TYPE . "]' type='text' value='".$site_setting."' />");
    }

    public function mzGoogleCodeSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_GOOGLE_WEBMASTER_CODE . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE]}'/>");
    }

    public function mzBingCodeSetting() {
        echo ("<input name='" . MZ_ANALYTICS_OPTIONS . "[" . MZ_BING_WEBMASTER_CODE . "]' type='text' value='{$this->arMzAnalyticsOptions[MZ_BING_WEBMASTER_CODE]}'/>");
    }

    /*
     * input
     */

    public function MzAnalyticsSectionCb() {

    }
    
    /*
     * input
     */

    public function MZ_WebmasterSectionCb() {

    }

    /*
     * validate settings
     */

    public function mzLiteValidateSetting($arAnalyticsOptions) {
// if this fails, check_admin_referer() will automatically print a "failed" page and die.
        if (!empty($_POST['mz_analytics_section']) && check_admin_referer('displayOptionsPage', 'mz_analytics_section')) {
            if (!empty($arAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE])) {
                $arAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE] = sanitize_text_field($arAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE]);
            }
            if (!empty($arAnalyticsOptions[MZ_BING_WEBMASTER_CODE])) {
                $arAnalyticsOptions[MZ_BING_WEBMASTER_CODE] = sanitize_text_field($arAnalyticsOptions[MZ_BING_WEBMASTER_CODE]);
            }
        }

        return $arAnalyticsOptions;
    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            MzAnalyticsOption::addMenuPage();
        });
add_action('admin_init', function() {
            new MzAnalyticsOption();
        });
?>