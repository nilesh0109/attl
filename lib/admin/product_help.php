<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Rupinder Kaur
 * Purpose : Customize Wordpress help Section
 *
 */

class ProductHelp {

    function __construct() {

        add_action('contextual_help', array($this, 'setContextualHelp'), 10, 3);
    }

    function setContextualHelp($contextual_help, $screen_id, $screen) {
        $screen->remove_help_tabs();
        $file_path = get_stylesheet_directory() . '/extlib/help.xml'; // Check if XML file exists in child theme
        if (!file_exists($file_path)) {
            $file_path = get_template_directory() . '/extlib/help.xml'; // Use Master theme XML for Help
        }
        $xml = simplexml_load_file($file_path);
        foreach ($xml->xpath('/HelpXML/screen') as $arItem) {
            if ($screen_id == $arItem['id']) {
                foreach ($arItem->help->section as $arHelpTab) {
                    $screen->add_help_tab($arHelpTab);
                }
                $screen->set_help_sidebar($arItem->sidebar);
                break;
            }
        }
    }

}

$obHelp = new ProductHelp();
?>