<?php

/**
 *  @author Rupinder Kaur <rkaur7@sapient.com>
 *  @copyright (c) 2013, Mizkan
 *
 * Purpose : Remove Non Required Functionalities from Admin Section
 *
 */
class AdminFixes {

    function __construct() {

        add_filter(CAROUSEL_TAXONOMY . '_row_actions', array($this, 'removeQuickViewLink'), 10, 1);
        add_filter(TEASER_TAXONOMY . '_row_actions', array($this, 'removeQuickViewLink'), 10, 1);
        add_filter('post_row_actions', array($this, 'removePostQuickViewLink'), 10, 2);
        add_action(CAROUSEL_TAXONOMY . '_edit_form', array($this, 'removeTaxonomyExtraField'));
        add_action(TEASER_TAXONOMY . '_edit_form', array($this, 'removeTaxonomyExtraField'));

        add_action('add_tag_form', array($this, 'removeAddTermForm'));
        add_action('admin_footer', array($this, 'modifyNavMenuOption'));
        add_action('admin_footer', array($this, 'removePreviewOption'));
        add_filter('lostpassword_url', array($this, 'changeLostPasswordURL'), 10, 2);
        add_action('widgets_init', array($this, 'removeDefaultWidgets'), 1);
        add_action('wp_before_admin_bar_render', array($this, 'removeCommentsMenu'));
        add_action('init', array($this, 'removeCommentSupport',));        // Removes Comments Support from post and pages
        add_action('admin_menu', array($this, 'removeCommentsAdminMenus'));    // Removes comments section menu from left navigation
        add_action('wp_trash_post', array($this, 'restrictDefaultPageDletion'), 10, 1);    //restrict users to delete the default pages while creating at the time site set up
        add_action('before_delete_post', array($this, 'restrictDefaultPageDletion'), 10, 1);  //restrict users to delete the default pages while creating at the time site set up
        add_action('admin_bar_menu', array($this, 'removeAdminBarMenu'), 999, 1); // Removes extra menus from admin bar
        add_action('wp_dashboard_setup', array($this, 'removeDashboardWidgets')); //Remove extra widgets from the dashboard
        add_filter('gettext', array($this, 'removeTagLine'), 20, 3); //Remove Tagline from dashboard
        add_filter('gettext', array($this, 'removeInvalidly'), 20, 3); //Remove Tagline from dashboard
        add_filter('admin_footer_text', array($this, 'removeFooterAdminLink')); // Remove default footer link
        add_filter('update_footer', array($this, 'removeFooterVersion'), 9999); //remove wordpress version from footer
        add_action('admin_head-index.php', array($this, 'removeDiscussion'), 10, 1); // Remove Discussuion for the admin dasboard
        add_action('right_now_content_table_end', array($this, 'dashboardRightNowWidget'), 10, 1); //customize the right now dashboard widget
        add_filter('gettext', array($this, 'renameAdminDefaults'), 20, 3); //Rename Dashboard ,Wordpress and Posts Name across the admin
        add_filter('ngettext', array($this, 'renameAdminDefaults'), 20, 3); //Rename Dashboard ,Wordpress and Posts Name across the admin
        //add_action('admin_menu', array($this, 'removeExcerptMetabox')); //remove excerpt meta box from the post edit screen
        add_action('admin_head-edit.php', array($this, 'removeQuickeditCategory')); //removes quick edit
        add_action('login_head', array($this, 'customLoginLogoImage')); // change the login page header logo image
        remove_action('welcome_panel', 'wp_welcome_panel'); // Remove default dashboard welcome panel
        add_action('welcome_panel', array($this, 'customeWelcomePanel')); // Customized dashboard welcome panel
        add_filter('login_headerurl', array($this, 'changeWPLogoLoginUrl')); // CUSTOM ADMIN LOGIN LOGO LINK
        add_filter('login_headertitle', array($this, 'changeWPLogoLoginTitle')); // CUSTOM ADMIN LOGIN LOGO ALT TEXT
        add_action('wp_dashboard_setup', array($this, 'addPendingDashboardWidgets'), 20, 3); // Pending Content Widget for Dasboard
        add_filter('screen_layout_columns', array($this, 'defaultScreenLayoutColumns')); // dafault screen layout across admin
        add_filter('get_user_option_screen_layout_dashboard', array($this, 'defaultScreenLayoutDashboard')); //default screen layout for dashbodard etc
        add_filter('ngettext', array($this, 'removeThemeRightNow')); // remove theme name from right now dashboard widget
        add_action('init', array($this, 'removeDaefaultHeadMetaTags')); //remove default meta tag of wordpress from the head of theme
        add_filter('xmlrpc_enabled', '__return_false');
        add_action('admin_init', array($this, 'removePointers')); //remove pointers from wp admin
        // add_action('template_redirect', array($this, 'contactUsRedirect'), 1); //redirect the contact us page to secure url
        //add_filter('pre_post_link', 'contactUsRedirectPermalink', 10, 3); // permalink issue for contact us page to redirect to secure url
        add_action('load-index.php', array($this, 'showWelcomeMessagePanel')); // show the welcome panel on login
        //add_action('admin_init', array($this, 'removeEmailDb')); //remove email id of user from database
        add_filter('get_avatar', array($this, 'removeGravatar'), 1, 2); // remove gravatar from admin

        add_filter('manage_posts_columns', array($this, 'addImageCustomColumns')); // These two function adds a new column of featured image on all custom post types
        add_action('manage_posts_custom_column', array($this, 'showImageCustomColumns'), 5, 2);

        //add_filter('intermediate_image_sizes', array($this, 'carausalSize')); // Caraousal Image size
        //add_filter('image_size_names_choose', array($this, 'carausalSizesName')); // Caraousal Image Name for Drop Downs
        //for http and hhtps
        add_filter('wp_get_attachment_url', array($this, 'fix_ssl_attachment_url'));


        /**
         * Theme Option capabilty redfined
         */
        add_filter('option_page_capability_update_mz_theme', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_update_theme_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_home_site_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_social_icons_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_analytics_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_lite_site_widget_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_lite_appearance_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_lite_site_options', array($this, 'themeOptionCapability'), 10, 1);
        add_filter('option_page_capability_mz_facebook_comments_options', array($this, 'themeOptionCapability'), 10, 1);
        
        //for the login dirty fixes if w3 total cache exists to create cookie on httponly for roles
        if (function_exists('w3_instance')) {
            add_action('set_logged_in_cookie', array($this, 'check_login_action'), 3, 5);
        }
    }

    function themeOptionCapability($capability) {
        return THEME_OPTION_CAPABILITY;
    }

    /**
     * This function View option from Custom Taxonomies
     *
     * @param type $name Description
     */
    function removeQuickViewLink($actions) {
        unset($actions['view']);
        return $actions;
    }

    /**
     * This function remove View option from Custom Post type (craousel, Teasers,spotlight)
     */
    function removePostQuickViewLink($actions, $post) {
        if ($post->post_type == CAROUSEL_SLIDE_CUSTOM_POST || $post->post_type == TEASER_CUSTOM_POST || $post->post_type == PROMO_COL_CUSTOM_POST || $post->post_type == SPOTLIGHT_CUSTOM_POST) {
            unset($actions['view']);
        }
        return $actions;
    }

    /**
     * This function is used to remove non required Admin interface
     *
     */
    function removeAddTermForm() {
        // Remove ad Term form for custom taxonomy Teaser and Carousel
        if ($_GET['taxonomy'] == TEASER_TAXONOMY || $_GET['taxonomy'] == CAROUSEL_TAXONOMY) {
            ?>
            <script type="text/javascript">
                // <![CDATA[

                jQuery("#col-left").remove();
                jQuery("#col-right").css("width", "100%");

                // ]]>
            </script>
            <?php
        }
    }

    function removeQuickeditCategory() {
        // Remove ad Term form for custom taxonomy Teaser and Carousel
        ?>
        <script type="text/javascript">
            // <![CDATA[
            jQuery(document).ready(function() {
                jQuery('.inline-edit-categories').css("display", "none");
            })


            // ]]>
        </script>
        <?php
    }

    /**
     * This function is used to remove non required Nav Menu Options
     *
     */
    function modifyNavMenuOption() {
        global $current_screen;
        if ($current_screen->id == 'nav-menus') {
            // Remove Delete option for Default Menu Items
            $arDefaultMenu = get_option(SITE_SETUP_MENU_ITEMS_IDS);
            foreach ($arDefaultMenu as $menu_id) {
                ?>
                <script type="text/javascript">
                    // <![CDATA[
                    jQuery('.add-new-menu-action').remove();
                    jQuery('.delete-action').remove();
                    // ]]>
                </script>
                <?php
            }
            ?>
            <script type="text/javascript">
                // <![CDATA[

                //	jQuery('.delete-action').remove(); // Remove Delete menu option
                jQuery('.menu-add-new').remove();	// Remove Add Menu Option
                jQuery('#menu-name').attr("readonly", "readonly");

                // ]]>
            </script>
            <?php
        }
    }

    /**
     * This function remove Slug/Parent field from edit taxonomy form
     *
     */
    function removeTaxonomyExtraField() {
        ?>
        <script type="text/javascript">
            // <![CDATA[
            jQuery("tr.form-field").each(function(index) {

                if (index == 1 || index == 2)
                    jQuery(this).remove();	// Remove Slug,parent field from Custom Taxonomy Teaser Carousel and Carousel
            });

            // ]]>
        </script>
        <?php
    }

    /**
     * This function remove default sidebar widgets from the admin
     *
     */
    function removeDefaultWidgets() {
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Search');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Links ');
        unregister_widget('WP_Widget_Recent_Posts');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Nav_Menu_Widget');
        unregister_widget('WP_Widget_Archives');
        unregister_widget('WP_Widget_Text');
    }

    function removeCommentsMenu() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('comments');
    }

    /**
     * This function remove comments section menu from left navigation
     *
     */
    function removeCommentsAdminMenus() {
        remove_menu_page('edit-comments.php');
    }

    /**
     * This function remove comments support from post and pages
     *
     */
    function removeCommentSupport() {
        remove_post_type_support('post', 'comments');
        remove_post_type_support('page', 'comments');
    }

    /**
     * This function remove previow button for building block custom post type
     *
     */
    function removePreviewOption() {
        $arCustomPost = array
            (PROMO_COL_CUSTOM_POST
            , SPOTLIGHT_CUSTOM_POST
            , TEASER_CUSTOM_POST
            , CAROUSEL_SLIDE_CUSTOM_POST);
        global $post;
        if (
                in_array($post->post_type, $arCustomPost)) {
            ?>
            <script type="text/javascript">
                // <![CDATA[
                jQuery("#post-preview").remove();
                jQuery("#view-post-btn").remove();
                // ]]>
            </script>
            <?php
        }
    }

    /**
     * This function restrict users to delete the default pages while creating at the time site set up
     *
     */
    function

    restrictDefaultPageDletion($post_ID) {
        $user_id = '0';
                //get_current_user_id();
        $arAllowedUser = array(SUPER_ADMIN_ID
        );
        $arDefaultPageIds =
                get_option(
                SITE_SETUP_PAGE_IDS);
        $arDefaultIds = array_values($arDefaultPageIds);
        if (!in_array($user_id, $arAllowedUser) && in_array($post_ID, $arDefaultIds)) {
            wp_die(__('You are not authorized to delete this page.'));
        }
    }

    /**
     * This function is used to change Lost Password URL
     *
     */
    function changeLostPasswordURL() {
        $get_url = get_option(BRAND_SITE_SETTING_KEY);
        return $get_url[BRAND_SITE_LOST_PASSWORD_RESET_URL];
    }

    /**
     * This function remove the extra menus from the admin bar
     *
     */
    function removeAdminBarMenu($wp_admin_bar) {
        $wp_admin_bar->remove_node('wp-logo');
        if (!is_super_admin()) {
            $wp_admin_bar->remove_node('my-sites');
        }
        $wp_admin_bar->remove_node('user-info');
        $wp_admin_bar->remove_node('edit-profile');
        $wp_admin_bar->remove_node('w3tc-faq');
        $wp_admin_bar->remove_node('w3tc-support');
        $wp_admin_bar->remove_node('w3tc-pgcache-purge-post');

        $my_account = $wp_admin_bar->get_node('my-account');
        $new_title = str_replace('Howdy,', 'Welcome', $my_account->title);
        $wp_admin_bar->add_node(array
            (
            'id' => 'my-account',
            'title' => $new_title,
        ));
    }

    /**
     * This function remove the extra dasboard widgets from the welcome dashboard
     *
     */
    function removeDashboardWidgets() {
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
        remove_meta_box('dashboard_primary', 'dashboard', 'side');
        remove_meta_box('dashboard_secondary', 'dashboard', 'side');
        remove_meta_box('dashboard_welcome_panel', 'dashboard', 'normal');
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');
        remove_meta_box('dashboard_browser_nag', 'dashboard', 'normal');
    }

    /**
     * This function remove the tagline You are using wordpress from dashboard
     *
     */
    function removeTagLine($translated_text, $untranslated_text, $domain) {

        $custom_field_text = 'You are using <span class="b">WordPress %s</span>.';

        if (is_admin() && $untranslated_text === $custom_field_text) {
            return 'You are using <span class="b">Mizkan</span>';
        }

        return $translated_text;
    }

    function removeInvalidly($translated_text, $untranslated_text, $domain) {

        $custom_field_text = 'WordPress should correct invalidly nested XHTML automatically';

        if (is_admin() && $untranslated_text === $custom_field_text) {
            return 'Mizkan should correct invalid nested XHTML automatically';
        }

        return $translated_text;
    }
    
    /**
     * This function remove the default admin link from footer
     *
     */
    function removeFooterAdminLink() {
        echo '<span id="footer-thankyou">Developed by <a href="#" target="_blank">Developers Name</a></span>';
    }

    /**
     * This function remove wordpress version from footer
     *
     */
    function removeFooterVersion() {
        return ' ';
    }

    /**
     * This function remove Discussuion for the admin dasboard
     *
     */
    function removeDiscussion() {

        echo '<style>#dashboard_right_now .table_discussion {display:none !important;}</style>';
    }

    /**
     * This function customize the right now dashboard widget
     *
     */
    function dashboardRightNowWidget() {
        $args = array(
            'public' =>
            true,
            '_builtin' => false
        );
        $output = 'object';
        $operator = 'and';
        $post_types =
                get_post_types($args, $output, $operator);

        foreach ($post_types as $post_type) {
            $num_posts = wp_count_posts($post_type->name);
            $num = number_format_i18n($num_posts->publish);
            $text = _n($post_type->labels->singular_name, $post_type->labels->name, intval($num_posts->publish));
            if (current_user_can('edit_posts')) {
                $num = "<a href='edit.php?post_type=$post_type->name'>$num</a>";
                $text = "<a href='edit.php?post_type=$post_type->name'>" . __($text) . '</a>';
            }
            echo '<tr><td class="first b b-' . $post_type->name . '">' . $num . '</td>';
            echo '<td class="t ' . $post_type->name . '">' . __($text) . '</td></tr>';
        }
    }

    /**
     * This function Rename Dashboard ,Wordpress and Posts Name across the admin
     *
     */
    function renameAdminDefaults($menu) {

        //$menu = str_ireplace('Dashboard', 'Home', $menu);
        $menu = str_ireplace('Posts', 'Articles', $menu);
        $menu = str_ireplace('WordPress', 'Mizkan', $menu);
        return $menu;
    }

    /**
     * This function remove excerpt  meta box from the post edit screen
     *
     */
    function removeExcerptMetabox() {
        remove_meta_box('postexcerpt', 'post', 'normal');
    }

    /**
     * This function change the login page header logo image
     *
     */
    function customLoginLogoImage() {
        echo '<style  type="text/css"> h1 a {  background-image:url(' . get_bloginfo('template_directory') . '/img/default/wp-logo.png)  !important; } </style>';
    }

    /**
     * This function customize dashboard welcome panel
     *
     */
    function customeWelcomePanel() {
        ?>
        <script type="text/javascript">
            /* Hide default welcome message */
            jQuery(document).ready(function($)
            {
                $('div.welcome-panel-content').hide();
            });
        </script>

        <div class="custom-welcome-panel-content">
            <h3><?php _e('Welcome to Mizkan !'); ?></h3>
            <p class="about-description"><?php printf(__('Here is your step-by-step guide to build your website. Have you checked out the <a href="%s" target="_blank">Documentation?</a>'), HELP_URL); ?></p>
            <div class="welcome-panel-column-container">

                <div class="welcome-panel-column">
                    <h4><?php _e("Let's Get Started"); ?></h4>
                    <ul>
                        <?php if (current_user_can('list_users')) : ?>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize">Site Configuration</a>'), admin_url('themes.php?page=' . MZ_EDIT_OPTION_SLUG)); ?></li>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize">Social Widget Setup</a>'), admin_url('themes.php?page=' . MZ_WIDGET_EDIT_OPTION_SLUG)); ?></li>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize">Analytics Tagging Setup</a>'), admin_url('themes.php?page=' . MZ_ANALYTICS_EDIT_OPTION_SLUG)); ?></li>
                        <?php elseif (current_user_can('edit_posts')) : ?>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize">Site Configuration</a>'), admin_url('themes.php?page=' . MZ_EDIT_OPTION_SLUG)); ?></li>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize">Social Icon Setup</a>'), admin_url('themes.php?page=' . MZ_SOCIAL_ICONS_OPTION_SLUG)); ?></li>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize"  >Theming</a>'), admin_url('themes.php?page=' . MZ_EDIT_ADMIN_INTERFACE_SLUG)); ?></a></li>
                        <?php else: ?>
                            <li><?php printf(__('<a href="%s" class="load-customize welcome-icon hide-if-no-customize"  >Theming</a>'), admin_url('themes.php?page=' . MZ_EDIT_ADMIN_INTERFACE_SLUG)); ?></a></li>
                        <?php endif; ?>

                    </ul>
                </div>

                <div class="welcome-panel-column">
                    <h4><?php _e('Next Steps'); ?></h4>
                    <ul>
                        <?php if (current_user_can('manage_categories') || current_user_can('edit_published_posts')) : ?>
                            <li><?php printf('<a href = "%s" class = "load-customize welcome-icon welcome-edit-page">' . __('Product Catgories') . '</a>', admin_url('edit-tags.php?taxonomy=' . PRODUCT_TAXONOMY . '&post_type=' . PRODUCT_CUSTOM_POST)); ?></li>
                            <li><?php printf('<a href = "%s" class = "load-customize welcome-icon welcome-add-page">' . __('Article Catgories') . '</a>', admin_url('edit-tags.php?taxonomy=category')); ?></li>
                            <li><?php printf('<a href = "%s" class = "load-customize welcome-icon welcome-add-page">' . __('Tag Setup') . '</a>', admin_url('edit-tags.php?taxonomy=post_tag')); ?></li>

                        <?php endif; ?>

                    </ul>
                </div>
                <div class="welcome-panel-column welcome-panel-last">
                    <h4><?php _e('More Actions'); ?></h4>
                    <ul>
                        <?php if (current_user_can('list_users')) : ?>
                            <li><?php printf('<div class = "welcome-icon welcome-widgets-menus">' . __('<a href = "%1$s">Manage Widgets</a> ') . '</div>', admin_url('widgets.php')); ?></li>
                            <li><?php printf('<div class = "welcome-icon welcome-widgets-menus">' . __('<a href = "%1$s">Manage Menus</a>') . '</div>', admin_url('nav-menus.php')); ?></li>
                            <li><?php printf('<a href = "%s" class = "welcome-icon welcome-learn-more">' . __('Learn more about getting started') . '</a>', __(HELP_URL)); ?></li>
                            <li><?php printf('<a href="%s" class="welcome-icon welcome-view-site">' . __('View your site') . '</a>', home_url('/')); ?></li>
                        <?php elseif (current_user_can('edit_published_posts')) : ?>
                            <li><?php printf('<a href="%s" class="load-customize welcome-icon welcome-add-page">' . __('Carousel') . '</a>', admin_url('edit.php?post_type=' . CAROUSEL_SLIDE_CUSTOM_POST)); ?></li>
                            <li><?php printf('<a href="%s" class="load-customize welcome-icon welcome-add-page">' . __('Teaser Carousel') . '</a>', admin_url('edit.php?post_type=' . TEASER_CUSTOM_POST)); ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <!-- <div class="">
                 <h3><?php _e('If you need more space'); ?></h3>
                 <p class="about-description">Create a new paragraph!</p>
                 <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Etiam porta sem malesuada magna mollis euismod.

                     Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
             </div>
            -->
        </div>

        <?php
    }

    /**
     * This function c   ustomize

      wp loging logo url
     *
     */
    function changeWPLogoLoginUrl() {
        return LOIGIN_IMAGE_URL;
    }

    /**
     * This function customize wp loging logo Alt Text
     *
     */
    function changeWPLogoLoginTitle() {
        return LOIGIN_IMAGE_LOGO_TITLE;
    }

    /**
     * This function create output pending contents Widget on our Dashboard
     *
     */
    function showPendingDashboardWidget() {
        $args = array(
            'public' => true
        );
        $output = 'object';
        $operator = 'and';
        $post_types = get_post_types($args, $output, $operator);

        foreach ($post_types as $post_type) {
            $pendings_query = new WP_Query(array(
                'post_type' => $post_type->name,
                'post_status' => 'pending',
                'posts_per_page' => 10,
                'orderby' => 'date', // sort by order created, regardless of date
                'order' => 'DESC'
            ));
            $pendings = & $pendings_query->posts;
            $num_posts = wp_count_posts($post_type->name);
            $text = _n($post_type->labels->singular_name, $post_type->labels->name, intval($num_posts->publish));

            if ($pendings && is_array($pendings)) {
                $list = array();
                echo "<a href='edit.php?post_status=pending&post_type=" . $post_type->name . "'>";
                echo "<h4>Pending " . $text . " </h4></a><hr style='border:solid 1px #eee'>";
                foreach ($pendings as $pending) {
                    $url = get_edit_post_link($pending->ID);
                    $title = _draft_or_post_title($pending->ID);
                    $item = "<h4><a href='$url' title='" . sprintf(__('Edit "%s"'), attribute_escape($title)) . "'>$title</a>";
                    //if ( $pending->au
                    $item .= "<abbr title='" . get_the_time(__('Y/m/d g:i:s A'), $pending) . "'> " . get_the_time(get_option('date_format'), $pending) . '</abbr></h4>';
                    if ($the_content = preg_split('#\s#', strip_tags($pending->post_content), 11, PREG_SPLIT_NO_EMPTY))
                        $item .= '<p>' . join(' ', array_slice($the_content, 0, 10)) . ( 10 < count($the_content) ? '&hellip;' : '' ) . '</p>';
                    $list[] = $item;
                }
                ?>
                <ol>
                    <li><?php echo join("</li>\n<li>", $list); ?></li>
                </ol>
                <p class="textright"><a href="edit.php?post_status=pending&post_type=<?php echo $post_type->name ?>" class="button"><?php _e('View all'); ?></a></p>

                <?php
            }
        }
    }

    /**
     * This function create output drafts contents Widget on our Dashboard
     *
     */
    function showDraftsDashboardWidget($drafts = false) {
        $args = array(
            'public' => true
        );
        $output = 'object';
        $operator = 'and';
        $post_types = get_post_types($args, $output, $operator);

        foreach ($post_types as $post_type) {
            $pendings_query = new WP_Query(array(
                'post_type' => $post_type->name,
                'post_status' => 'draft',
                'author' => $GLOBALS['current_user']->ID,
                'posts_per_page' => 10,
                'orderby' => 'modified', // sort by order created, regardless of date
                'order' => 'DESC'
            ));
            $pendings = & $pendings_query->posts;
            $num_posts = wp_count_posts($post_type->name);
            $text = _n($post_type->labels->singular_name, $post_type->labels->name, intval($num_posts->publish));

            if ($pendings && is_array($pendings)) {
                $list = array();
                echo "<a href='edit.php?post_status=draft&post_type=" . $post_type->name . "'>";
                echo "<h4>Drafts " . $text . " </h4></a><hr style='border:solid 1px #eee'>";
                foreach ($pendings as $pending) {
                    $url = get_edit_post_link($pending->ID);
                    $title = _draft_or_post_title($pending->ID);
                    $item = "<h4><a href='$url' title='" . sprintf(__('Edit "%s"'), attribute_escape($title)) . "'>$title</a>";
                    //if ( $pending->au
                    $item .= "<abbr title='" . get_the_time(__('Y/m/d g:i:s A'), $pending) . "'> " . get_the_time(get_option('date_format'), $pending) . '</abbr></h4>';
                    if ($the_content = preg_split('#\s#', strip_tags($pending->post_content), 11, PREG_SPLIT_NO_EMPTY))
                        $item .= '<p>' . join(' ', array_slice($the_content, 0, 10)) . ( 10 < count($the_content) ? '&hellip;' : '' ) . '</p>';
                    $list[] = $item;
                }
                ?>
                <ol>
                    <li><?php echo join("</li>\n<li>", $list); ?></li>
                </ol>
                <p class="textright"><a href="edit.php?post_status=draft&post_type=<?php echo $post_type->name ?>" class="button"><?php _e('View all'); ?></a></p>

                <?php
            }
        }
    }

    /**
     * Create the function use in the action hook
     *
     */
    function addPendingDashboardWidgets() {
        if (current_user_can('publish_posts')) {
            add_meta_box('pending_dashboard_widget', 'Pending Contents For Review', array(&$this, 'showPendingDashboardWidget'), 'dashboard', 'side', 'high');
        }
        add_meta_box('custom_recent_drafts', 'Recent Drafts', array(&$this, 'showDraftsDashboardWidget'), 'dashboard', 'side', 'high');
    }

    /**
     * Create the function for dafault screen layout across admin
     *
     */
    function defaultScreenLayoutColumns($columns) {
        $columns['dashboard'] = 3;
        return $columns;
    }

    /**
     * Create the function for default screen layout for dashbodard etc
     *
     */
    function defaultScreenLayoutDashboard() {
        return 3;
    }

    /**
     * Create the function for remove theme name from right now dashboard widget
     *
     */
    function removeThemeRightNow($text) {

        if ('Theme' == substr($text, 0, 5))
            return '';

        return $text;
    }

    /**
     * Create the function for remove default meta tag of wordpress from the head of theme
     *
     */
    function removeDaefaultHeadMetaTags() {
        remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
        remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
        remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
        remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
        remove_action('wp_head', 'index_rel_link'); // index link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // prev link
        remove_action('wp_head', 'start_post_rel_link', 10, 0); // start link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
        remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
    }

    /*
     * This function remove the wp pointers from the wp-admin
     */

    function removePointers() {
        remove_action('admin_enqueue_scripts', array('WP_Internal_Pointers', 'enqueue_scripts'));
    }

    /*
     * This function redirect the contact us page to secure url
     */

    function contactUsRedirect() {
        if (is_page('contact-us') && !is_ssl()) {
            if (0 === strpos($_SERVER['REQUEST_URI'], 'http')) {
                wp_redirect(preg_replace('|^http://|', 'https://', $_SERVER['REQUEST_URI']), 301);
                exit();
            } else {
                wp_redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301);
                exit();
            }
        } else if (!is_page('contact-us') && is_ssl() && !is_admin()) {
            if (0 === strpos($_SERVER['REQUEST_URI'], 'http')) {
                wp_redirect(preg_replace('|^https://|', 'http://', $_SERVER['REQUEST_URI']), 301);
                exit();
            } else {
                wp_redirect('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301);
                exit();
            }
        }
    }

    /*
     * This function solve the permalink issue for contact us page to redirect to secure url
     */

    function contactUsRedirectPermalink($permalink, $post, $leavename) {
        if (is_page('contact-us'))
            return preg_replace('|^http://|', 'https://', $permalink);
        return $permalink;
    }

    /*
     * This function show the welcome panel on login
     */

    function showWelcomeMessagePanel() {
        $user_id = get_current_user_id();
        update_user_meta($user_id, 'show_welcome_panel', 1);
    }

    //R2 Functions
    /*
     * This function remoove the email id of user from the database
     */

    function removeEmailDb() {
        $current_user = wp_get_current_user();
        $args = array(
            'ID' => $current_user->id,
            'user_email' => ''
        );

        wp_update_user($args);
    }

    /*
     * This function remove users image(gravatar) from admin
     */

    function removeGravatar($id_or_email, $size = '96', $default = '', $alt = false) {
        $avatar = "";
        return $avatar;
    }

    /*
     * These two function adds a new column of featured image on all custom post types
     */

    function addImageCustomColumns($defaults) {
        $defaults['mz_thumbs'] = __('Featured Image');
        return $defaults;
    }

    function showImageCustomColumns($column_name, $id) {
        if ($column_name === 'mz_thumbs') {
            if (has_post_thumbnail())
                echo the_post_thumbnail(array(100, 100));

            else {
                // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
                echo '<img src="' . get_template_directory_uri() . '/img/default/noimage.jpg" />';
            }
        }
    }

    /*
     * This function return the size for carausal mobile images size
     */

    function carausalSize($sizes) {
        $type = get_post_type($_REQUEST['post_id']);

        foreach ($sizes as $key => $value) {
            if ($type == CAROUSEL_SLIDE_CUSTOM_POST && $value != CARAUSAL_MOBILE_IMAGE) {
                unset($sizes[$key]);
            }
            if ($type != CAROUSEL_SLIDE_CUSTOM_POST && $value == CARAUSAL_MOBILE_IMAGE) {
                unset($sizes[$key]);
            }
        }

        return $sizes;
    }

    /*
     * This function show the name of image size in human readable
     */

    function carausalSizesName($sizes) {
        return array_merge($sizes, array(
            'carausal-mobile' => __('Carausal Small'),
        ));
    }

    /*
     * This function show replaces http:// to https:// on ssl
     */

    function fix_ssl_attachment_url($url) {

        if (is_ssl() && (!is_admin())) //previous change was causing secured(https) image urls in admin
            $url = str_replace('http://', 'https://', $url);
        return $url;
    }

    function check_login_action($logged_in_cookie = false, $expire = ' ', $expiration = 0, $user_id = 0, $action = 'logged_out') {
        $config = w3_instance('W3_Config');
        
        global $current_user;
        if (isset($current_user->ID) && !$current_user->ID){
            $user_id = new WP_User($user_id);
        } else{
            $user_id = $current_user;
        }
        if (is_string($user_id->roles)) {
            $role = $user_id->roles;
        } elseif (!is_array($user_id->roles)) {
            return;
        } else {
            $role = array_shift( $user_id->roles );
        }

        $role_hash = md5(NONCE_KEY . $role);       
        if (in_array($role, $config->get_array('pgcache.reject.roles'))) {
            setcookie('w3tc_logged_' . $role_hash, true, $expire, COOKIEPATH, COOKIE_DOMAIN, false, true);
        }
    }
}

new AdminFixes();
?>