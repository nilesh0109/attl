<?php
/**
 * Purpose : Site settings for.
 *
 */
//require_once 'helpers/FieldValidator.php';

/**
 * Defines site level options.
 */
class MzOption {

    public $arMzOptions;

    /**
     * Initializes the settings fields for site options.
     */
    function __construct() {
        if (is_admin() && (isset($_GET['page']) && $_GET['page'] == 'update_mz_theme')) {
            wp_enqueue_media();
            wp_enqueue_script(
                    'script-upload', content_url() . '/' . LIB_NAME . '/js/admin/script.js', array('jquery', 'media-upload'), time(), true
            );
        }

        $this->arMzOptions = get_option(MZ_SITE_OPTIONS);
        // If site options doesn't exist, then set to empty array.
        // This case arises for any site which is fresh set-up.
        if ($this->arMzOptions === FALSE) {
            $this->arMzOptions = array();
            add_option(MZ_SITE_OPTIONS);
        }

        $this->registerSettingsAndFields();
    }

    /**
     * Adding a Menu Page
     *
     * @return void
     */
    public function addMenuPage() {
        $site_options_page = add_theme_page('MZ Site Level Options', __('Site Level Options'), THEME_OPTION_CAPABILITY, MZ_EDIT_OPTION_SLUG, array($this, 'displayOptionsPage'));

        // Include page-specific JS.
    }

    /**
     * Displays form fields.
     *
     * @return void
     */
    public function displayOptionsPage() {
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2>
                <?php echo __('Site Level Options'); ?>
            </h2>
            <form action="options.php" method="post" enctype="multipart/form-data">
                <?php settings_errors(); ?>
                <?php settings_fields(MZ_SITE_OPTIONS); ?>
                <?php do_settings_sections(MZ_EDIT_OPTION_SLUG) ?>
                <p class="submit">
                    <input type="submit" name="submit" class="button-primary"
                           value="<?php _e('Save Changes'); ?>" id="btnSaveTypekit"  />
                </p>
            </form>
        </div>

        <?php
    }

    /**
     * Registers fields and sections on the page.
     *
     * @return void
     */
    public function registerSettingsAndFields() {
        $page = MZ_EDIT_OPTION_SLUG;

        register_setting(MZ_SITE_OPTIONS, MZ_SITE_OPTIONS, array($this, 'mzValidateSetting'));

        // ALL Sections
        add_settings_section('mz_header_section', __('Header Section'), array($this, 'MzMainSectionCb'), $page);
        add_settings_section('mz_layout_section', __('Site Layout Section'), array($this, 'MzMainSectionCb'), $page);

        //Header Settings
        add_settings_field(MZ_HEADER_LOGO, __('Header Logo'), array($this, 'mzHeaderLogoSetting'), MZ_EDIT_OPTION_SLUG, 'mz_header_section');
        add_settings_field(MZ_HEADER_FAVICON, __('Favicon Image'), array($this, 'mzHeaderFaviconSetting'), MZ_EDIT_OPTION_SLUG, 'mz_header_section');


        // Fields for Site Layout settings
        add_settings_field(LAYOUT_SETTINGS, __('Site Layout'), array($this, 'mzLayoutSettings'), $page, 'mz_layout_section');

    }

    /**
     * Generates fields for BIN Base URL setting.
     *
     * @return void
     */
    public function mzBinBaseUrlSetting() {
        $bin_base_url = $this->arMzOptions[MZ_BIN_BASE_URL];

        if ($this->arMzOptions[MZ_BIN_TYPE] == BIN_TYPE_UPC_BASED) {
            $token_index = strpos($bin_base_url, '{0}');

            if ($token_index !== false && $token_index === strlen($bin_base_url) - 3) {
                $bin_base_url = str_replace('{0}', '', $this->arMzOptions[MZ_BIN_BASE_URL]);
            }
        }

        echo ('<input name="' . MZ_SITE_OPTIONS . '[' . MZ_BIN_BASE_URL . "]\" type=\"text\" value=\"{$bin_base_url}\" />");
    }

    /**
     * Generates fields for BIN Width setting.
     *
     * @return void
     */
    public function mzBinWidthSetting() {
        $bin_width = $this->arMzOptions[MZ_BIN_WIDTH];
        echo ('<input name="' . MZ_SITE_OPTIONS . '[' . MZ_BIN_WIDTH . "]\" type=\"text\" value=\"{$bin_width}\" /> px");
    }
    
        /**
     * Generates fields for BIN Button text setting.
     *
     * @return void
     */
    public function mzBinButtonText() {
        $bin_button_text = $this->arMzOptions[MZ_BIN_BUTTON_TEXT];
        echo ('<input name="' . MZ_SITE_OPTIONS . '[' . MZ_BIN_BUTTON_TEXT . "]\" type=\"text\" value=\"{$bin_button_text}\" />");
    }

    /**
     * Generates fields for BIN Height setting.
     *
     * @return void
     */
    public function mzBinHeightSetting() {
        $bin_height = $this->arMzOptions[MZ_BIN_HEIGHT];
        echo ('<input name="' . MZ_SITE_OPTIONS . '[' . MZ_BIN_HEIGHT . "]\" type=\"text\" value=\"{$bin_height}\" /> px");
    }
    
    /**
     * Generates fields for related teasers ordering settings
     * 
     * @return null
     */
    public function mzRelatedProductOrderingSettings() {
        
        $arProductOrderingtypes = array(ORDER_BY_NAME,ORDER_BY_LEFT_NAV);
        echo ('<select name="' . MZ_SITE_OPTIONS . '[' . RELATED_PRODUCT_ORDERING_SETTINGS . ']' . '" required="required">');

        foreach ($arProductOrderingtypes as $arProductOrderingtype) {
            $selected_attr = '';
            if (isset($this->arMzOptions[RELATED_PRODUCT_ORDERING_SETTINGS]) && $this->arMzOptions[RELATED_PRODUCT_ORDERING_SETTINGS] == $arProductOrderingtype) {
                $selected_attr = 'selected="selected"';
            }

            echo ("<option value=\"$arProductOrderingtype\" $selected_attr>$arProductOrderingtype</option>");
        }

        echo ('</select>');
    }
    
    /**
     * Generates fields for related teasers ordering settings
     * 
     * @return null
     */
    public function mzRelatedArticleOrderingSettings() {
  
        $arArticleOrderingtypes = array(ORDER_BY_NAME,ORDER_BY_LEFT_NAV,ORDER_BY_DATE_ASC,ORDER_BY_DATE_DSC);
        echo ('<select name="' . MZ_SITE_OPTIONS . '[' . RELATED_ARTICLE_ORDERING_SETTINGS . ']' . '" required="required">');

        foreach ($arArticleOrderingtypes as $arArticleOrderingtype) {
            $selected_attr = '';
            if (isset($this->arMzOptions[RELATED_ARTICLE_ORDERING_SETTINGS]) && $this->arMzOptions[RELATED_ARTICLE_ORDERING_SETTINGS] == $arArticleOrderingtype) {
                $selected_attr = 'selected="selected"';
            }

            echo ("<option value=\"$arArticleOrderingtype\" $selected_attr>$arArticleOrderingtype</option>");
        }

        echo ('</select>');
    }
    
    /**
     * Validates settings
     *
     * @param array $arSiteOptions Array of field values submitted.
     *
     * @return array Array of valid fields. If field is invalid then the
     * previous value from DB is returned for that specific field.
     */
    public function mzValidateSetting($arSiteOptions) {

        return $arSiteOptions;
    }



    /**
     * Renders fields for header logo.
     *
     * @return void
     */
    public function mzHeaderLogoSetting() {
        if (!empty($this->arMzOptions[MZ_HEADER_LOGO])) {
            $image_header = wp_get_attachment_image_src(($this->arMzOptions[MZ_HEADER_LOGO]), 'full');
            $image_header = $image_header[0];
        }
        ?>
        <div class="widget_image" style="clear:both">
            <input id="<?php echo MZ_SITE_OPTIONS . "[" . MZ_HEADER_LOGO . "]"; ?>" name="<?php echo MZ_SITE_OPTIONS . "[" . MZ_HEADER_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions[MZ_HEADER_LOGO]) ?>" />
            <img src="<?php echo esc_attr($image_header) ?>" class="widget_preview_image" alt="" /><br>
            <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions[MZ_HEADER_LOGO] ?>">Choose Image</a>
            <?php
            if (!empty($this->arMzOptions[MZ_HEADER_LOGO])) {
                ?>
                <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
            <?php } ?>
        </div>
        <?php
    }

    /**
     * Renders fields for site fav-icon.
     *
     * @return void
     */
    public function mzHeaderFaviconSetting() {

        if (!empty($this->arMzOptions[MZ_HEADER_FAVICON])) {
            $header_favicon = wp_get_attachment_image_src(($this->arMzOptions[MZ_HEADER_FAVICON]), 'full');
            $header_favicon = $header_favicon[0];
        }
        ?>
        <div class="widget_image" style="clear:both">
            <input id="<?php echo MZ_SITE_OPTIONS . "[" . MZ_HEADER_FAVICON . "]"; ?>" name="<?php echo MZ_SITE_OPTIONS . "[" . MZ_HEADER_FAVICON . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions[MZ_HEADER_FAVICON]) ?>" />
            <img src="<?php echo esc_attr($header_favicon) ?>" class="widget_preview_image" alt="" /><br>
            <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions[MZ_HEADER_FAVICON] ?>">Choose Image</a>
            <?php
            if (!empty($this->arMzOptions[MZ_HEADER_FAVICON])) {
                ?>
                <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
            <?php } ?>
        </div>
        <?php
    }



    /**
     * Callback method
     *
     * @return void
     */
    public function MzMainSectionCb() {

    }

    /**
     * Renders field for Site Layout Settings.
     *
     * @return void
     */
    public function mzLayoutSettings() {
        $arLayout = array(LAYOUT_FULL, LAYOUT_FIXED);
        echo ('<select name="' . MZ_SITE_OPTIONS . '[' . LAYOUT_SETTINGS . ']' . '" id="layout-settings">');

        foreach ($arLayout as $layout_value) {
            $selected_attr = '';
            if ($this->arMzOptions[LAYOUT_SETTINGS] == $layout_value) {
                $selected_attr = 'selected="selected"';
            }

            echo ("<option value=\"$layout_value\" $selected_attr>$layout_value</option>");
        }

        echo ('</select>');
    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            $ob = new MzOption();
            $ob->addMenuPage();
        });
?>
