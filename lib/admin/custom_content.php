<?php
/**
 * Purpose : Register Custom Content Type/ Custom Taxonomy.
 *
 */
require_once WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/custom_content_ui_handler.php';
require_once WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/custom_content_data_handler.php';

/**
 * Register Custom Content Type/ Custom Taxonomy for Mizkan.
 *
 */
class Custom_Content {

    function __construct() {

        $obContentUI = $this->getCustomContentUIHandler();
        $obContentData = $this->getCustomContentDataHandler();

        add_action('init', array($this, 'registerCustomContent'));
        add_action('admin_menu', array($this, 'changeDefaultPostMenuLabels'));
        add_filter('post_updated_messages', array($this, 'updateCustomMessage'));
        add_action('admin_head', array($this, 'changeCustomPostIcons'));

        // All Operation Related to handling UI in Admin Section
        add_action('admin_init', array($obContentUI, 'manageMetaBoxes'));
        add_action('admin_print_styles', array($obContentUI, 'addFindPostStyle'));
        add_action('admin_print_scripts', array($obContentUI, 'addFindPostScripts'));

        //add_action('edit_category_form',array( $obContentUI,'editTaxonomyForm'));

        add_action(ARTICLE_TAXONOMY . '_edit_form', array($obContentUI, 'editTaxonomyForm'));
        add_action(CAROUSEL_TAXONOMY . '_edit_form', array($obContentUI, 'addAssociateSlideBox'));
        add_action(CAROUSEL_TAXONOMY . '_edit_form', array($obContentUI, 'addCarouselSettings')); //adding carousel settings for bullets
        add_action(TEASER_TAXONOMY . '_edit_form', array($obContentUI, 'addAssociateSlideBox'));
        add_action('wp_ajax_find-custom-posts', array($obContentUI, 'custom_find_posts'));
        add_filter('manage_posts_columns', array($obContentUI, 'postColumnHead'));
        add_action('manage_posts_custom_column', array($obContentUI, 'postColumnContent'), 10, 2);
        add_action('edit_form_after_title', array($obContentUI, 'addFindPostBox'));

        // All Operation Related to handling Data in Admin Section
        add_action('save_post', array($obContentData, 'saveMetaData'));
        add_filter('edited_terms', array($obContentData, 'saveTaxonomyData'));
        add_filter('wp_insert_post_data', array($obContentData, 'createBuildingBlockForPostType'), 10, 2);
        add_filter('delete_post', array($obContentData, 'deleteBuildingBlockTaxonomy'));
        add_filter('created_term', array($obContentData, 'createTermAssociatedBuildingBlocks'), 10, 3);
        add_action('delete_term', array($obContentData, 'removeTermAssociatedBuildingBlocks'), 10, 4);
    }

    function getCustomContentDataHandler() {
        return new Custom_Content_Data_Handler();
    }

    function getCustomContentUIHandler() {
        return new Custom_Content_UI_Handler();
    }

    /**
     * Register all the Custom Taxonomy and Custom Post Type in the System
     */
    function registerCustomContent() {

        $this->registerCarouselTaxonomy(); // Register Carousel
        $this->registerCarouselSlides(); // Register Carousel Slides
        $this->registerTeaserTaxonomy(); // Register Teaser Carousel
        $this->registerTeasers(); // Register Teaser
        $this->registerSpotlight(); // Register Spotlight
        $this->changeDefaultCategoryLabels(); // Change Default Category Labels to Article Category
        $this->changeDefaultPostLabels(); // Change Default Post Labels to Article
    }

    function changeCustomPostIcons() {
        ?>
        <style type="text/css" media="screen">
            #menu-posts-<?php echo SPOTLIGHT_CUSTOM_POST; ?> .wp-menu-image {
                background: url(<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/admin_menu.png) no-repeat -49px -35px !important;
            }
            #menu-posts-<?php echo SPOTLIGHT_CUSTOM_POST; ?>:hover .wp-menu-image, #menu-posts-<?php echo SPOTLIGHT_CUSTOM_POST; ?>.wp-has-current-submenu .wp-menu-image {
                background-position:-49px -2px !important;
            }
            #menu-posts-<?php echo TEASER_CUSTOM_POST; ?> .wp-menu-image {
                background: url(<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/admin_menu.png) no-repeat -125px -34px !important;
            }
            #menu-posts-<?php echo TEASER_CUSTOM_POST; ?>:hover .wp-menu-image, #menu-posts-<?php echo TEASER_CUSTOM_POST; ?>.wp-has-current-submenu .wp-menu-image {
                background-position:-125px -2px !important;
            }
            #menu-posts-<?php echo CAROUSEL_SLIDE_CUSTOM_POST; ?> .wp-menu-image {
                background: url(<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/admin_menu.png) no-repeat -23px -34px !important;
            }
            #menu-posts-<?php echo CAROUSEL_SLIDE_CUSTOM_POST; ?>:hover .wp-menu-image, #menu-posts-<?php echo CAROUSEL_SLIDE_CUSTOM_POST; ?>.wp-has-current-submenu .wp-menu-image {
                background-position:-24px -2px !important;
            }
            .icon32.icon32-posts-<?php echo CAROUSEL_SLIDE_CUSTOM_POST; ?>{
                background:url('<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/screen_icon.png') no-repeat -57px -80px !important;
            }
            .icon32.icon32-posts-<?php echo TEASER_CUSTOM_POST; ?>{
                background:url('<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/screen_icon.png') no-repeat -252px -80px !important;
            }
            .icon32.icon32-posts-<?php echo SPOTLIGHT_CUSTOM_POST; ?>{
                background:url('<?php echo content_url() . '/' . LIB_NAME; ?>/img/admin/screen_icon.png') no-repeat -104px -80px !important;
            }

        </style>
        <?php
    }

    /**
     * Convert All Labels of default category Article Categories in the System
     * This action was required because we don't want to introduce another taxonomy for article category.
     */
    function changeDefaultCategoryLabels() {
        global $wp_taxonomies;
        $obLabels = $wp_taxonomies['category']->labels;
        $obLabels->name = __('Article Category');
        $obLabels->singular_name = __('Article Category');
        $obLabels->add_new_item = __('Add Article Category');
        $obLabels->edit_item = __('Edit Article Category');
        $obLabels->new_item_name = __('New Article Category Name');
        $obLabels->view_item = __('View Article Category');
        $obLabels->update_item = __('Update Article Category');
        $obLabels->search_items = __('Search Article Category');
        $obLabels->all_items = __('All Article Category');
        $obLabels->menu_name = __('Article Category');
        $obLabels->name_admin_bar = __('Article Category');
    }

    /**
     * Convert All labels of default post type from Posts to Articles in the System
     * This action was required because we don't want to introduce another post type articles.
     * Rather we would like to use existing post type for articles
     */
    function changeDefaultPostLabels() {
        add_theme_support('menus');

        global $wp_post_types;
        $obLabels = $wp_post_types['post']->labels;
        $obLabels->name = __('Article');
        $obLabels->singular_name = __('Article');
        $obLabels->add_new = __('Add New Article');
        $obLabels->add_new_item = __('Add New Article');
        $obLabels->edit_item = __('Edit Article');
        $obLabels->new_item = __('Article');
        $obLabels->view_item = __('View Article');
        $obLabels->search_items = __('Search Articles');
        $obLabels->all_items = __('All Articles');
        $obLabels->menu_name = __('Articles');
        $obLabels->name_admin_bar = __('Article');
        $obLabels->not_found = __('No Articles found');
        $obLabels->not_found_in_trash = __('No Articles found in Trash');
    }

    /**
     * change Default Labels for Post to Articles.
     */
    function changeDefaultPostMenuLabels() {
        global $menu;
        global $submenu;

        if (user_can(get_current_user_id(), 'edit_posts')) {
            $menu[5][0] = __('Articles');
            $submenu['edit.php'][5][0] = __('All Articles');
            $submenu['edit.php'][10][0] = __('Add New Article');
        }
    }

    /**
     * Define Custom messages for all the Custom Post Type
     */
    function updateCustomMessage($messages) {
        global $post, $post_ID;

        $messages[ARTICLE_POST_TYPE] = array(
            0 => '', // Unused. Messages start at index 1.
            1 => sprintf(__('Article updated. <a href="%s">View Article</a>'), esc_url(get_permalink($post_ID))),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Article updated.'),
            5 => isset($_GET['revision']) ? sprintf(__('Article restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
            6 => sprintf(__('Article published. <a href="%s">View article</a>'), esc_url(get_permalink($post_ID))),
            7 => __('Article saved.'),
            8 => sprintf(__('Article submitted. <a target="_blank" href="%s">Preview Article</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
            9 => sprintf(__('Article scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Article</a>'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
            10 => sprintf(__('Article draft updated. <a target="_blank" href="%s">Preview article</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
        );

        $messages[SPOTLIGHT_CUSTOM_POST] = array(
            0 => '', // Unused. Messages start at index 1.
            1 => sprintf(__('Spotlight updated.')),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Spotlight updated.'),
            5 => isset($_GET['revision']) ? sprintf(__('Spotlight restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
            6 => sprintf(__('Spotlight published.')),
            7 => __('Spotlight saved.'),
            8 => sprintf(__('Spotlight submitted.')),
            9 => sprintf(__('Spotlight scheduled for: <strong>%1$s</strong>.'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date))),
            10 => sprintf(__('Spotlight draft updated.')),
            102 => __('Call To Action Association cannot be done. You have to specify a Target'),
        );

        $messages[TEASER_CUSTOM_POST] = array(
            0 => '', // Unused. Messages start at index 1.
            1 => sprintf(__('Teaser updated.')),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Teaser updated.'),
            5 => isset($_GET['revision']) ? sprintf(__('Teaser restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
            6 => sprintf(__('Teaser published.')),
            7 => __('Teaser saved.'),
            8 => sprintf(__('Teaser submitted.')),
            9 => sprintf(__('Teaser scheduled for: <strong>%1$s</strong>.'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date))),
            10 => sprintf(__('Teaser draft updated.')),
            102 => __('Call To Action Association cannot be done. You have to specify a Target'),
        );
        $messages[CAROUSEL_SLIDE_CUSTOM_POST] = array(
            0 => '', // Unused. Messages start at index 1.
            1 => sprintf(__('Slide updated.')),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Slide updated.'),
            5 => isset($_GET['revision']) ? sprintf(__('Slide restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
            6 => sprintf(__('Slide published.')),
            7 => __('Slide saved.'),
            8 => sprintf(__('Slide submitted.')),
            9 => sprintf(__('Slide scheduled for: <strong>%1$s</strong>.'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date))),
            10 => sprintf(__('Slide draft updated.')),
            102 => __('Call To Action Association cannot be done. You have to specify a Target'),
        );
        return $messages;
    }

    /**
     * Register Home Page Spotlight
     */
    function registerSpotlight() {
        $arLabels = array(
            'name' => _x('Spotlight', 'Post type general name'),
            'singular_name' => _x('Spotlight', 'Post type singular name'),
            'add_new' => _x('Add New Spotlight', 'Products item'),
            'all_items' => __('All Spotlights'),
            'add_new_item' => __('Add New Spotlight'),
            'edit_item' => __('Edit Spotlight'),
            'new_item' => __('New Spotlight'),
            'view_item' => __('View Spotlight'),
            'search_items' => __('Search Spotlight'),
            'not_found' => __('No Spotlight found'),
            'not_found_in_trash' => __('No Spotlight found in Trash'),
            'parent_item_colon' => ''
        );
        $arArguments = array(
            'labels' => $arLabels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position' => 5,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
            'rewrite' => true
        );
        register_post_type(SPOTLIGHT_CUSTOM_POST, $arArguments);
    }

    /**
     * Register Custom taxonomy Teaser Carousel
     */
    function registerTeaserTaxonomy() {
        $arLabels = array(
            'name' => _x('Teaser Carousel', 'taxonomy general name'),
            'singular_name' => _x('Teaser Carousel', 'taxonomy singular name'),
            'search_items' => __('Search Teaser Carousel'),
            'all_items' => __('All Teaser Carousel'),
            'edit_item' => __('Edit Teaser Carousel'),
            'update_item' => __('Update Teaser Carousel'),
            'add_new_item' => __('Add New Teaser Carousel'),
            'new_item_name' => __('New Teaser Carousel Name'),
            'menu_name' => __('Teaser Carousel'),
        );
        $capabilities = array(
            'manage_terms' => 'manage_categories',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_options',
            'assign_terms' => 'edit_posts',
        );

        $arArguments = array('hierarchical' => true, 'show_in_nav_menus' => false, 'show_ui' => true, 'show_admin_column' => false, 'labels' => $arLabels, 'capabilities' => $capabilities);
        register_taxonomy(TEASER_TAXONOMY, TEASER_CUSTOM_POST, $arArguments);
    }

    /**
     * Register Custom Post Teasers
     */
    function registerTeasers() {
        $arLabels = array(
            'name' => _x('Teaser', 'Post type general name'),
            'singular_name' => _x('Teaser', 'Post type singular name'),
            'add_new' => _x('Add New Teaser', 'Teasers item'),
            'all_items' => __('All Teasers'),
            'add_new_item' => __('Add New Teaser'),
            'edit_item' => __('Edit Teasers'),
            'new_item' => __('New Teaser'),
            'view_item' => __('View Teaser'),
            'search_items' => __('Search Teasers'),
            'not_found' => __('No Teaser found'),
            'not_found_in_trash' => __('No Teaser found in Trash'),
            'parent_item_colon' => ''
        );
        $arArguments = array(
            'labels' => $arLabels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position' => 5,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
            'rewrite' => true
        );
        register_post_type(TEASER_CUSTOM_POST, $arArguments);
    }

    /**
     * Register Custom taxonomy Carousel
     */
    function registerCarouselTaxonomy() {
        $arLabels = array(
            'name' => _x('Carousel', 'taxonomy general name'),
            'singular_name' => _x('Carousel', 'taxonomy singular name'),
            'search_items' => __('Search Carousel'),
            'all_items' => __('All Carousel'),
            'edit_item' => __('Edit Carousel'),
            'update_item' => __('Update Carousel'),
            'add_new_item' => __('Add New Carousel'),
            'new_item_name' => __('New Carousel Name'),
            'menu_name' => __('Carousel'),
        );
        $capabilities = array(
            'manage_terms' => 'manage_categories',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_options',
            'assign_terms' => 'edit_posts',
        );

        $arArguments = array('hierarchical' => true, 'show_in_nav_menus' => false, 'show_ui' => true, 'show_admin_column' => true, 'labels' => $arLabels, 'capabilities' => $capabilities);
        register_taxonomy(CAROUSEL_TAXONOMY, CAROUSEL_SLIDE_CUSTOM_POST, $arArguments);
    }

    /**
     * Register Custom Post Teasers
     */
    function registerCarouselSlides() {
        $arLabels = array(
            'name' => _x('Carousel Slide', 'Post type general name'),
            'singular_name' => _x('Carousel Slide', 'Post type singular name'),
            'add_new' => _x('Add New Carousel Slide', 'Teasers item'),
            'all_items' => __('All Carousel Slides'),
            'add_new_item' => __('Add New Carousel Slide'),
            'edit_item' => __('Edit Carousel Slide'),
            'new_item' => __('New Carousel Slide'),
            'view_item' => __('View Carousel Slide'),
            'search_items' => __('Search Carousel Slides'),
            'not_found' => __('No Carousel Slide found'),
            'not_found_in_trash' => __('No Carousel Slide found in Trash'),
            'parent_item_colon' => ''
        );
        $arArguments = array(
            'labels' => $arLabels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position' => 5,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
            'rewrite' => true
        );
        register_post_type(CAROUSEL_SLIDE_CUSTOM_POST, $arArguments);
    }

}

$obContent = new Custom_Content();
?>
