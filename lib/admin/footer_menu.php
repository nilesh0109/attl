<?php

$file_path = get_stylesheet_directory() . "/extlib/footer_menu.xml"; // Check if XML file exists in child theme
if (file_exists($file_path)) {

    $xml = simplexml_load_file($file_path);
} else {
    return false;
}
foreach ($xml->xpath('/FooterMenu/menu') as $arItem) {

    if (empty($arFooterOption) || !get_post($arFooterOption['Cookie_policy'])) {
        $id = wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' => $arItem[name],
            'menu-item-type' => 'custom',
            'menu-item-classes' => '',
            'menu-item-url' => $arItem->url,
            'menu-item-status' => 'publish',
            'menu-item-target' => '_blank'
        ));
        $arFooterOption['Cookie_policy'] = $id;
    }
}
?>