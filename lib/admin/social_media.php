<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Paritosh Gautam
 * Purpose : Site settings for Mizkan.
 *
 */

class MzSocialMedia {

    function __construct() {
        if (is_admin() && (isset($_GET['page']) && $_GET['page'] == 'social_icons')) {
            wp_enqueue_media();
            wp_enqueue_script(
                    'script-upload', content_url() . '/' . LIB_NAME . '/js/admin/script.js', array('jquery', 'media-upload'), time(), true
            );
        }

        $this->arMzOptions1 = get_option(MZ_SOCIAL_ICONS_OPTIONS);
        $this->registerSettingsAndFields();
        if (isset($_GET['page']) && $_GET['page'] == MZ_SOCIAL_ICONS_OPTION_SLUG) {
            add_action('admin_head', array($this, 'addJavascript'));
        }
    }

    /*
     * Adding a Menu Page
     */

    public function addMenuPage() {
        add_theme_page(__("MZ Social Icons Settings"), __("Social Icons Settings"), THEME_OPTION_CAPABILITY, MZ_SOCIAL_ICONS_OPTION_SLUG, array($this, 'displayOptionsPage'));
    }

    public function displayOptionsPage() {
        ?>
        <div class="wrap">
            <?php screen_icon() ?>
            <h2>
                <?php _e('Social Icons Settings'); ?>
            </h2>
            <?php settings_errors(); ?>
            <form action="options.php" method="post" enctype="multipart/form-data" class="social-settings">
                <?php settings_fields(MZ_SOCIAL_ICONS_OPTIONS); ?>
                <?php
                if (!isset($this->arMzOptions1[MZ_FOLLOW_SHARE_SELECT]) || ( $this->arMzOptions1[MZ_FOLLOW_SHARE_SELECT] == 'share')) {
                    $second_checked = "";
                    $first_checked = " checked='checked ' ";
                } else {
                    $first_checked = "";
                    $second_checked = "checked=' checked '";
                }
                ?>
                <div class="follow-share">
                    <?php
                    echo ("<label>" . __("Share") . "</label><input id='share' rel='share' class='follow-share' name='" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FOLLOW_SHARE_SELECT . "]' type='radio'" . $first_checked . " value='share' />");
                    //echo ("<label>" . __("Follow") . "</label><input id='follow' rel='follow' class='follow-share' name='" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FOLLOW_SHARE_SELECT . "]' type='radio'" . $second_checked . " value='follow' />");
                    ?>
                </div>
                
                <div class="signupforemails-link">
                    <?php
                    echo ("<label>" . __("Sign Up for Emails") . "</label><input id='signup_for_emails' rel='signup-for-emails' class='signup-for-emails' name='" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SIGN_UP_FOR_EMAILS . "]' type='text' value=" . $this->arMzOptions1[MZ_SIGN_UP_FOR_EMAILS] . ">");
                    ?>
                </div>
                
                <!-- Facebook -->
                <div class="fb block">
                    <?php echo ("<h3>" . __('Facebook Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Facebook ID") . "</label> <input id='fb_url'  name='" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FACEBOOK_URL . "]' type='text' value='{$this->arMzOptions1[MZ_FACEBOOK_URL]}' /></div>");
                    echo ("<div class='share'><label>Facebook AppID </label> <input id='fb_app_id' class='share' name='" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FACEBOOK_APP_ID . "]' type='text' value='{$this->arMzOptions1[MZ_FACEBOOK_APP_ID]}'/></div>");
                    echo ("<div class='follow'><label>" . __("Facebook Logo") . " </label>");

                    if (!empty($this->arMzOptions1[MZ_FACEBOOK_LOGO])) {
                        $image_fb = wp_get_attachment_image_src(($this->arMzOptions1[MZ_FACEBOOK_LOGO]), 'full');
                        $image_fb = $image_fb[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FACEBOOK_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_FACEBOOK_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_FACEBOOK_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_fb); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_FACEBOOK_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_FACEBOOK_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";


                    echo ("<div class = 'share'><label>" . __("Show Facebook Like") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_FACEBOOK_LIKE]) && strlen($this->arMzOptions1[MZ_SHOW_FACEBOOK_LIKE]) > 2) {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_LIKE . "]' checked = 'checked' value = 'fblogo' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_LIKE . "]' value = 'fblogo' /></div>");
                    }
                    echo ("<div class = 'follow'><label>" . __("Show Facebook Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_FACEBOOK_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_FACEBOOK_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_FOLLOW . "]' checked = 'checked' value = 'fbfollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_FOLLOW . "]' value = 'fbfollow' /></div>");
                    }
                    ?>
                </div>

                <!-- Twitter -->
                <div class="twitter block">
                    <?php echo ("<h3>" . __('Twitter Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Twitter ID") . "</label> <input id = 'twitter_app_id' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TWITTER_URL . "]' type = 'text' value = '{$this->arMzOptions1[MZ_TWITTER_URL]}'/></div>");
                    echo ("<div class = 'follow'><label>" . __("Twitter Logo") . "</label>");
                    if (!empty($this->arMzOptions1[MZ_TWITTER_LOGO])) {
                        $image_twitter = wp_get_attachment_image_src(($this->arMzOptions1[MZ_TWITTER_LOGO]), 'full');
                        $image_twitter = $image_twitter[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TWITTER_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TWITTER_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_TWITTER_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_twitter); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_TWITTER_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_TWITTER_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";


                    echo ("<div class = 'share'><label>" . __("Show Tweet") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TWEET]) && strlen($this->arMzOptions1[MZ_SHOW_TWEET]) > 2) {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWEET . "]' checked = 'checked' value = 'tweet' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWEET . "]' value = 'tweet' /></div>");
                    }
                    echo ("<div class = 'follow'><label>" . __("Show Twitter Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TWITTER_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_TWITTER_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWITTER_FOLLOW . "]' checked = 'checked' value = 'twitterfollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWITTER_FOLLOW . "]' value = 'twitterfollow' /></div>");
                    }
                    ?>
                </div>

                <!-- Pinterest -->
                <div class="pinit block">
                    <?php echo ("<h3>" . __('Pinterest Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Pinterest ID") . "</label> <input id = 'pinterest' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_PIN_URL . "]' type = 'text' value = '{$this->arMzOptions1[MZ_PIN_URL]}'/></div>");
                    echo ("<div class = 'follow'><label>" . __("Pinterest Logo") . "</label>");

                    if (!empty($this->arMzOptions1[MZ_PIN_LOGO])) {
                        $image_pinterest = wp_get_attachment_image_src(($this->arMzOptions1[MZ_PIN_LOGO]), 'full');
                        $image_pinterest = $image_pinterest[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_PIN_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_PIN_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_PIN_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_pinterest); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_PIN_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_PIN_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";

                    echo ("<div class = 'share'><label>" . __("Show Pinterest") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_PINTEREST]) && strlen($this->arMzOptions1[MZ_SHOW_PINTEREST]) > 2) {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINTEREST . "]' checked = 'checked' value = 'pinterest' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINTEREST . "]' value = 'pinterest' /></div>");
                    }

                    echo ("<div class = 'follow'><label>" . __("Show Pinterest Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_PINTEREST_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_PINTEREST_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINTEREST_FOLLOW . "]' checked = 'checked' value = 'pinterestfollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINTEREST_FOLLOW . "]' value = 'pinterestfollow' /></div>");
                    }
                    ?>
                </div>
                <!-- Tumblr -->
                <div class="tumblr block">
                    <?php echo ("<h3>" . __('Tumblr Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Tumblr ID") . "</label> <input id = 'tumblrid' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TUMBLR_URL . "]' type = 'text' value = '{$this->arMzOptions1[MZ_TUMBLR_URL]}'/></div>");
                    echo ("<div class = 'follow'><label>" . __("Tumblr Logo") . "</label>");
                    if (!empty($this->arMzOptions1[MZ_TUMBLR_LOGO])) {
                        $image_tumblr = wp_get_attachment_image_src(($this->arMzOptions1[MZ_TUMBLR_LOGO]), 'full');
                        $image_tumblr = $image_tumblr[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TUMBLR_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_TUMBLR_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_TUMBLR_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_tumblr); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_TUMBLR_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_TUMBLR_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";

                    echo ("<div class = 'share'><label>" . __('Show Tumblr') . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TUMBLR]) && strlen($this->arMzOptions1[MZ_SHOW_TUMBLR]) > 2) {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR . "]' checked = 'checked' value = 'tumblr' /></div>");
                    } else {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR . "]' value = 'tumblr' /></div>");
                    }
                    echo ("<div class = 'follow'><label>" . __("Show Tumblr Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TUMBLR_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_TUMBLR_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR_FOLLOW . "]' checked = 'checked' value = 'tumblrfollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR_FOLLOW . "]' value = 'tumblrfollow' /></div>");
                    }
                    ?>
                </div>

                <div class="google block">
                    <?php echo ("<h3>" . __('Google Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Google Plus ID") . "</label> <input id = 'googleid' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_GOOGLE_URL . "]' type = 'text' value = '{$this->arMzOptions1[MZ_GOOGLE_URL]}'/></div>");
                    echo ("<div class = 'follow'><label>" . __("Google Plus Logo") . "</label>");
                    if (!empty($this->arMzOptions1[MZ_GOOGLE_LOGO])) {
                        $image_google = wp_get_attachment_image_src(($this->arMzOptions1[MZ_GOOGLE_LOGO]), 'full');
                        $image_google = $image_google[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_GOOGLE_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_GOOGLE_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_GOOGLE_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_google); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_GOOGLE_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_GOOGLE_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";



                    echo ("<div class = 'share'><label>" . __('Show Google Plus') . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_GOOGLE]) && strlen($this->arMzOptions1[MZ_SHOW_GOOGLE]) > 2) {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE . "]' checked = 'checked' value = 'google' /></div>");
                    } else {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE . "]' value = 'google' /></div>");
                    }
                    echo ("<div class = 'follow'><label>" . __("Show Google Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_GOOGLE_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_GOOGLE_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE_FOLLOW . "]' checked = 'checked' value = 'googlefollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE_FOLLOW . "]' value = 'googlefollow' /></div>");
                    }
                    ?>
                </div>

                <div class="other block">
                    <?php echo ("<h3>" . __('Other Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Youtube ID") . "</label> <input id = 'youtube' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_YOUTUBE_URL . "]' type = 'text' value = '{$this->arMzOptions1[MZ_YOUTUBE_URL]}'/></div>");
                    echo ("<div><label>" . __("Youtube Logo") . "</label>");

                    if (!empty($this->arMzOptions1[MZ_YOUTUBE_LOGO])) {
                        $image_youtube = wp_get_attachment_image_src(($this->arMzOptions1[MZ_YOUTUBE_LOGO]), 'full');
                        $image_youtube = $image_youtube[0];
                    }
                    ?>
                    <div class="widget_image" style="clear:both">
                        <input id="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_YOUTUBE_LOGO . "]"; ?>" name="<?php echo MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_YOUTUBE_LOGO . "]"; ?>" type="hidden" class="widget_upload_image" value="<?php echo ($this->arMzOptions1[MZ_YOUTUBE_LOGO]) ?>" />
                        <img src="<?php echo esc_attr($image_youtube); ?>" class="widget_preview_image" alt="" /><br>
                        <a href="#" class="widget_upload_image_button button" rel="<?php echo $this->arMzOptions1[MZ_YOUTUBE_LOGO] ?>">Choose Image</a>
                        <?php
                        if (!empty($this->arMzOptions1[MZ_YOUTUBE_LOGO])) {
                            ?>
                            <small>&nbsp;<a href="#" class="widget_clear_image_button button">Remove Image</a></small>
                        <?php } ?>
                    </div>
                    <?php
                    echo "</div>";


                    echo ("<div><label>" . __("Show Youtube Follow") . " </label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_YOUTUBE_FOLLOW]) && strlen($this->arMzOptions1[MZ_SHOW_YOUTUBE_FOLLOW]) > 2) {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_YOUTUBE_FOLLOW . "]' checked = 'checked' value = 'youtubefollow' /></div>");
                    } else {
                        echo ("<input id = 'fb_like' class = 'follow' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_YOUTUBE_FOLLOW . "]' value = 'youtubefollow' /></div>");
                    }

                    echo ("<div class = 'share'><label>" . __("Show Add this") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_ADDTHIS]) && strlen($this->arMzOptions1[MZ_SHOW_ADDTHIS]) > 2) {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_ADDTHIS . "]' checked = 'checked' value = 'addthis' /></div>");
                    } else {
                        echo ("<input id = 'showtumblr' class = 'share' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_ADDTHIS . "]' value = 'addthis' /></div>");
                    }
                    ?>
                </div>

                <div class="body block">
                    <?php echo ("<h3>" . __('Social Media Content Settings') . "</h3>"); ?>
                    <?php
                    echo ("<div><label>" . __("Show Facebook Icon") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_FACEBOOK_LIKE_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_FACEBOOK_LIKE_BODY]) > 2) {
                        echo ("<input id = 'showfblikebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_LIKE_BODY . "]' checked = 'checked' value = 'facebook' /></div>");
                    } else {
                        echo ("<input id = 'showfblikebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_FACEBOOK_LIKE_BODY . "]' value = 'facebook' /></div>");
                    }

                    echo ("<div><label>" . __("Show Tweet Icon") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TWEET_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_TWEET_BODY]) > 2) {
                        echo ("<input id = 'showtweetlikebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWEET_BODY . "]' checked = 'checked' value = 'twitter' /></div>");
                    } else {
                        echo ("<input id = 'showtweetlikebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TWEET_BODY . "]' value = 'twitter' /></div>");
                    }
                    echo ("<div><label>" . __("Show Pinit Icon") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_PINIT_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_PINIT_BODY]) > 2) {
                        echo ("<input id = 'showtumblrbody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINIT_BODY . "]' checked = 'checked' value = 'pinit' /></div>");
                    } else {
                        echo ("<input id = 'showtumblrbody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_PINIT_BODY . "]' value = 'pinit' /></div>");
                    }

                    echo ("<div><label>" . __("Show Tumblr Icon") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_TUMBLR_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_TUMBLR_BODY]) > 2) {
                        echo ("<input id = 'showtumblrbody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR_BODY . "]' checked = 'checked' value = 'tumblr' /></div>");
                    } else {
                        echo ("<input id = 'showtumblrbody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_TUMBLR_BODY . "]' value = 'tumblr' /></div>");
                    }


                    echo ("<div><label>" . __("Show Google Plus") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_GOOGLE_PLUS_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_GOOGLE_PLUS_BODY]) > 2) {
                        echo ("<input id = 'showgooglebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE_PLUS_BODY . "]' checked = 'checked' value = 'googleplus' /></div>");
                    } else {
                        echo ("<input id = 'showgooglebody' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_GOOGLE_PLUS_BODY . "]' value = 'googleplus' /></div>");
                    }
                    echo ("<div><label>" . __("Show Add This") . "</label>");
                    if (isset($this->arMzOptions1[MZ_SHOW_ADDTHIS_BODY]) && strlen($this->arMzOptions1[MZ_SHOW_ADDTHIS_BODY]) > 2) {
                        echo ("<input id = 'showaddthis' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_ADDTHIS_BODY . "]' checked = 'checked' value = 'addthis' /></div>");
                    } else {
                        echo ("<input id = 'showaddthis' type = 'checkbox' name = '" . MZ_SOCIAL_ICONS_OPTIONS . "[" . MZ_SHOW_ADDTHIS_BODY . "]' value = 'addthis' /></div>");
                    }
                    ?>


                </div>

                <?php do_action('add_social_media_setting'); ?>


                <p class="submit">
                    <input type="submit" name="submit" class="button-primary" value="<?php _e("Save Changes"); ?>" />
                </p>
                <?php
                // Add an nonce field so we can check for it later.
                wp_nonce_field('displayOptionsPage', 'mz_social_icons_setting');
                ?>
            </form>
        </div>

        <?php
    }

    public function registerSettingsAndFields() {
        register_setting(MZ_SOCIAL_ICONS_OPTIONS, MZ_SOCIAL_ICONS_OPTIONS, array($this, 'mzValidateSetting')); //3rd param is callback
    }

    /**
     * Generates fields for BIN Base URL setting.
     */
    /*
     * validate settings
     */

    public function mzValidateSetting($arSiteOptions) {
        if (!empty($_POST['mz_social_icons_setting']) && check_admin_referer('displayOptionsPage', 'mz_social_icons_setting')) {
            $arSiteOptions[MZ_SIGN_UP_FOR_EMAILS] = sanitize_text_field($arSiteOptions[MZ_SIGN_UP_FOR_EMAILS]);
            if ($arSiteOptions[MZ_FOLLOW_SHARE_SELECT] == 'share') {
                if (!empty($arSiteOptions[MZ_FACEBOOK_URL])) {
                    $arSiteOptions[MZ_FACEBOOK_URL] = sanitize_text_field($arSiteOptions[MZ_FACEBOOK_URL]);
                }
                if (!empty($arSiteOptions[MZ_FACEBOOK_APP_ID])) {
                    $arSiteOptions[MZ_FACEBOOK_APP_ID] = sanitize_text_field($arSiteOptions[MZ_FACEBOOK_APP_ID]);
                }
                if (!empty($arSiteOptions[MZ_TWITTER_URL])) {
                    $arSiteOptions[MZ_TWITTER_URL] = sanitize_text_field($arSiteOptions[MZ_TWITTER_URL]);
                }
                if (!empty($arSiteOptions[MZ_PIN_URL])) {
                    $arSiteOptions[MZ_PIN_URL] = sanitize_text_field($arSiteOptions[MZ_PIN_URL]);
                }
                if (!empty($arSiteOptions[MZ_TUMBLR_URL])) {
                    $arSiteOptions[MZ_TUMBLR_URL] = sanitize_text_field($arSiteOptions[MZ_TUMBLR_URL]);
                }
                if (!empty($arSiteOptions[MZ_GOOGLE_URL])) {
                    $arSiteOptions[MZ_GOOGLE_URL] = sanitize_text_field($arSiteOptions[MZ_GOOGLE_URL]);
                }
            }
            if ($arSiteOptions[MZ_FOLLOW_SHARE_SELECT] == 'follow') {
                if (!empty($arSiteOptions[MZ_YOUTUBE_URL])) {
                    $arSiteOptions[MZ_YOUTUBE_URL] = sanitize_text_field($arSiteOptions[MZ_YOUTUBE_URL]);
                }
            }

            //$arSiteOptions = $this->mzImgHandler(MZ_FACEBOOK_LOGO, $arSiteOptions);
            //$arSiteOptions = $this->mzImgHandler(MZ_TWITTER_LOGO, $arSiteOptions);
            //$arSiteOptions = $this->mzImgHandler(MZ_PIN_LOGO, $arSiteOptions);
            //$arSiteOptions = $this->mzImgHandler(MZ_TUMBLR_LOGO, $arSiteOptions);
            //$arSiteOptions = $this->mzImgHandler(MZ_GOOGLE_LOGO, $arSiteOptions);
            //$arSiteOptions = $this->mzImgHandler(MZ_YOUTUBE_LOGO, $arSiteOptions);
        }
        return $arSiteOptions;
    }

    /**
     * Validates user input for img handler
     * @return boolean
     * True, if fields pass validation checks.
     */
    public function mzImgHandler($attr, $arSiteOptions) {

        if (!empty($_FILES[$attr]["tmp_name"])) {
            if ($this->checkValidImage($attr)) {
                $overrides = array('test_form' => false);
                $arFile = wp_handle_upload($_FILES[$attr], $overrides);

                $arSiteOptions[$attr] = $arFile["url"];
            } else {
                $arSiteOptions[$attr] = $this->arMzOptions1[$attr];
            }
        } else {
            $arSiteOptions[$attr] = $this->arMzOptions1[$attr];
        }

        return $arSiteOptions;
    }

    public function checkValidImage($attribute) {
        $imgeType = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_JPG);
        if ($_FILES[$attribute]["size"] >= 2120000) {
            add_settings_error(
                    $attribute, // setting title
                    'Image_Big_Size', // error ID
                    __('Please enter a valid Image'), // error message
                    'error'                        // type of message
            );
            return false;
        } else {

            $arImageData = @getimagesize($_FILES[$attribute]["tmp_name"]);



            if ($arImageData === FALSE || !($arImageData[2] == IMAGETYPE_GIF || $arImageData[2] == IMAGETYPE_JPEG || $arImageData[2] == IMAGETYPE_PNG)) {
                add_settings_error(
                        $attribute, // setting title
                        'Image_Invalid_Size', // error ID
                        __('Please enter a valid Image Format for ' . $attribute), // error message
                        'error'                        // type of message
                );

                return false;
            }
        }

        return true;
    }

    /*
     * input
     */

    public function addJavascript() {
        wp_register_script('custom', 'https://code.jquery.com/jquery-1.10.1.min.js', false, '1.10.1');
        wp_enqueue_script('custom');
        ?>
        <style >
            .social-settings {
                font-size: 12px;
            }
            .block {
                border: 1px solid #DDDDDD;
                margin: 10px 0;
                overflow: auto;
                padding: 10px;
            }
            label {
                padding: 10px;
                text-align: left;
                vertical-align: top;
                width: 200px;
                display: inline-block;
                cursor: default !important;
            }
            .fb-logo-img {
                display: block;
            }
            .follow-share label {
                width: auto;
            }
            input[type="checkbox"] {
                margin: 5px 0 0 5px !important;
            }
            input[type="radio"] {
                margin: 10px 0 0;
                padding: 0;
                vertical-align: text-top;
            }
        </style>

        <script type="text/javascript">


            jQuery(document).ready(function() {
                $('.follow-share input').each(function(index, Element) {
                    if (!$(this).is(":checked")) {
                        var socialClass = $(this).attr('rel');
                        $(document).find('.' + socialClass).each(function() {
                            $(this).hide();
                        });
                    }
                });

                $('.follow-share input:radio').change(function() {
                    $(document).find('.' + $(this).attr('rel')).each(function() {
                        $(this).show();
                    });
                    $(document).find('.' + $(this).siblings('input').attr('rel')).each(function() {
                        $(this).hide();
                    });
                });
            });
        </script>

        <?php
    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            $ob = new MzSocialMedia();
            $ob->addMenuPage();
        });
?>