<?php
/**
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  :    Deepak Sood
 * Purpose : To lock theme option admin panel when one user is editing
 *
 */
/*
 * To set heartbeat tick setting/interval to 15 sec
 * @param array $settings
 * @return array $settings
 */

function wptuts_heartbeat_settings($settings) {
    $settings['interval'] = 5; //Anything between 15-60
    return $settings;
}
add_filter('heartbeat_settings', 'wptuts_heartbeat_settings');

/**
 * Check lock status on the theme option screen and refresh the lock
 * @param array $data,$screen_id
 * @return array $data
 */
function wp_take_theme_lock($data, $screen_id) {
    $blog_id = get_current_blog_id();
    if ($_POST['screen_id'] == 'appearance_page_theme_options') {
        $lock_info = wp_check_theme_lock($blog_id);
        $user_id = $lock_info['user'];
        $lock = $lock_info['lock'];
        if ( ($lock == 'nolock' || $lock == 'expiredlock')) {
            wp_set_theme_lock($blog_id);
        } elseif (!$user_id && $lock == 'expiredlock') {
            // Do Nothing
        }
        return $data;
    }
}
add_filter('heartbeat_send', 'wp_take_theme_lock', 10, 3);

/**
 * Check to see if theme is opened by some user.
 * @since 2.5.0
 * @param $blog_id
 * @return array bool|int False: if not locked | Int: user ID of user with lock. locl status
 */
function wp_check_theme_lock($blog_id) {
    $lock = get_option('_edit_lock_site_id_' . $blog_id, false);
    if (!$lock) {
        return array('user' => false, 'lock' => 'nolock');
    }
    $lock = explode(':', $lock);
    $time = $lock[0];
    $user_id = $lock[1];

    if ((time() - $time) < 30 && $user_id){
        return array('user' => $user_id, 'lock' => 'locked', 'time'=> $time);
    } else{
        return array('user' => $user_id, 'lock' => 'expiredlock', 'time'=> $time);
    }
}

/**
 * Mark the theme as currently being edited by user
 * @param array $blog_id
 * @return  Returns false if
 */
function wp_set_theme_lock($blog_id) {
     if (0 == ($user_id = get_current_user_id())) {
        return false;
    }
    $now = time();
    $lock = "$now:$user_id";
    delete_option('_edit_lock_site_id_' . $blog_id);
    update_option('_edit_lock_site_id_' . $blog_id, $lock);
}

function wp_remove_theme_lock() {
    if (0 == ($blog_id = get_current_blog_id())){
        return false;
    }
    delete_option('_edit_lock_site_id_' . $blog_id);
}
add_action('wp_logout', 'wp_remove_theme_lock');

/**
 * Notify user about locked item
 */
function _admin_notice_theme_locked() {
    if (isset($_GET['page']) && $_GET['page'] == 'theme_options') {
        $blog_id = get_current_blog_id();
        $lock_info = wp_check_theme_lock($blog_id);
        $user_id = $lock_info['user'];
        $lock = $lock_info['lock'];
        $time = $lock_info['time'];
        $user_info = get_userdata($user_id);
        $user_name = $user_info->user_login;
        if(!$user_id && $lock == 'nolock'  ){
            wp_set_theme_lock($blog_id);
            $locked = false;
        } elseif($user_id != get_current_user_id() &&  $lock == 'expiredlock' && (time() - $time) > 60){
            wp_set_theme_lock($blog_id);
            $locked = false;
        } elseif($user_id && $user_id == get_current_user_id()){
            $locked = false;
        } else{
            $locked = true;
        }
        if ($locked) {

            $sendback_text = __('Go back');
            $sendback = admin_url('index.php');
        }

        $hidden = $locked ? '' : ' hidden';
        ?>
        <div id="post-lock-dialog" class="notification-dialog-wrap<?php echo esc_attr($hidden); ?>">
            <div class="notification-dialog-background"></div>
            <div class="notification-dialog">
                <div class="post-locked-message">
                    <p class="currently-editing wp-tab-first" tabindex="0">
                        <?php
                        _e('This content is currently locked by ' . $user_name);
                        ?>
                    </p><p>
                        <a class="button" href="<?php echo esc_url_raw($sendback); ?>"><?php echo esc_attr($sendback_text); ?></a>
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
}
add_action('admin_footer', '_admin_notice_theme_locked');