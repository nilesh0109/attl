<?php

add_action('wp', 'mz_sitemap_setup_schedule');

/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 */
function mz_sitemap_setup_schedule() {
    if (!wp_next_scheduled('mz_sitemap_hourly_event')) {
        wp_schedule_event(current_time('timestamp'), 'threehrs', 'mz_sitemap_hourly_event');
    }
}

add_action('mz_sitemap_hourly_event', 'mz_sitemap_do_this_hourly');

add_filter('cron_schedules', 'cron_add_three_hours');

function cron_add_three_hours($schedules) {

    $schedules['threehrs'] = array(
        'interval' => 10800,
        'display' => __('Once 3 hours')
    );
    return $schedules;
}

function mz_sitemap_do_this_hourly() {

    generate_sitemap();
}

/**
 * Generates Sitemap in xml format
 */
function generate_sitemap() {
    $output = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0">';
    $output .= get_menu_items(CUSTOM_HEADER_MENU_LOCATION);
    $output .= get_menu_items(CUSTOM_FOOTER_MENU_LOCATION);
    $output .= get_menu_items(CUSTOM_LEFT_NAVIGATION_ARTICLE_LOCATION);
    $output .= get_menu_items(CUSTOM_LEFT_NAVIGATION_PRODUCT_LOCATION);
    $output .="</urlset>";
    file_put_contents(get_stylesheet_directory() . "/sitemap.xml", $output);
}

function get_menu_items($menu_name) {

    if (( $locations = get_nav_menu_locations() ) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);

        $menu_items = wp_get_nav_menu_items($menu->term_id, array('post_status' => 'publish'));


        foreach ((array) $menu_items as $key => $menu_item) {
            $post_id = $menu_item->object_id;
            $arrModified = split(' ', $menu_item->post_modified);
            $url = $menu_item->object_id;
            $url = get_permalink($url);
            $img_url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
            $img_alt = get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true);
            $menu_list .= '<url> <loc>' . $url . '</loc><lastmod>' . $arrModified[0] . '</lastmod><image:image><image:loc>' . $img_url . '</image:loc><image:caption>' . $img_alt . '</image:caption></image:image></url>';
        }
    }
    return $menu_list;
    // $menu_list now ready to output
}

?>