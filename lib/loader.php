<?php

header('Content-Type: text/html; charset=utf-8');
//mb_internal_encoding("UTF-8");
require_once('lib_config.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/constants.php');
// Get Brand Site Settings from Database
$arBrandSiteSettings = get_option(BRAND_SITE_SETTING_KEY);
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/custom_content.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/admin_fixes.php');
// Start - All Admin Setting under Appearance Tab
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/update_theme_option.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/update-footer-option.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/social_media.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/update_analytics_option.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/update_widget_option.php');
///require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/admin_theme_option.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/update_cache.php');
// End -All Admin Setting under Appearance Tab
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/widgets.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/utility/helper.php');
//require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/search_module.php');
//require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/generate_left_nav_items.php');
//require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/promo_col.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/carousel.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/site_setup.php');
//require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/related_products.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/meta_fields.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/parse_class.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/child_items.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/taxonomy-images.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/container_tag.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/filter_setup.php');
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/core/render_social_media.php');

require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/admin/helpers/CategoryHelper.php');
// Encryption Decryption id for data sent to url
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/encrypt-decrypt.php');
//require_once( WP_CONTENT_DIR.'/'.LIB_NAME.'/admin/sitemap_xml.php');

//Base Extensions
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/' . EXTENSIONS . '/base/loader.php');

//Meta boxes Extensions
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/' . EXTENSIONS . '/metaboxes/loader.php');

//Let us load admin panel framework
require_once( WP_CONTENT_DIR . '/' . LIB_NAME . '/' . EXTENSIONS . '/admin-panel-framework/loader.php');


// Multicategory loader
require_once (WP_CONTENT_DIR . '/' . LIB_NAME . '/' . EXTENSIONS . '/multiple-categories/loader.php');
require_once (WP_CONTENT_DIR . '/' . LIB_NAME . '/' . EXTENSIONS . '/rich-spotlight/loader.php');
?>
