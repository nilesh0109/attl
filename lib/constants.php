<?php

define('ARTICLE_TAXONOMY', 'category');
define('ARTICLE_POST_TYPE', 'post');
define('PRODUCT_TAXONOMY', 'product-category');
define('RECIPES_TAXONOMY', 'recipes-category');
define('PRODUCT_CUSTOM_POST', 'products');
define('RECIPES_CUSTOM_POST', 'recipes');
define('PRODUCT_ARTICLE_ASSOCIATION', 'product_article_association');
define('RECIPES_ASSOCIATION', 'recipes_association');
define('PRODUCT_INGREDIENT_ATTRIBUTE', 'product_ingredient');
define('PRODUCT_UPC_CODE_ATTRIBUTE', 'product_upc_code');
define('PRODUCT_BV_UPC_CODE_ATTRIBUTE', 'product_bv_upc_code');
define('ASSOCIATED_PRODUCT_VARIATION', 'associated_product_variation');
define('PRODUCT_IS_FEATURED_ATTRIBUTE', 'product_is_featured');
define('PRODUCT_NOT_VISIBLE_ON_STORE_LOCATOR', 'not_product_visible_on_store_locator');
define('PRODUCT_SUMMARY_ATTRIBUTE', 'product_summary');
define('PRODUCT_SUMMARY_ATTRIBUTE_LANDING', 'product_summary_landing');
define('PRODUCT_ADDITIONAL_DETAILS_ATTRIBUTE', 'product_additional_details');
define('CONTACTUS_ADDITIONAL_DETAILS_ATTRIBUTE', 'contactus_additional_details');
define('SEO_TITLE_ATTRIBUTE', 'seo_title');
define('SEO_DESCRIPTION_ATTRIBUTE', 'seo_description');
define('SEO_KEYWORDS_ATTRIBUTE', 'seo_kewwords');
define('SEO_H1_ATTRIBUTE', 'seo_h1_text');
define('LEFT_NAV_OPTION', 'left_nav_otion');
define('RESPONSIVE_IFRAME_ATTRIBUTE', 'responsive_iframe');
// for related product title on product category detail page
define('RELATED_PRODUCT_TITLE_OPTION', 'related_product_title_option');

define('PROMO_COL_TAXONOMY', 'promo-placeholder');
define('PROMO_COL_CUSTOM_POST', 'promo');
define('SPOTLIGHT_CUSTOM_POST', 'spotlight');
define('TEASER_TAXONOMY', 'teaser-carousel');
define('TEASER_CUSTOM_POST', 'teasers');
define('CAROUSEL_TAXONOMY', 'carousel');
define('CAROUSEL_SLIDE_CUSTOM_POST', 'carousel-slides');
define('PROMO_TYPE', 'promo_type');
define('PROMO_TYPE_2', 'promo2');
define('PROMO_TYPE_3', 'promo3');

define('CTA_LABEL_ATTRIBUTE', 'cta_label');
define('CTA_URL_TYPE', 'cta_url_type');
define('CTA_URL_ATTRIBUTE', 'cta_url');
define('CTA_TARGET_ATTRIBUTE', 'cta_target_new');
define('CTA_URL_TYPE_EXTERNAL', 'external');
define('CTA_URL_TYPE_YOUTUBE', 'youtube');
define('CTA_URL_TYPE_VIDEO', 'video');

define('PROMO2_COL_TAXONOMY_SLUG_PREFIX', 'promo2-pd-');
define('PROMO3_COL_TAXONOMY_SLUG_PREFIX', 'promo3-pd-');
define('PROMO2_COL_PRODUCT_CATEGORY_SLUG_PREFIX', 'promo2-pc-');
define('PROMO3_COL_PRODUCT_CATEGORY_SLUG_PREFIX', 'promo3-pc-');
define('CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX', 'carousel-ac-');
define('CAROUSEL_PRODUCT_CATEGORY_SLUG_PREFIX', 'carousel-pc-');
define('CAROUSEL_RECIPES_CATEGORY_SLUG_PREFIX', 'carousel-rc-');

//carousel custom settings
define('CAROUSEL_CUSTOM_FIELD_PREFIX', 'carousel-custom-field-');
define('CAROUSEL_NAVIGATION_SETTING', 'carousel-navigation-setting');
define('CAROUSEL_AUTO_TRANSITION', 'carousel-auto-transition');
define('CAROUSEL_PAUSE_PLAY_FEATURE', 'carousel-pause-play-feature');
define('CAROUSEL_TRANSITION_TIME_INTERVAL', 'carousel-transition-time-interval');

//carousel types of navigation
define('CAROUSEL_NAVIGATION_ARROWS', 'arrows');
define('CAROUSEL_NAVIGATION_BULLETS', 'bullets');
define('CAROUSEL_NAVIGATION_BOTH', 'both');

//carousel default values if it is not set from database
define('CAROUSEL_NAVIGATION_DEFAULT_VALUE', 'arrows');
define('CAROUSEL_AUTO_TRANSITION_DEFAULT_VALUE', '1');
define('CAROUSEL_PAUSE_PLAY_FEATURE_DEFAULT_VALUE', '0');
define('CAROUSEL_TRANSITION_TIME_INTERVAL_DEFAULT_VALUE', '3');

// Terms need to be created on site Setup
define('CAROUSEL_HOME_PAGE', 'carousel-home');
define('CAROUSEL_PRODUCT_LANDING', 'carousel-product-landing');
define('CAROUSEL_ARTICLE_LANDING', 'carousel-article-landing');
define('CAROUSEL_RECIPES_LANDING', 'carousel-recipes-landing');
define('TEASER_CAROUSEL_HOME_PAGE', 'teaser-carousel-home');
define('TEASER_CAROUSEL_PRODUCT_DETAIL', 'teaser-carousel-product-detail');
define('PROMO2_COL_PRODUCT_LANDING', 'promo2-product-landing');
define('PROMO3_COL_PRODUCT_LANDING', 'promo3-product-landing');

define('PROMO2_COL_RECIPES_LANDING', 'promo2-recipes-landing');
define('PROMO3_COL_RECIPES_LANDING', 'promo3-recipes-landing');
define('PROMO2_COL_ARTICLE_LANDING', 'promo2-article-landing');
define('PROMO3_COL_ARTICLE_LANDING', 'promo3-article-landing');
define('TEASER_CAROUSEL_RECIPES_PAGE', 'teaser-carousel-recipes');

define('CUSTOM_CATEGORY_FIELDS', 'custom_term_field');
define('CUSTOM_TAXONOMY_IMAGE_FIELDS', 'custom_taxonomy_images');
define('TAXONOMY_IMAGE_ATTRIBUTE', 'image_id');
define('DEFAULT_TAXONOMY_IMAGE', site_url() . '/wp-includes/images/crystal/default.png');

// Start Initial Page Setup
define('PAGE_CREATOR_ID', "1");
define('PRODUCT_LANDING_PAGE_TEMPLATE', "product_landing.php");
define('RECIPES_LANDING_PAGE_TEMPLATE', "recipes_landing.php");
define('ARTICLE_LANDING_PAGE_TEMPLATE', "article_landing.php");
define('RECIPES_LANDING_PAGE_TEMPLATE', "recipes_landing.php");
define('CONTACT_US_TEMPLATE', "contact_us.php");
define('SIGN_UP_TEMPLATE', "signup.php");
define('ABOUT_US_TEMPLATE', "about-us.php");
define('SITEMAP_TEMPLATE', "sitemap.php");
define('SITEMAP_XML_TEMPLATE', "sitemap_xml.php");
define('HEADER_TEMPLATE', "template_header.php");
define('FOOTER_TEMPLATE', "template_footer.php");

//Landing pages Slug
define('PRODUCT_LANDING_SLUG', 'products');
define('ARTICLE_LANDING_SLUG', 'articles');
define('RECIPES_LANDING_SLUG', 'recipes');
define('CONTACT_US_SLUG', 'contact-us');
define('ABOUT_US_SLUG', 'about-us');
define('SITEMAP_SLUG', 'site-map');
define('SITEMAP_XML_SLUG', 'sitemap-xml');
define('HEADER_SLUG', 'header');
define('FOOTER_SLUG', 'footer');
define('OUR_CATEGORIES', 'Our Categories');

//page titles for the pages craeted by code
define('HOME_PAGE_TITLE', 'Home');
define('ARTICLE_LANDING_PAGE_TITLE', 'Articles');
define('RECIPES_LANDING_PAGE_TITLE', 'Recipes');
define('PRODUCT_LANDING_PAGE_TITLE', 'Products');
define('CONTACT_US_PAGE_TITLE', 'Contact Us');
define('ABOUT_US_PAGE_TITLE', 'About Us');
define('SITEMAP_PAGE_TITLE', 'Site Map');
define('SITEMAP_XML_PAGE_TITLE', 'SiteMap Xml');
define('HEADER_PAGE_TITLE', 'Header');
define('FOOTER_PAGE_TITLE', 'Footer');

// End Initial Page Setup
define('MZ_SITE_OPTIONS', 'mz_site_options');
define('MZ_APPEARANCE_OPTION_SLUG', 'update_theme_options');
define('MZ_APPEARANCE_OPTIONS', 'mz_appearance_options');
define('MZ_HOME_OPTION_SLUG', 'mz_home_options');
define('MZ_HOME_OPTIONS', 'mz_home_site_options');

/* -----typekit constants----- */
define('MZ_TYPEKIT_ENABLED', 'mz_typekit_enabled');
define('MZ_TYPEKIT_URL', 'mz_typekit_url');
define('MZ_TYPEKIT_FONTS', 'mz_typekit_fonts');
define('MZ_TYPEKIT_OPTION_SLUG', 'update_typekit_options');
define('MZ_TYPEKIT_OPTION', 'mz_typekit_options');
define('TYPEKIT_ID', 'mz_typekit_ID');
define('TYPEKIT_FONTS', 'mz_typekit_fonts');
define('TYPEKIT_BASE_URL', 'https://use.typekit.com/');
define('TYPEKIT_ENABLED', 'Enabled');
define('TYPEKIT_DISABLED', 'Disabled');
define('TYPEKIT_FAILSAFE', 'false');

/* -----RTL constants----- */
define('MZ_RTL_ENABLED', 'mz_rtl_enabled');
define('RTL_ENABLED', 'Enabled');
define('RTL_DISABLED', 'Disabled');

/* -----Site Layout constants----- */
define('LAYOUT_SETTINGS', 'layout_settings');
define('LAYOUT_FIXED', 'Fixed');
define('LAYOUT_FULL', 'Full');

/* ----Related Teaser Settings---- */
define('RELATED_PRODUCT_TITLE_SETTINGS', 'related_product_title_settings');
define('RELATED_PRODUCT_ORDERING_SETTINGS', 'related_product_ordering_settings');
define('RELATED_ARTICLE_ORDERING_SETTINGS', 'related_article_ordering_settings');
define('ORDER_BY_NAME', 'Name (Alphabetical)');
define('ORDER_BY_DATE_ASC', 'Published Date(Ascending)');
define('ORDER_BY_DATE_DSC','Published Date(Descending)');
define('ORDER_BY_LEFT_NAV', 'Left Navigation Menu');

/* -----menu nav constants----- */
define('CUSTOM_FOOTER_MENU_LOCATION', 'footer_navigation');
define('CUSTOM_HEADER_MENU_LOCATION', 'header_navigation');
define('CUSTOM_LEFT_NAVIGATION_ARTICLE_LOCATION', 'left_navigation_article');
define('CUSTOM_LEFT_NAVIGATION_PRODUCT_LOCATION', 'left_navigation_product');
define('CUSTOM_FOOTER_MENU_NAME', __('Footer Navigation'));
define('CUSTOM_SITEMAP_INFO_MENU_NAME', __('Sitemap General Information'));
define('CUSTOM_SITEMAP_SEARCH_MENU_NAME', __('Sitemap Search Information'));
define('CUSTOM_TOP_HEADER_MENU_NAME', __('Top Header Navigation'));
define('CUSTOM_SITEMAP_INFO_MENU_LOCATION', 'sitemap_general_navigation');
define('CUSTOM_SITEMAP_SEARCH_MENU_LOCATION', 'sitemap_search_navigation');
define('CUSTOM_TOP_HEADER_MENU_LOCATION', 'top_header_navigation');
define('CUSTOM_HEADER_MENU_NAME', __('Header Navigation'));
define('CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME', __('Left Navigation Article'));
define('CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME', __('Left Navigation Product'));
define('CUSTOM_LEFT_NAVIGATION_RECIPES_NAME', __('Left Navigation Recipes'));
define('CUSTOM_SITEMAP_STATIC_PAGES', __('Sitemap items for static HTML'));
define('CUSTOM_SITEMAP_STATIC_PAGES_LOCATION', __('sitemap_for_static_html'));
define('NAVGATION_MENU_TAXNOMY', 'nav_menu');
define('NAV_ITEM_POST_TYPE', 'nav_menu_item');
define('NAV_ITEM_TERM_TYPE_PREFIX', 'menu_nav_tax_');
define('NAV_ITEM_POST_TYPE_PREFIX', 'menu_nav_post_');
//site Options
define('SITE_OPTION_CAPABILITY', 'administrator');
define('MZ_HEADER_TITLE', 'mz_header_title');
define('MZ_HEADER_LOGO', 'mz_header_logo');
define('MZ_FOOTER_LOGO', 'mz_footer_logo');
define('MZ_FOOTER_MORE_TEXT', 'mz_footer_more_text');
define('MZ_FOOTER_PRODUCT_PAGE_TEXT', 'mz_footer_product_text');
define('MZ_FOOTER_ARTICLE_PAGE_TEXT', 'mz_footer_article_text');
define('MZ_FOOTER_HOME_PAGE_TEXT', 'mz_footer_home_text');
define('MZ_FOOTER_HELP_PAGE_TEXT', 'mz_footer_help_text');
define('MZ_SEO_TITLE', 'mz_seo_title');
define('MZ_FOOTER_TEXT', 'mz_footer_text');
define('MZ_SEO_KEYWORDS', 'mz_seo_keywords');
define('MZ_SEO_DESCRIPTION', 'mz_seo_description');
define('MZ_SEO_H1_TEXT', 'mz_seo_h1_text');
define('MZ_SOCIAL_ICONS_FB_LINK', 'mz_social_icons_fb_link');
define('MZ_SOCIAL_ICONS_TWITTER_LINK', 'mz_social_icons_twitter_link');
define('MZ_BIN_TYPE', 'mz_bin_type');
define('MZ_BIN_BASE_URL', 'mz_bin_base_url');
define('MZ_BIN_WIDTH', 'mz_bin_width');
define('MZ_BIN_BUTTON_TEXT', 'mz_bin_button_text');
define('MZ_BIN_HEIGHT', 'mz_bin_height');
//BIN default values if it is not set from database
define('MZ_BIN_DEFAULT_WIDTH', '320');
define('MZ_BIN_DEFAULT_HEIGHT', '480');
//End of BIN default values if it is not set from database
define('MZ_EDIT_OPTION_SLUG', 'update_mz_theme');
define('MZ_HEADER_FAVICON', 'mz_header_favicon');
define('MZ_HEADER_APPLETOUCHICONLARGE', 'mz_header_touch_icon_large');
define('MZ_HEADER_APPLETOUCHICONMEDIUM', 'mz_header_touch_icon_medium');
define('MZ_HEADER_APPLETOUCHICONSMALL', 'mz_header_touch_icon_small');

//Site widget Options
define('MZ_TWITTER_WIDGET_ID', 'mz_twitter_widget_Id');
define('MZ_TWITTER_ID', 'mz_twitter_Id');
define('MZ_TWITTER_LINKCOLOR', 'mz_twitter_link_color');
define('MZ_TWITTER_CHROME', 'mz_twitter_chrome');
define('MZ_TWITTER_SCHEME', 'mz_twitter_scheme');
define('MZ_TWITTER_TWEETCOUNT', 'mz_twitter_tweet_count');
define('MZ_WIDGET_EDIT_OPTION_SLUG', 'update_mz_widgets');
define('MZ_FACEBOOK_ID', 'mz_facebook_Id');
define('MZ_FACEBOOK_BORDER_COLOR', 'mz_facebook_border_color');
define('MZ_FACEBOOK_FONT', 'mz_facebook_font');
define('MZ_FACEBOOK_COLOR_SCHEME', 'mz_facebook_color_scheme');
define('MZ_SITE_WIDGET_OPTIONS', 'mz_lite_site_widget_options');

// page names
define('HOME_PAGE_NAME', 'home');
define('PRODUCT_LANDING_PAGE_NAME', 'product_landing');
define('RECIPES_LANDING_PAGE_NAME', 'recipes_landing');
define('ARTICLE_LANDING_PAGE_NAME', 'article_landing');
define('PRODUCT_CATEGORY_PAGE_NAME', 'product_category');
define('ARTICLE_CATEGORY_PAGE_NAME', 'article_category');
define('ARTICLE_DETAIL_PAGE_NAME', 'article_detail');
define('PRODUCT_DETAIL_PAGE_NAME', 'product_detail');
define('COLOUMN_GRID_WIDTH', '12');
//siderbar
define('HOME_WIDGET_PLACEHOLDER_NAME', __('Home Page Spotlight Container'));
define('HOME_WIDGET_PLACEHOLDER_ID', 'home_sidebar');

define('PRODUCT_LANDING_WIDGET_PLACEHOLDER_NAME', __('Product Landing Page Spotlight Container'));
define('PRODUCT_LANDING_WIDGET_PLACEHOLDER_ID', 'product_landing_sidebar');

define('ARTICLE_LANDING_WIDGET_PLACEHOLDER_NAME', __('Article Landing Page Spotlight Container'));
define('ARTICLE_LANDING_WIDGET_PLACEHOLDER_ID', 'article_landing_sidebar');

define('ARTICLE_CATEGORY_WIDGET_PLACEHOLDER_NAME', __('Article Category Page Spotlight Container'));
define('ARTICLE_CATEGORY_WIDGET_PLACEHOLDER_ID', 'article_category_sidebar');

define('ARTICLE_DETAIL_WIDGET_PLACEHOLDER_NAME', __('Article Detail Page Spotlight Container'));
define('ARTICLE_DETAIL_WIDGET_PLACEHOLDER_ID', 'article_detail_sidebar');

define('PRODUCT_CATEGORY_WIDGET_PLACEHOLDER_NAME', __('Product Category Page Spotlight Container'));
define('PRODUCT_CATEGORY_WIDGET_PLACEHOLDER_ID', 'product_category_sidebar');

define('PRODUCT_DETAIL_WIDGET_PLACEHOLDER_NAME', __('Product Detail Page Spotlight Container'));
define('PRDUCT_DETAIL_WIDGET_PLACEHOLDER_ID', 'product_detail_sidebar');

define('CONTACTUS_WIDGET_PLACEHOLDER_NAME', __('Contact Us Page Spotlight Container'));
define('CONTACTUS_WIDGET_PLACEHOLDER_ID', 'contact_us_sidebar');

define('RECIPE_SEARCH_WIDGET_PLACEHOLDER_NAME', __('Recipe Search Page Spotlight Container'));
define('RECIPE_SEARCH_WIDGET_PLACEHOLDER_ID', 'recipe_search_sidebar');

define('RECIPES_DETAIL_WIDGET_PLACEHOLDER_NAME', __('Recipes Detail Page Spotlight Container'));
define('RECIPES_DETAIL_WIDGET_PLACEHOLDER_ID', 'recipes_page_sidebar');

define('RECIPES_LANDING_WIDGET_PLACEHOLDER_NAME', __('Recipes Landing Page Spotlight Container'));
define('RECIPES_LANDING_WIDGET_PLACEHOLDER_ID', 'recipes_landing_page_sidebar');

//Analytics
define('MZ_ANALYTICS_EDIT_OPTION_SLUG', 'mz_update_analytics_options');
define('MZ_ANALYTICS_OPTIONS', 'mz_analytics_options');
define('MZ_DOMNAME', 'mz_domain');
define('MZ_SITE_TYPE', 'mz_site_type');
define('MZ_SITE_TYPE_DEFAULT_VALUE', 'AWS-Mizkan');
define('MZ_CT_SERVER_URL', 'mz_ct_server_url');
define('MZ_GLOBAL_BRANDNAME', 'mz_global_brand_name');
define('MZ_TAGGING_CATEGORY', 'mz_global_brand_category');
define('MZ_BRAND_NAME', 'mz_global_brand_name');
define('MZ_TAGGING_COUNTRY', 'mz_global_brand_country');
define('MZ_TAGGING_LOCAL_BRAND', 'mz_local_brand_name');
define('MZ_TAGGING_CHANNEL', 'mz_global_tagging_channel');
define('MZ_TAGGING_MOBILE_CHANNEL', 'mz_global_tagging_mobile_channel');
define('MZ_TAGGING_GID', 'mz_tagging_gid');
define('MZ_TAGGING_GOOGLE_ANALYTICS', 'mz_tagging_ga');
define('MZ_COUNTRY', 'mz_country');
define('MZ_MZ_BRAND_NAME', 'mz_brand_name');
define('CHILDCSSPATH', get_stylesheet_directory() . '/style.css');
define('CONSTANTSCSSPATH', get_template_directory() . '/admin/style.css');
define('MZ_EDIT_ADMIN_INTERFACE_SLUG', 'theme_options');
define('SEARCH_RESULT_WIDGET_PLACEHOLDER_NAME', __('Search Result Page Spotlight Container'));
define('SEARCH_RESULT_WIDGET_PLACEHOLDER_ID', 'search_result_sidebar');
define('MZ_GOOGLE_WEBMASTER_CODE', 'mz_google_webmaster_code');
define('MZ_BING_WEBMASTER_CODE', 'mz_bing_webmaster_code');

//related items
define('ENABLE_ONLY_TAGS', 'only_tags');
define('ENABLE_ONLY_PARENT_TERM', 'only_term');
define('RELATED_PRODUCT_QUERY_TYPE', '');

// spotlight widget
define('SPOTLIGHT_WIDGET_NAME', __('Spotlight'));
define('SPOTLIGHT_WIDGET_ID', 'Spotlight');

//youtube url for videos
define('YOUTUBE_URL', '//www.youtube.com/embed/');
define('YOUTUBE_IMAGE_PREFIX', 'http://img.youtube.com/vi/');
define('YOUTUBE_IMAGE_SUFFIX', '/0.jpg');


// Buy It Now constants
define('BIN_TYPE_DISABLED', 'Disabled');
define('BIN_TYPE_UPC_BASED', 'UPC Based');
define('BIN_TYPE_SITE_DEFAULT', 'Site Default');

//Social Media
define('MZ_FACEBOOK_URL', 'mz_facebook_url');
define('MZ_TWITTER_URL', 'mz_twitter_url');
define('MZ_PIN_URL', 'mz_pinit_url');
define('MZ_TUMBLR_URL', 'mz_tumblr_url');
define('MZ_FACEBOOK_LOGO', 'mz_facebook_logo');
define('MZ_TWITTER_LOGO', 'mz_twitter_logo');
define('MZ_PIN_LOGO', 'mz_pinit_logo');
define('MZ_TUMBLR_LOGO', 'mz_tumblr_logo');
define('MZ_SHOW_TWEET_BODY', 'mz_show_tweet_body');
define('MZ_SHOW_FACEBOOK_LIKE_BODY', 'mz_show_facebook_like_body');
define('MZ_SHOW_PINIT_BODY', 'mz_show_pinit_body');
define('MZ_SHOW_TUMBLR_BODY', 'mz_show_tumblr_body');
define('MZ_SHOW_TWEET', 'mz_show_tweet');
define('MZ_SHOW_PINTEREST', 'mz_show_pinterest');
define('MZ_SHOW_FACEBOOK_LIKE', 'mz_show_facebook_like');
define('MZ_SHOW_PINIT', 'mz_show_pinit');
define('MZ_SHOW_TUMBLR', 'mz_show_tumblr');
define('MZ_SOCIAL_ICONS_OPTION_SLUG', 'social_icons');
define('MZ_SOCIAL_ICONS_OPTIONS', 'mz_social_icons_options');
define('MZ_FACEBOOK_APP_ID', 'mz_fb_app_id');
define('MZ_FOLLOW_SHARE_SELECT', 'mz_share_select_header');
define('MZ_SHOW_ADDTHIS_BODY', 'mz_show_addthis_body');
define('MZ_SHOW_GOOGLE_PLUS_BODY', 'mz_show_google_plus_body');
define('MZ_YOUTUBE_LOGO', 'mz_youtube_logo');
define('MZ_SHOW_ADDTHIS', 'mz_show_add_this');
define('MZ_YOUTUBE_URL', 'mz_youtube_url');
define('MZ_SHOW_GOOGLE', 'mz_show_google');
define('MZ_GOOGLE_URL', 'mz_google_url');
define('MZ_GOOGLE_LOGO', 'mz_google_logo');
define('FACBOOK_SITE_URL', 'https://www.facebook.com/');
define('TWITTER_SITE_URL', 'https://www.twitter.com/');
define('TUMBLR_SITE_URL', '.tumblr.com');
define('YOUTUBE_SITE_URL', 'https://www.youtube.com/user/');
define('GOOGLEPLUS_SITE_URL', '//plus.google.com/');
define('PINIT_SITE_URL', 'https://www.pinterest.com/');
define('MZ_SHOW_FACEBOOK_FOLLOW', 'mz_show_facebook_follow');
define('MZ_SHOW_TWITTER_FOLLOW', 'mz_show_twitter_follow');
define('MZ_SHOW_PINTEREST_FOLLOW', 'mz_show_pinterest_follow');
define('MZ_SHOW_TUMBLR_FOLLOW', 'mz_show_tumblr_follow');
define('MZ_SHOW_GOOGLE_FOLLOW', 'mz_show_google_follow');
define('MZ_SHOW_YOUTUBE_FOLLOW', 'mz_show_youtube_follow');
define('MZ_SIGN_UP_FOR_EMAILS', 'mz_sign_up_for_emails');


// Define Capabilities
define('THEME_OPTION_CAPABILITY', 'edit_theme_options');
define('MZ_FLUSH_ALL', 'flush_all');
//site setup option names
define('SITE_SETUP_MENU_ITEMS_IDS', 'site_setup_menu_id');
define('SITE_SETUP_PAGE_IDS', 'site_setup_page_id');
define('SUPER_ADMIN_ID', '0');
define('LOIGIN_IMAGE_URL', '#');
define('LOIGIN_IMAGE_LOGO_TITLE', 'Villabertolli');



define('ADCHOICEHTML', get_stylesheet_directory() . "/adchoice_html.php");
define('ADCHOICEJS', get_stylesheet_directory() . "/adchoice_js.php");

define('THUMBNAIL_IMAGE_HEIGHT', '140');
define('THUMBNAIL_IMAGE_WIDTH', '140');
define('THUMBNAIL_CROP_IMAGE', '0');

//Campaign defaults
define('CAMPAIGN_TAXONOMY', 'campaign-category');
define('CAMPAIGN_CUSTOM_POST', 'campaign');
define('CAMPAIGN_FIRST_DESCRIPTION', 'campaign_first_description');
define('CAMPAIGN_SECOND_DESCRIPTION', 'campaign_second_description');
define('CAMPAIGN_IFRAME_URL', 'campaign_iframe_url');
define('CAMPAIGN_HEADER_FOOTER', 'campaign_header_footer');
define('CAMPAIGN_IFRAME_WIDTH', '1200');
define('CAMPAIGN_IFRAME_HEIGHT', '1000');

//HELP defaults
define('HELP_URL', '#');

//Carasoual Small Image Name
define('CARAUSAL_MOBILE_IMAGE', 'carausal-mobile');
//Language Define
$lang = get_option('WPLANG');
if (empty($lang)) {
    define('DEFAULT_LANGUAGE', 'en_US');
} else {
    define('DEFAULT_LANGUAGE', get_option('WPLANG'));
}

// Brand Site Settings
define('BRAND_SITE_SETTING_KEY', 'brand_site_settings');
define('BRAND_SITE_OPTIONS', 'brand_site_options');
define('BRAND_SITE_POST_ADDITIONAL_DETAIL', 'post_additional_meta');
define('BRAND_SITE_LOST_PASSWORD_RESET_URL', 'lost_password_reset_url');
define('BRAND_SITE_V2_ENDPOINT_URL', 'webservicev2_endpoint_url');
define('BRAND_SITE_CHINA_EXTENSION', 'china_extension');
define('BRAND_SITE_CAMPAIGN_EXTENSION', 'campaign_extension');

// Related Teaser Site Level Option
define('MZ_RELATED_PRODUCT_TITLE', 'related_product_title');
define('MZ_RELATED_RECIPES_TITLE', 'related_recipes_title');
define('BRAND_SITE_BAZAAR_VOICE_EXTENSION', 'bazaar_voice_extension');

// Contact us form encryption decryption constants
define('MIZKAN_SALT', '#^#@@%##$AFD');
define('MIZKAN_USER_SALT', '@%##^$#^#');
define('MIZKAN_USER_IV', '#&%FJ#@&%');
define('MIZKAN_ENCRYPTION_METHOD', 'AES-256-CBC');

?>