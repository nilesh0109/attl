<?php
/* This file is part of Contact Form Encryption Decryption to database.
 * 
 */
class encryptDecrypt {

    public $secret_key;
    public $secret_iv;
    public $iv;
    public $encrypt_method;
    public $encrypted_string;
    public $decrypted_string;

    function encryptDecrypt() {
        $this->secret_key = constant('MIZKAN_USER_SALT');
        $this->secret_iv = constant('MIZKAN_USER_IV');
        $this->encrypt_method = constant('MIZKAN_ENCRYPTION_METHOD');
        $this->iv = substr(hash('sha256', $this->secret_iv), 0, 16);
    }

    public function encrypt($string) {
        $this->encrypted_string = openssl_encrypt($string, $this->encrypt_method, $this->secret_key, 0, $this->iv);
        $this->encrypted_string = base64_encode($this->encrypted_string);

        return $this->encrypted_string;
    }

    public function decrypt($string) {
        if($this->encrypt(openssl_decrypt(base64_decode($string), $this->encrypt_method, $this->secret_key, 0, $this->iv)) === $string){
            $this->decrypted_string = openssl_decrypt(base64_decode($string), $this->encrypt_method, $this->secret_key, 0, $this->iv);
        }else{
            $this->decrypted_string = $string;
        }
        return $this->decrypted_string;
    }

}