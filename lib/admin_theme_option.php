<?php

class MzAdminInterface {

    function __construct() {
        require_once 'helpers/business_layer.php';
        require_once 'helpers/cssFilter.php';
        if (isset($_GET['page']) && $_GET['page'] == 'MZ_EDIT_ADMIN_INTERFACE_SLUG') {
            wp_register_style('prefix-style', '/wp-content/lib/css/admin/mizkan-admin.css');
            wp_register_script('prefix-js', '/wp-content/lib/js/admin/mizkan-admin.js', '', '', true);
            wp_enqueue_style('prefix-style');
            wp_enqueue_script('prefix-js');
        }
    }

    /*
     * Adding a Menu Page
     */

    public function addMenuPage() {
        //add_menu_page(__("Admin interface"), __("Admin Interface"), SITE_OPTION_CAPABILITY, MZ_EDIT_ADMIN_INTERFACE_SLUG, array('MzAdminInterface', 'displayOptionsPage','','62'));
        //add_options_page(__("Admin interface"), __("Admin Interface"), SITE_OPTION_CAPABILITY, MZ_EDIT_ADMIN_INTERFACE_SLUG, array('MzAdminInterface', 'displayOptionsPage'));
        add_submenu_page('themes.php', __("Theme Options"), __("Theme Options"), SITE_OPTION_CAPABILITY, MZ_EDIT_ADMIN_INTERFACE_SLUG, array('MzAdminInterface', 'displayOptionsPage'));
        remove_submenu_page('themes.php', 'ot-theme-options');
    }

    public function displayOptionsPage() {
        ?>
        <section id="theme-body">
            <div class="top">
                <?php _e("Theme Styling Options") ?>
            </div>
            <?php include(get_template_directory() . '/admin/mod/left-menu.php'); ?>

            <?php
            if (!isset($_POST["hidden"])) {
                MzAdminInterface::generateUserInterface();
            }
            ?>

            <?php
            if (isset($_POST["hidden"]) && (isset($_POST['submitForm']))) {
                $input = $_POST["hidden"];
                $input = stripslashes($input);
                $count = 0;

                foreach ($_FILES as $eachFile) {
                    if ($eachFile['size'] > 0) {
                        $fileName = "datafile";
                        $fileName = MzAdminInterface::getFileURL($fileName);
                    }
                }
                $arrCssParser = parse($input);
                $existingContents = file_get_contents(get_stylesheet_directory() . '/style.css');
                if (isset($_POST["timestamp"]) && strlen($_POST["timestamp"]) > 1) {
                    $timestamp = $_POST["timestamp"];
                }
                if (!isset($arr1[1]) || (strpos($existingContents, $arr1[1])) !== false) {


                    $check = parse($existingContents);
                    $finalContents = "";
                    $startcolor = "";
                    $endcolor = "";
                    $arrGradient = "";
                    $loopstop = 0;

                    foreach ($arrCssParser as $key => $value) {
                        $attr = "";
                        $cssSubstring = "";
                        $gradient = false;
                        foreach ($value as $innerKey => $innerValue) {

                            if (strrpos($innerKey, "filter") === false) {
                                if (strpos($innerKey, "background-image") !== false) {

                                    if (strpos($innerValue, "linear-gradient") !== false) {
                                        if (strpos($innerValue, ",") !== false) {
                                            $arrGardient = explode(',', $innerValue);
                                            $startcolor = trim($arrGardient[1]);
                                            $startcolor = str_Replace(" 0%", "", $arrGardient[1]);
                                            $endcolor = trim(str_Replace(" 100%)", "", $arrGardient[2]));
                                            $attr .= "background-image: -moz-linear-gradient(top," . $startcolor . " 0%," . $endcolor . " 100%);\n";
                                            $attr .= "background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%," . $startcolor . "), color-stop(100%," . $endcolor . "));";
                                            $attr .= "background-image: -webkit-linear-gradient(top," . $startcolor . " 0%," . $endcolor . " 100%);\n";
                                            $attr .= "background-image: -o-linear-gradient(top," . $startcolor . " 0%," . $endcolor . " 100%);\n";
                                            $attr .= "background-image: -ms-linear-gradient(top," . $startcolor . " 0%," . $endcolor . " 100%);\n";
                                            $attr .= "background-image: linear-gradient(to bottom," . $startcolor . " 0%," . $endcolor . " 100%);\n";
                                            $attr .= "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='" . $startcolor . "',endColorstr='" . $endcolor . "',GradientType=0 );";
                                        }
                                    }
                                }
                                if (strpos($innerValue, "linear-gradient") === false) {
                                    $attr.=$innerKey . ":" . $innerValue . ";";
                                }
                            }
                        }

                        $cssSubstring = $key . "{" . $attr . "}";

                        $attr = "";


                        if (array_key_exists($key, $check)) {


                            $regex = '/(' . $key . '[ \n\t]*\{([^\}]*)\})/U';
                            $existingContents = preg_replace($regex, $cssSubstring, $existingContents);
                        } else {
                            $finalContents.= $cssSubstring;
                        }
                    }
                }
                // echo $existingContents;
                $existingContents = $existingContents . $finalContents;
                $datecheck = file_get_contents(get_stylesheet_directory() . '/style.css');
                if (isset($timestamp) && strlen(timestamp) > 2) {

                    if (strpos($datecheck, $timestamp) !== false) {
                        $existingContents = preg_replace('/(\/\*\*timestamp[\d]+\*\*\/)/', '', $existingContents);
                        $existingContents.="/**timestamp" . time() . "**/";
                        file_put_contents(get_stylesheet_directory() . '/style.css', $existingContents);
                        MzAdminInterface::generateUserInterface();
                        ?>
                        <h3> <?php _e("Styles have been updated") ?></h3>

                <?php
                } else {
                    echo '<p class ="adminError">' . __("Error Occured") . "</p>";
                    MzAdminInterface::generateUserInterface();
                    ?>

                        <?php
                    }
                } else {
                    $existingContents.="/**timestamp" . time() . "**/";
                    file_put_contents(get_stylesheet_directory() . '/style.css', $existingContents);
                    MzAdminInterface::generateUserInterface();
                    ?>
                    <h3> <?php _e("Styles have been changed to default for current screen") ?></h3>
                <?php
                }
            }

            if (isset($_POST["hidden"]) && (isset($_POST['reset']))) {
                $input = $_POST["hidden"];
                $datecheck = file_get_contents(get_stylesheet_directory() . '/style.css');
                if (isset($_POST["timestamp"]) && strlen($_POST["timestamp"]) > 1) {
                    $timestamp = $_POST["timestamp"];
                }
                $input = stripslashes($input);
                $count = 0;
                $existingContents = file_get_contents(get_stylesheet_directory() . '/style.css');
                $arrCssParser = explode(",", $input);

                foreach ($arrCssParser as $key) {
                    if (strlen($key) >= 2) {

                        $regex = '/(' . $key . '[ \n\t]*\{([^\}]*)\})/U';
                        $existingContents = preg_replace($regex, '', $existingContents);
                    }
                }
                // echo $existingContents;

                if (isset($timestamp) && strlen($timestamp) > 2) {

                    if (strpos($datecheck, $timestamp) !== false) {
                        $existingContents = preg_replace('/(\/\*\*timestamp[\d]+\*\*\/)/', '', $existingContents);
                        $existingContents.="/**timestamp" . time() . "**/";
                        file_put_contents(get_stylesheet_directory() . '/style.css', $existingContents);
                        ?>
                        <h3> <?php _e("Styles have been changed to default for current screen") ?></h3>
                        <?php
                        MzAdminInterface::generateUserInterface();
                    } else {
                        echo '<p class ="adminError">' . __("Error Occured") . "</p>";
                        MzAdminInterface::generateUserInterface();
                    }
                } else {
                    $existingContents.="/**timestamp" . time() . "**/";
                    ?>
                    <h3> <?php _e("Styles have been changed to default for current screen") ?></h3>
                    <?php
                    file_put_contents(get_stylesheet_directory() . '/style.css', $existingContents);
                    MzAdminInterface::generateUserInterface();
                }
            }
            ?>
        </section>
            <?php
        }

        function writeTemplate($loadClass, $results, $existing) {

            $html = get_template_directory() . '/admin/data/tpls/base/' . $loadClass . ".tpl";
            $template_info = file_get_contents($html);
            $array = array();
            $doc = new DOMDocument();
            $doc->loadHTML($template_info);
            $xpath = new DomXpath($doc);
            $tags = $xpath->query('//*[@class="field-container"]');
            foreach ($tags as $tag) {
                preg_match_all("/(\[(.*)\+\])/U", $tag->nodeValue, $arResults);
                foreach ($arResults[1] as $key => $value) {
                    $tpl = get_template_directory() . '/admin/data/tpls/design/' . substr($value, 2, ( strlen($value) - 4)) . ".tpl";
                    $tpl = file_get_contents($tpl);

                    preg_match_all('/data-css="([^"]*)"/U', $tpl, $arrChildTpl);
                    foreach ($arrChildTpl[1] as $keyChild => $ChildValue) {
                        if (strcmp($ChildValue, "top") == 0) {
                            $ChildValue = str_replace("top", "background-image", $ChildValue);
                        }
                        if (strcmp($ChildValue, "bottom") == 0) {
                            $ChildValue = str_replace("bottom", "background-image", $ChildValue);
                        }

                        $tpl = AdminTheme\Business\BusinessLayer::mapLabeltoAttribute($tpl, $results, $tag->getAttribute("data-selector"), $ChildValue, $existing);
                    }
                    $template_info = preg_replace('/(\[(.*)\+\])/U', $tpl, $template_info, 1);
                }
            }
            echo $template_info;
        }

        function getFileURL($filename) {
            if ($_FILES[$filename]["error"] > 0) {
                echo "Return Code: " . $_FILES[$filename]["error"] . "<br>";
            } else {
                $target_path = get_stylesheet_directory() . "/img/" . $_FILES[$filename]['name'];
                if (move_uploaded_file($_FILES[$filename]['tmp_name'], $target_path)) {
                    echo $_FILES[$filename]['name'];
                    return ".img/" . basename($_FILES[$filename]['name']);
                } else {
                    echo "There was an error uploading the file, please try again!";
                }
            }
        }

        function generateUserInterface() {
            //$handle = fopen(CHILDCSSPATH,"r+");
            // if(flock($handle, LOCK_EX)) {
            //   echo "here";
            $parse = file_get_contents(CHILDCSSPATH);

            preg_match('/(\/\*\*timestamp[\d]+\*\*\/)/', $parse, $arr1);
            $timeStamp = str_replace('/**timestamp', "", $arr1[1]);
            $timeStamp = str_replace('**/', "", $timeStamp);
            $arrCssParser = parse($parse);
            $parse = file_get_contents(CONSTANTSCSSPATH);
            $arrCssDefault = parse($parse);
            $arBusinessToClassMapper = AdminTheme\Business\BusinessLayer::getClassArray();

            if (!isset($_GET['class'])) {
                $currentClass = "global";
            } else {
                $currentClass = $_GET['class'];
            }
            $currentClass = $arBusinessToClassMapper[$currentClass];
            ?>

        <div class="content">
            <div class="content-inner">
        <?php
        if (!isset($_GET['class'])) {
            $currentClass = "global";
        } else {
            $currentClass = $_GET['class'];
        }
        switch ($currentClass) {
            case "global":
                echo "<h2>" . __("Global Styles") . "</h2>";
                break;
            case "header":
                echo "<h2>" . __("Header") . "</h2>";
                break;
            case "carousel":
                echo "<h2>" . __("Carousel") . "</h2>";
                break;
            case "teaser":
                echo "<h2>" . __("Teaser and Promos") . "</h2>";
                break;
            case "footer":
                echo "<h2>" . __("Footer") . "</h2>";
                break;
        }
        ?>


                <p> <?php _e("All the styles saved here will reflect directly on site ") ?></p>
                <div class="section">
                    <form action ="" method="post" id="form" enctype="multipart/form-data" id="navigation" class="admin-options">

                <?php
                if (strrpos($currentClass, ",") > -1) {
                    $arLoadClass = explode(',', $currentClass);
                    foreach ($arLoadClass as $currentClass) {
                        MzAdminInterface::writeTemplate($currentClass, $arrCssParser, $arrCssDefault);
                    }
                } else {
                    MzAdminInterface::writeTemplate($currentClass, $arrCssParser, $arrCssDefault);
                }
                ?>
                        <div class="preview-frame field-container-wrapper">
                            <h3><?php _e("PREVIEW") ?></h3>
                            <iframe id="preview-frame" src="<?php echo (get_template_directory_uri() . "/admin/iframes/" . $currentClass . ".html") ?>"></iframe>
                        </div>
                        <!-- .preview -->
                        <!-- Hidden Container -->
                        <textarea id="hidden" class="" name="hidden" rows="10" cols="100"></textarea>
                        <input type="hidden" name="timestamp" value="<?php echo $timeStamp ?>"/>
                        <div class="btn">
                            <input type="submit" name="reset" value="<?php _e("Reset") ?>"/>
                            <button type="button" name="preview"><?php _e("Preview") ?></button>
                            <input type="submit" name="submitForm" value="<?php _e("Submit") ?>"/>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
        // }
        //else {
        // echo '<p class="adminError">'.__("File has been opened by some other user").'</p>';
        //fclose($handle);
        //}
    }

}

/* Anonymous Functions */
add_action('admin_menu', function() {
            MzAdminInterface::addMenuPage();
        });
add_action('admin_init', function() {
            new MzAdminInterface();
        });


/* * timestamp
 * Enqueue plugin style-file
 */
?>