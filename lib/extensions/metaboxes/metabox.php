<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Atul Gupta
 * Purpose : China Social Share settings for Mizkan.
 *
 */

class MzMetaBox extends BaseClassExtensions {

    function __construct() {
        // Field Array
        global $custom_meta_fields;
        global $prefix;
        $prefix = 'mz_';

        add_action('add_meta_boxes', array($this, 'add_custom_meta_box'));
        add_action('save_post', array($this, 'save_custom_meta'));
        add_action('admin_head', array($this, 'add_custom_scripts'));
        // enqueue scripts and styles, but only if is_admin
        if (is_admin()) {
            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_script('jquery-ui-slider');

            wp_enqueue_script('custom-js', content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/metaboxes/js/custom-js.js');
            wp_enqueue_style('jquery-ui-custom', content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/metaboxes/css/aristo.css');
        }
        // Custom fields
        $custom_meta_fields = array(
            array(
                'label' => 'Left Navigation Option',
                'desc' => 'By Default: Site Wide Enabled',
                'id' => LEFT_ENABLE_DISABLE,
                'type' => 'radio',
                'options' => array(
                    'one' => array(
                        'label' => 'Enable',
                        'value' => 'enable'
                    ),
                    'two' => array(
                        'label' => 'Disable',
                        'value' => 'disable'
                    )
                )
            ),
            array(
                'label' => 'Pop Up Links',
                'desc' => 'Popup Links on Detail Page. (Example Like - Ingrediants)',
                'id' => POPUP_LINKS,
                'type' => 'repeatable'
            )
        );
    }

    public function add_custom_scripts() {
        global $custom_meta_fields, $post;

        $output = '<script type="text/javascript">
				jQuery(function() {';

        foreach ($custom_meta_fields as $field) { // loop through the fields looking for certain types
            // date
            if ($field['type'] == 'date')
                $output .= 'jQuery(".datepicker").datepicker();';
            // date

            if ($field['type'] == 'checkbox_group')
                $output .= 'jQuery("#check").buttonset();';
            // slider

            if ($field['type'] == 'slider') {
                $value = get_post_meta($post->ID, $field['id'], true);
                if ($value == '')
                    $value = $field['min'];
                $output .= '
					jQuery( "#' . $field['id'] . '-slider" ).slider({
						value: ' . $value . ',
						min: ' . $field['min'] . ',
						max: ' . $field['max'] . ',
						step: ' . $field['step'] . ',
						slide: function( event, ui ) {
							jQuery( "#' . $field['id'] . '" ).val( ui.value );
						}
					});';
            }
        }

        $output .= '});
		</script>';

        echo $output;
    }

// Add the Meta Box
    public function add_custom_meta_box() {
      add_meta_box('custom_meta_box', 'Options', array($this, 'show_custom_meta_box'), PRODUCT_CUSTOM_POST, 'normal', 'high');
      add_meta_box('custom_meta_box', 'Options', array($this, 'show_custom_meta_box'), ARTICLE_POST_TYPE, 'normal', 'high');
      add_meta_box('custom_meta_box', 'Options', array($this, 'show_custom_meta_box'), RECIPES_CUSTOM_POST, 'normal', 'high');
      add_meta_box('custom_meta_box', 'Options', array($this, 'show_custom_meta_box'), 'page', 'normal', 'high');
    }

// The Callback
    public function show_custom_meta_box() {
        global $custom_meta_fields, $post;
        // Use nonce for verification
        echo '<input type="hidden" name="custom_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

        // Begin the field table and loop
        echo '<table class="form-table">';
        foreach ($custom_meta_fields as $field) {
            // get value of this field if it exists for this post
            $meta = get_post_meta($post->ID, $field['id'], true);
            // begin a table row with

            echo '<tr><th>';
            if ($field['type'] == 'repeatable' && (get_post_type() == PRODUCT_CUSTOM_POST || get_post_type() == ARTICLE_POST_TYPE || get_post_type() == RECIPES_CUSTOM_POST))
                echo'<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
            else
            if ($field['type'] != 'repeatable')
                echo'<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
            echo '</th><td>';
            switch ($field['type']) {
                // text
                case 'text':
                    echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" />
								<br /><span class="description">' . $field['desc'] . '</span>';
                    break;
                // editor
                case 'editor':
                    $arSettings = array(
                        'teeny' => true,
                        'textarea_rows' => 15,
                        'tabindex' => 1,
                        'textarea_name' => $field['id'],
                        'media_buttons' => false
                    );
                    wp_editor($meta, $field['id'], $arSettings);
                    echo ' <br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // textarea
                case 'textarea':
                    echo '<textarea name = "' . $field['id'] . '" id = "' . $field['id'] . '" cols = "60" rows = "4">' . $meta . '</textarea>
                <br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // checkbox
                case 'checkbox':
                    echo '<input type = "checkbox" name = "' . $field['id'] . '" id = "' . $field['id'] . '" ', $meta ? ' checked = "checked"' : '', '/>
                <label for = "' . $field['id'] . '">' . $field['desc'] . '</label>';
                    break;
                // select
                case 'select':
                    echo '<select name = "' . $field['id'] . '" id = "' . $field['id'] . '">';
                    foreach ($field['options'] as $option) {
                        echo '<option', $meta == $option['value'] ? ' selected = "selected"' : '', ' value = "' . $option['value'] . '">' . $option['label'] . '</option>';
                    }
                    echo '</select><br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // radio
                case 'radio':

                    echo '<div id = "radio" class = "ui-buttonset">';
                    foreach ($field['options'] as $option) {
                        echo '<input type = "radio" name = "' . $field['id'] . '" id = "' . $option['value'] . '" value = "' . $option['value'] . '" ', $meta == $option['value'] ? ' checked = "checked"' : '', ' />
                <label for = "' . $option['value'] . '">' . $option['label'] . '</label>&nbsp;';
                    }
                    echo '<br><span class = "description">' . $field['desc'] . '</span>';
                    echo '</div>';
                    break;
                // checkbox_group
                case 'checkbox_group':
                    echo '<div id="check" class = "ui-buttonset">';
                    foreach ($field['options'] as $option) {
                        echo '<input type = "checkbox" value = "' . $option['value'] . '" name = "' . $field['id'] . '[]" id = "' . $option['value'] . '"', $meta && in_array($option['value'], $meta) ? ' checked = "checked"' : '', ' />
                <label for = "' . $option['value'] . '">' . $option['label'] . '</label>';
                    }
                    echo '<span class = "description">' . $field['desc'] . '</span>';
                    echo '</div>';
                    break;
                // tax_select
                case 'tax_select':
                    echo '<select name = "' . $field['id'] . '" id = "' . $field['id'] . '">
                <option value = "">Select One</option>'; // Select One
                    $terms = get_terms($field['id'], 'get = all');
                    $selected = wp_get_object_terms($post->ID, $field['id']);
                    foreach ($terms as $term) {
                        if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug))
                            echo '<option value = "' . $term->slug . '" selected = "selected">' . $term->name . '</option>';
                        else
                            echo '<option value = "' . $term->slug . '">' . $term->name . '</option>';
                    }
                    $taxonomy = get_taxonomy($field['id']);
                    echo '</select><br /><span class = "description"><a href = "' . get_bloginfo('home') . '/wp-admin/edit-tags.php?taxonomy=' . $field['id'] . '">Manage ' . $taxonomy->label . '</a></span>';
                    break;
                // post_list
                case 'post_list':
                    $items = get_posts(array(
                        'post_type' => $field['post_type'],
                        'posts_per_page' => -1
                    ));
                    echo '<select name = "' . $field['id'] . '" id = "' . $field['id'] . '">
                <option value = "">Select One</option>'; // Select One
                    foreach ($items as $item) {
                        echo '<option value = "' . $item->ID . '"', $meta == $item->ID ? ' selected = "selected"' : '', '>' . $item->post_type . ': ' . $item->post_title . '</option>';
                    } // end foreach
                    echo '</select><br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // date
                case 'date':
                    echo '<input type = "text" class = "datepicker" name = "' . $field['id'] . '" id = "' . $field['id'] . '" value = "' . $meta . '" size = "30" />
                <br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // slider
                case 'slider':
                    $value = $meta != '' ? $meta : '0';
                    echo '<div id = "' . $field['id'] . '-slider"></div>
                <input type = "text" name = "' . $field['id'] . '" id = "' . $field['id'] . '" value = "' . $value . '" size = "5" />
                <br /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // image
                case 'image':
                    $image = content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/metaboxes/images/image.png';
                    echo '<span class = "custom_default_image" style = "display:none">' . $image . '</span>';
                    if ($meta) {
                        $image = wp_get_attachment_image_src($meta, 'medium');
                        $image = $image[0];
                    }
                    echo '<input name = "' . $field['id'] . '" type = "hidden" class = "custom_upload_image" value = "' . $meta . '" />
                <img src = "' . $image . '" class = "custom_preview_image" alt = "" /><br />
                <input class = "custom_upload_image_button button" type = "button" value = "Choose Image" />
                <small>&nbsp;
                <a href = "#" class = "custom_clear_image_button">Remove Image</a></small>
                <br clear = "all" /><span class = "description">' . $field['desc'] . '</span>';
                    break;
                // repeatable
                case 'repeatable':
                    if (get_post_type() == PRODUCT_CUSTOM_POST || get_post_type() == ARTICLE_POST_TYPE || get_post_type() == RECIPES_CUSTOM_POST) {
                        echo '<a class = "repeatable-add button" href = "#">+</a>
                <ul id = "' . $field['id'] . '-repeatable" class = "custom_repeatable">';
                        $i = 0;
                        if ($meta) {
                            //echo '<pre>';
                            //print_r($meta);
                            //echo '</pre>';
                            foreach ($meta as $row) {
                                //echo '<li><span class = "sort hndle">|||</span>
                                echo '<li><input type = "text" name = "' . $field['id'] . '[' . $i . '][]" id = "' . $field['id'] . '" value = "' . $row[0] . '" size = "30" placeholder="Name" />
                    <a class = "repeatable-remove button" href = "#">-</a> <br>
                 <textarea name="' . $field['id'] . '[' . $i . '][]" id="' . $field['id'] . '" cols="60" rows="4" placeholder="Description">' . $row[1] . '</textarea><br>
                </li>';

                                $i++;
                            }
                        } else {
                            //echo '<li><span class = "sort hndle">|||</span>
                            echo '<li><input type = "text" name = "' . $field['id'] . '[' . $i . '][]" id = "' . $field['id'] . '" value = "" size = "30" placeholder="Name" />
                <a class = "repeatable-remove button" href = "#">-</a> <br>
                 <textarea name="' . $field['id'] . '[' . $i . '][]" id="' . $field['id'] . '" cols="60" rows="4" placeholder="Description"></textarea><br>
                </li>';
                        }
                        echo '</ul>
                <span class = "description">' . $field['desc'] . '</span>';
                    }
                    break;
            } //end switch
            echo '</td></tr>';
        } // end foreach
        echo '</table>'; // end table
    }

// Save the Data
    function save_custom_meta($post_id) {
        global $custom_meta_fields;

        // verify nonce
        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
            return $post_id;
        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;
        // check permissions
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // loop through fields and save the data
        foreach ($custom_meta_fields as $field) {
            if ($field['type'] == 'tax_select')
                continue;
            $old = get_post_meta($post_id, $field['id'], true);
            $new = $_POST[$field['id']];
            if ($new && $new != $old) {
                update_post_meta($post_id, $field['id'], $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        } // enf foreach
    }

}

$ob = new MzMetaBox();
?>