<?php
/*
 * Large Spotlight
 */
class RichSpotlight extends WP_Widget {
    /**
     * This is Rich Page Spotlight Widget Name option key configurator
     */
    const RICH_SPOTLIGHT_WIDGET_NAME = 'Rich Spotlight';
    /**
     * This is Rich Page Spotlight Widget ID configurator
     */
    const RICH_SPOTLIGHT_WIDGET_ID = 'rich_spotlight';
    
    public function __construct() {

        parent::__construct(
                self::RICH_SPOTLIGHT_WIDGET_ID, // Base ID
                self::RICH_SPOTLIGHT_WIDGET_NAME, // Name
                array('description' => __('Widget to dispaly rich spotlights',LANGUAGE_DOMAIN_NAME))// Args
        );
        /*
         * Call on admin intialization
         */
        add_action('widgets_init', array($this, 'RegisterRichSpotlightInit'));
    }
    /*
     * Register Rich Spotlight with wordpress
     */
    public function RegisterRichSpotlightInit(){
        register_widget('RichSpotlight');
    }
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        static $is_script_added = false;
        if($is_script_added==false){
            wp_enqueue_script('custom_js', content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/rich-spotlight/js/custom.js');
            $is_script_added = true;
        }
        global $spotlightSection;
        // outputs the content of the widget
        if ($instance['spotlight_id'] == 0 || $instance['spotlight_id'] == "") {
            return;
        }
        $obSpotlight = get_post($instance['spotlight_id']);

        $arPostMeta = get_post_meta($obSpotlight->ID);
        $image = wp_get_attachment_image_src($arPostMeta['_thumbnail_id'][0], 'full');
        $mobile_image = wp_get_attachment_image_src($arPostMeta['mobileimage'][0], 'full');
        $alt_text = get_post_meta($arPostMeta['_thumbnail_id'][0], '_wp_attachment_image_alt', true);
        if (empty($alt_text)) {
            $alt_text = $obSpotlight->post_title;
        }
        if (isset($arPostMeta[CTA_TARGET_ATTRIBUTE][0]) && $arPostMeta[CTA_TARGET_ATTRIBUTE][0] == 1) {
            $target = 'target ="_blank"';
        } else {

            $target = '';
        }
        if (class_exists('Utility')) {
            global $obUtility;
            $cta_url = $obUtility->getCtaUrl($arPostMeta[CTA_URL_ATTRIBUTE][0], $arPostMeta[CTA_URL_TYPE][0]);
        }
        if (class_exists('ContainerTag')) {
            global $obContainerTag;
            $container_tag_string = $obContainerTag->getTagsForCta($obSpotlight->ID, $arPostMeta[CTA_URL_TYPE][0], $obSpotlight->post_title, SPOTLIGHT_CUSTOM_POST);
            //modifying the spotlight id for container tag for clicking on spotlight image
            $container_tag_string_image = $obContainerTag->getTagsForCta('image_' . $obSpotlight->ID, $arPostMeta[CTA_URL_TYPE][0], $obSpotlight->post_title, SPOTLIGHT_CUSTOM_POST);
        }

        $column_width = isset($instance['span_size']) && $instance['span_size'] != 0 ? $instance['span_size'] : '3'; //passing default span size when no column width is passed
        $spotlight_width = COLOUMN_GRID_WIDTH / $column_width; //getting width of each spotlight
        $cta_lable = isset($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) ? $arPostMeta[CTA_LABEL_ATTRIBUTE][0] : '';
        switch ($args['id']) {

            case HOME_WIDGET_PLACEHOLDER_ID:
                $class_name = "span" . $spotlight_width;  //rendering span classes according to the column layout selected
                break;
            default:
                $class_name = "";
                break;
        }
        if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_YOUTUBE) {
            $video_id = $args['widget_id'];
            if (empty($image)) {
                $image_url = YOUTUBE_IMAGE_PREFIX . $arPostMeta[CTA_URL_ATTRIBUTE][0] . YOUTUBE_IMAGE_SUFFIX;
            } else {
                $image_url = $image[0];
            }
            ?>
            <article class="<?php echo $class_name; ?>">
                <div class="teaser3 video spotlight-<?php echo $obSpotlight->ID ?> <?php echo $instance['show_style']; ?>  section_content_container">
                    <?php if (!empty($image_url)) { ?>
                        <figure>
                            <img class="lazy" src="<?php
                            if ($image) {
                                echo $image[0];
                            }
                            ?>" data-desk="<?php echo $image[0]; ?>" data-mob="<?php echo $mobile_image[0]; ?>" alt="<?php echo strip_tags($alt_text); ?>" title="<?php echo strip_tags($obSpotlight->post_title); ?>" />
                        </figure>
                    <?php } ?>
                    <?php if (!empty($obSpotlight->post_title) || !empty($obSpotlight->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                        <div class="caption inner_caption_container container">
                            <?php if($instance['show_title']) { ?>
                                <?php if (!empty($obSpotlight->post_title) && ($instance['show_style'] == 'sectiontop')) { ?><h2 id="FromBean_title"><?php echo $instance['show_title']." ".$obSpotlight->post_title ?></h2><?php } elseif (!empty($obSpotlight->post_title) && ($instance['show_style'] != 'sectiontop')) { ?><h3><?php echo $obSpotlight->post_title ?></h3><?php } ?>
                            <?php } ?>
                            <?php if (!empty($obSpotlight->post_content)) { ?><?php echo $obSpotlight->post_content ?><?php } ?>
                            <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                                <a <?php echo $container_tag_string; ?> href="#<?php echo $video_id ?>" role="button" class="btn btn-small cta_frombean" title="<?php
                                if (!empty($obSpotlight->post_title)) {
                                    echo strip_tags($obSpotlight->post_title);
                                } else {
                                    echo strip_tags($cta_lable);
                                }
                                ?>" data-toggle="my-modal"><?php echo $cta_lable; ?></a>
                                                                    <?php } ?>
                        </div>
                    <?php } ?>
                    <!-- /.caption -->
                    <div class="panel"></div>
                    <!-- Modal -->
                    <div id="<?php echo $video_id ?>" class="my-modal hide" tabindex="-1" role="dialog">
                        <div class="my-modal-header">
                            <button type="button" class="close" data-dismiss="my-modal">x</button>
                            <?php if (!empty($obSpotlight->post_title)) { ?>
                                <h3><?php echo $obSpotlight->post_title; ?> </h3>
                            <?php } ?>
                        </div>
                        <div class="my-modal-body">
                            <div class="model-iframe" data-component="Spotlight" data-width="100%" data-height="349" title="<?php echo $obSpotlight->post_title ?>" data-video-id="<?php echo $arPostMeta[CTA_URL_ATTRIBUTE][0]; ?>">
                                <div id="player<?php echo $video_id; ?>"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.teaser4 -->
            </article>
            <!-- /.span4 -->
            <?php
        } else if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_VIDEO) {
            $video_id = $args['widget_id'];
            $image_url = $image[0];
            ?>
            <article class="<?php echo $class_name; ?>">
                <div class="teaser3 video spotlight-<?php echo $obSpotlight->ID ?>  <?php echo $instance['show_style']; ?> section_content_container">
                    <?php if (!empty($image_url)) { ?>
                        <figure>
                            <img class="lazy" src="<?php
                            if ($image) {
                                echo $image[0];
                            }
                            ?>" data-desk="<?php echo $image[0]; ?>" data-mob="<?php echo $mobile_image[0]; ?>" alt="<?php echo strip_tags($alt_text); ?>" title="<?php echo strip_tags($obSpotlight->post_title); ?>" />
                        </figure>
                    <?php } ?>
                    <?php if (!empty($obSpotlight->post_title) || !empty($obSpotlight->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                        <div class="caption inner_caption_container container">
                            <?php if($instance['show_title']) { ?>
                                <?php if (!empty($obSpotlight->post_title) && ($instance['show_style'] == 'sectiontop')) { ?><h2 id="FromBean_title"><?php echo $obSpotlight->post_title ?></h2><?php } elseif (!empty($obSpotlight->post_title) && ($instance['show_style'] != 'sectiontop')) { ?><h3><?php echo $obSpotlight->post_title ?></h3><?php } ?>
                            <?php } ?>
                            <?php
                            if (!empty($obSpotlight->post_content)) {
                                echo $obSpotlight->post_content;
                            }
                            ?>
                            <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                                <a <?php echo $container_tag_string; ?> href="#<?php echo $video_id ?>" role="button" class="btn btn-small cta_frombean" title="<?php
                                if (!empty($obSpotlight->post_title)) {
                                    echo strip_tags($obSpotlight->post_title);
                                } else {
                                    echo strip_tags($cta_lable);
                                }
                                ?>" data-toggle="my-modal"  video-href="<?php echo $arPostMeta[CTA_URL_ATTRIBUTE][0] ?>"><?php echo $cta_lable; ?></a>
                                                                    <?php } ?>
                        </div>
                    <?php } ?>
                    <!-- /.caption -->
                    <div class="panel"></div>
                    <!-- Modal -->
                    <div id="<?php echo $video_id ?>" class="my-modal hide" tabindex="-1" role="dialog">
                        <div class="my-modal-header">
                            <button type="button" class="close" data-dismiss="my-modal">x</button>
                            <?php if (!empty($obSpotlight->post_title)) { ?>
                                <h3><?php echo $obSpotlight->post_title; ?> </h3>
                            <?php } ?>
                        </div>
                        <div class="my-modal-body">
                            <span class="model-iframe" data-href="<?php echo $cta_url ?>" data-width="100%" data-height="349"></span>
                        </div>
                    </div>
                </div>
                <!-- /.teaser4 -->
            </article>
            <!-- /.span4 -->
            <?php
        } else {
            ?>
            <section id="aboutSpotlight">
            <article class="container">
               
                
                    <?php if (!empty($obSpotlight->post_title) || !empty($obSpotlight->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                       
                            <?php if($instance['show_title']) { ?>
                               <h2><?php echo $obSpotlight->post_title ?></h2>
                            <?php } ?>
                            <?php
                            if (!empty($obSpotlight->post_content)) {
                                echo $obSpotlight->post_content;
                            }
                            ?>
                            <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                                <a <?php echo $container_tag_string; ?><?php echo $target; ?> href="<?php echo $cta_url ?>" class="big-yellow-btn" title="<?php
                                    if (!empty($obSpotlight->post_title)) {
                                        echo strip_tags($obSpotlight->post_title);
                                    } else {
                                        echo strip_tags($cta_lable);
                                    }
                                    ?>" data-toggle="modal" data-target="#bioLightbox" ><?php echo $cta_lable; ?></a>
                                <?php } ?>
                 
                    <?php } ?>
                    <!-- /.caption -->
          
                <!-- /.teaser3 -->

                <div class="modal fade" id="bioLightbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      </div>
      <div class="modal-body ">
      <div >
                    <img src="images/bishopjakes_bg.jpg"/>
                </div>
                <div >
                <h3>BISHOP T. D. JAKES  CEO, TDJ Enterprises</h3>
                <p>Bishop T.D. Jakes is pastor at the Potter's House in Dallas, Texas. While presiding over a congregation of 30,000 parishioners, his church services are broadcast on TV around the world. His annual revival, "MegaFest“, draws more than 100,000 people. Talented in a variety of ways, many of his 30 published books have made the NY Times best seller list, and his six feature films have generated strong box office returns in addition to recognition including an NAACP Image Award.</p>          
                </div>  
      </div>
      
    </div>
  </div>
</div>


            </article>
            </section>
            <!-- /.span3 -->
            <?php
        }
    }
    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     *
     * @return string Back-end widget form
     */
    public function form($instance) {
        $selectedSpotlight = isset($instance['spotlight_id']) ? $instance['spotlight_id'] : '';
        $selectedSpotlightWidth = isset($instance['span_size']) ? $instance['span_size'] : ''; //for spotlight width or column layout
        $selectedSpotlightTitle = $instance['show_style'];  //for spotlight title show/hide
        $args = array('post_type' => SPOTLIGHT_CUSTOM_POST, 'posts_per_page' => -1);
        $arSpotlights = get_posts($args);
        // If no menus exists, direct the user to go and create some.
        if (!$arSpotlights) {
            _e('No SpotLight have been created yet.');
            return;
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('spotlight_id'); ?>"><?php _e('Select Spotlight:', LANGUAGE_DOMAIN_NAME); ?></label>
            <select id="<?php echo $this->get_field_id('spotlight_id'); ?>" name="<?php echo $this->get_field_name('spotlight_id'); ?>">
                <?php
                foreach ($arSpotlights as $obSpotlights) {
                    echo '<option value="' . $obSpotlights->ID . '"'
                    . selected($selectedSpotlight, $obSpotlights->ID, false)
                    . '>' . $obSpotlights->post_title . '</option>';
                }
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('spotlight_id'); ?>"><?php _e('Column Layout:', LANGUAGE_DOMAIN_NAME); ?></label>
            <select id="<?php echo $this->get_field_id('span_size'); ?>" name="<?php echo $this->get_field_name('span_size'); ?>">
                <?php
                for ($i = 3; $i >= 1; $i--) {
                    echo '<option value="' . $i . '"'
                    . selected($selectedSpotlightWidth, $i, false)
                    . '>' . $i . '</option>';
                }
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('spotlight_id'); ?>"><?php _e('Section Style:', LANGUAGE_DOMAIN_NAME); ?></label>
            <select id="<?php echo $this->get_field_id('spotlight_id'); ?>" name="<?php echo $this->get_field_name('show_style'); ?>">
                <?php
                $selectedSpotlightTitle = array("sectiontop" => "sectiontop", "sectionmiddle" => "sectionmiddle", "sectionbottom_left" => "sectionbottom_left", "sectionbottom_right" => "sectionbottom_right");
                foreach ($selectedSpotlightTitle as $show_style => $value) {
                    $selected = '';
                    $instance['show_style'] == $show_style ? $selected = " selected='selected'" : "";
                    echo "<option value ='{$value}'" . $selected . ">" . $value . "</option>";
                }
                ?>

            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('spotlight_id'); ?>"><?php _e('Show Title:', LANGUAGE_DOMAIN_NAME); ?></label>
            <?php
                if ($instance['show_title']) {
                    $checked = 'checked';
                }
            ?>
            <input type="checkbox" name="<?php echo $this->get_field_name('show_title'); ?>" id="<?php echo $this->get_field_id('spotlight_id'); ?>" value="1" <?php echo $checked;?>>
        </p>
        <?php
    }
    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     *
     * @see WP_Widget::update()
     */
    public function update($new_instance, $old_instance) {
        // processes widget options to be saved
        $instance['spotlight_id'] = $new_instance['spotlight_id'];
        $instance['span_size'] = $new_instance['span_size'];
        $instance['show_style'] = $new_instance['show_style'];
        $instance['show_title'] = $new_instance['show_title'];
        return $instance;
    }
}