<?php
/*
 * Loads large spotlight files
 */
require_once 'class-rich-spotlight-widget-base.php';
require_once 'class-rich-spotlight-widget-option.php';
//Creating object enable the checkbox and then we can check and load rest of the lib
    try {
            require_once 'rich-spotlight.php';
            if(class_exists('RichSpotlight')){
                new RichSpotlight;
            }
    }
    catch(Exception $e){
        echo 'Caught Exception :'.$e->getMessage();
    }
?>
