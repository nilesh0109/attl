var MG = MG || {};
MG.App = (function ($) {
    var globalvariables = {
        body: $('html, body'),
        caption: $('.caption'),
        scrollElement: '.scroll_down',
    };
    return {
        appendScroller: function ()
        {


            globalvariables.caption.each(function (i, val) {
                var $divSelected = $(this).find('div');
                if ($divSelected.hasClass('scrollRequired'))
                {
                    var scroll_p_Text = "Scroll this down to begin journey";
                    var scroll_now_anchor_text = "scroll now";
                    var scrollContainer = $('<div>', {'class': 'scroll_container'});
                    scrollContainer.append($('<p>', {'id': 'scroll_down_text', 'class': 'section_desc'}).html(scroll_p_Text));
                    scrollContainer.append($('<a>', {'href': '#', 'class': 'scroll_down'}).html(scroll_now_anchor_text));

                    var scrollContainer_notext = $('<div>', {'class': 'scroll_container'});
                    scrollContainer_notext.append($('<a>', {'href': '#', 'class': 'scroll_down'}).html(scroll_now_anchor_text));

                    if (whichDevice() != "mobile") {
                        if ($(this).parent().hasClass('sectiontop'))
                        {
                            $(this).after(scrollContainer);
                        }
                        else
                        {
                            $(this).after(scrollContainer_notext);
                        }
                    }
                    else
                    {
                        if ($(this).parent().hasClass('sectiontop'))
                            $(this).append(scrollContainer);
                        else
                            $(this).append(scrollContainer_notext);
                    }

                }
            });
        },
        scrollBehavior: function ()
        {

            $(document).on('click', globalvariables.scrollElement, function (event) {
                event.preventDefault();
                globalvariables.body.animate({scrollTop: $(this).parents('article').next().offset().top}, 'slow');
            });
        },
      
        init: function () {
            MG.App.appendScroller();
            MG.App.scrollBehavior();
        }
    };
})(jQuery);

(function (w, d, $) {
    $(document).ready(function () {
        MG.App.init();
    });
}(window, document, jQuery));
