jQuery(function ($) {

  
    // the upload image button, saves the id and outputs a preview of the image
    var imageFrame;
    var bindImageChooser = function () {
        $('.meta_box_upload_image_button').on('click', function (event) {
            event.preventDefault();

            var options, attachment;

            $self = $(event.target);
            $div = $self.closest('div.meta_box_image');

            // if the frame already exists, open it
            if (imageFrame) {
                imageFrame.open();
                return;
            }

            // set our settings
            imageFrame = wp.media({
                title: 'Choose Image',
                multiple: false,
                library: {
                    type: 'image'
                },
                button: {
                    text: 'Use This Image'
                }
            });

            // set up our select handler
            imageFrame.on('select', function () {
                selection = imageFrame.state().get('selection');

                if (!selection)
                    return;

                // loop through the selected files
                selection.each(function (attachment) {
                   
                    var src = attachment.attributes.sizes.medium.url;
                    var id = attachment.id;

                    $div.find('.meta_box_preview_image').attr('src', src);
                    $div.find('.meta_box_upload_image').val(id);
                });
            });

            // open the frame
            imageFrame.open();
        });

        // the remove image link, removes the image id from the hidden field and replaces the image preview
        $('.meta_box_clear_image_button').click(function () {
            var defaultImage = $(this).parent().siblings('.meta_box_default_image').text();
            $(this).parent().siblings('.meta_box_upload_image').val('');
            $(this).parent().siblings('.meta_box_preview_image').attr('src', defaultImage);
            return false;
        });

        
    }
    bindImageChooser();
    // the remove image link, removes the image id from the hidden field and replaces the image preview
    var bindImageRemover = function () {
        $('.meta_box_clear_file_button').click(function () {
            $(this).parent().siblings('.meta_box_upload_file').val('');
            $(this).parent().siblings('.meta_box_filename').text('');
            $(this).parent().siblings('.meta_box_file').removeClass('checked');
            return false;
        });
    }
    bindImageRemover();
  
    

  
});