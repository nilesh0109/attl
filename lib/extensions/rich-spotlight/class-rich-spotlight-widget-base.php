<?php

/**
 * Abstract Class
 */
abstract class RichSpotlightWidget_Base extends BaseClassExtensions {

    public function __construct() {
        parent::__construct();
    }

    public static function getInstance() {
        static $instance;
        if (null === $instance) {
            $instance = new static();
        }
        return $instance;
    }

    private function __clone() {

    }

    private function __wakeup() {

    }

}

