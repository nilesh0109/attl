<?php

/**
 * Base Class
 */
class RichSpotlightWidget_Option extends RichSpotlightWidget_Base {
    /**
     * This is brandsite option key configurator
     */
    const RICH_SPOTLIGHT_OPTION_KEY = 'richspotlight';
    /**
     * Nonce Key for the Rich Spotlight.
     */
    const RICH_SPOTLIGHT_NONCE_FIELD = 'richspotlightkey';
    /**
     * Nonce action for the Spotlight.
     */
    const RICH_SPOTLIGHT_NONCE_ACTION = 'richspotlightact';
    /**
     * Store the complete brandsite option. Need this as property because it is needed
     * on the several event.
     *
     * @var array
     * @access private
     */
    private $brand_site_option = array();

    /**
     * Method construct
     *
     * @return void Nothing returns
     */
    public function __construct() {
        parent::__construct();
        $this->brand_site_option = get_option(BRAND_SITE_SETTING_KEY);
            add_action('admin_init', array($this,'createMobileImageMetaBox'));
            add_action('save_post', array($this,'save_mobile_image'));
    }
    /**
    * Add Custom Field For Mobile Image In Admin Section
    */
    function createMobileImageMetaBox() {
        // enqueue scripts and styles, but only if is_admin
         if (is_admin()) {
             wp_enqueue_script('spotligt-js', content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/rich-spotlight/js/scripts.js');
         }
         add_meta_box('mobile_image', __('Mobile Image'), array($this,'uploadMobileImage'), SPOTLIGHT_CUSTOM_POST, 'normal', 'low');
    }
    /*
     * This code is for upload mobile image in rich spotlight section
     */
    function uploadMobileImage() {
        global $post;
        $meta = get_post_meta($post->ID, 'mobileimage', true); 
        $image = content_url() . "/" . LIB_NAME . '/' . EXTENSIONS . '/metaboxes/images/image.png';
        if ($meta) 
        {
            $image = wp_get_attachment_image_src($meta, 'medium');
            $image = $image[0];
        }
        ?>
        <div class="meta_box_image">
            <span class="custom_default_image" style="display:none"><?php echo $image;?></span>
            <input name="mobileimage" type="hidden" class="meta_box_upload_image" value="<?php echo $meta;?>" />
            <img src="<?php echo $image;?>" class="meta_box_preview_image" alt="" /><br />
            <input class="meta_box_upload_image_button button" type="button" value="Choose Image" />
            <small><a href="#" class="meta_box_clear_image_button">Remove Image</a></small>
            <br clear="all" />
        </div>
        <?php
    }
    /*
     * This function is for save mobile image in database of rich spotlight section 
     * $post_id is for spotlight mobile image id
     */
    function save_mobile_image($post_id) {
        $old = get_post_meta($post_id, 'mobileimage', true);
        $new = $_POST['mobileimage']; 
        if ($new && $new != $old) {
            update_post_meta($post_id, 'mobileimage', $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, 'mobileimage', $old);
        } 
    }
}
