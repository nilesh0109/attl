<?php

class CheckboxOption extends DropdownOption {

    function CheckboxOption($_name, $_desc, $_id, $_options, $_stdIndex = 0) {
        $this->DropdownOption($_name, $_desc, $_id, $_options, $_stdIndex);
    }

    function WriteHtml() {
        ?>
        <tr valign="top">
            <th scope="row"><?php echo esc_html($this->name); ?></th>
            <td>

                <?php
                $counter = 1;
                foreach ($this->options as $option) :
                    $key = (isset($option['key']) && is_array($option)) ? $option['key'] : $option;
                    $value = (isset($option['value']) && is_array($option)) ? $option['value'] : $option;
                    $default_key = (isset($this->options[$this->std]['key']) && is_array($option)) ? $this->options[$this->std]['key'] : $this->options[$this->std];
                    ?>
                    <input type="checkbox" name="<?php echo esc_attr($this->id) ?>[]" id="<?php echo esc_attr($this->id . '_' . $counter) ?>" <?php
                    if ((get_option($this->id) == $key) || (is_array(get_option($this->id)) && in_array($key, get_option($this->id)) ) || (!get_option($this->id) && $default_key == $key )) {
                        echo ' checked="checked"';
                    }
                    ?> value="<?php echo esc_html($key); ?>" /><label for="<?php echo esc_attr($this->id . '_' . $counter) ?>"><?php echo esc_html($value); ?></label><br />
                           <?php
                           $counter++;
                       endforeach;
                       ?>

            </td>
        </tr>
        <tr valign="top">
            <td colspan=2>
                <small><?php echo esc_html($this->desc); ?></small><hr />
            </td>
        </tr>
        <?php
    }

}
?>