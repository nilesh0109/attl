<?php

class ColorOption extends TextOption {

    function ColorOption($_name, $_desc, $_id, $_std = '') {
        $this->TextOption($_name, $_desc, $_id, $_std);
    }

    function WriteHtml() {
        $stdText = $this->std;

        $stdTextOption = get_option($this->id);
        if (!empty($stdTextOption))
            $stdText = $stdTextOption;
        ?>
        <tr valign="top">
            <th scope="row"><?php echo esc_html($this->name . ':'); ?></th>
            <?php
            $commentWidth = 2;
            ?>
            <td><input name="<?php echo esc_attr($this->id); ?>" id="<?php echo esc_attr($this->id); ?>" type="text" value="<?php echo esc_attr($stdText); ?>" size="40" class="colorpickerField1 pickerfield" />

            </td>
        </tr>
        <tr valign="top"><td colspan="<?php echo absint($commentWidth); ?>"><small><?php echo esc_html($this->desc); ?></small></td></tr><tr valign="top"><td colspan="2"><hr /></td></tr>
        <?php
    }

}
?>