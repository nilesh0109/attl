<?php

class Option {

    var $name;
    var $desc;
    var $id;
    var $_key;
    var $std;
    var $callback;

    function Option($_name, $_desc, $_id, $_std, $callback = '') {
        $this->name = $_name;
        $this->desc = $_desc;
        $this->id = $_id;
        $this->_key = $_id;
        $this->std = $_std;
        $this->callback = $callback;
    }

    function WriteHtml() {
        echo '';
    }

    function Update($ignored) {
        $value = stripslashes_deep($_POST[$this->id]);
    
        $value = (is_array($value)) ? $value : trim($value);
        if('' != $this->callback )
        {
            $value = call_user_func($this->callback , $value);
        }
        update_option($this->id, $value);
    }

    function Reset($ignored) {
        if('' != $this->callback )
        {
            $value = call_user_func($callback , $value);
        }
        update_option($this->id, $this->std);
    }

    function Import($data) {
        if (array_key_exists($this->id, $data->dict)){
            update_option($this->id, $data->dict[$this->id]);
        }
    }

    function Export($data) {
        $data->dict[$this->id] = get_option($this->id);
    }

    function get() {
        return get_option($this->id);
    }

}

?>