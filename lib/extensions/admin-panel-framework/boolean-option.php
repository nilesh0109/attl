<?php

class BooleanOption extends DropdownOption {

    var $default;

    function BooleanOption($_name, $_desc, $_id, $_default = false) {
        $this->default = $_default;
        $this->DropdownOption($_name, $_desc, $_id, array('Disabled', 'Enabled'), $_default ? 1 : 0 );
    }

    function get() {
        $value = get_option($this->id, $this->default);
        if (is_bool($value)){
            return $value;
        }
            
        switch (strtolower($value)) {
            case 'true':
            case 'enable':
            case 'enabled':
                return true;
            default:
                return false;
        }
    }

}

?>