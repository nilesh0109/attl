<?php

class ThemeOption {

    private static $instances = array();
    public $option_namespace;
    private $parent = null;
    private $capability = 'manage_options';
    private $slug;
    private $icon_url;
    private $position;
    private $panel_created = 0;
    private $name = '';
    private $groups = array();

    function __construct($namesapce) {
        $namesapce = str_replace(' ', '-', trim(strtolower($namesapce)));
        $this->option_namespace = $namesapce;
    }

    public function create_admin_panel($settings) {
        if ($this->panel_created == 0) {
            $this->parent = isset($settings['parent']) ? $settings['parent'] : null;
            $this->capability = isset($settings['capability']) ? $settings['capability'] : $this->capability;
            $this->slug = isset($settings['slug']) ? $settings['slug'] : $this->option_namespace;
            $this->icon_url = isset($settings['icon_url']) ? $settings['icon_url'] : '';
            $this->position = isset($settings['position']) ? $settings['position'] : 81;
            $this->name = isset($settings['name']) ? $settings['name'] : 'Mz Option';
            add_action('admin_menu', array($this, 'register_menu'));
            $this->panel_created = 1;
        } else {
            trigger_error('Panel settings has already been registered. To redefine the setting please call remove_admin_panel method');
        }
    }

    public function remove_admin_panel() {
        if ($this->panel_created == 1) {
            $this->parent = null;
            $this->capability = 'manage_options';
            $this->slug = null;
            $this->icon_url = null;
            $this->position = null;
            $this->name = '';
            remove_action('admin_menu', array($this, 'register_menu'));
            $this->panel_created = 0;
        } {
            trigger_error('Pannel settings are not registered yet');
        }
    }

    public function create_group($group_name, $group_id) {
        if (!isset($this->groups[$group_id])) {
            $this->groups[$group_id] = new Group($group_name, $group_id);
        } else {
            trigger_error('Group ID ' . $group_id . ' is already registered', E_USER_ERROR);
        }
    }

    public function get_group($group_id) {
        if (isset($this->groups[$group_id])) {
            return $this->groups[$group_id];
        } else {
            return false;
        }
    }

    public function add_option($setting, $group_id) {
        $setting_type = isset($setting['type']) ? strtolower($setting['type']) : 'text';
        $setting_label = isset($setting['label']) ? $setting['label'] : '';
        $setting_help = isset($setting['help']) ? $setting['help'] : '';
        $setting_option_name = isset($setting['option_name']) ? $setting['option_name'] : '';
        $setting_default_value = isset($setting['default']) ? $setting['default'] : '';
        $setting_textarea_true = (isset($setting['textarea']) && ( $setting_type == 'text')) ? true : false;
        $setting_ddl_option = (isset($setting['option_array']) && is_array($setting['option_array'])) ? $setting['option_array'] : array();
        $setting_callback = isset($setting['callback']) ? $setting['callback'] : '';
        $class_name = ucfirst($setting_type) . "Option";

        switch ($class_name) {
            case 'TextOption':
                if (class_exists($class_name) && isset($this->groups[$group_id])) {


                    $this->groups[$group_id]->add_option(
                            new $class_name($setting_label, $setting_help, $setting_option_name, $setting_default_value, $setting_textarea_true, $setting_callback)
                    );
                }
                break;

            case 'ColorOption':
                if (class_exists($class_name) && isset($this->groups[$group_id])) {
                    $this->groups[$group_id]->add_option(
                            new $class_name($setting_label, $setting_help, $setting_option_name, $setting_default_value)
                    );
                }
                break;

            case 'BooleanOption':
                if (class_exists($class_name) && isset($this->groups[$group_id])) {
                    $this->groups[$group_id]->add_option(
                            new $class_name($setting_label, $setting_help, $setting_option_name, $setting_default_value)
                    );
                }
                break;
            case 'CheckboxOption':
            case 'RadioOption':
            case 'DropdownOption':
                if (class_exists($class_name) && isset($this->groups[$group_id])) {
                    $this->groups[$group_id]->add_option(
                            new $class_name($setting_label, $setting_help, $setting_option_name, $setting_ddl_option, $setting_default_value)
                    );
                }
                break;

            case 'DraggabledivOption':
                if (class_exists($class_name) && isset($this->groups[$group_id])) {
                    $this->groups[$group_id]->add_option(
                            new $class_name($setting_label, $setting_help, $setting_option_name, $setting_ddl_option)
                    );
                }
                break;
        }
    }

    function register_menu() {


        if (isset($_GET['page']) && $_GET['page'] == $this->slug) {
            if (did_action('before_theme_option_render') == 0) {
                do_action('before_theme_option_render');
            }

            $options = $this->groups;
            $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
            $method = false;
            $done = false;
            $data = false;
            switch ($action) {
                case 'save':
                    $method = 'Update';
                    break;
                case 'Reset':
                    $method = 'Reset';
                    break;
            }

            if ($method) {
                foreach ($options as $group) {
                    foreach ($group->options as $option) {
                        call_user_func(array($option, $method), $data);
                    }
                }
            }
        }

        if ($this->parent == null) {
            $this->panel_id = add_menu_page($this->name, $this->name, $this->capability, $this->slug, array($this, 'generate_admin_page'), $this->icon_url, $this->position);
        } else {

            $this->panel_id = add_submenu_page($this->parent, $this->name, $this->name, $this->capability, $this->slug, array($this, 'generate_admin_page'));
        }

        add_action('admin_print_scripts-' . $this->panel_id, array($this, 'load_required_js'));
        add_action('admin_footer-' . $this->panel_id, array($this, 'add_js_function'));
    }

    function load_required_js() {

        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-sortable');
        
        //Check if the ColorOption is available then add the colorpicker js
        if ($this->has_option_added('color')) {
            wp_enqueue_script('colorbox', content_url() . '/' . LIB_NAME . '/js/admin/colorpicker.js');
            wp_enqueue_style('colorboxcs', content_url() . '/' . LIB_NAME . '/css/admin/colorpicker.css');
        }
    }

    function has_option_added($option_class) {
        foreach ($this->groups as $group) {
            foreach ($group->options as $option) {
                $class_name = ucfirst(strtolower($option_class)) . 'Option';
                if (is_a($option, $class_name)) {
                    return true;
                }
            }
        }
        return false;
    }

    function add_js_function() {
        ?>

        <script type="text/javascript">
            /* <![CDATA[ */
            jQuery(document).ready(function($) {
                $("#config-tabs").tabs();
                $('.sortable').sortable({'update': function(evt , ui){
                        var list = ui.item.parent('.sortable');
                        var target = $('#'+list.attr('target-id'));
                        var target_val = '';
                        var all_lis = list.find('li').each(function(index){
                            target_val += target_val == '' ? $(this).attr('option-key') : ','+$(this).attr('option-key');
                        });
                        target.val(target_val);
                        
                    }});
            });
                            
            /* ]]> */
        </script>
        <script type="text/javascript">
            var colorPicker = function(sel) {
                jQuery(sel).each(function() {
                    jQuery(this).css('background-color', jQuery(this).val());
                    var _this = this;

                    jQuery(this).ColorPicker({
                        color: jQuery(_this).val(),
                        onShow: function(colpkr) {
                            jQuery(colpkr).fadeIn(500);
                            return false;
                        },
                        onHide: function(colpkr) {
                            jQuery(colpkr).fadeOut(500);
                            return false;
                        },
                        onChange: function(hsb, hex, rgb) {
                            jQuery(_this).val("#" + hex);
                            jQuery(_this).css('background-color', '#' + hex);
                        }
                    });

                });
            };
            colorPicker('input.pickerfield');
        </script>
        <?php
    }

    public static function getInstance($namespace) {
        $namespace = str_replace(' ', '-', trim(strtolower($namespace)));
        foreach (self::$instances as $instance) {
            if ($instance->option_namespace == $namespace) {
                return $instance;
            }
        }
        $new_instance = new ThemeOption($namespace);
        self::$instances[] = $new_instance;
        return $new_instance;
    }

    function generate_admin_page() {


        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'save') {
            echo '<div id="message" class="updated fade"><p><strong>' . esc_html($this->name . ' settings saved.') . '</strong></p></div>';
        }
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'reset') {
            echo '<div id="message" class="updated fade"><p><strong>' . esc_html($this->name . ' settings reset.') . '</strong></p></div>';
        }

        if (did_action('before_theme_option_render') == 0) {
            do_action('before_theme_option_render');
        }
        ?>

        <div class="wrap">
            <h2><b><?php echo esc_html($this->name . ' Theme Options'); ?></b></h2>

            <form method="post" id="<?php echo esc_attr($this->option_namespace) ?>">

                <div id="config-tabs">
                    <ul>
                        <?php
                        $groups = $this->groups;
                        foreach ($groups as $group) :
                            ?>
                            <li><a href='<?php echo esc_attr('#' . $group->id); ?>'><?php echo esc_html($group->name); ?></a></li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                    <?php
                    foreach ($groups as $group) :
                        ?>
                        <div id='<?php echo esc_attr($group->id); ?>'>
                            <?php
                            $group->WriteHtml();
                            ?>
                        </div>
                        <?php
                    endforeach;
                    ?>
                </div>
                <p class="submit alignleft">
                    <input type="hidden" name="action" value="save" />
                    <input name="save" type="submit" value="Save changes" />
                </p>
            </form>
            <form enctype="multipart/form-data" method="post">
                <p class="submit alignleft">
                    <input name="action" type="submit" value="Reset" />
                </p>


            </form>


            <?php
        }

    }

    