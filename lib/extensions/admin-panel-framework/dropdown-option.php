<?php

class DropdownOption extends Option {

    var $options;

    function DropdownOption($_name, $_desc, $_id, $_options, $_stdIndex = 0) {
        $this->Option($_name, $_desc, $_id, $_stdIndex);
        $this->options = $_options;
    }

    function WriteHtml() {
        ?>
        <tr valign="top">
            <th scope="row"><?php echo esc_html($this->name); ?></th>
            <td>
                <select name="<?php echo esc_attr($this->id); ?>" id="<?php echo esc_attr($this->id); ?>">
                    <?php
                    foreach ($this->options as $option) :
                        $key = (isset($option['key']) && is_array($option)) ? $option['key'] : $option;
                        $value = (isset($option['value']) && is_array($option)) ? $option['value'] : $option;
                        $default_key = (isset($this->options[$this->std]['key']) && is_array($option)) ? $this->options[$this->std]['key'] : $this->options[$this->std];
                        ?>
                        <option<?php
                        if (get_option($this->id) == $key || (!get_option($this->id) && $default_key == $key )) {
                            echo ' selected="selected"';
                        }
                        ?> value="<?php echo esc_attr($key) ?>" ><?php echo esc_html($value); ?></option>
                            <?php
                        endforeach;
                        ?>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <td colspan=2>
                <small><?php echo esc_html($this->desc); ?></small><hr />
            </td>
        </tr>
        <?php
    }

    function get() {
        $value = get_option($this->id, $this->std);
        if (strtolower($value) == 'disabled')
            return false;
        return $value;
    }

}
?>