<?php

class TextOption extends Option {

    var $useTextArea;

    function TextOption($_name, $_desc, $_id, $_std = '', $_useTextArea = false , $callback = '') {
        $this->Option($_name, $_desc, $_id, $_std , $callback);
        $this->useTextArea = $_useTextArea;
    }

    function WriteHtml() {
        $stdText = $this->std;

        $stdTextOption = $this->get();
        if (!empty($stdTextOption)){
            $stdText = $stdTextOption;
        }
            
        ?>
        <tr valign="top">
            <th scope="row"><?php echo esc_html($this->name . ':'); ?></th>
            <?php
            $commentWidth = 2;
            if ($this->useTextArea) :
                $commentWidth = 1;
                ?>
                <td rowspan="2"><textarea style="width:100%;height:100%;" name="<?php echo esc_attr($this->id); ?>" id="<?php echo esc_attr($this->id); ?>"><?php echo esc_textarea($stdText); ?></textarea>
                    <?php
                else :
                    ?>
                <td><input name="<?php echo esc_attr($this->id); ?>" id="<?php echo esc_attr($this->id); ?>" type="text" value="<?php echo esc_attr($stdText); ?>" size="40" />
                <?php
                endif;
                ?>
            </td>
        </tr>
        <tr valign="top"><td colspan="<?php echo absint($commentWidth); ?>"><small><?php echo esc_html($this->desc); ?></small></td></tr><tr valign="top"><td colspan="2"><hr /></td></tr>
        <?php
    }

    function get() {
        $value = get_option($this->id);
        if (empty($value)){
         return urldecode($this->std);   
        }
        return urldecode($value);
    }

}
?>