<?php

require_once dirname(__FILE__) . '/group.php';
require_once dirname(__FILE__) . '/option.php';
require_once dirname(__FILE__) . '/text-option.php';
require_once dirname(__FILE__) . '/color-option.php';
require_once dirname(__FILE__) . '/dropdown-option.php';
require_once dirname(__FILE__) . '/boolean-option.php';
require_once dirname(__FILE__) . '/radio-option.php';
require_once dirname(__FILE__) . '/checkbox-option.php';
require_once dirname(__FILE__) . '/auto-config.php';
require_once dirname(__FILE__) . '/theme-options.php';
require_once dirname(__FILE__) . '/draggablediv-option.php';