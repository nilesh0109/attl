<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class DraggabledivOption extends Option {

    var $options;

    function DraggableDivOption($_name, $_desc, $_id, $_options, $_stdIndex = 0) {
        $this->Option($_name, $_desc, $_id, $_stdIndex);
        $this->options = $_options;
    }

    function WriteHtml() {
        $hidden_key = '';

        $order = get_option($this->id);

        if ('' !== trim($order)) {
            $arr_order = explode(',', $order);
            $sorted_option = array();
            $raw_opt = $this->options;
            foreach ($arr_order as $index) {
                foreach ($this->options as $key => $option) {
                    if ($option['key'] == $index) {
                        $sorted_option[] = $option;
                        unset($raw_opt[$key]);
                    }
                }
            }
            
            foreach ($raw_opt as $opt) {
                $sorted_option[] = $opt;
            }
            
            $this->options = $sorted_option;
        }
        ?>
        <tr valign="top">
            <th scope="row"><?php echo esc_html($this->name); ?></th>
            <td>
                <ul class="sortable" id="<?php echo $this->id ?>_sortable" target-id="<?php echo $this->id ?>" ><?php
        foreach ($this->options as $option) {
            $key = (isset($option['key']) && is_array($option)) ? $option['key'] : $option;
            $value = (isset($option['value']) && is_array($option)) ? $option['value'] : $option;
            $hidden_key .= $hidden_key == '' ? $key : ',' . $key;
            ?>
                        <li class = "menu-item-handle" option-key="<?php echo $key ?>" >
                            <span class="item-title" style="line-height:37px;padding-left:13px" >
                                <?php echo $value; ?>
                            </span>    
                        </li>
                        <?php
                    }
                    ?>
                </ul>

                <input type="hidden" name="<?php echo $this->id ?>" id="<?php echo $this->id ?>" value="<?php echo get_option($this->id) ?>" />
            </td>
        </tr>
        <?php
    }

}
?>
