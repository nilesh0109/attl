<?php

class Group {

    var $name;
    var $id;
    var $options;

    function Group($_name, $_id, $_options = array()) {
        $this->name = $_name;
        $this->id = "cap_$_id";
        if (count($_options) > 0) {
            $this->options = $_options;
        }
    }

    public function add_option($option) {
        $this->options[] = $option;
    }

    function WriteHtml() {
        ?>
        <table class="form-table" width="100%">

            <?php
            for ($i = 0; $i < count($this->options); $i++) {
                $this->options[$i]->WriteHtml();
            }
            ?>
        </table>
        <?php
    }

}
?>