<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Multicategory_Options {

    const MULTICATEGORY_EXTENSION_OPTION = 'multicategory_extension_option';

    public function __construct() {
        $this->brand_site_options = get_option(BRAND_SITE_SETTING_KEY);
        add_action('admin_init', array($this, 'add_multicategory_options'), 10);
        add_action('update_option_' . BRAND_SITE_SETTING_KEY, array($this, 'UpdateMultipleCategory'), 1, 2);
    }
    
    /**
     * Method used to create the singlton implmentation
     * @staticvar static $instance
     * @return \static $instance
     */
    public function get_instance(){
        static $instance;
        if($instance === null){
            $instance = new static();
        }        
        return $instance;
    }

    public function add_multicategory_options() {
        add_settings_field(self::MULTICATEGORY_EXTENSION_OPTION, __('Enable Multicategory Extension'), array($this, 'displayCheckBox'), BRAND_SITE_OPTIONS, 'brand_site_extension', array(self::MULTICATEGORY_EXTENSION_OPTION));
    }

    /**
     * Function to create checkbox for usermanagement module option.
     * @param type $attribute
     */
    public function displayCheckBox($attribute) {
        $field_id = $attribute[0];
        if (isset($this->brand_site_options[$field_id])) {
            $checked = 'checked';
        }
        echo '<input type="checkbox" '
        . $checked
        . ' '
        . 'value="1" id="multicategory_extension_option" name="'
        . esc_attr(BRAND_SITE_SETTING_KEY)
        . '['
        . esc_attr($field_id)
        . ']">';
    }

    /**
     * Function to verify whether usermanagement module is enabled or not
     * @return boolean
     */
    public function is_multicategory_enable() {
        if (isset($this->brand_site_options[self::MULTICATEGORY_EXTENSION_OPTION]) && $this->brand_site_options[self::MULTICATEGORY_EXTENSION_OPTION] == 1) {
            return true;
        }
        return false;
    }

    public function UpdateMultipleCategory($old_value, $new_value) {
        if ((1 == $old_value[self::MULTICATEGORY_EXTENSION_OPTION] || 1== $new_value[self::MULTICATEGORY_EXTENSION_OPTION]) && $old_value[self::MULTICATEGORY_EXTENSION_OPTION] != $new_value[self::MULTICATEGORY_EXTENSION_OPTION]) {
            $args = array(
                'post_type' => PRODUCT_CUSTOM_POST,
                'numberposts' => -1,
            );
            $posts = get_posts($args);
            foreach ($posts as $post) {
                if ($post->post_type == PRODUCT_CUSTOM_POST) {
                    $taxonomy = PRODUCT_TAXONOMY;
                }
                $categories = wp_get_object_terms($post->ID, $taxonomy, array('orderby' => 'term_order'));
                wp_set_post_terms($post->ID, $categories[0]->term_id, $taxonomy);
            }
        }
    }

}

?>
