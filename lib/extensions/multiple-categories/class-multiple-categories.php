<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class MultipleCategories {

    function __construct() {
        add_action('admin_init', array($this, 'updateMetaBoxes'));
        add_action('edit_post', array($this, 'SaveMultipleCategory'), 1);
        add_action('edit_post', array($this, 'editLeftNavMenuItemPost'), 1, 3);
    }

    function updateMetaBoxes() {
        if (class_exists('Custom_Content_UI_Handler')) {
            $obCustom_Content_UI_Handler = new Custom_Content_UI_Handler();
            add_meta_box('post_category', __('Product Category'), array($obCustom_Content_UI_Handler, 'postCategoriesMetaBox'), PRODUCT_CUSTOM_POST, 'side', 'low', array('taxonomy' => PRODUCT_TAXONOMY, 'options' => 'checkbox'));
            add_meta_box('primary_category', __('Choose Primary Category'), array($this, 'createPrimaryCategoryBox'), PRODUCT_CUSTOM_POST, 'side', 'low');
        }
    }

    /**
     * This function define UI part of Interface Provided to select Primary Category from the Categories associated with Products/ Articles
     */
    function createPrimaryCategoryBox() {
        global $post;
        if ($post->post_type == PRODUCT_CUSTOM_POST) {
            $taxonomy = PRODUCT_TAXONOMY;
        }
        $arCategories = wp_get_object_terms($post->ID, $taxonomy, array('orderby' => 'term_order'));
        $i = 1;
        echo '<ul>';
        foreach ($arCategories as $obCategory) {
            $checked = '';
            if ($i == 1) {
                $checked = 'checked';
            }
            echo "<li><input name='sort_category[]' type='radio' " . $checked . " value='" . $obCategory->term_id . "'>&nbsp;" . $obCategory->name . "</li>";
            $i++;
        }
        echo '</ul>';
    }

    /**
     * This function calls savePrimaryCategory to save primary category in database term_relationship table
     * @global type $post
     */
    function SaveMultipleCategory() {
        global $post;
        switch ($post->post_type) {
            case PRODUCT_CUSTOM_POST: // Post type is Products
                $this->savePrimaryCategory();
                break;
        }
    }

    /**
     * This function is used to define Primary Category for Products/Articles
     * Products/ Articles can have multiple Category Associated with them
     * User will be having a provision to Choose Primary Category from the Associated Categories.
     * Database Explanation: In term_relationship Table the coulmn name term_order will be used to define primary category,
     * If value is 0 then its a primary category other all associated categories will have value set to 1
     */
    function savePrimaryCategory() {
        if (isset($_POST['sort_category']) && $_POST['sort_category'] > 0) {
            global $wpdb, $post;
            $taxonomy = 'category';
            if ($post->post_type == PRODUCT_CUSTOM_POST) {
                $taxonomy = PRODUCT_TAXONOMY;
            }
            $arCategories = wp_get_object_terms($post->ID, $taxonomy, array('orderby' => 'term_order'));
            $query_category = 'update ' . $wpdb->term_relationships . ' set term_order = 1 where object_id =' . $post->ID;
            $wpdb->query($wpdb->prepare($query_category, array()));
            foreach ($arCategories as $obCategory) {
                if ($obCategory->term_id == $_POST['sort_category'][0]) {
                    $query_primary_category = 'update ' . $wpdb->term_relationships . ' set term_order = 0 where object_id =' . $post->ID . ' and term_taxonomy_id =' . $obCategory->term_taxonomy_id;
                    $wpdb->query($wpdb->prepare($query_primary_category, array()));
                    $set_primary = true;
                }
            }
            if (!$set_primary) {
                $query_primary_category = 'update ' . $wpdb->term_relationships . ' set term_order = 0 where object_id =' . $post->ID . ' and term_taxonomy_id =' . $arCategories[0]->term_id;
                $wpdb->query($wpdb->prepare($query_primary_category, array()));
            }
        }
    }

    /**
     * edit the nav menu item to the left nav  when  post is edited
     * @param int ($postId) post id of the creadted post
     * @param object ($post) whole created post object
     */
    function editLeftNavMenuItemPost($postId, $post) {
        global $obMzLeftNavigation;
        if ($post->post_type != PRODUCT_CUSTOM_POST) {
            return;
        } else {
            $tax = PRODUCT_TAXONOMY;
        }

        // variable to check number of terms and create post slug accordingly
        $order = 1;
        $terms = wp_get_post_terms($post->ID, $tax, array('orderby' => 'term_order', 'order' => 'ASC'));
        // removing the action editLeftNavMenuItemPost from generate_left_nav_items.php
       //if (class_exists('MzLeftNavigation') && !empty($terms)) {
       // $check = remove_action('edit_post', array($obMzLeftNavigation, 'editLeftNavMenuItemPost'), 2);
       // }

        if (empty($terms)) {
            $order = 2;
        }
        // set  the slugs of associated nav menu post type with the edited term
        foreach ($terms as $term) {
            $postAppend = $postId;
            if ($order != 1) {
                $postAppend = $postId . '-' . $order;
            }

            $nav_post_slug = NAV_ITEM_POST_TYPE_PREFIX . $postAppend;
            $args = array(
                'name' => $nav_post_slug,
                'post_type' => 'nav_menu_item',
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            );

            // get  the   nav menu post type associated with the edited post
            $arLeftNavPost = get_posts($args);
            if (empty($arLeftNavPost)) {
                $arLeftNavPostId = $obMzLeftNavigation->addLeftNavMenuItemPost($postId, $post);
                $nav_post_slug = NAV_ITEM_POST_TYPE_PREFIX . $postAppend;

                $args = array(
                    'name' => $nav_post_slug,
                    'post_type' => 'nav_menu_item',
                    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
                );
                $arLeftNavPost = get_posts($args);
            }
            $obLeftNavTerm = wp_get_post_terms($arLeftNavPost[0]->ID, NAVGATION_MENU_TAXNOMY, array('orderby' => 'term_order', 'order' => 'ASC'));
            if (empty($obLeftNavTerm)) {
                return;
            }

            $obLeftNavMenuItems = wp_get_nav_menu_items($obLeftNavTerm[0]->term_id);
            // set  the slug of  nav menu post type associated with parent term of edited term
            $nav_parent_post_slug = NAV_ITEM_TERM_TYPE_PREFIX . $term->term_taxonomy_id;
            $args = array(
                'name' => $nav_parent_post_slug,
                'post_type' => 'nav_menu_item',
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            );
            // get   nav menu post type associated with parent term of edited post
            $arLeftNavParentPost = get_posts($args);
            // check if parent post exists if not set it to zero
            $menuParent = (is_array($arLeftNavParentPost)) ? $arLeftNavParentPost[0]->ID : 0;
            // update the status of the  nav menu post  associated with the edited post
            $leftNavPostStatus = ('publish' == $post->post_status ) ? 'publish' : 'draft';


            foreach ($obLeftNavMenuItems as $obLeftNavMenuItem) {
                if ($obLeftNavMenuItem->db_id == $arLeftNavPost[0]->ID && $obLeftNavMenuItem->menu_item_parent != $menuParent) {
                    $args = array(
                        'menu-item-db-id' => $arLeftNavPost[0]->ID,
                        'menu-item-object-id' => $postId,
                        'menu-item-object' => $post->post_type,
                        'menu-item-parent-id' => $menuParent,
                        'menu-item-position' => $arLeftNavPost[0]->menu_order,
                        'menu-item-type' => 'post_type',
                        'menu-item-title' => $post->post_title,
                        'menu-item-url' => '',
                        'menu-item-description' => '',
                        'menu-item-attr-title' => '',
                        'menu-item-target' => '',
                        'menu-item-classes' => '',
                        'menu-item-xfn' => '',
                        'menu-item-status' => $leftNavPostStatus,
                    );
                    if (isset($arLeftNavPost) && $arLeftNavPost != null) {
                        wp_update_nav_menu_item($obLeftNavTerm[0]->term_id, $arLeftNavPost[0]->ID, $args);
                    }
                }
            }
            $order = $order + 1;
        }
        $this->delete_extra_posts($postId, $order);
        // add_action('edit_post', array(&$this, 'leftNavPostUpdate'), 1, 3);
    }

    function delete_extra_posts($postId, $order) {
        do {
            $postAppend = $postId;
            if ($order != 1) {
                $postAppend = $postId . '-' . $order;
            }
            $nav_post_slug = NAV_ITEM_POST_TYPE_PREFIX . $postAppend;
            $args = array(
                'name' => $nav_post_slug,
                'post_type' => 'nav_menu_item',
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            );
            // get  the   nav menu post type associated with the edited post
            $arLeftNavPost = get_posts($args);
            if (!empty($arLeftNavPost)) {
                wp_delete_post($arLeftNavPost[0]->ID);
            }
            $order = $order + 1;
        } while (!empty($arLeftNavPost));
    }

}

$obContent = new MultipleCategories();
?>
