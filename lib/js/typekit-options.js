jQuery(document).ready(function(){
	var typekitId = jQuery('#typekitId').val();
	var hidTypekitFonts = jQuery('#typekit-fonts');
	
	if(typekitFailSafe){
		hidTypekitFonts.show();
	}else{
		hidTypekitFonts.hide();
	}
	
	
	if(typekitId == ''){
		if(!typekitFailSafe){
			hidTypekitFonts.hide();
			hidTypekitFonts.attr('disabled', 'disabled');
		}else {
			hidTypekitFonts.show();
			hidTypekitFonts.removeAttr('disabled');
		}
	}
});

function fetchFonts() {
	var btnSaveOptions = jQuery('#btnSaveTypekit'), 
	//txtTypekitFonts = jQuery('#txt-typekit-fonts'),
	typekitFonts = jQuery('#typekit-fonts'),
	typekitId = jQuery('#typekitId').val();
	
	if(typekitId != ''){
		disableSubmit();
		window.Typekit = undefined;
		
		var typekitUrl = typekitBaseUrl + typekitId + '.js';
		loadJsFile(typekitUrl,'typekit-js');
		
		var timedFn = 0;
		timedFn = setTimeout(function(){
			if (typeof window.Typekit !== 'undefined'
				&& typeof window.Typekit.config !== "undefined") {
				try {
					var arFontNames = window.Typekit.config.c;
		
					var fontNames = '';
					var fontList = [];
					for ( var i = 0; i < arFontNames.length; i++) {
						if(i%2 == 1){
							fontNames += arFontNames[i] + '\r\n';
							fontList.push(arFontNames[i]);
						}
					}
		
					//txtTypekitFonts.val(fontNames);
					createList(fontList);
					typekitFonts.val(fontNames.replace('\r\n','|'));
				} catch (e) {
					// Show hidden field if there is exception and fail-safe is ON in the back-end.
					if(typekitFailSafe){
						typekitFonts.removeAttr('disabled');
						typekitFonts.show();
					}
				}
			}
			else{
				//txtTypekitFonts.val('');
				clearList();
				jQuery('#font-list').append('<li>-</li>');
				typekitFonts.val('');
			}
			
			btnSaveOptions.removeAttr('disabled');
		},4000);
	}else{
		//txtTypekitFonts.val('');
		clearList();
		jQuery('#font-list').append('<li>-</li>');
		typekitFonts.val('');
	}
}

function createList(fonts){
	var ulTypekitFonts = jQuery('#font-list');
	
	clearList();
	
	var items = [];
	jQuery.each(fonts, function(i, font) {
		items.push('<li>' + font + '</li>');
		});
	
	ulTypekitFonts.append( items.join('') );
}

function clearList(){
	jQuery('#font-list > li').remove();
}

function handleUrlChange(previousId){
	var typekitEnabled = jQuery('#typekit-enabled');
	
	if(typekitEnabled.val()=='Enabled'){
		var typekitId = jQuery('#typekitId').val();
		if(previousId == typekitId){
			jQuery('#btnSaveTypekit').removeAttr('disabled');
		}else {
			jQuery('#btnSaveTypekit').attr('disabled','disabled');
		}
	}
}

function disableSubmit(){
	jQuery('#btnSaveTypekit').attr('disabled', 'disabled');
}

function loadJsFile(url, id) {
	if(url=='' || id==''){
		return;
	}
	
	var fjs = document.getElementsByTagName('head')[0];

	var js = jQuery('#'+id);
	if(js.length>0){
		js.remove();
	}

	js = document.createElement('script');
	js.id = id;
	js.src = url;
	
	fjs.parentNode.insertBefore(js, fjs);
}