jQuery(function($) {

    var imageFrame;

    var bindImageChooser = function() {
        $('.widget_upload_image_button').click(function(event) {
            event.preventDefault();

            var options, attachment;

            $self = $(event.target);
            $div = $self.closest('div.widget_image');

            // if the frame already exists, open it
            if (imageFrame) {
                imageFrame.open();
                return;
            }

            // set our settings
            imageFrame = wp.media({
                title: 'Choose Image',
                multiple: false,
                library: {
                    type: 'image'
                },
                button: {
                    text: 'Use This Image'
                }
            });

            // set up our select handler
            imageFrame.on('select', function() {
                selection = imageFrame.state().get('selection');

                if (!selection)
                    return;

                // loop through the selected files
                selection.each(function(attachment) {
                    var src = "";
                    if (attachment.attributes.sizes)
                    {
                        src = attachment.attributes.sizes.full.url;
                    }
                    else
                    {
                        src = attachment.attributes.url;
                    }
                    //var src = attachment.attributes.sizes.full.url;
                    var id = attachment.id;
                    $div.find('.widget_preview_image').attr('src', src);
                    $div.find('.widget_upload_image').val(id);
                });
            });

            // open the frame
            imageFrame.open();
        });
    }
    bindImageChooser();

    // the remove image link, removes the image id from the hidden field and replaces the image preview
    var bindImageRemover = function() {
        $('.widget_clear_image_button').click(function() {
            var defaultImage = $(this).parent().siblings('.widget_default_image').text();
            $(this).parent().siblings('.widget_upload_image').val('');
            $(this).parent().siblings('.widget_preview_image').attr('src', defaultImage);
            return false;
        });

    }
    bindImageRemover();


});