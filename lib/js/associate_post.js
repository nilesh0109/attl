jQuery(document).ready(function($) {
	
	jQuery("#all_slides li input").click( function() {     
		var post_id = $(this).attr('id').split('_')[1];
		var post_name = $(this).attr('label');		
		if($(this).attr('checked'))
		{
			var li_html = '<li id="associated_slide_'+post_id+'" class="menu-item-handle"><span class="item-title">'+post_name+'</span><input type="hidden" value="'+post_id+'" name="slide_items[]"></li>';
			jQuery("#custom-order-list").append(li_html);
		}
		else
		{
			jQuery("#associated_slide_"+post_id).remove();
		}
		
	})    
    
});