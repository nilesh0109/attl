jQuery(document).ready(function($) {
	
	$(".add_taxonomy_image").live("click",function(event) {
			var tt_id = $(this).attr('data');
			var frame;				
				frame = wp.media();
				frame.on( "select", function() {
					// Grab the selected attachment.
					var attachment = frame.state().get("selection").first();							
					console.log(attachment.attributes);
					if(attachment.attributes.sizes.thumbnail)
					{
					$("#taxonomy_image_path_"+tt_id).attr("src",attachment.attributes.sizes.thumbnail.url);
					}
					else
					{
					$("#taxonomy_image_path_"+tt_id).attr("src",attachment.attributes.sizes.full.url);				
					}
					var attachment_id = attachment.attributes.id;
					$.ajax({
						type : 'POST',
						url : ajaxurl,
						data : {action: 'taxonomy_image_association', tt_id:tt_id, attachment_id:attachment_id},
						success : function(data) {
											if(data.status)
											{												
												$('#remove-image-'+tt_id).show();
												$('#ajax-response').css('background-color','yellow');
											}
											else
											{
												$('#ajax-response').css('background-color','red');
											}							
											$('#ajax-response').html(data.msg);
											frame.close();
										},
						error : function(data) { $('#ajax-response').html("error")  
						frame.close();
						}
					});			
					
					
				});
				frame.open();			
		});	

		$(".remove_taxonomy_image").live("click",function(event) {
			var tt_id = $(this).attr('data');			
					$.ajax({
						type : 'POST',
						url : ajaxurl,
						data : {action: 'remove_taxonomy_image', tt_id:tt_id},
						success : function(data) {
											if(data.status)
											{												
												$('#remove-image-'+tt_id).hide();
												var default_image = $('#default_image').val();
												$("#taxonomy_image_path_"+tt_id).attr("src",default_image);
												
												$('#ajax-response').css('background-color','yellow');
											}
											else
											{
												$('#ajax-response').css('background-color','red');
											}							
											$('#ajax-response').html(data.msg);
											
										},
						error : function(data) { $('#ajax-response').html("error")  }
					});											
		});	
});