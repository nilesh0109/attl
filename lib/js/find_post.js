
var findItems;
(function($){
	findItems = {
		open : function(name1, val1,name2, val2) {
			var st = document.documentElement.scrollTop || $(document).scrollTop(),
				overlay = $( '.ui-find-overlay' );

			if ( overlay.length == 0 ) {
				$( 'body' ).append( '<div class="ui-find-overlay"></div>' );
				findItems.overlay();
			}
			overlay.show();

			if ( name1 && val1 ) {
				$('#item_type').attr('name', name1).val(val1);
			}
			if ( name2 && val2 ) {
				$('#item_attribute').attr('name', name2).val(val2);
			}
			$('#find-posts').show().draggable({
				handle: '#find-posts-head'
			}).css({'top':'50px','left':'50%','marginLeft':'-300px'});
		
			$('#find-posts-input').focus().keyup(function(e){
				if (e.which == 27) { findItems.close(); } // close on Escape
			});

			// Pull some results up by default
			findItems.send();

			return false;
		},

		close : function() {
			$('#find-posts-response').html('');
			$('#find-posts').draggable('destroy').hide();
			$( '.ui-find-overlay' ).hide();
		},

		overlay : function() {
			$( '.ui-find-overlay' ).css(
				{ 'z-index': '999', 'width': $( document ).width() + 'px', 'height': $( document ).height() + 'px' }
			).on('click', function () {
				findItems.close();
			});
		},

		send : function() {
			var found_action = $('#found_action').val();
			var item_type_name = $('#item_type').attr('name');			
			var item_type_value = $('#item_type').val();		
			var meta_key = $('#item_attribute').attr('name');
			var meta_value = $('#item_attribute').val();
			
			var post = {
					ps: $('#find-posts-input').val(),
					action: 'find-custom-posts',
					found_action:found_action,
					item_type_name:item_type_name,
					item_type_value: item_type_value,
					meta_key: meta_key,
					meta_value: meta_value,
					_ajax_nonce: $('#_ajax_nonce').val()
				},
			spinner = $( '.find-box-search .spinner' );
			spinner.show();

			$.ajax({
				type : 'POST',
				url : ajaxurl,
				data : post,
				success : function(x) { findItems.show(x); spinner.hide(); },
				error : function(r) { findItems.error(r); spinner.hide(); }
			});
		},

		show : function(x) {

			if ( typeof(x) == 'string' ) {
				this.error({'responseText': x});
				return;
			}

			var r = wpAjax.parseAjaxResponse(x);
			
			

			if ( r.errors ) {
				this.error({'responseText': wpAjax.broken});
			}
			r = r.responses[0];
						
			$('#find-posts-response').html(r.data);			
			// Enable whole row to be clicked
			$( '.found-posts td' ).on( 'click', function () {
				$( this ).parent().find( '.found-radio input' ).prop( 'checked', true );
			});
		},

		error : function(r) {
			var er = r.statusText;

			if ( r.responseText ) {
				er = r.responseText.replace( /<.[^<>]*?>/g, '' );
			}
			if ( er ) {
				$('#find-posts-response').html(er);
			}
		}
	};

	$(document).ready(function() {
		$('#find-posts-submit').click(function(e) {
			if ( '' == $('#find-posts-response').html() )
				e.preventDefault();
		});
		$( '#find-posts .find-box-search :input' ).keypress( function( event ) {
			if ( 13 == event.which ) {
				findItems.send();
				return false;
			}
		} );
		$( '#find-posts-search' ).click( findItems.send );
		$( '#find-posts-close' ).click( findItems.close );
		$('#doaction, #doaction2').click(function(e){
			$('select[name^="action"]').each(function(){
				if ( $(this).val() == 'attach' ) {
					e.preventDefault();
					findItems.open();
				}
			});
		});
	});
	$(window).resize(function() {
		findItems.overlay();
	});
})(jQuery);
