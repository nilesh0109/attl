<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : vipul sohal
 * Purpose : get carousel and produce html for  it .
 *
 */

class Carousel {

    /**
     * This function gets all the slides with in a carousel in the specified Order
     * @return array of slides.
     */
    function getCarousel($carousel_type, $page_type = '', $page_id = '') {

        global $wpdb;
        if ($carousel_type == CAROUSEL_TAXONOMY) {
            switch ($page_type) {
                case PRODUCT_CATEGORY_PAGE_NAME:
                    $carousel_slug = CAROUSEL_PRODUCT_CATEGORY_SLUG_PREFIX . $page_id;

                    break;
                case ARTICLE_CATEGORY_PAGE_NAME:
                    $carousel_slug = CAROUSEL_ARTICLE_CATEGORY_SLUG_PREFIX . $page_id;
                    break;
                case HOME_PAGE_NAME:
                    $carousel_slug = CAROUSEL_HOME_PAGE;
                    break;
                case PRODUCT_LANDING_PAGE_NAME :
                    $carousel_slug = CAROUSEL_PRODUCT_LANDING;
                    break;
                case PRODUCT_DETAIL_PAGE_NAME :
                    $carousel_slug = TEASER_CAROUSEL_PRODUCT_DETAIL;
                    break;
                
                case ARTICLE_LANDING_PAGE_NAME :
                    $carousel_slug = CAROUSEL_ARTICLE_LANDING;
                    break;
                case RECIPES_LANDING_PAGE_NAME :
                    $carousel_slug = CAROUSEL_RECIPES_LANDING;
                    break;  
            }
            $term = get_term_by('slug', $carousel_slug, CAROUSEL_TAXONOMY);
            $arCarouselQuery = array('posts_per_page' => -1, 'post_type' => CAROUSEL_SLIDE_CUSTOM_POST, CAROUSEL_TAXONOMY => $carousel_slug);
            $arCarousel = get_posts($arCarouselQuery);
        } else if ($carousel_type = TEASER_TAXONOMY) {
            if($page_type == TEASER_CAROUSEL_RECIPES_PAGE) {
              $carousel_slug = TEASER_CAROUSEL_RECIPES_PAGE;
            } else {
              $carousel_slug = TEASER_CAROUSEL_HOME_PAGE;
            }
            $term = get_term_by('slug', $carousel_slug, TEASER_TAXONOMY);
            $arCarouselQuery = array('posts_per_page' => -1, 'post_type' => TEASER_CUSTOM_POST, TEASER_TAXONOMY => $carousel_slug);
            $arCarousel = get_posts($arCarouselQuery);
        }
        if (empty($arCarousel)) {
            return;
        }

        $arSelectedSlideQuery = 'SELECT object_id,term_order FROM ' . $wpdb->term_relationships . ' where term_taxonomy_id =' . $term->term_taxonomy_id . ' order by term_order ASC';

        $arSlideID = $wpdb->get_results($arSelectedSlideQuery);

        $max_order = 1000;
        foreach ($arSlideID as $obSlideId) {
            if ($obSlideId->term_order) {
                $arId[$obSlideId->term_order] = $obSlideId->object_id;
            } else {

                $arId[$max_order] = $obSlideId->object_id;
                $max_order++;
            }
        }
        foreach ($arCarousel as $index => $arSlide) {
            $arSlide->term_taxonomy_id = $term->term_taxonomy_id;

            $orderd_index = array_search($arSlide->ID, $arId);
            $arOrderedSlides[$orderd_index] = $arSlide;
        }
        ksort($arOrderedSlides);
        return $arOrderedSlides;
    }

}

?>