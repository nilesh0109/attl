<?php

class RenderSocialMedia {

    public $arSocialMediaSettings;
    public $arSocialtems;
    public $facebook_js;
    public $twitter_js;
    public $google_plus_js;
    public $tumbler_js;
    public $addthis_js;
    public $pinit_js;

    function __construct() {
        $this->arSocialMediaSettings = get_option(MZ_SOCIAL_ICONS_OPTIONS);
        $this->arSocialtems = array('signup', 'facebook', 'twitter', 'pinit', 'tumblr', 'googleplus', 'youtube', 'addthis');
    }

    function socialMediaHeader() {
        global $image;
        global $title;
        global $obContainerTag;
        $site_lang = DEFAULT_LANGUAGE;
        $addthis_lang = explode("_", $site_lang);

        if ($this->arSocialMediaSettings[MZ_FOLLOW_SHARE_SELECT] == 'follow') {
            if (!empty($this->arSocialMediaSettings[MZ_FACEBOOK_LOGO]) || !empty($this->arSocialMediaSettings[MZ_TWITTER_LOGO]) || !empty($this->arSocialMediaSettings[MZ_GOOGLE_LOGO]) || !empty($this->arSocialMediaSettings[MZ_PIN_LOGO]) || !empty($this->arSocialMediaSettings[MZ_TUMBLR_LOGO]) || !empty($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO])) {
                ?>
                <div class="social-icons  social-button follow-icon">
                    <?php
                    foreach ($this->arSocialtems as $item) {
                        switch ($item) {
                            case 'signup':
                                //$alt_text = get_post_meta($this->arSocialMediaSettings[MZ_FACEBOOK_LOGO], '_wp_attachment_image_alt', true);
                                if (empty($alt_text)) {
                                    $alt_text = 'Signup for emails';
                                }
                                ?>
                                <a class ="signup-link"  href="<?php echo site_url($item); ?>" title="<?php _e($alt_text, LANGUAGE_DOMAIN_NAME) ?>">
                                    <img src="<?php echo get_template_directory_uri() . '/img/default/signup-icon.png' ?>" alt="<?php echo $alt_text ?>"><strong><?php _e('Sign Up for Emails'); ?></strong>
                                </a>
                                <?php
                                break;

                            case 'facebook':

                                if (!empty($this->arSocialMediaSettings[MZ_FACEBOOK_URL]) && !empty($this->arSocialMediaSettings[MZ_FACEBOOK_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_FOLLOW] == 'fbfollow') {
                                    $image_fb = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_FACEBOOK_LOGO], 'full');
                                    $image_fb = $image_fb[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_FACEBOOK_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'facebook';
                                    }
                                    ?>
                                    <a class ="social-facebook"  href="<?php echo FACBOOK_SITE_URL . $this->arSocialMediaSettings[MZ_FACEBOOK_URL]; ?>" target="_blank" title="<?php _e('facebook', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img src="<?php echo $image_fb; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;

                            case 'twitter' :
                                if (strtoupper($this->arSocialMediaSettings[MZ_TWITTER_URL]) === 'BERTOLLI') {
                                    $data_show_screen_name = "false";
                                } else {
                                    $data_show_screen_name = "true";
                                }
                                if (!empty($this->arSocialMediaSettings[MZ_TWITTER_URL]) && !empty($this->arSocialMediaSettings[MZ_TWITTER_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_TWITTER_FOLLOW] == 'twitterfollow') {
                                    $image_twitter = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_TWITTER_LOGO], 'full');
                                    $image_twitter = $image_twitter[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_TWITTER_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'twitter';
                                    }
                                    ?>

                                    <a class ="social-twitter"  href="<?php echo TWITTER_SITE_URL . $this->arSocialMediaSettings[MZ_TWITTER_URL]; ?>" target="_blank" title="<?php _e('twitter', LANGUAGE_DOMAIN_NAME) ?>" data-show-screen-name="<?php echo $data_show_screen_name; ?>" >
                                        <img src="<?php echo $image_twitter; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;

                            case 'googleplus' :
                                if (!empty($this->arSocialMediaSettings[MZ_GOOGLE_URL]) && !empty($this->arSocialMediaSettings[MZ_GOOGLE_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_GOOGLE_FOLLOW] == 'googlefollow') {
                                    $image_google = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_GOOGLE_LOGO], 'full');
                                    $image_google = $image_google[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_GOOGLE_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'google+';
                                    }
                                    ?>
                                    <a  class ="social-googleplus" href="<?php echo GOOGLEPLUS_SITE_URL . $this->arSocialMediaSettings[MZ_GOOGLE_URL]; ?>" target="_blank" title="<?php _e('google+', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img src="<?php echo $image_google; ?>"  alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;

                            case 'pinit' :
                                if (!empty($this->arSocialMediaSettings[MZ_PIN_URL]) && !empty($this->arSocialMediaSettings[MZ_PIN_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_PINTEREST_FOLLOW] == 'pinterestfollow') {
                                    $image_pinit = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_PIN_LOGO], 'full');
                                    $image_pinit = $image_pinit[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_PIN_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'pinit';
                                    }
                                    ?>
                                    <a class ="social-pinit" href="<?php echo PINIT_SITE_URL . $this->arSocialMediaSettings[MZ_PIN_URL]; ?>" target="_blank" title="<?php _e('pinit', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img src="<?php echo $image_pinit; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;
                            case 'tumblr' :
                                if (!empty($this->arSocialMediaSettings[MZ_TUMBLR_URL]) && !empty($this->arSocialMediaSettings[MZ_TUMBLR_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_TUMBLR_FOLLOW] == 'tumblrfollow') {
                                    $image_tumblr = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_TUMBLR_LOGO], 'full');
                                    $image_tumblr = $image_tumblr[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_TUMBLR_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'tumblr';
                                    }
                                    ?>
                                    <a class ="social-tumblr"  href="<?php echo 'http://' . $this->arSocialMediaSettings[MZ_TUMBLR_URL] . TUMBLR_SITE_URL; ?>" target="_blank" title="<?php _e('tumblr', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img src="<?php echo $image_tumblr; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;

                            case 'youtube' :
                                if (!empty($this->arSocialMediaSettings[MZ_YOUTUBE_URL]) && !empty($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO]) && $this->arSocialMediaSettings[MZ_SHOW_YOUTUBE_FOLLOW] == 'youtubefollow') {
                                    $image_youtube = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO], 'full');
                                    $image_youtube = $image_youtube[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'youtube';
                                    }
                                    ?>
                                    <a class ="social-youtube" href="<?php echo YOUTUBE_SITE_URL . $this->arSocialMediaSettings[MZ_YOUTUBE_URL]; ?>" target="_blank" title="<?php _e('YouTube', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img  src="<?php echo $image_youtube; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;
                        }
                    }
                    ?>
                </div>

                <?php
            }
        } elseif ($this->arSocialMediaSettings[MZ_FOLLOW_SHARE_SELECT] == 'share') {
            if (!empty($this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_LIKE]) || !empty($this->arSocialMediaSettings[MZ_SHOW_TWEET]) || !empty($this->arSocialMediaSettings[MZ_SHOW_GOOGLE]) || !empty($this->arSocialMediaSettings[MZ_SHOW_TUMBLR]) || !empty($this->arSocialMediaSettings[MZ_SHOW_ADDTHIS])) {
                ?>
                <div class="social-icons  social-button follow">
                    <?php
                    foreach ($this->arSocialtems as $item) {
                        switch ($item) {
                            case 'signup':
                                if (!empty($this->arSocialMediaSettings[MZ_SIGN_UP_FOR_EMAILS])) {
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_SIGN_UP_FOR_EMAILS], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = __('Sign Up for Emails');
                                    }
                                    $sign_up_link = __('Sign Up for Emails');
                                    ?>
                                    <a class ="signup-link"  href="<?php echo site_url($this->arSocialMediaSettings[MZ_SIGN_UP_FOR_EMAILS]); ?>"  title="<?php _e($alt_text, LANGUAGE_DOMAIN_NAME) ?>">
                                        <img src="<?php echo get_template_directory_uri() . '/img/default/signup-icon.png' ?>" alt="<?php echo $alt_text ?>"><strong><?php echo $sign_up_link; ?></strong>
                                    </a>
                                    <?php
                                }
                                break;
                            case 'twitter' :
                                if (strtoupper($this->arSocialMediaSettings[MZ_TWITTER_URL]) === 'BERTOLLI') {
                                    $data_show_screen_name = "false";
                                    $data_show_count = "false";
                                } else if (LANGUAGE_DOMAIN_NAME == 'raguus') {
                                    $data_show_screen_name = "true";
                                    $data_show_count = "true";
                                } else {
                                    $data_show_screen_name = "true";
                                    $data_show_count = "false";
                                }

                                if (!empty($this->arSocialMediaSettings[MZ_TWITTER_URL]) && !empty($this->arSocialMediaSettings[MZ_SHOW_TWEET]) && $this->arSocialMediaSettings[MZ_SHOW_TWEET] == 'tweet') {
                                    $this->twitter_js = '<script>!function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "https://platform.twitter.com/widgets.js";
                                fjs.parentNode.insertBefore(js, fjs);
                                }
                                }(document, "script", "twitter-wjs");</script>';
                                    ?>

                                    <a href="<?php echo TWITTER_SITE_URL . $this->arSocialMediaSettings[MZ_TWITTER_URL]; ?>" class="twitter-follow-button  social-twitter raguus-desktop" data-show-count="<?php print $data_show_count; ?>" data-lang="<?php echo $site_lang; ?>" data-size="medium" data-show-screen-name="<?php echo $data_show_screen_name; ?>">Follow @twitter</a>
                                    <?php if (LANGUAGE_DOMAIN_NAME == 'raguus') { ?>
                                        <a href="<?php echo TWITTER_SITE_URL . $this->arSocialMediaSettings[MZ_TWITTER_URL]; ?>" class="twitter-follow-button  social-twitter hidden-tablet hidden-desktop" data-show-count="false" data-lang="<?php echo $site_lang; ?>" data-size="medium" data-show-screen-name="false">Follow @twitter</a>   
                                    <?php }
                                    ?>
                                    <?php
                                }
                                break;
                            case 'facebook':
                                if (!empty($this->arSocialMediaSettings[MZ_FACEBOOK_URL]) && !empty($this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_LIKE]) && $this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_LIKE] == 'fblogo') {
                                    get_facebook_js($this->arSocialMediaSettings[MZ_FACEBOOK_APP_ID]); //calling fb js to implement fb like in header section
                                    ?>
                                    <a class ="social-facebook"> <div  href ="<?php echo FACBOOK_SITE_URL . $this->arSocialMediaSettings[MZ_FACEBOOK_URL] ?>" id ="fbBrandLikeButton" class="fb-like" data-locale="<?php echo $site_lang; ?>" data-layout="button_count">
                                        </div> </a>
                                    <?php
                                }
                                break;



                            case 'tumblr' :
                                if (!empty($this->arSocialMediaSettings[MZ_TUMBLR_URL]) && !empty($this->arSocialMediaSettings[MZ_SHOW_TUMBLR]) && $this->arSocialMediaSettings[MZ_SHOW_TUMBLR] == 'tumblr') {
                                    if (class_exists('ContainerTag')) {
                                        $container_tag_string = $obContainerTag->getTagsSocialMedia('Tumblr', 'Brand');
                                    }
                                    ?>
                                    <a <?php echo $container_tag_string ?> class ="social-tumblr">
                                        <iframe  border="0" allowtransparency="true" src="https://platform.tumblr.com/v1/follow_button.html?button_type=2&amp;tumblelog=<?php echo $this->arSocialMediaSettings[MZ_TUMBLR_URL] ?>&amp;color_scheme=dark" frameborder="0" height="25" scrolling="no" width="114" title="<?php _e('tumblr', LANGUAGE_DOMAIN_NAME) ?>"></iframe>
                                    </a>
                                    <?php
                                }
                                break;

                            case 'googleplus' :
                                if (!empty($this->arSocialMediaSettings[MZ_GOOGLE_URL]) && !empty($this->arSocialMediaSettings[MZ_SHOW_GOOGLE]) && $this->arSocialMediaSettings[MZ_SHOW_GOOGLE] == 'google') {
                                    $this->google_plus_js = '<script type="text/javascript">window.___gcfg = {lang: "' . $site_lang . '"};
                                                        (function() {
                                                            var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
                                                            po.src = "https://apis.google.com/js/plusone.js";
                                                            var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
                                                          })();
                                                        </script>';
                                    ?>
                                    <!-- Place this tag where you want the widget to render. -->
                                    <a  class="social-googleplus"><div data-onendinteraction="openBubble" class="g-plusone" data-callback="trackGoogleShareBrand" data-href="<?php echo GOOGLEPLUS_SITE_URL . $this->arSocialMediaSettings[MZ_GOOGLE_URL]; ?>" data-annotation ="bubble" data-action="share" ></div></a>

                                    <?php
                                }
                                break;
                            case 'pinit':
                                if (!empty($this->arSocialMediaSettings[MZ_PIN_URL]) && !empty($this->arSocialMediaSettings[MZ_SHOW_PINTEREST]) && $this->arSocialMediaSettings[MZ_SHOW_PINTEREST] == 'pinterest') {
                                    $this->pinit_js = '<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>';
                                    ?>
                                    <span class="pinit-icon">
                                        <a ct-target-type="Brand" ct-type="Pinterest" href="//pinterest.com/<?php echo $this->arSocialMediaSettings[MZ_PIN_URL] ?>" target="_blank" title="Pinterest">
                                            <img alt="go to pinterest" src="<?php echo site_url('/'); ?>wp-content/themes/villabertolli/img/default/pintrest.png" height="20" />
                                        </a>
                                    </span>
                                    <?php
                                }
                                break;
                            case 'addthis':
                                if (!empty($this->arSocialMediaSettings[MZ_SHOW_ADDTHIS]) && $this->arSocialMediaSettings[MZ_SHOW_ADDTHIS] == 'addthis') {

                                    if (LANGUAGE_DOMAIN_NAME == 'raguca') {
                                        $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                            <script type="text/javascript">
                                var addthis_config = {
                                    ui_language: "' . $addthis_lang[0] . '",
                                    data_use_cookies: false
                                }
                            </script>';
                                        ?>
                                        <a tabindex="-1" class="addthis_button_compact addthis_20x20_style social-addthis"></a>
                                    <?php } elseif (LANGUAGE_DOMAIN_NAME == 'raguus') {
                                        $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                            <script type="text/javascript">
                                var addthis_config = {
                                    services_compact: "facebook,email,print,wordpress,blogger,typepad,digg,stumbleupon",
                                    services_expanded: "facebook,email,print,wordpress,blogger,typepad,digg,stumbleupon",
                                    ui_language: "' . $addthis_lang[0] . '",
                                    data_use_cookies: false
                                }
                            </script>';
                                        ?>
                                        <a tabindex="-1" class="addthis_counter addthis_20x20_style social-addthis"></a>
                                    <?php
                                    } else {
                                        $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                            <script type="text/javascript">
                                var addthis_config = {
                                    services_compact: "email,print,tumblr,wordpress,blogger,typepad,digg,stumbleupon",
                                    ui_language: "' . $addthis_lang[0] . '",
                                    data_use_cookies: false
                                }
                            </script>';
                                        ?>
                                        <a tabindex="-1" class="addthis_button_compact addthis_20x20_style social-addthis"></a>
                                    <?php
                                    }
                                }
                                break;
                            case 'youtube' :
                                if (!empty($this->arSocialMediaSettings[MZ_YOUTUBE_URL]) && !empty($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO])) {
                                    $image_youtube = wp_get_attachment_image_src($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO], 'full');
                                    $image_youtube = $image_youtube[0];
                                    $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_YOUTUBE_LOGO], '_wp_attachment_image_alt', true);
                                    if (empty($alt_text)) {
                                        $alt_text = 'youtube';
                                    }
                                    ?>
                                    <a class ="social-youtube" href="<?php echo YOUTUBE_SITE_URL . $this->arSocialMediaSettings[MZ_YOUTUBE_URL]; ?>" target="_blank" title="<?php _e('YouTube', LANGUAGE_DOMAIN_NAME) ?>">
                                        <img height="20"  src="<?php echo $image_youtube; ?>" alt="<?php echo $alt_text ?>">
                                    </a>
                                    <?php
                                }
                                break;
                        }
                    }
                    ?>
                </div>
                <?php
            }
        }
    }

    function socialMediaContent() {
        if (empty($this->arSocialtems)) {
            return;
        }
        if (LANGUAGE_DOMAIN_NAME == "raguus") {
            $this->arSocialtems = array('signup', 'facebook', 'twitter', 'tumblr', 'googleplus', 'youtube', 'addthis', 'pinit');
        }
        global $image;
        global $title;
        global $obContainerTag;
        $site_lang = DEFAULT_LANGUAGE;
        $addthis_lang = explode("_", $site_lang);
        if (!empty($this->arSocialMediaSettings[MZ_SHOW_TWEET_BODY]) || !empty($this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_LIKE_BODY]) || !empty($this->arSocialMediaSettings[MZ_SHOW_GOOGLE_PLUS_BODY]) || !empty($this->arSocialMediaSettings[MZ_SHOW_PINIT_BODY]) || !empty($this->arSocialMediaSettings[MZ_SHOW_TUMBLR_BODY]) || !empty($this->arSocialMediaSettings[MZ_SHOW_ADDTHIS_BODY])) {
            ?>
            <div class="social-icons pull-right social-button share">
                <?php
                foreach ($this->arSocialtems as $key => $item) {
                    switch ($item) {
                        case 'twitter':
                            if (LANGUAGE_DOMAIN_NAME == "raguus") {
                                $twitter_title = 'data-text="Check out @RaguSauce ' . $title . '"';
                            } elseif (LANGUAGE_DOMAIN_NAME == "villabertolli") {
                                $twitter_title = 'data-text="Check out @Bertolli ' . $title . '"';
                            } else {
                                $twitter_title = $title;
                            }
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_TWEET_BODY])) {

                                if (class_exists('ContainerTag')) {
                                    $container_tag_string = $obContainerTag->getTagsSocialMedia('Tweet', 'Content');
                                }
                                $this->twitter_js = '<script>!function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (!d.getElementById(id)) {
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "https://platform.twitter.com/widgets.js";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }
                                    }(document, "script", "twitter-wjs");</script>';
                                /* Setting Locale for twitter ($site_lang) */
                                ?>
                                <a <?php echo $container_tag_string; ?> class="twitter-share-button social-twitter" data-related="jasoncosta" data-lang="<?php echo $site_lang; ?>" data-size="medium" data-count="none" data-show-screen-name="false" <?php echo $twitter_title; ?>>Tweet</a>
                                <?php
                            }
                            break;

                        case 'facebook':
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_FACEBOOK_LIKE_BODY])) {
                                get_facebook_js(); //calling fb js to implement fb like in content section
                                if (class_exists('ContainerTag')) {
                                    $container_tag_string = $obContainerTag->getTagsSocialMedia('Like', 'Content');
                                }
                                /* Setting Locale for facebook ($site_lang) */
                                ?>
                                <a <?php echo $container_tag_string; ?> class ="social-facebook"> <div   class="fb-like" data-locale="<?php echo $site_lang; ?>" data-layout="button_count">
                                    </div> </a>
                                <?php
                            }
                            break;

                        case 'googleplus':
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_GOOGLE_PLUS_BODY])) {
                                $this->google_plus_js = '<script type="text/javascript">window.___gcfg = {lang: "' . $site_lang . '"};
                                                        (function() {
                                                            var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
                                                            po.src = "https://apis.google.com/js/plusone.js";
                                                            var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
                                                          })();


                                                        </script>';
                                /* Setting Locale for googleplus */
                                ?>
                                <a class="social-googleplus"><div class="g-plusone" data-annotation ="bubble" data-action="share" data-callback="trackGoogle"></div></a>
                                <?php
                            }
                            break;

                        case 'pinit':
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_PINIT_BODY])) {
                                $alt_text = get_post_meta($this->arSocialMediaSettings[MZ_PIN_LOGO], '_wp_attachment_image_alt', true);
                                if (class_exists('ContainerTag')) {
                                    $container_tag_string = $obContainerTag->getTagsSocialMedia('Pinterest', 'Content');
                                    $this->pinit_js = '<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>';
                                }
                                if (empty($alt_text)) {
                                    $alt_text = "Pinterest";
                                }
                                ?>
                                <a <?php echo $container_tag_string; ?> target="_blank" class ="social-pinit" href="http://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>&amp;media=<?php echo $image[0]; ?>&amp;description=<?php echo $title ?>" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt="<?php echo $alt_text; ?>" /></a>
                                <?php
                            }
                            break;

                        case 'tumblr':
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_TUMBLR_BODY])) {
                                $this->tumbler_js = '<script src="http://platform.tumblr.com/v1/share.js"></script>';
                                if (class_exists('ContainerTag')) {
                                    $container_tag_string = $obContainerTag->getTagsSocialMedia('Tumblr', 'Content', 'Custom'); //third parameter added for CUSTOM event
                                }
                                ?>
                                <a <?php echo $container_tag_string; ?>  class ="social-tumblr" href="http://www.tumblr.com/share" title="Share on Tumblr" style="display:inline-block; overflow:hidden; width:62px; height:20px; background:url('http://platform.tumblr.com/v1/share_2.png') top left no-repeat transparent;"></a>
                                <?php
                            }
                            break;

                        case 'addthis':
                            if (!empty($this->arSocialMediaSettings[MZ_SHOW_ADDTHIS_BODY])) {
                                if (LANGUAGE_DOMAIN_NAME == 'raguca') {
                                    $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                            <script type="text/javascript">
                                var addthis_config = {
                                    ui_language: "' . $addthis_lang[0] . '",
                                    data_use_cookies: false
                                }
                            </script>';
                                } elseif (LANGUAGE_DOMAIN_NAME == 'raguus') {
                                    $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                             <script type="text/javascript">
                                 var addthis_config = {
                                    services_compact: "facebook,email,print,wordpress,blogger,typepad,digg,stumbleupon",
                                    services_expanded: "facebook,email,print,wordpress,blogger,typepad,digg,stumbleupon",
                                     ui_language: "' . $addthis_lang[0] . '",
                                     data_use_cookies: false
                                 }
                             </script>';
                                } else {
                                    $this->addthis_js = '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547f749c5449196b" async="async"></script>
                                                            <script type="text/javascript">
                                var addthis_config = {
                                    services_compact: "email,print,tumblr,wordpress,blogger,typepad,digg,stumbleupon",
                                    ui_language: "' . $addthis_lang[0] . '",
                                    data_use_cookies: false
                                }
                            </script>';
                                }
                                /* Setting Locale for ADD THIS */
                                ?>

                                <a class="addthis_button_compact addthis_20x20_style social-addthis"></a>


                            <?php
                        }
                        break;
                }
                ?>
            <?php }
            ?>
            </div>
            <?php
        }
    }

    function addSocialMediaJs() {

        echo $this->facebook_js;
        echo $this->twitter_js;
        echo $this->google_plus_js;
        echo $this->tumbler_js;
        echo $this->addthis_js;
        echo $this->pinit_js;
    }

    function socialMediaHeaderStaticLinks() {
        $output = '<div class="social-icons  social-button follow">';
        // Sign up link;
        $alt_text = __('Sign Up for Emails',LANGUAGE_DOMAIN_NAME);
        $sign_up_link = __('Sign Up for Emails',LANGUAGE_DOMAIN_NAME);

        $output .= '<a class ="signup-link" id="signup" href="' . site_url($this->arSocialMediaSettings[MZ_SIGN_UP_FOR_EMAILS]) . '"  title="' . __($alt_text, LANGUAGE_DOMAIN_NAME) . '">';
        $output .= '<strong>' . $sign_up_link . '</strong>';
        $output .= '<img class="visible-phone" src="'.get_template_directory_uri() . '/img/default/signup-icon.png" alt="'.$alt_text. '">';
        $output .='</a>';

        // Static Follow us text
        $output .= '<div class="follow-us-container hidden-phone">';
        $output .= '<span> | </span>';
        $output .= '<span class="social-follow-us">'.__('Follow us:').'</span>';
        $output .='</div>';
        
        // Twitter link
        $output .= '<a href="https://twitter.com/bertolli" target="_blank" class="social-facebook" title="'.__('Twitter',LANGUAGE_DOMAIN_NAME).'">';
        $output .= '<img src="' . get_template_directory_uri() . '/img/ico/twitter_icon.png" alt="' . __('Twitter',LANGUAGE_DOMAIN_NAME) . '">';
        $output .= '</a>';
        
        // Pinterest link
        $output .= '<a href="https://www.pinterest.com/Bertolli/" target="_blank" class ="social-facebook" title="'.__('Pinterest',LANGUAGE_DOMAIN_NAME).'">';
        $output .= '<img src="' . get_template_directory_uri() . '/img/ico/pinterest.png" alt="' . __('Pinterest',LANGUAGE_DOMAIN_NAME) . '">';
        $output .= '</a>';
        
        // Facebook link
        $output .= '<a href="https://www.facebook.com/Bertolli" target="_blank" class ="social-facebook" title="'.__('Facebook',LANGUAGE_DOMAIN_NAME).'">';
        $output .= '<img src="' . get_template_directory_uri() . '/img/ico/facebook-icon-new.png" alt="' . __('Facebook',LANGUAGE_DOMAIN_NAME) . '">';
        $output .= '</a>';
        
        $output .= '</div>';
        print $output;
    }

}

global $obSocialMedia;
$obSocialMedia = new RenderSocialMedia();
?>