<?php

/**
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Atul Gupta
 * Purpose : Parsing contact us xml to create the HTML form
 *
 */

/**
 * Parse ContactUs XML
 */
class ParseContactXml {

    /**
     * Initializes instance of ParseContactXml class.
     */

    function parserIntialise($file) {

//Initialize the XML parser
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_set_object($parser, $this);

//Specify element handler
        xml_set_element_handler($parser, 'start', 'stop');

//Specify data handler
        xml_set_character_data_handler($parser, 'char');

//Open XML file
        $file_pointer = fopen($file, 'r');

//Read data from the xml file
        while ($data = fread($file_pointer, 4096)) {
            xml_parse($parser, $data, feof($file_pointer)) or
                    die(sprintf('XML Error: %s at line %d', xml_error_string(xml_get_error_code($parser)), xml_get_current_line_number($parser)));
        }

//Free XML parser
        xml_parser_free($parser);
    }

//Function to use at the start of an element for xml
    function start($parser, $element_name, $arAttributes) {
        switch ($element_name) {
            case 'Row';
                echo "<div class='" . $arAttributes['class'] . "'>";
                break;

            case 'RowGroup';
                if (isset($arAttributes['rel'])) {
                    $rel = "rel='" . $arAttributes['rel'] . "'";
                }
                if ($arAttributes['required'] == 'true') {
                    $required = "data-validate='required'";
                }

                if (!empty($arAttributes['data-error'])) {
                    $data_error = "data-error='" . __($arAttributes['data-error'], LANGUAGE_DOMAIN_NAME) . "'";
                }

                if (!empty($arAttributes['data-blank-error'])) {
                    $data_blank_error = "data-blank-error='" . __($arAttributes['data-blank-error'], LANGUAGE_DOMAIN_NAME) . "'";
                }
                echo "<div class='" . $arAttributes['class'] . "' " . $required . ' ' . $data_error . '' . $data_blank_error . '' . $rel . '>';
                break;

            case 'RowFieldSet';
                echo '<fieldset>';
                break;

            case 'field':
                switch ($arAttributes['type']) {
                    case 'label':
                        $this->createLabel($arAttributes);
                        break;

                    case 'text':
                        $this->createText($arAttributes);
                        break;

                    case 'hidden':
                        $this->createHidden($arAttributes);
                        break;
                    case 'tooltip':
                        $this->createTooltip($arAttributes);
                        break;

                    case 'checkbox':
                        $this->createCheckbox($arAttributes);
                        break;

                    case 'radio':
                        $this->createRadio($arAttributes);
                        break;

                    case 'select':
                        $this->createSelect($arAttributes);
                        break;

                    case 'option':
                        $this->createOption($arAttributes);
                        break;

                    case 'dob':
                        $this->createDob($arAttributes);
                        break;

                    case 'button':
                        $this->createButton($arAttributes);
                        break;

                    case 'captcha':
                        $this->createCaptcha($arAttributes);
                        break;
                }
                break;
        }
    }

//Function to use at the end of an element
    function stop($parser, $element_name) {
        switch ($element_name) {
            case 'Row';
                echo '</div>';
                break;

            case 'RowGroup';
                echo '</div>';
                break;

            case 'RowFieldSet';
                echo '</fieldset>';
                break;
        }
    }

//Function to use when finding character data
    function char($parser, $data) {
        if (!trim($data)) {
            return;
        }
        $this->data .= trim($data);
    }

//Function to parse the html for Labels
    function createLabel($arAttributes) {
        $required = '';
        if ($arAttributes['required'] == 'true') {
            $required = "<span class='form-asterik'>*</span>";
        }
        if (isset($arAttributes['class'])) {
            $css_class = 'class=' . $arAttributes['class'];
        }
        if ($arAttributes['tagType']=="label"){ // name removed from label tag for wcag issue
            echo '<' . $arAttributes['tagType'] . " for='" . $arAttributes['name'] . "' " . $css_class . '>' . __($arAttributes['value'], LANGUAGE_DOMAIN_NAME) . '' . $required . '</' . $arAttributes['tagType'] . '>';
        } else { // this is for span and legend tag for wcag issues
            echo '<' . $arAttributes['tagType'] . " " . $css_class . '>' . __($arAttributes['value'], LANGUAGE_DOMAIN_NAME) . '' . $required . '</' . $arAttributes['tagType'] . '>';
        }
        
    }

//Function to parse the html for input type text
    function createText($arAttributes) {
        $required = '';
        if ($arAttributes['required'] == 'true') {
            $required = "data-validate='required'";
        }

        if (!empty($arAttributes['data-error'])) {
            $data_error = "data-error='" . __($arAttributes['data-error'], LANGUAGE_DOMAIN_NAME) . "'";
        }
        if (!empty($arAttributes['data-mask'])) {
            $data_error = "data-mask='" . __($arAttributes['data-mask'], LANGUAGE_DOMAIN_NAME) . "'";
        }

        if (!empty($arAttributes['data-pattern'])) {
            $data_pattern = "data-pattern='" . __($arAttributes['data-pattern'], LANGUAGE_DOMAIN_NAME) . "'";
        }

        if (!empty($arAttributes['data-pattern-error'])) {
            $data_error_pattern = "data-pattern-error='" . __($arAttributes['data-pattern-error'], LANGUAGE_DOMAIN_NAME) . "'";
        }

        if ($arAttributes['visible'] == 'no') {
            $disabled = 'readonly="readonly"';
        }

        $data_regex = "";

        if (isset($arAttributes['data-regex'])) {
            $data_regex = 'data-regex="' . htmlspecialchars($arAttributes['data-regex']) . '"';
        }

        echo "<div class='" . $arAttributes['class'] . "'>";
        if ($arAttributes['multiline'] == 'no') {
            if(!empty($arAttributes['title'])){
                echo "<input type='text' id='" . $arAttributes['id'] . "' name='" . $arAttributes['name'] . "' title='" . __($arAttributes['title'], LANGUAGE_DOMAIN_NAME) . "' placeholder='" . __($arAttributes['placeholder'], LANGUAGE_DOMAIN_NAME) . "'  value='" . $arAttributes['value'] . "' " . $required . ' ' . $data_error . ' ' . $data_error_pattern . ' ' . $data_regex . '' . $data_pattern . ' ' . $disabled . " maxlength='" . $arAttributes['maxlength'] . "'>";
            } else {
                echo "<input type='text' id='" . $arAttributes['id'] . "' name='" . $arAttributes['name'] . "' placeholder='" . __($arAttributes['placeholder'], LANGUAGE_DOMAIN_NAME) . "'  value='" . $arAttributes['value'] . "' " . $required . ' ' . $data_error . ' ' . $data_error_pattern . ' ' . $data_regex . '' . $data_pattern . ' ' . $disabled . " maxlength='" . $arAttributes['maxlength'] . "'>";
            }
        } else {
            echo "<textarea id='" . $arAttributes['id'] . "' title='" . __($arAttributes['title'], LANGUAGE_DOMAIN_NAME) . "' name='" . $arAttributes['name'] . "'  " . $required . ' ' . $data_error . ' ' . $data_error_pattern . ' ' . $disabled . '' . $data_regex . '>' . $arAttributes['value'] . '</textarea>';
        }
        echo '</div>';
    }

//Function to parse the html for input type hidden
    function createHidden($arAttributes) {
        if(!empty($arAttributes['id'])){
            echo "<input type='hidden' class='" . $arAttributes['class'] . "' id='" . $arAttributes['id'] . "' name='" . $arAttributes['name'] . "' value='" . __($arAttributes['value'], LANGUAGE_DOMAIN_NAME) . "'>";
        } else {
            echo "<input type='hidden' class='" . $arAttributes['class'] . "' name='" . $arAttributes['name'] . "' value='" . __($arAttributes['value'], LANGUAGE_DOMAIN_NAME) . "'>";
        }
    }

    //Function to parse the html for tooltip
    function createTooltip($arAttributes) {

        if ($arAttributes['upc-image']) {
            $upc_img = '<img src="' . get_template_directory_uri() . '/img/ico/upc.jpg">';
        }
        echo "<a href='#' data-toggle='tooltip' data-html='true' class='" . $arAttributes['class'] . "' title='" . __($arAttributes['title'], LANGUAGE_DOMAIN_NAME) . $upc_img . "'>" . __('hover', LANGUAGE_DOMAIN_NAME) . "</a>";
    }

//Function to parse the html for input type checkbox
    function createCheckbox($arAttributes) {
        if (isset($arAttributes['checked'])) {
            $checked = "checked='" . $arAttributes['checked'] . "'";
        }
        if (!empty($arAttributes['data-pattern-error'])) {
            $data_error_pattern = "data-pattern-error='" . __($arAttributes['data-pattern-error'], LANGUAGE_DOMAIN_NAME) . "'";
        }
        echo "<label class='" . $arAttributes['class'] . "'  for='" . $arAttributes['id'] . "'>";
        echo "<input type='checkbox' " . $data_error_pattern . " id='" . $arAttributes['id'] . "' name='" . $arAttributes['name'] . "' value='" . $arAttributes['value'] . "' " . $checked . '>' . __($arAttributes['dataValue'], LANGUAGE_DOMAIN_NAME);
        echo '</label>';
    }

//Function to parse the html for input type radio
    function createRadio($arAttributes) {
        if (isset($arAttributes['checked'])) {
            $checked = "checked='" . $arAttributes['checked'] . "'";
        }
        if(empty($arAttributes['id'])){
            $arAttributes['id'] = $arAttributes['dataValue'];
        }
        echo "<label class='" . $arAttributes['class'] . "'  for='" . $arAttributes['id'] . "'>";
        echo "<input type='radio' id='" . $arAttributes['id'] . "' name='" . $arAttributes['name'] . "' value='" . $arAttributes['value'] . "' " . $checked . '>' . __($arAttributes['dataValue'], LANGUAGE_DOMAIN_NAME);
        echo '</label>';
    }

//Function to parse the html for select drop down
    function createSelect($arAttributes) {
        $required = '';
        if ($arAttributes['required'] == 'true') {
            $required = "data-validate='required'";
        }
        if (!empty($arAttributes['data-error'])) {
            $data_error = "data-error='" . __($arAttributes['data-error'], LANGUAGE_DOMAIN_NAME) . "'";
        }
        if (isset($arAttributes['rel'])) {
            $rel = "rel='" . $arAttributes['rel'] . "'";
        }
        echo '<' . $arAttributes['tagType'] . " class='" . $arAttributes['class'] . "'>";
        echo "<select name='" . $arAttributes['name'] . "' " . $rel . ' ' . $required . " id='" . $arAttributes['id'] . "' " . $data_error . ">";

        if (isset($arAttributes['dataSource'])) {
            $theme_path = get_stylesheet_directory() . '/';
            $this->parserIntialise($theme_path . $arAttributes['dataSource']);
        } else {
            $options = explode(',', $arAttributes['value']);
            if (isset($arAttributes['data-answer-id'])) {
                $arAnswerId = explode(',', $arAttributes['data-answer-id']);
            }
            $arKeys = explode(',', $arAttributes['key']);
            if (count($arKeys) > 1) {
                foreach ($options as $index => $key) {
                    $value = $arKeys[$index];
                    if ($value == 'null') {
                        $value = "";
                    }
                    echo "<option value='" . $value . "'>" . __($key, LANGUAGE_DOMAIN_NAME) . '</option>';
                }
            } else if ($arAttributes['key'] == 'yes') {
                foreach ($options as $value => $key) {
                    if ($value == 0) {
                        $value = '';
                    }
                    echo "<option value='" . $value . "'>" . __($key, LANGUAGE_DOMAIN_NAME) . '</option>';
                }
            } else {
                $count = 0;
                foreach ($options as $value) {
                    $key = $value;
                    if (count($arAnswerId) > 0) {
                        $data_source_answer_id = "data-answer-id='" . $arAnswerId[$count] . "'";
                    }
                    if ($count == 0) {
                        $key = '';
                    }
                    echo "<option " . $data_source_answer_id . " value='" . $key . "'>" . __($value, LANGUAGE_DOMAIN_NAME) . '</option>';
                    $count++;
                }
            }
        }

        echo '</select>';
        echo '</' . $arAttributes['tagType'] . '>';
    }

    //Function to parse the html for select option text
    function createOption($arAttributes) {
        echo "<option value='" . $arAttributes['value'] . "'>" . __($arAttributes['data_value'], LANGUAGE_DOMAIN_NAME) . '</option>';
    }

    //Function to parse the html for date of birth
    function createDob($arAttributes) {
        echo '<span class = "select-group-field"><select name = "birthdayMonth" id = "birthdayMonth" class = "span2" title = "Please fill out this field." rel = "month">
<option value = "" selected = "selected">' . __('Month', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "1">' . __('January', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "2">' . __('February', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "3">' . __('March', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "4">' . __('April', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "5">' . __('May', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "6">' . __('June', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "7">' . __('July', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "8">' . __('August', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "9">' . __('September', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "10">' . __('October', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "11">' . __('November', LANGUAGE_DOMAIN_NAME) . '</option>
<option value = "12">' . __('December', LANGUAGE_DOMAIN_NAME) . '</option>
</select></span>';
        echo '<span class = "select-group-field"><select id = "birthdayDay" name = "birthdayDay" class = "span1" rel = "day"><option value = "" selected = "selected">' . __('Day', LANGUAGE_DOMAIN_NAME) . '</option>';
        for ($i = 1; $i <= 31; $i++) {
            echo '<option value = "' . $i . '">' . $i . '</option>';
        }
        echo '</select></span>';

        echo '<span class = "select-group-field"><select id = "birthdayYear" name = "birthdayYear" class = "span2" rel = "year"><option value = "" selected = "selected">' . __('Year', LANGUAGE_DOMAIN_NAME) . '</option>';
        for ($i = 1900; $i <= date('Y') - 13; $i++) {
            echo '<option value = "' . $i . '">' . $i . '</option>';
        }

        echo '</select></span>';
    }

    //Function to parse the html for input type button
    function createButton($arAttributes) {
        echo "<div class='" . $arAttributes['class'] . "'>";
        echo "<input type='button' name='" . $arAttributes['name'] . "' value='" . __($arAttributes['value'], LANGUAGE_DOMAIN_NAME) . "' class='" . $arAttributes['childclass'] . "'>";
        if ($arAttributes['id'] == 'submit')
            echo '<img class="ajax-loader" src="' . get_template_directory_uri() . '/img/ico/ajax-loader.gif" alt="Loading">';
        echo '</div>';
    }

    //Function to parse the html for reCaptcha from google
    function createCaptcha($arAttributes) {
        echo "<div class='" . $arAttributes['class'] . "'>";
        echo '<script type = "text/javascript" src = "https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>';
        wp_enqueue_script('recaptcha', content_url() . '/' . LIB_NAME . '/js/recaptcha.js');
        $site_language = get_option('WPLANG');
        $arLanguageCode = explode('_', $site_language);
        $arTranslation = array('lang' => __($arLanguageCode[0]), 'public_key' => $arAttributes['publickey']);
        wp_localize_script('recaptcha', 'racptachObj', $arTranslation);
        echo '<div id = "recaptcha_div">';
        echo '</div></div>';
    }

}

?>