<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : vipul sohal
 * Purpose : get the  SEO meta fields.
 *
 */

class MetaFields {

    public $obQuiredObject;
    Public $arPageMeta;

    function __construct() {

        $this->obQuiredObject = get_queried_object();
        global $post;
    }

    /* returns the seo meta tags  fo the pages */

    function getMetaFields() {
        global $arMzOption;
        if (is_home()) {
            $arSeoMeta[SEO_TITLE_ATTRIBUTE] = $arMzOption[MZ_SEO_TITLE];
            $arSeoMeta[SEO_DESCRIPTION_ATTRIBUTE] = $arMzOption[MZ_SEO_DESCRIPTION];
            $arSeoMeta[SEO_KEYWORDS_ATTRIBUTE] = $arMzOption[MZ_SEO_KEYWORDS];
            $arSeoMeta[SEO_H1_ATTRIBUTE] = $arMzOption[MZ_SEO_H1_TEXT];
        } elseif (is_single() || is_page()) {

            $arPostMeta = get_post_meta($this->obQuiredObject->ID);
            $this->arPageMeta = $arPostMeta;
            $arSeoMeta[SEO_TITLE_ATTRIBUTE] = $arPostMeta[SEO_TITLE_ATTRIBUTE][0];
            $arSeoMeta[SEO_DESCRIPTION_ATTRIBUTE] = $arPostMeta[SEO_DESCRIPTION_ATTRIBUTE][0];
            $arSeoMeta[SEO_KEYWORDS_ATTRIBUTE] = $arPostMeta[SEO_KEYWORDS_ATTRIBUTE][0];
            $arSeoMeta[SEO_H1_ATTRIBUTE] = $arPostMeta[SEO_H1_ATTRIBUTE][0];
        } elseif (is_tax() || is_category()) {
            $arCustomField = get_option(CUSTOM_CATEGORY_FIELDS);
            $arSeoMeta[SEO_TITLE_ATTRIBUTE] = $arCustomField[$this->obQuiredObject->term_taxonomy_id][SEO_TITLE_ATTRIBUTE];
            $arSeoMeta[SEO_DESCRIPTION_ATTRIBUTE] = $arCustomField[$this->obQuiredObject->term_taxonomy_id][SEO_DESCRIPTION_ATTRIBUTE];
            $arSeoMeta[SEO_KEYWORDS_ATTRIBUTE] = $arCustomField[$this->obQuiredObject->term_taxonomy_id][SEO_KEYWORDS_ATTRIBUTE];
            $arSeoMeta[SEO_H1_ATTRIBUTE] = $arCustomField[$this->obQuiredObject->term_taxonomy_id][SEO_H1_ATTRIBUTE];
        }
        return $arSeoMeta;
    }

}

?>