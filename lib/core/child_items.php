<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 * Copyright(c) Mizkan 2014
 * Author  : vipul sohal
 */

class ChildItems {

    public $arTaxonomyImages;
    public $item_type;

    function getRelatedItems($term_Id = 0) {

        global $pageTaxonomy;

        $arQuery = array('parent' => $term_Id, 'hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC', 'numberposts' => -1);
        $arRelatedItems = get_terms($pageTaxonomy, $arQuery);
        if (empty($arRelatedItems)) {

            if ($pageTaxonomy == ARTICLE_TAXONOMY) {
                $post_type = ARTICLE_POST_TYPE;
            } elseif ($pageTaxonomy == PRODUCT_TAXONOMY) {
                $post_type = PRODUCT_CUSTOM_POST;
            }
            //if product or article landing page get all posts
            if ($term_Id == 0) {
                $args = array('post_type' => array($post_type),
                    'orderby' => 'title', 'order' => 'ASC', 'numberposts' => -1,
                );
            } else {
                $arquery[] = array(
                    'taxonomy' => $pageTaxonomy,
                    'field' => 'id',
                    'terms' => $term_Id,
                    'numberposts' => -1,
                );

                $args = array('post_type' => array(PRODUCT_CUSTOM_POST, ARTICLE_POST_TYPE),
                    'tax_query' => $arquery,
                    'orderby' => 'title', 'order' => 'ASC', 'numberposts' => -1,
                );
            }

            $arRelatedItems = get_posts($args);
            $this->item_type = 'post_type';
        } else {

            $this->arTaxonomyImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS);
            $this->item_type = 'taxonomy';
        }
        return $arRelatedItems;
    }

}

?>