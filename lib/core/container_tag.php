<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : vipul sohal
 * Purpose : container tags
 *
 */

class ContainerTag {

    public $arContainerTagOptions;

    function __construct() {
        $this->arContainerTagOptions = get_option(MZ_ANALYTICS_OPTIONS);
        //     $this->addHiddenfields();
    }

    function addHiddenfields() {
        if (empty($this->arContainerTagOptions)) {
            return;
        }
        foreach ($this->arContainerTagOptions as $key => $option) {
            ?>
            <input type="hidden" name="<?php echo $key ?>" value="<?php echo $option ?>" />
            <?php
        }
    }

    /**
     * This function returns the  container tags  in form of string  for the child Items

     */
    function getTagsRelatedItems($post_type, $item_id, $item_title) {
        if (!in_array($post_type, array(ARTICLE_POST_TYPE, PRODUCT_CUSTOM_POST))) {
            return false;
        }
        if ($post_type == ARTICLE_POST_TYPE) {
            $data_ct_action = 'Article Click';
        } elseif ($post_type == PRODUCT_CUSTOM_POST) {
            $data_ct_action = 'Product Info';
        }
        $container_tag_string = 'DATA-CT-ACTION = "' . $data_ct_action . '" DATA-CT-CATEGORY ="Other" DATA-CT-IDENTIFIER="related_item_' . $item_id . '" DATA-CT-INFORMATION="' . $item_title . '-Bottom Images/Text"';
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for the Site Search

     */
    function getTagsSearchItems($post_type, $item_id, $item_title) {
        if (!in_array($post_type, array(ARTICLE_POST_TYPE, PRODUCT_CUSTOM_POST))) {
            return false;
        }
        if ($post_type == ARTICLE_POST_TYPE) {
            $data_ct_action = 'Article Click';
        } elseif ($post_type == PRODUCT_CUSTOM_POST) {
            $data_ct_action = 'Product Info';
        }
        $container_tag_string = 'DATA-CT-ACTION = "' . $data_ct_action . '" DATA-CT-CATEGORY ="Other" DATA-CT-IDENTIFIER="search-' . $item_id . '" DATA-CT-INFORMATION="' . $item_title . '"';
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for the child Items

     */
    function getTagsLeftNav($item_id, $item_name, $navigation_name, $slug) {

        if ($navigation_name == CUSTOM_LEFT_NAVIGATION_ARTICLE_NAME) {
            $data_ct_action = 'Article Click';
            $data_ct_infromation = 'Article Left Menu';
        } elseif ($navigation_name == CUSTOM_LEFT_NAVIGATION_PRODUCT_NAME) {
            $data_ct_action = 'Product Info';
            $data_ct_infromation = 'Product Left Menu';
        }
        if (!strstr($slug, NAV_ITEM_POST_TYPE_PREFIX)) {
            $container_tag_string = '';
        } else {

            $container_tag_string = 'DATA-CT-ACTION = "' . $data_ct_action . '" DATA-CT-CATEGORY ="Other" DATA-CT-IDENTIFIER="leftnav_item_' . $item_id . '" DATA-CT-INFORMATION="' . esc_attr($item_name) . '-' . $data_ct_infromation . '"';
        }
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for  pormo col,spotlights,teasers,carousels

     */
    function getTagsForCta($item_id, $item_type, $item_name, $module_type) {

        if ($item_type == ARTICLE_POST_TYPE) {
            $data_ct_action = 'Article Click';
        } elseif ($item_type == PRODUCT_CUSTOM_POST) {
            $data_ct_action = 'Product Info';
        } elseif ($item_type == CTA_URL_TYPE_YOUTUBE || $item_type == CTA_URL_TYPE_VIDEO) {
            $data_ct_action = 'Video Plays';
        } else {
            //no need for tags except articles and products
            return;
        }
        $data_ct_infromation = $item_name . '-' . $module_type;
        if ($item_name == "") {
            $data_ct_infromation = $data_ct_action . $item_id . '-' . $module_type;
        }
        $container_tag_string = 'DATA-CT-ACTION = "' . $data_ct_action . '" DATA-CT-CATEGORY ="Other" DATA-CT-IDENTIFIER="cta_' . $item_id . '" DATA-CT-INFORMATION="' . esc_attr($data_ct_infromation) . '"';
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for  carousel left/right arrows

     */
    function getTagsCarouselArrow($arrow_type, $carousel_type) {

        $container_tag_string = 'DATA-CT-ACTION="Navigation Click" DATA-CT-CATEGORY="Custom" DATA-CT-IDENTIFIER="' . $carousel_type . '-' . $arrow_type . '" DATA-CT-INFORMATION="' . $carousel_type . '-' . $arrow_type . '"';
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for  carousel bullets

     */
    function getTagsCarouselBullet($bullet, $carousel_type, $bullet_i) {

        $container_tag_string = 'DATA-CT-ACTION="Navigation Click" DATA-CT-CATEGORY="Custom" DATA-CT-IDENTIFIER="' . $carousel_type . ' ' . $bullet_i . '" DATA-CT-INFORMATION="' . $carousel_type . ' ' . $bullet . '"';
        return $container_tag_string;
    }

    /**
     * This function returns the  container tags  in form of string  for  Social media Icons

     */
    function getTagsSocialMedia($action_type, $media_type, $category = 'Referral') {
//adding REFFERAL as by default category if other category needed pass in the function directly
        $container_tag_string = 'DATA-CT-ACTION = "' . $action_type . '" DATA-CT-CATEGORY ="' . $category . '" DATA-CT-IDENTIFIER="' . $action_type . '-' . $media_type . '" DATA-CT-INFORMATION="' . $media_type . '"';
        return $container_tag_string;
    }

    /**
     * Creates CT tags attributes string for BIN module
     * @param string $identifier ct-identifier for tracking purpose.
     * @return string
     */
    function getTagsBuyItNow($identifier, $ctInformation) {
        return 'data-ct-action="Click to Purchase" data-ct-category="Conversion" data-ct-identifier="' . $identifier . '" data-ct-information="' . $ctInformation . '"';
    }

}

global $obContainerTag;
$obContainerTag = new ContainerTag();
?>