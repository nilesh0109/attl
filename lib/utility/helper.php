<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : vipul sohal
 *
 */

class Utility {

    function __construct() {

    }

    function getCtaUrl($item_id, $item_type) {


        switch ($item_type) {
            case ARTICLE_TAXONOMY:
            case PRODUCT_TAXONOMY:
                $arterm = get_term($item_id, $item_type);
                $item_url = get_term_link($arterm);
                break;

            case ARTICLE_POST_TYPE:
            case PRODUCT_CUSTOM_POST:
            case RECIPES_CUSTOM_POST:  
            case 'page':
                $item_url = get_permalink($item_id);
                break;
            case CTA_URL_TYPE_EXTERNAL:
                $item_url = $item_id;
                break;
            case CTA_URL_TYPE_VIDEO:
                $item_url = $this->getVideoURL($item_id);
                break;
            case CTA_URL_TYPE_YOUTUBE:
                $item_url = YOUTUBE_URL . $item_id;
        }
        if (is_wp_error($item_url)) {
            $item_url = '';
        }
        return $item_url;
    }

    function getVideoURL($url) {
        // Prior to 5.4.7 this would show the path as "//www.example.com/path"

        $arrURL = parse_url($url);

        if ($arrURL['host'] == 'www.iqiyi.com') {
            $match = preg_match('/http:\/\/(www\.iqiyi\.com\/(.*)|iqiyi\.com\/(.*))/i', $url, $matches, PREG_OFFSET_CAPTURE);
            if ($match) {
                $searchfor = array('.html');
                $video_id = str_replace($searchfor, '', $matches[2][0]);
                return sprintf('http://player.video.qiyi.com/a4464923a7de1ddf6c36ee8418edfe40/0/8407/%1$s.swf-albumId=204154700-tvId=204154700-isPurchase=0-cnId=6', esc_attr($video_id));
            }
        }
        if ($arrURL['host'] == 'www.tudou.com') {
            $match = preg_match('/http:\/\/(www\.tudou\.com\/programs\/view\/(.*)|tudou\.com\/programs\/view\/(.*))/i', $url, $matches, PREG_OFFSET_CAPTURE);
            if ($match) {
                $searchfor = array('/');
                $video_id = str_replace($searchfor, '', $matches[2][0]);
                return sprintf('http://www.tudou.com/v/%1$s/v.swf', esc_attr($video_id));
            }
        }
        if ($arrURL['host'] == 'v.youku.com') {
            $match = preg_match('/http:\/\/(v\.youku\.com\/v_show\/(.*)|v\.youku\.com\/v_playlist\/(.*))/i', $url, $matches, PREG_OFFSET_CAPTURE);
            if ($match) {
                $searchfor = array('id_', '.html');
                $video_id = str_replace($searchfor, '', $matches[2][0]);
                return sprintf('http://player.youku.com/player.php/sid/%1$s/v.swf"', esc_attr($video_id));
            }
        }
    }

}

global $obUtility;
$obUtility = new Utility();
?>