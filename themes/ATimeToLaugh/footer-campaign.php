<?php
/**
 * The template for displaying the blank footer.
 *
 * @package WordPress
 * @since Mizkan
 */
?>

<?php
global $arMzOption;
global $arMzThemeOptions;
global $site_title;
?>



<!--hidden fields container tags-->
<?php
if (class_exists('ContainerTag')) {
    global $obContainerTag;
    $obContainerTag->addHiddenfields();
}
?>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/libs/bootstrap.min.js', array(), false, true);
wp_enqueue_script('mizkan', get_template_directory_uri() . '/js/custom/mizkan.js', array('bootstrap'), false, true);

if (file_exists(ADCHOICEHTML) && file_exists(ADCHOICEJS)) {
    require_once ADCHOICEJS;
}

if (class_exists('RenderSocialMedia')) {
    global $obSocialMedia;
    $obSocialMedia->addSocialMediaJs();
}
?>
<!-- / He javascript -->
<?php wp_footer();
?>
</body>
</html>
