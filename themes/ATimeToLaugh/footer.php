<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @since Mizkan
 */
?>

<?php
global $arMzOption;
global $arMzThemeOptions;
global $site_title;
?>
<?php
  $all_products = get_posts(array('post_type' => 'products', 'post_status' => 'publish', 'posts_per_page' => 6));
  $all_products = array_chunk($all_products, 2);
?>
<footer id="footer" class="modern-footer">

    <div class="container">
        <?php
        if (class_exists('RenderNavigation')) {
            global $obNavigation;
            $obNavigation->renderFooterNav();
        }
        ?>
        <aside class="footer-banner clearfix">
            <?php
            if (!empty($arMzThemeOptions[MZ_FOOTER_LOGO])) {
                $image_footer = wp_get_attachment_image_src(intval($arMzThemeOptions[MZ_FOOTER_LOGO]), 'full');
                $image_footer = $image_footer[0];

                $alt_text = get_post_meta($arMzThemeOptions[MZ_FOOTER_LOGO], '_wp_attachment_image_alt', true);
                if (empty($alt_text)) {
                    $alt_text = get_bloginfo('title');
                }
                ?>
                <figure>
                    <img alt="<?php echo $alt_text ?>" src="<?php echo $image_footer; ?>" title="<?php echo get_bloginfo('title') ?>">
                    <?php echo!empty($arMzThemeOptions[MZ_FOOTER_TEXT]) ? '<figcaption>' . str_replace(PHP_EOL, '<br/>', $arMzThemeOptions[MZ_FOOTER_TEXT]) . '</figcaption>' : ''; ?>
            <?php 
            $url_raw = get_permalink();
            $url = explode('/', $url_raw);
            if(get_post_type() == 'products' || in_array('products', $url)) { ?>
            <aside class="footer-disclaimers">
                <?php
                global $arMzThemeOptions;
                echo str_replace(PHP_EOL, '<br/>', $arMzThemeOptions[MZ_FOOTER_PRODUCT_PAGE_TEXT]);
                ?>
            </aside>
            <?php } ?>
                </figure>
            <?php } else { ?>
                <?php echo!empty($arMzThemeOptions[MZ_FOOTER_TEXT]) ? '<p>'.str_replace(PHP_EOL, '<br/>', $arMzThemeOptions[MZ_FOOTER_TEXT]) . '</p>' : ''; ?>
            <?php } ?>
            <?php echo!empty($arMzThemeOptions[MZ_FOOTER_MORE_TEXT]) ? '<p class="site-footer">' . str_replace(PHP_EOL, '<br/>', $arMzThemeOptions[MZ_FOOTER_MORE_TEXT]) . '</p>' : ''; ?>
        </aside> <!-- /.footer-banner -->

    </div> <!-- /.container -->
</footer> <!-- /#footer -->

<!--hidden fields container tags-->
<?php
if (class_exists('ContainerTag')) {
    global $obContainerTag;
    $obContainerTag->addHiddenfields();
}
?>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php
wp_enqueue_script('lazy', get_template_directory_uri() . '/js/libs/lazyload.js', array(), false, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/libs/bootstrap.js', array(), false, true);
wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/libs/jquery.flexslider-min.js', array(), false, true);
wp_enqueue_script('swipe', get_template_directory_uri() . '/js/libs/swipe.min.js', array(), false, true);
wp_enqueue_script('mizkan', get_template_directory_uri() . '/js/custom/mizkan.js', array('bootstrap', 'lazy'), false, true);
wp_register_script( 'mz-script', get_template_directory_uri() . '/js/custom/my-custom.js',true ); 
    wp_enqueue_script('mz-script'); 

 
if (file_exists(ADCHOICEHTML) && file_exists(ADCHOICEJS)) {
    require_once ADCHOICEJS;
}


if (class_exists('RenderSocialMedia')) {
    global $obSocialMedia;
    $obSocialMedia->addSocialMediaJs();
}
?>
<!-- / He javascript -->
<?php wp_footer();
?>
<?php
if ($arMzOption[LAYOUT_SETTINGS] == LAYOUT_FIXED) {
    echo '</div>';
}
?>
<?php 
  $analytics_options = get_option(MZ_ANALYTICS_OPTIONS);
  if ($analytics_options['mz_tagging_gid'] && $analytics_options['mz_domain']) { 
?>
 <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?php echo $analytics_options['mz_tagging_gid']; ?>']);
    _gaq.push(['_setDomainName', '<?php echo $analytics_options['mz_domain']; ?>']);
    _gaq.push(['_trackPageview']);
    <?php if (!empty($analytics_options['mz_tagging_ga'])){ ?>
        _gaq.push(['rollup._setAccount', '<?php echo $analytics_options['mz_tagging_ga']; ?>']);
        _gaq.push(['rollup._setDomainName', '<?php echo $analytics_options['mz_domain']; ?>']);
        _gaq.push(['rollup._trackPageview']);
    <?php } ?>
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <!-- Lightning Bolt Begins -->
<script type="text/javascript">
	//var lbTrans = '[TRANSACTION ID]';
 	//var lbValue = '[TRANSACTION VALUE]';
	//var lbData = '[Attribute/Value Pairs for Custom Data]';
	var lb_rn = new String(Math.random()); var lb_rns = lb_rn.substring(2, 12);
	var boltProtocol = ('https:' == document.location.protocol) ? 'https://' : 'http://';
	try {
	      var newScript = document.createElement('script');
	      var scriptElement = document.getElementsByTagName('script')[0];
	      newScript.type = 'text/javascript';
	      newScript.id = 'lightning_bolt_' + lb_rns;
	      newScript.src = boltProtocol + 'b3.mookie1.com/2/LB/' + lb_rns + '@x96?';
	      scriptElement.parentNode.insertBefore(newScript, scriptElement);
	      scriptElement = null; newScript = null;
	} catch (e) { }
</script>
	<!-- Lightning Bolt Ends -->
  <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Bertolli Home Page
URL of the webpage where the tag is expected to be placed: http://www.villabertolli.com/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/18/2015
-->
  <script type="text/javascript">
  //  var axel = Math.random() + "";
   // var a = axel * 10000000000000;
    //document.write('<iframe src="//4645923.fls.doubleclick.net/activityi;src=4645923;type=homep0;cat=homep0;ord=' + a + '?" width="1" height="1" frameborder="0 style="display:none"></iframe>');
  </script>
  <!--
    <noscript>
        <iframe src="//4645923.fls.doubleclick.net/activityi;src=4645923;type=homep0;cat=homep0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>
  -->
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->    
  
<?php } ?>
</body>
</html>
