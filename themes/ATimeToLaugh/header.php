<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @since Mizkan
 */
global $body_class;
global $page_id;
global $arMzOption;
global $arMzHomeOption;
global $arMzThemeOptions;
global $arPostMeta;
global $thumbnail_image;
global $arMzAnalyticsOptions;

$arMzOption = get_option(MZ_SITE_OPTIONS);
$arMzHomeOption = get_option(MZ_HOME_OPTIONS);
$arMzThemeOptions = get_option(MZ_APPEARANCE_OPTIONS);
$arMzAnalyticsOptions = get_option(MZ_ANALYTICS_OPTIONS);
global $site_title;
$site_title = get_bloginfo('title');
global $title;
global $obQuiredObject;
$obQuiredObject = get_queried_object();
if (class_exists('MetaFields')) {
    $obMetaFields = new MetaFields();
    $arSeoMeta = $obMetaFields->getMetaFields();
    $arPostMeta = $obMetaFields->arPageMeta;
}
//set ogtags dynamically
if (is_single() || is_page()) {
    $thumbnail_image = wp_get_attachment_image_src(get_post_thumbnail_id($obQuiredObject->ID), 'thumbnail');
    $og_image = $thumbnail_image[0];
    $og_type = 'article';
    $og_url = get_permalink();
} elseif (is_tax() || is_category()) {
    $arTermImages = get_option(CUSTOM_TAXONOMY_IMAGE_FIELDS);
    $thumbnail_image = wp_get_attachment_image_src($arTermImages[$obQuiredObject->term_taxonomy_id][TAXONOMY_IMAGE_ATTRIBUTE], 'thumbnail');
    $og_image = $thumbnail_image[0];
    $og_type = 'article';
    $og_url = get_term_link($obQuiredObject, $obQuiredObject->taxonomy);
} else {
    $og_type = 'website';
    $og_url = site_url();
}
if (empty($og_image)) {
    $thumbnail_image = wp_get_attachment_image_src($arMzOption[MZ_HEADER_LOGO], 'full');
    $og_image = $thumbnail_image[0];
}
global $current_page_title;
if (is_tax()) {
    $term = get_queried_object();
    if ($term) {
        $current_page_title = single_term_title('', false);
    }
} else {
    $current_page_title = wp_title('', false);
}
$title = (empty($arSeoMeta[SEO_TITLE_ATTRIBUTE])) ? $current_page_title : $arSeoMeta[SEO_TITLE_ATTRIBUTE];
$html_to_replace = array("<sup>","</sup>");
$html_to_replace_with = array('','');
$title = str_replace($html_to_replace, $html_to_replace_with, html_entity_decode($title));

/**
 *  Code for getting H1 Text from admin otherwise seo_h1_tag will be title as above in previous code
 */
$seo_h1_tag = (empty($arSeoMeta[SEO_H1_ATTRIBUTE])) ? ((is_home()) ? $site_title : $current_page_title) : $arSeoMeta[SEO_H1_ATTRIBUTE];
$rtl_attr = ($arMzOption[MZ_RTL_ENABLED] == RTL_ENABLED) ? "dir='rtl'" : "dir='ltr'";
?><!doctype html>
<!--[if IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie7" <?php echo $rtl_attr ?>> <![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="no-js ie8" <?php echo $rtl_attr ?>> <![endif]-->
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="no-js ie9" <?php echo $rtl_attr ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?> class="no-js" <?php echo $rtl_attr ?> >
    <!--<![endif]-->

    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <title><?php echo ((empty($title) && is_home()) ? trim($site_title) : _e(trim($title), LANGUAGE_DOMAIN_NAME)) ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
        <meta name="description" content="<?php echo esc_attr($arSeoMeta[SEO_DESCRIPTION_ATTRIBUTE]); ?>">
        <meta name="keywords" content="<?php echo esc_attr($arSeoMeta[SEO_KEYWORDS_ATTRIBUTE]); ?>">
        <meta property="og:url" content="<?php echo $og_url; ?>"/>
        <meta property="og:title" content="<?php echo esc_attr(trim($seo_h1_tag)) ?>"/>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
        <meta property="og:site_name" content="<?php echo $site_title; ?>"/>
        <meta property="og:type" content="<?php echo $og_type; ?>"/>
        <meta property="og:description" content="<?php echo esc_attr($arSeoMeta[SEO_DESCRIPTION_ATTRIBUTE]); ?>"/>
        <?php if($post->post_type=="recipes" && $post->post_status=="publish"){ ?>
            <link rel="canonical" href="<?php echo get_permalink($post->ID);?>"/>
        <?php }?>
        <meta name="google-site-verification" content="vBtyNxb7nY6o-FX5HITtQS-f1WnkizcJfgrSB3dl5oU" />    
        <meta name="ROBOTS" content="NOODP" />
        <?php if($post->post_name=="coming-soon" || $post->post_name=="privacy-policy" || $post->post_name=="terms-of-service" || $post->post_name=="terms-of-service-spanish" || $post->post_name=="privacy-policy-spanish"){ ?>
            <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
        <?php } ?>
        <?php if (!empty($arMzAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE])) { ?>
            <!-- Google Web Master -->
            <meta name="google-site-verification" content="<?php echo($arMzAnalyticsOptions[MZ_GOOGLE_WEBMASTER_CODE]) ?>" />
            <!-- /Google Web Master -->
        <?php } ?>
        <?php if (!empty($arMzAnalyticsOptions[MZ_BING_WEBMASTER_CODE])) { ?>
            <!-- Bing Web Master -->
            <meta name="msvalidate.01" content="<?php echo($arMzAnalyticsOptions[MZ_BING_WEBMASTER_CODE]) ?>" />
            <!-- /Bing Web Master -->
        <?php } ?>
        <!-- Fav and touch icons -->
        <?php
        if (isset($arMzOption[MZ_HEADER_APPLETOUCHICONLARGE])) {
            $apple_icon_large = wp_get_attachment_image_src(($arMzOption[MZ_HEADER_APPLETOUCHICONLARGE]), 'full');
            $apple_icon_large = $apple_icon_large[0];
            ?>
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $apple_icon_large; ?>"/>
        <?php } ?>
        <?php
        if (isset($arMzOption[MZ_HEADER_APPLETOUCHICONMEDIUM])) {
            $apple_icon_medium = wp_get_attachment_image_src(($arMzOption[MZ_HEADER_APPLETOUCHICONMEDIUM]), 'full');
            $apple_icon_medium = $apple_icon_medium[0];
            ?>
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $apple_icon_medium; ?>"/>
        <?php } ?>
        <?php
        if (isset($arMzOption[MZ_HEADER_APPLETOUCHICONSMALL])) {
            $apple_icon_small = wp_get_attachment_image_src(($arMzOption[MZ_HEADER_APPLETOUCHICONSMALL]), 'full');
            $apple_icon_small = $apple_icon_small[0];
            ?>
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $apple_icon_small; ?>" />
        <?php } ?>
        <?php
        if (isset($arMzOption[MZ_HEADER_FAVICON])) {
            $header_favicon = wp_get_attachment_image_src(($arMzOption[MZ_HEADER_FAVICON]), 'full');
            $header_favicon = $header_favicon[0];
            ?>
            <link rel="shortcut icon" href="<?php echo $header_favicon; ?>">
        <?php } ?>
        <!-- He styles -->
        <?php
        $parse = file_get_contents(CHILDCSSPATH);
        preg_match('/(\/\*\*timestamp[\d]+\*\*\/)/', $parse, $arr1);
        $timeStamp = str_replace('/**timestamp', '', $arr1[1]);
        $timeStamp = str_replace('**/', '', $timeStamp);

        wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/css/framework/bootstrap.css');
        wp_enqueue_style('flexslidercss', get_template_directory_uri() . '/css/framework/flexslider.css');
        wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array(), $timeStamp);
        ?>
        <!-- / He styles -->
        <?php
        //custom styles
        // $custom = get_stylesheet_directory() . '/custom.css';

        // if (file_exists($custom)) {
        //     $parse = file_get_contents($custom);
        //     preg_match('/(\/\*\*timestamp[\d]+\*\*\/)/', $parse, $arr1);
        //     $timeStamp = str_replace('/**timestamp', '', $arr1[1]);
        //     $timeStamp = str_replace('**/', '', $timeStamp);
        //     wp_enqueue_style('custom-style', get_stylesheet_directory_uri() . '/custom.css', array(), $timeStamp, 'screen');
        // }
        // $custom_print = get_stylesheet_directory() . '/custom_print.css';

        // if (file_exists($custom_print)) {
        //     $parse = file_get_contents($custom);
        //     preg_match('/(\/\*\*timestamp[\d]+\*\*\/)/', $parse, $arr1);
        //     $timeStamp = str_replace('/**timestamp', '', $arr1[1]);
        //     $timeStamp = str_replace('**/', '', $timeStamp);
        //     wp_enqueue_style('custom-style-print', get_stylesheet_directory_uri() . '/custom_print.css', array(), $timeStamp, 'print');
        // }
        wp_enqueue_script('jquery');
        ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/js/libs/html5shiv.js"></script>
                <![endif]-->
        <script type="text/javascript">
            var ajaxurl = '<?php echo site_url('/').'wp-admin/admin-ajax.php'; ?>';
            var page_slug= '<?php echo $post->post_name?>';
            var body_class = '<?php echo $body_class?>';
        </script>
        <script type="text/javascript">
<?php
if ($arMzOption[MZ_TYPEKIT_ENABLED] == TYPEKIT_ENABLED) {
    ?>
                (function() {
                    var config = {
                        kitId: '<?php echo esc_html($arMzOption[TYPEKIT_ID]); ?>'
                    };
                    var d = false;
                    var tk = document.createElement('script');
                    tk.src = '//use.typekit.net/' + config.kitId + '.js';
                    tk.type = 'text/javascript';
                    tk.async = 'true';
                    tk.onload = tk.onreadystatechange = function() {
                        var rs = this.readyState;
                        if (d || rs && rs != 'complete' && rs != 'loaded')
                            return;
                        d = true;
                        try {
                            Typekit.load(config);
                        } catch (e) {
                        }
                    };
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(tk, s);
                })();
    <?php
}
?>
        </script>
        
        <?php
        /* Always have wp_head() just before the closing </head>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to add elements to <head> such
         * as styles, scripts, and meta tags.
         */
        wp_head();
        ?>
   
    </head>

    <body id ="<?php echo $page_id; ?>" <?php body_class($body_class);
        ?>>
              <?php
              if ($arMzOption[LAYOUT_SETTINGS] == LAYOUT_FIXED) {
                  echo '<div class="main-wrapper">';
              }
              ?>

<header>
 <nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
<?php 
    if (!empty($arMzOption[MZ_HEADER_LOGO])) {
                        $image_header = wp_get_attachment_image_src(intval($arMzOption[MZ_HEADER_LOGO]), 'full');
                        $image_header = $image_header[0];
                      
                        $alt_text = get_post_meta($arMzOption[MZ_HEADER_LOGO], '_wp_attachment_image_alt', true);
                        if (empty($alt_text)) {
                            $alt_text = get_bloginfo('title');
                        }
                        ?>
                        <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>" title="<?php echo get_bloginfo('title') ?>">
                            <img class="lazy" alt="<?php echo $alt_text ?>" src="<?php echo $image_header; ?>">
                        </a>
                    <?php } ?>

    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


       <?php
                            if (class_exists('RenderNavigation')) {
                                global $obNavigation;
                                // $obNavigation->renderHeaderNav();
                            }
                            if (class_exists('RenderHeaderMenuWalker')) {
                                $params = array(
                                    'menu' => CUSTOM_HEADER_MENU_NAME,
                                    'theme_location' => CUSTOM_HEADER_MENU_LOCATION,
                                    'menu_class' => 'nav navbar-nav navbar-right',
                                    'walker' => new RenderHeaderMenuWalker()
                                );
                                wp_nav_menu($params);
                            }
                            ?>

    </div>
  </div>
</nav>
</header> 