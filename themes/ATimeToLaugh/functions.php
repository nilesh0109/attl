<?php
/**
 * Villabertolli
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 */
$theme_constant_file_path = get_stylesheet_directory() . "/theme_constants.php"; // Check if theme constant file exists in child theme
if (!file_exists($theme_constant_file_path)) {
    $theme_constant_file_path = get_template_directory() . "/theme_constants.php"; // Use Master theme constant
}

function mz_content_width() {
    global $content_width;

    // This is a dummy width to satisfy the theme check.
    if (has_post_format('audio')) {
        $content_width = 484;
    }
}

function sanitize_post_form_data($str) {
    $filtered = wp_check_invalid_utf8($str);

    $filtered = htmlspecialchars($filtered, ENT_QUOTES, get_option('blog_charset'));


    $found = false;
    while (preg_match('/%[a-f0-9]{2}/i', $filtered, $match)) {
        $filtered = str_replace($match[0], '', $filtered);
        $found = true;
    }

    if ($found) {
        // Strip out the whitespace that may now exist after removing the octets.
        $filtered = trim(preg_replace('/ +/', ' ', $filtered));
    }

    return $filtered;
}

require_once($theme_constant_file_path);
//require_once('lib/render_navigation.php');
//require_once('lib/render-modules.php');
require_once(LOADERPATH);
add_theme_support('post-thumbnails');


add_action('after_setup_theme', 'my_theme_setup');

function my_theme_setup() {

    load_theme_textdomain(LANGUAGE_DOMAIN_NAME, get_template_directory() . '/languages');
}

//R2 New Setup
require_once('lib/render_walker_menu.php');
add_image_size(CARAUSAL_MOBILE_IMAGE, 640, 270);

//FB JS only at once place so that it can be called whereever needed
$fb_js = FALSE;

function get_facebook_js($appId='') {
    global $fb_js;
    if ($fb_js == FALSE) {
        ?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/<?php echo DEFAULT_LANGUAGE; ?>/sdk.js#version=v2.3&xfbml=1&appId=<?php echo $appId; ?>";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php
        $fb_js = TRUE;
    }
}

// Add Plugin Code to add Secondary or Overlay Image on Main Carousel Image 
if (class_exists('MultiPostThumbnails')) {
    
    new MultiPostThumbnails(array(
                'label' => 'Secondary Image',
                'id' => 'secondary-image',
                'post_type' => 'carousel-slides'
            ));
    new MultiPostThumbnails(array(
                'label' => 'Product Image for Category page',
                'id' => 'product-category-image',
                'post_type' => 'products'
            ));
}

/**
 * Function to set recipe view counter
 */
function update_view_count() {
    $recipie_id = $_POST['recipie_id'];
    $views_count_key = PREFIX_MZ . 'views_count';
    $curr_views_count_arr = get_post_meta($recipie_id, $views_count_key);
    $curr_views_count = !empty($curr_views_count_arr) ? $curr_views_count_arr[0] : 0;
    $new_views_count = (int) ($curr_views_count) + 1;
    update_post_meta($recipie_id, $views_count_key, $new_views_count);
}

// add AJAX action to update recipe View counter
add_action('wp_ajax_update_view_count', 'update_view_count');
add_action('wp_ajax_nopriv_update_view_count', 'update_view_count');

// Recipe image settings
add_image_size('recipe_landing', 220, 220);
add_image_size('recipe_search', 140, 140);
add_image_size('recipe_most_view', 40, 40);

function get_most_viewed_recipes($block_title = 'Most Viewed Recipes', $post_per_page = 10) {
    $postsdata = get_most_viewed_recipe_raw_format($post_per_page);

    $content = '<h3>' . __($block_title, LANGUAGE_DOMAIN_NAME) . '</h3>';
    $content .= '<div class="carousel1 scroller most-viewed-recipes ">';
    $content .= '<ul>';
    $counter = 0;
    $no_of_slides = count($postsdata);
    foreach ($postsdata as $postdata) {
        $recipe_metadata = get_post_meta($postdata->ID);
        if (!empty($recipe_metadata[_thumbnail_id])) {
            $image = wp_get_attachment_image_src($recipe_metadata[_thumbnail_id][0], 'recipe_most_view');
        }
        $content .= '<li>';
        if (!empty($image)) {
            $content .= '<a class="image" href="' . get_permalink($postdata->ID) . '">';
            $content .= '<figure><img src="' . $image[0] . '" alt="' . htmlentities($postdata->post_title) . '" title="' . htmlentities($postdata->post_title) . '"/></figure>';
            $content .= '<div class="copy"><p>';
            $content .= $postdata->post_title;
            $content .= '</p></div></a></li>';
        }
    }
    $content .= '</ul></div><span class="prev"></span><span class="next"></span>';

    print $content;
}

/*
 * 
 */

function get_most_viewed_recipe_raw_format($post_per_page = 10) {
    $args = array('post_type' => 'recipes', 'posts_per_page' => $post_per_page);
    $args += array('meta_query' => array(
            array(
                'key' => PREFIX_MZ . 'views_count',
        )),
        'orderby' => 'meta_value',
        'meta_key' => PREFIX_MZ . 'views_count',
        'order' => DESC);
    $postsdata = get_posts($args);
    return $postsdata;
}

function get_product_images($arPostMeta, $size) {
    $image_array = array();
    $image = wp_get_attachment_image_src($arPostMeta['_thumbnail_id'][0], $size);
    if (!empty($image)) {
        $image_array[] = wp_get_attachment_image_src($arPostMeta['_thumbnail_id'][0], $size);
    }
    for ($i = 1; $i < 6; $i++) {
        $image = wp_get_attachment_image_src($arPostMeta['products_secondary-product-image-' . $i . '_thumbnail_id'][0], $size);
        if (!empty($image)) {
            $image_array[] = $image;
        }
    }
    return $image_array;
}

// Please Include at the bottom
//include 'recp_migration.php';
function trim_superscirpt_html($value) {
    $html_to_replace = array("<sup>","</sup>");
    $html_to_replace_with = array('','');
    $new_value = str_replace($html_to_replace, $html_to_replace_with, html_entity_decode($value));
    return $new_value;
}
function dasd($value) {
    remove_filter('pre_term_name', 'wp_filter_kses', 10);
    remove_filter('pre_term_name', 'sanitize_text_field', 10);
    return $value;
}

add_filter('pre_term_name', 'dasd', 1, 1);


function minTohour($time,$min_format){
    $hour_format = __(RECIPE_HOUR_FORMAT,LANGUAGE_DOMAIN_NAME);
    if($time>59){
        $hr = floor($time / 60);
        $min = ($time % 60);
        $value = $min!='0' ? $hr." ".$hour_format." ".$min." ".$min_format : $hr." ".$hour_format." ";
    } else {
        $hr = '';
        $min = $time;
        $value = ($min!=0) ? $min.' '.$min_format : '';
    }
    return $value;
}

/* if bazaar voice enable then PRODUCT_TAXONOMY will be save unique id */
$feed_enable = get_option('brand_site_settings');
if($feed_enable[BRAND_SITE_BAZAAR_VOICE_EXTENSION]=="1") {
    add_action('edited_'.PRODUCT_TAXONOMY, 'SaveBVUniqueID',1,2);
}
// Function to save bazaar voice unique id
function SaveBVUniqueID($term_id, $tt_id){
    $feed_enable = get_option('bv_enable_feed');
    $feed_enable[0] = 0;

    $arTermImages = get_option(CUSTOM_CATEGORY_FIELDS);
    if (empty($arTermImages[$tt_id]['bv_term_id'])) {
        $arTermImages[$tt_id]['bv_term_id'] = $tt_id . '_' . time(true);
        update_option(CUSTOM_CATEGORY_FIELDS, $arTermImages);
    }
}
// Replace "{" and "}" with "[" and "}" respectively in all mails
// & adding return path for bounced mails
add_action( 'phpmailer_init', 'modify_mail_body' );
function modify_mail_body($phpmailer){
        $string_to_replace = array('{','}');
        $string_replace_with = array('[',']');
        $phpmailer->Body = str_replace($string_to_replace,$string_replace_with,$phpmailer->Body);
        $phpmailer->From = str_replace('[', '', $phpmailer->From);
        $phpmailer->From = str_replace(']', '', $phpmailer->From);
        $phpmailer->ReturnPath = 'Mizkan|MSOSupport@sapient.com';
        $phpmailer->Sender = $phpmailer->ReturnPath;
        return $phpmailer;

}