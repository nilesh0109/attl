<?php
/**
 * The Template for displaying all single articles.
 *
 * @package WordPress
 * @subpackage Mizkan
 */
global $body_class;
$body_class = 'homepage';
global $page_id;
$page_id = 'home';
get_header();
if (class_exists('RenderModules')) {
    $obModules = new RenderModules();
}
?>
<section id="main" role="main">		
    
        <?php
        if (method_exists('RenderModules', 'renderMainCarousel')) {
            $obModules->renderMainCarousel(HOME_PAGE_NAME);
        }
        ?>
		
            <section id="whatsHppnNow" class="container">
                 <div class="spotlights-container row">

                <?php
                if (is_active_sidebar(HOME_WIDGET_PLACEHOLDER_ID)) {
                    dynamic_sidebar(HOME_WIDGET_PLACEHOLDER_ID);
                }
                ?>
                </div>
            </section>
        <!-- /.row -->
        <?php
        if (method_exists('RenderModules', 'renderTeaserCarousel')) {
            $obModules->renderTeaserCarousel(HOME_PAGE_NAME);
        }
        ?>
        
		
        <aside class="footer-disclaimers"><?php
            global $arMzThemeOptions;
            echo isset($arMzThemeOptions[MZ_FOOTER_HOME_PAGE_TEXT]) ? str_replace(PHP_EOL, '<br/>', $arMzThemeOptions[MZ_FOOTER_HOME_PAGE_TEXT]) : '';
            ?></aside>
  
    <!-- /.container -->
</section>
<!-- /#main -->
<?php get_footer(); ?>
