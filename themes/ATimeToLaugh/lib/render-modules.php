<?php
/*
 * THis is for carousel and teaser in front end
 */
class RenderModules {

    public $arMzOptions;

    function __construct() {
        
    }

    /**
     * renders main carousel
     */
    function renderMainCarousel($page_type, $page_id = '') {
        global $obUtility;
        $active = ' active';
        $obCarousel = new Carousel();
        $arSlides = $obCarousel->getCarousel(CAROUSEL_TAXONOMY, $page_type, $page_id);
        //getting term taxonomy id for getting carousel custom setting
        $carousel_term_taxonomy_id = $arSlides[1]->term_taxonomy_id;

        if (empty($arSlides)) {
            return;
        }
        $first_slide = 1;
        $no_of_slides = count($arSlides);
        ?>
        <section id="aboutCarousel">

        <div class="flexslider">
            <ul class="slides">
                <?php
                foreach ($arSlides as $obSlide) {

                    $arPostMeta = get_post_meta($obSlide->ID);
                    if (class_exists('ContainerTag')) {
                        global $obContainerTag;
                        $container_tag_string = $obContainerTag->getTagsForCta($obSlide->ID, $arPostMeta[CTA_URL_TYPE][0], $obSlide->post_title, 'CAROUSEL SLIDE');
                    }
                    if (class_exists('Utility')) {
                        $cta_url = $obUtility->getCtaUrl($arPostMeta[CTA_URL_ATTRIBUTE][0], $arPostMeta[CTA_URL_TYPE][0]);
                    }
                    $image = wp_get_attachment_image_src($arPostMeta[_thumbnail_id][0], 'full');
                    $secondary_image = wp_get_attachment_image_src($arPostMeta['carousel-slides_secondary-image_thumbnail_id'][0], 'full');

                    $image_mobile = wp_get_attachment_image_src($arPostMeta['carousel-slides_mobile-image_thumbnail_id'][0], 'full');
                    $alt_text = get_post_meta($arPostMeta[_thumbnail_id][0], '_wp_attachment_image_alt', true);
                    if (empty($alt_text)) {
                        $alt_text = $obSlide->post_title;
                    }
                    $alt_text_sec_img = get_post_meta($arPostMeta['carousel-slides_secondary-image_thumbnail_id'][0], '_wp_attachment_image_alt', true);
                    if (empty($alt_text_sec_img)) {
                        $alt_text_sec_img = $obSlide->post_title;
                    }
                    if (isset($arPostMeta[CTA_TARGET_ATTRIBUTE][0]) && $arPostMeta[CTA_TARGET_ATTRIBUTE][0] == 1) {
                        $target = 'target ="_blank"';
                    } else {

                        $target = '';
                    }
                    $video_attr = '';
                    if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_YOUTUBE) {
                        $video_url = $cta_url;
                        $video_id = 'carasual_' . $obSlide->ID;
                        $cta_url = '#carasual_' . $obSlide->ID;
                        $video_attr = 'role="button" data-toggle="my-modal" data-video-pop="video-pop-up" ';
                    }
                    if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_VIDEO) {
                        $video_url = $cta_url;
                        $video_id = 'carasual_' . $obSlide->ID;
                        $cta_url = '#carasual_' . $obSlide->ID;
                        $video_attr = 'role="button" data-toggle="my-modal" data-video-pop="video-pop-up" video-href="' . $arPostMeta[CTA_URL_ATTRIBUTE][0] . '"';
                    }
                    $first_image = ($first_slide == 1) ? $image[0] : get_template_directory_uri() . '/img/default/black.jpg';
                    $first_image_mobile = ($first_slide == 1) ? $image_mobile[0] : get_template_directory_uri() . '/img/default/black.jpg';
                    $overlay_image = $secondary_image[0];
                    if (isset($obSlide) && $obSlide != '') {
                        ?>
                        <li>
                            <?php if (!empty($image)) { ?>

                                            <!--<a title ="<?php echo esc_attr($alt_text); ?>" href="<?php echo $cta_url ?>" <?php echo $video_attr; ?>><figure><img class="lazy"  src="<?php echo $first_image; ?>" data-desk="<?php echo $image[0]; ?>" data-mob="<?php echo $image_mobile[0]; ?>" alt="<?php echo $alt_text; ?>"></figure></a>-->
                                    <figure>
                                        <img class="lazy" src="<?php echo $image[0]; ?>"  alt="<?php echo $alt_text; ?>">              
                                    </figure>
                                    <?php } ?>

                            <?php if (!empty($obSlide->post_title) || !empty($obSlide->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                                <!--                                <div class="container">-->
                                <div class="caption container">
                                        <?php
                                        if (!empty($obSlide->post_title)) {

                                            if (($first_slide == 1)) {
                                                ?>
                                                <h1><?php echo $obSlide->post_title ?></h1>   
                                            <?php } else {
                                                ?>
                                                <h2><?php echo $obSlide->post_title ?></h2>
                                            <?php }
                                        } ?>

                    <?php if (!empty($obSlide->post_content)) {  echo $obSlide->post_content; } ?>
                  
                    <?php if (!empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0]) && !empty($cta_url)) { ?>
                                            <a <?php echo $container_tag_string; ?> <?php echo $target; ?> href="<?php echo $cta_url ?>" class="button big-yellow-btn" title="<?php if (empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) {
                            echo esc_attr($obSlide->post_title);
                        } else {
                            echo esc_attr($arPostMeta[CTA_LABEL_ATTRIBUTE][0]);
                        } ?>" <?php echo $video_attr; ?>> <?php if (isset($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) echo $arPostMeta[CTA_LABEL_ATTRIBUTE][0]; ?><span class="button-arrow"></span> </a>
                    <?php } ?>
                                </div>
                                <!--                                </div>-->
                <?php } ?>

                <?php if ($arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_VIDEO || $arPostMeta[CTA_URL_TYPE][0] == CTA_URL_TYPE_YOUTUBE) { ?>
                                <div class="panel"></div>
                                <!-- Modal -->
                                <div id="<?php echo $video_id ?>" class="my-modal hide" tabindex="-1" role="dialog">
                                    <div class="my-modal-header">
                                        <button type="button" class="close" data-dismiss="my-modal">x</button>
                                        <h3><?php echo $obSlide->post_title ?></h3>
                                    </div>
                                    <div class="my-modal-body">
                                        <span class="model-iframe" data-href="<?php echo $video_url ?>" data-width="100%" data-height="349"></span>
                                    </div>

                                </div>
                        <?php } ?>
                        </li>

                    <?php
                }
                $active = '';
                $first_slide++;
                $first_image_mobile++;
            }
            ?>
            </ul>
            </div>
           
        </section>
        <?php
    }

    /**
     * render teaser carousel
     */
    function renderTeaserCarousel($page_type = '') {
        global $obUtility;
        $obCarousel = new Carousel();
        $arTeaserSlides = $obCarousel->getCarousel(TEASER_TAXONOMY, $page_type);
        $no_of_slides = count($arTeaserSlides);
        if (empty($arTeaserSlides)) {
            return;
        }
        $counter = 0;
        if (isset($arPostMeta[CTA_TARGET_ATTRIBUTE][0]) && $arPostMeta[CTA_TARGET_ATTRIBUTE][0] == 1) {
            $target = 'target ="_blank"';
        } else {

            $target = '';
        }
        ?>


       <section id="theTeamCarousel" class="container">
            <div class="flexslider">
                
                    <ul class="slides">
                        <?php
                        foreach ($arTeaserSlides as $obTeaserSlide) {
                            $arPostMeta = get_post_meta($obTeaserSlide->ID);
                            if (class_exists('Utility')) {
                                $cta_url = $obUtility->getCtaUrl($arPostMeta[CTA_URL_ATTRIBUTE][0], $arPostMeta[CTA_URL_TYPE][0]);
                            }
                            if (class_exists('ContainerTag')) {
                                global $obContainerTag;
                                $container_tag_string = $obContainerTag->getTagsForCta($obTeaserSlide->ID, $arPostMeta[CTA_URL_TYPE][0], $obTeaserSlide->post_title, 'TEASER');
                            }
                            $image = wp_get_attachment_image_src($arPostMeta[_thumbnail_id][0], 'full');
                            $alt_text = get_post_meta($arPostMeta[_thumbnail_id][0], '_wp_attachment_image_alt', true);
                            if (empty($alt_text)) {
                                $alt_text = $obTeaserSlide->post_title;
                            }
                            if (isset($arPostMeta[CTA_TARGET_ATTRIBUTE][0]) && $arPostMeta[CTA_TARGET_ATTRIBUTE][0] == 1) {
                                $target = 'target ="_blank"';
                            } else {

                                $target = '';
                            }
                            
                                ?>
                <!--                        <div class="item<?php if ($counter == 0) echo ' ' . 'active'; ?>">-->

                                <li>
                                     <div class="personality-image">
                <?php if (!empty($image)) { ?>
                                            <figure><img src="<?php echo $image[0]; ?>" alt="<?php echo trim_superscirpt_html($alt_text); ?>"></figure>
                                        <?php } ?>
                                        </div>

                <?php if (!empty($obTeaserSlide->post_title) || !empty($obTeaserSlide->post_content) || !empty($arPostMeta[CTA_LABEL_ATTRIBUTE][0])) { ?>
                                            <div class="personality-caption-text">
                                            <?php if (!empty($obTeaserSlide->post_title)) { ?><h3><?php echo $obTeaserSlide->post_title ?></h3><?php } ?>
                                            <?php if (!empty($obTeaserSlide->post_content)) { ?><p><?php echo $obTeaserSlide->post_content ?></p><?php } ?>

                                            </div>
                <?php } ?>
                                  
                                  
                                </li>
                                <!-- /.list -->

                                        <?php
                                    

            
                }
                ?>
                    </ul>
              

                <!-- /.carousel-inner -->
    
              
            </div>
        </section>
        <?php
    }


}
?>