<?php
/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : vipul sohal
 * Purpose : Renders the html of navigation menus.
 *
 */

class RenderNavigation {

  function __construct() {
    // add_action('init', 'registrarNavMenus');
    //$this->renderFooterNav();
  }

  /**
   * Genrate the  html for the header navigation
   * @return Html for the header navigation
   */
  function renderHeaderNav() {
    global $post;

    //get the object taxonomy  for the header navigation
    $obNavObject = get_term_by('name', CUSTOM_HEADER_MENU_NAME, NAVGATION_MENU_TAXNOMY);
    //get the all header menu items
    $arMenuItems = wp_get_nav_menu_items($obNavObject->term_id, array('update_post_term_cache' => false));
    if (!$arMenuItems) {
      return;
    }
    $arDefaultMenu = get_option(SITE_SETUP_MENU_ITEMS_IDS);
    global $pageTaxonomy;
    switch ($pageTaxonomy) { // Logic for selecting default Landing Pages on selection of category and detail pages
      case PRODUCT_TAXONOMY:
        $navigation_id = $arDefaultMenu['product_landing']; // Default Product Landing page Id
        break;
      case ARTICLE_TAXONOMY:
        $navigation_id = $arDefaultMenu['article_landing']; // Default Article Landing page Id
        break;
    }
    $obQuiredObject = get_queried_object();
    if (isset($obQuiredObject->term_id)) {
      $navigation_item_id = $obQuiredObject->term_id;
      $arSelectedMenu = wp_get_associated_nav_menu_items($navigation_item_id, 'taxonomy');
    } else if (isset($obQuiredObject->ID)) {
      $navigation_item_id = $obQuiredObject->ID;
      $arSelectedMenu = wp_get_associated_nav_menu_items($navigation_item_id);
    }
    if (isset($arSelectedMenu) && count($arSelectedMenu) > 1) {
      $navigation_id = ""; // If Page is associated with a navigation Item, Unset Default landing page ID
    }
    echo '<ul class="nav">';
    foreach ($arMenuItems as $obMenuItem) {
      if (!empty($obMenuItem->target) && $obMenuItem->target == '_blank') {
        $target = 'target ="_blank"';
      } else {

        $target = '';
      }
      $class_active = "";
      if (($obMenuItem->object_id == 0 && is_home())) {
        $class_active = 'class = "active menu-item-' . $obMenuItem->object_id;
      } else {
        if ($obMenuItem->ID == $navigation_id) {
          $class_active = 'class = "active"';
        }
        if ($obMenuItem->object_id == $navigation_item_id) {
          $class_active = 'class = "active"';
        }
      }
      $url = ($obMenuItem->object_id == 0) ? get_site_url() : $obMenuItem->url;
      echo '<li ' . $class_active . '><a ' . $target . ' href = "' . $url . '" title = "' . $obMenuItem->title . '">' . $obMenuItem->title . '</a></li>';
    }
    echo'</ul>';
  }

  /**
   * Genrate the  html for the Footer navigation
   * @return Html for the Footer navigation
   */
  function renderFooterNav() {
    global $arMzThemeOptions;
    //get the object taxonomy  for the footer navigation
    $obNavObject = get_term_by('name', CUSTOM_FOOTER_MENU_NAME, NAVGATION_MENU_TAXNOMY);
    //get the all footer menu items
    $arMenuItems = wp_get_nav_menu_items($obNavObject->term_id, array('update_post_term_cache' => false));
    if (!$arMenuItems) {
      return;
    }

    echo '<nav id="footer_nav">
              <ul class="inline">';
    foreach ($arMenuItems as $obMenuItem) {
      if (!empty($obMenuItem->target) && $obMenuItem->target == '_blank') {
        $target = 'target ="_blank"';
      } else {

        $target = '';
      }
      if (!empty($obMenuItem->xfn)) {
        $xfn = 'rel="' . $obMenuItem->xfn . '"';
      } else {

        $xfn = '';
      }
      $url = $obMenuItem->url;
      if (($obMenuItem->object_id == 0)) {
        $class_active = 'class = "active footer-item-' . $obMenuItem->object_id;
      } else {
        $class_active = 'class="footer-item-' . $obMenuItem->object_id . '"';
      }
      echo '<li ' . $class_active . '> <a ' . $target . ' ' . $xfn . ' title="' . $obMenuItem->title . '" href="' . $url . '" class="navItemLink '.$obMenuItem->classes[0].'">' . $obMenuItem->title . '</a></li>';
    }
    if (file_exists(ADCHOICEHTML) && file_exists(ADCHOICEJS)) {
      echo '<li>';
      require_once ADCHOICEHTML;
      echo '</li>';
    }
    echo'</ul>
		</nav> <!-- /.footer_nav --> ';
  }

  /**
   * Genrate the  html for the Left navigation
   * @param string |required  name of the left navigation to be rendered(product or article)
   * @return Html for the Footer navigation
   */
  function renderLeftNav($navigation_name) {
        $quired_object = get_queried_object();

        //get the object taxonomy  for the Left  navigation
        $obNavObject = get_term_by('name', $navigation_name, NAVGATION_MENU_TAXNOMY);
        global $arMenuItems;
        $arMenuItems = wp_get_nav_menu_items($obNavObject->term_id, array('update_post_term_cache' => false));

        //get the all LeftNav menu items
        if (is_page($quired_object->ID)) {
            $cat_name = __('All Products', LANGUAGE_DOMAIN_NAME);
            $trems = get_terms(PRODUCT_TAXONOMY, array('parent' => 0, 'hide_empty' => false, 'fields' => 'ids'));

            foreach ($arMenuItems as $obitem) {

                if (in_array($obitem->object_id, $trems)) {

                    $nav_term_array[] = $obitem->ID;
                }
            }
            foreach ($arMenuItems as $obItem) {
                if (0 == $obItem->menu_item_parent || in_array($obItem->menu_item_parent, $nav_term_array)) {
                    $arOrderedMenuItems[$obItem->menu_item_parent][] = $obItem;
                }

                // $arOrderedMenuItems[$obItem->menu_item_parent][] = $obItem;
            }
        } elseif (is_single($quired_object->ID)) {
            $cat_name = __('All Products', LANGUAGE_DOMAIN_NAME);
            $product_term = wp_get_post_terms($quired_object->ID, PRODUCT_TAXONOMY);

            $trems = get_terms(PRODUCT_TAXONOMY, array('parent' => $product_term[0]->parent, 'hide_empty' => false, 'fields' => 'ids'));

            foreach ($arMenuItems as $obitem) {

                if (in_array($obitem->object_id, $trems)) {
                    if ($obitem->object_id == $quired_object->ID) {
                        $quired_nav_object_id = $obitem->ID;
                        $quired_object_parent = $obitem->menu_item_parent;
                    }
                    $nav_term_array[] = $obitem->ID;
                }
            }

            foreach ($arMenuItems as $obItem) {
                
                if (in_array($obItem->ID, $nav_term_array)) {
                    $arOrderedMenuItems[0][] = $obItem;
                } elseif (in_array($obItem->menu_item_parent, $nav_term_array)) { 
                    $arOrderedMenuItems[$obItem->menu_item_parent][] = $obItem;
                }
            }
            
            if($quired_object){
                $cat_obj_arr = get_the_terms($quired_object->ID,PRODUCT_TAXONOMY);
                if(is_array($cat_obj_arr)){
                    $cat_obj_child = array_shift($cat_obj_arr);
                    $cat_obj = get_term($cat_obj_child->parent, PRODUCT_TAXONOMY);
                    $cat_name = $cat_obj->name;
                }
            }
        } else {
            $cat_name = __('All Products', LANGUAGE_DOMAIN_NAME);
            $trems = get_terms($quired_object->taxonomy, array('parent' => $quired_object->parent, 'hide_empty' => false, 'fields' => 'ids'));
            foreach ($arMenuItems as $obitem) {
                
                if (in_array($obitem->object_id, $trems)) { 
                    if ($obitem->object_id == $quired_object->term_id) {
                        //print_r($quired_object);exit;
                        if($quired_object->parent){
                            $cat_obj = get_term($quired_object->parent, PRODUCT_TAXONOMY);
                            $cat_name =$cat_obj->name;
                        }
                        $quired_nav_object_id = $obitem->ID;
                        $quired_object_parent = $obitem->menu_item_parent;
                    }
                    $nav_term_array[] = $obitem->ID;
                }
            }
            foreach ($arMenuItems as $obItem) {
                
                if (in_array($obItem->menu_item_parent, $nav_term_array)) {
                    $arOrderedMenuItems[$obItem->menu_item_parent][] = $obItem;
                } elseif ($quired_object_parent == $obItem->menu_item_parent) {
                    $arOrderedMenuItems[0][] = $obItem;
                }
            }
        }
        ?>
        <div class="left_nav_menu_title"><?php echo $cat_name; ?></div>
        <nav id="left_nav" class="left-nav" role="navigation">
            <div id="second" class="left-nav nav-collapse">
                <ul class="nav">
                    <?php
                    $no_of_parents = count($arOrderedMenuItems[0]);

                    if (isset($arOrderedMenuItems[0])) {

                        $class = '';
                        foreach ($arOrderedMenuItems[0] as $obMenuItem) {
                            if (class_exists('ContainerTag')) {
                                global $obContainerTag;
                                $container_tag_string = $obContainerTag->getTagsLeftNav($obMenuItem->ID, $obMenuItem->title, $navigation_name, $obMenuItem->post_name);
                            }

                            $child_exists = array_key_exists($obMenuItem->ID, $arOrderedMenuItems);
                            if (($obMenuItem->object_id == get_queried_object()->ID && $obMenuItem->object == get_queried_object()->post_type ) || ($obMenuItem->object_id == get_queried_object()->term_id) && $obMenuItem->object == get_queried_object()->taxonomy) {
                                $active = 'class = "active"';
                            } else {
                                $active = '';
                            }
                            if ($obMenuItem == $arOrderedMenuItems[0][0]) {

                                $class = 'class = "first"';
                            } elseif ($obMenuItem == $arOrderedMenuItems[0][$no_of_parents - 1]) {

                                $class = 'class = "last"';
                            } else {
                                $class = '';
                            }
                            if (!empty($obMenuItem->target) && $obMenuItem->target == '_blank') {
                                $target = 'target ="_blank"';
                            } else {

                                $target = '';
                            }
                            ?><li  <?php echo $class; ?>  >
                                <a <?php echo $target ?> <?php echo $container_tag_string ?> <?php echo $active ?>  href="<?php echo $obMenuItem->url ?>" title="<?php echo esc_attr($obMenuItem->title); ?>"><?php
                echo $obMenuItem->title;
                if ($child_exists) {
                                ?><span tabindex="0" class="tree-toggler"><?php __('Click to Expand') ?></span><?php } ?></a>
                                    <?php if ($child_exists) { ?>
                                    <ul class="nav tree">
                                        <?php
                                        $this->renderLeftnavItem($arOrderedMenuItems[$obMenuItem->ID], $arOrderedMenuItems, $navigation_name);
                                        ?>
                                    </ul>
                                    <?php
                                }
                                ?></li>
                                <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </nav>
        <?php
    }

  /**
   * Genrate the  html for the Left navigation  menu item
   * @return Html for the Footer navigation
   */
  function renderLeftnavItem($arMenuitem, $arMenuItems, $navigation_name) {
    global $post;
    $no_of_parents = count($arMenuitem);
    $class = '';

    foreach ($arMenuitem as $obMenuItem) {

      $child_exists = array_key_exists($obMenuItem->ID, $arMenuItems);
      if (($obMenuItem->object_id == get_queried_object()->ID && $obMenuItem->object == get_queried_object()->post_type ) || ($obMenuItem->object_id == get_queried_object()->term_id) && $obMenuItem->object == get_queried_object()->taxonomy) {

        $active = 'class = "active"';
      } else {
        $active = '';
      }
      if ($obMenuItem == $arMenuitem[0]) {

        $class = 'class = "first"';
      } elseif ($obMenuItem == $arMenuitem[$no_of_parents - 1]) {

        $class = 'class = "last"';
      } else {

        $class = '';
      }
      if (!empty($obMenuItem->target) && $obMenuItem->target == '_blank') {
        $target = 'target ="_blank"';
      } else {

        $target = '';
      }
      if (class_exists('ContainerTag')) {
        global $obContainerTag;
        $container_tag_string = $obContainerTag->getTagsLeftNav($obMenuItem->ID, $obMenuItem->title, $navigation_name, $obMenuItem->post_name);
      }
      ?>
      <li <?php echo $class ?>>
        <?php
        if (!$child_exists) {
          ?>
          <a <?php echo $target ?><?php echo $container_tag_string; ?> <?php echo $active; ?>  href="<?php echo $obMenuItem->url ?>"> <?php echo $obMenuItem->title ?> </a>

          <?php
        }
        if ($child_exists) {
          ?>
          <a <?php echo $container_tag_string; ?> <?php echo $active; ?> href="<?php echo $obMenuItem->url ?>"><?php echo $obMenuItem->title; ?><span tabindex="0" class="tree-toggler"></span></a>
          <ul class="nav tree">
            <?php $this->renderLeftnavItem($arMenuItems[$obMenuItem->ID], $arMenuItems, $navigation_name); ?>
          </ul>
        <?php } ?>
      </li>
      <?php
    }
  }

}

global $obNavigation;

$obNavigation = new RenderNavigation();
?>