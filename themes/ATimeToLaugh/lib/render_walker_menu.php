<?php

/*
 * This document set is the property of Mizkan, and contains
 * confidential and trade secret information. It cannot be transferred from
 * the custody or control of Mizkan except as authorized in writing by an
 * officer of Mizkan. Neither this item nor the information it contains can
 * be used, transferred, reproduced, published, or disclosed, in whole or in
 * part, directly or indirectly, except as expressly authorized by an officer
 * of Mizkan, pursuant to written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * Author  : Atul Gupta
 * Purpose : Renders the html of navigation menus.
 *
 */

class RenderHeaderMenuWalker extends Walker {

    var $tree_type = array('post_type', 'taxonomy', 'custom');
    var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');
    var $data_set = array();
    var $need_selector = -1;

    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"nav\">\n";
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;

        if (class_exists('ContainerTag')) {
            global $obContainerTag;
            $container_tag_string = $obContainerTag->getTagsForCta($item->ID, $item->object, $item->title, $item->type_label . ' Flyout Menu');
        }

        // you know can access $item->hasChildren to do anything you want..
        $arDefaultMenu = get_option(SITE_SETUP_MENU_ITEMS_IDS);
        //echo '<pre>';
        //print_r($arDefaultMenu);
        // echo ' item id' . $item->ID;
        //$item->menu_item_parent;
        //echo $item->current_item_ancestor;
        //echo 'fix=' . $arDefaultMenu['product_landing'] . '<br>';
        //echo $item->menu_item_parent;
        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
        $class_names = $value = '';
        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $classes = in_array('current-menu-item', $classes) ? array('active') : array();

        if ($classes[0] != 'active') {
            $this->need_selector = -1;
            $this->go_recursive($this->data_set, $item->ID);
            //echo $item->title . '----' . $this->need_selector . '<br>';
            if ($this->need_selector >= 0 || $this->need_selector == $arDefaultMenu['product_landing']) {
                $classes = array('active');
            }
        }
        //echo 'The post type is: ' . get_post_type(get_the_ID());
        if ($item->ID == $arDefaultMenu['article_landing'] && get_post_type(get_the_ID()) == ARTICLE_POST_TYPE && !is_home()) {
            $classes = array('active');
        }
        //Check if Menu has child and child is selected
        /*
          $pages = (get_option(SITE_SETUP_MENU_ITEMS_IDS));
          if (in_array('current-menu-parent', $item->classes) || in_array('current-menu-ancestor', $item->classes) || in_array('current-menu-item', $item->classes)) {
          $classes = array('active');
          }
         *
         */
        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = strlen(trim($class_names)) > 0 ? ' class="menu-item-' . $item->ID . ' ' . esc_attr($class_names) . '"' : ' class="menu-item-' . $item->ID . '"';
        $id = apply_filters('nav_menu_item_id', '', $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';
        $output .= $indent . '<li' . $id . $value . $class_names . '>';
        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

        $item_output = $args->before;

        $item_output .= '<a' . $attributes . ' ' . $container_tag_string . '>';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        // $item_output .= $item->menu_item_parent ? '<span class="arrow">X</span><span class="arrow-desktop">X</span>' : '';
        $item_output .= $item->hasChildren ? $item->menu_item_parent ? '<span class="arrow">X</span><span class="arrow-desktop">X</span>' : '<span class="arrow">X</span><span class="top-desktop">X</span>'  : '';
        $item_output .= '</a>';
        $item_output .= $args->after;
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    function getChild($main_data, $data_id) {
        $data_x = array();
        foreach ($main_data as $data) {
            if ($data->menu_item_parent == $data_id) {
                $data_x[] = $data;
            }
        }
        return $data_x;
    }

    function go_recursive($main_data, $recursion) {

        foreach ($main_data as $data) {
            if ($data->menu_item_parent == $recursion) {



                if (in_array('current-menu-item', $data->classes)) {
                    $this->need_selector = $data->ID;
                }


                $childs = $this->getChild($main_data, $data->ID);


                if (count($childs) > 0) {
                    foreach ($childs as $child) {
                        if (in_array('current-menu-item', $child->classes)) {
                            $this->need_selector = $child->ID;
                        } else {

                            $this->go_recursive($main_data, $child->ID);
                        }
                    }
                }
            }
        }
    }

    function end_el(&$output, $item, $depth) {
        $output .= "</li>\n";
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        // check, whether there are children for the given ID and append it to the element with a (new) ID


        $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    function walk($elements, $max_depth) {

        $args = array_slice(func_get_args(), 2);
        $output = '';

        if ($max_depth < -1) //invalid parameter
            return $output;

        if (empty($elements)) //nothing to walk
            return $output;

        $this->data_set = $elements;
        $id_field = $this->db_fields['id'];
        $parent_field = $this->db_fields['parent'];

        // flat display
        if (-1 == $max_depth) {
            $empty_array = array();
            foreach ($elements as $e)
                $this->display_element($e, $empty_array, 1, 0, $args, $output);
            return $output;
        }

        /*
         * Need to display in hierarchical order.
         * Separate elements into two buckets: top level and children elements.
         * Children_elements is two dimensional array, eg.
         * Children_elements[10][] contains all sub-elements whose parent is 10.
         */
        $top_level_elements = array();
        $children_elements = array();
        foreach ($elements as $e) {
            if (0 == $e->$parent_field)
                $top_level_elements[] = $e;
            else
                $children_elements[$e->$parent_field][] = $e;
        }

        /*
         * When none of the elements is top level.
         * Assume the first one must be root of the sub elements.
         */
        if (empty($top_level_elements)) {

            $first = array_slice($elements, 0, 1);
            $root = $first[0];

            $top_level_elements = array();
            $children_elements = array();
            foreach ($elements as $e) {
                if ($root->$parent_field == $e->$parent_field)
                    $top_level_elements[] = $e;
                else
                    $children_elements[$e->$parent_field][] = $e;
            }
        }

        foreach ($top_level_elements as $e)
            $this->display_element($e, $children_elements, $max_depth, 0, $args, $output);

        /*
         * If we are displaying all levels, and remaining children_elements is not empty,
         * then we got orphans, which should be displayed regardless.
         */
        if (( $max_depth == 0 ) && count($children_elements) > 0) {
            $empty_array = array();
            foreach ($children_elements as $orphans)
                foreach ($orphans as $op)
                    $this->display_element($op, $empty_array, 1, 0, $args, $output);
        }

        return $output;
    }

}

class SitemapMenuWalker extends Walker {

    var $tree_type = array('post_type', 'taxonomy', 'custom');
    var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');

    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"unstyled\">\n";
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;
        global $wp_query;

        // you know can access $item->hasChildren to do anything you want..


        $pages = (get_option(SITE_SETUP_MENU_ITEMS_IDS));

        if (!in_array($item->ID, $pages) && !in_array($item->menu_item_parent, $pages)) {

            // echo $item->ID;
            //echo '<pre>';
            //print_r($item);
            //echo $item->current_item_ancestor;
            $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
            $class_names = $value = '';
            $classes = empty($item->classes) ? array() : (array) $item->classes;
            $classes = in_array('current-menu-item', $classes) ? array('active') : array();
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
            $class_names = strlen(trim($class_names)) > 0 ? ' class="' . esc_attr($class_names) . '"' : '';
            $id = apply_filters('nav_menu_item_id', '', $item, $args);
            $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';
            $output .= $indent . '<li' . $id . $value . $class_names . '>';
            $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
            $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
            $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
            $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
            $item_output = $args->before;

            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
            // $item_output .= $item->menu_item_parent ? '<span class="arrow">X</span><span class="arrow-desktop">X</span>' : '';
            $item_output .= $item->hasChildren ? $item->menu_item_parent ? '' : ''  : '';
            $item_output .= '</a>';
            $item_output .= $args->after;
            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }
    }

    function end_el(&$output, $item, $depth) {
        $output .= "</li>\n";
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        // check, whether there are children for the given ID and append it to the element with a (new) ID


        $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

}

?>