<?php
define('THEME_LIB_NAME', 'lib');
define('CAMPAIGN_SWITCHER', 0);
define('LANGUAGE_DOMAIN_NAME', 'ATTL');
define('LOADERPATH', WP_CONTENT_DIR . '/' . THEME_LIB_NAME . '/loader.php');
define('EXTENSION_CHINA', 0);
require_once('lib/render_navigation.php');
require_once('lib/render-modules.php');
////////////////////////////////////////////////////////////////////////////////
//////////////User Management Service Extension Configuration///////////////////
////////////////////////////////////////////////////////////////////////////////
//For User registration and login Process
//User Managment section will be available if  USER_MANAGMENT_SERVICE is on
define("USER_MANAGMENT_SERVICE", 0);

add_image_size('villabertolli_mob_logo', 186, 71);
//add_image_size('villabertolli_mob_carousel', 640, 279);

// Add Plugin Code to add Secondary or Overlay Image on Main Carousel Image 
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(array(
                'label' => 'Mobile Image',
                'id' => 'mobile-image',
                'post_type' => 'carousel-slides'
            ));
    new MultiPostThumbnails(array(
                'label' => 'Product Secondary Image 1',
                'id' => 'secondary-product-image-1',
                'post_type' => 'products'
            ));

    new MultiPostThumbnails(array(
                'label' => 'Product Secondary Image 2',
                'id' => 'secondary-product-image-2',
                'post_type' => 'products'
            ));

    new MultiPostThumbnails(array(
                'label' => 'Product Secondary Image 3',
                'id' => 'secondary-product-image-3',
                'post_type' => 'products'
            ));

    new MultiPostThumbnails(array(
                'label' => 'Product Secondary Image 4',
                'id' => 'secondary-product-image-4',
                'post_type' => 'products'
            ));


    new MultiPostThumbnails(array(
                'label' => 'Product Secondary Image 5',
                'id' => 'secondary-product-image-5',
                'post_type' => 'products'
            ));
}

?>