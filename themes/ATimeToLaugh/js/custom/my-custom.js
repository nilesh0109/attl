//Device detection
var isMobile = navigator.userAgent.match(/mobile/i);
var whichDevice = function () {
    if (isMobile) {
        var isIpad = navigator.userAgent.match(/ipad/i);
        if (isIpad) {
            return "desktop";
        } else {
            return "mobile";
        }

    } else {
        return "desktop";
    }
}


var ATTL = ATTL || {};
ATTL.App = (function($) {
    return {
        loadAboutCarousel: function()
        {
             $('.flexslider').flexslider({
                animation: "slide",
                pauseOnHover: true,
                controlNav: false, 
                directionNav: true,
                 slideshow:false
              });
        },
        
        init:function(page_id){
            if(whichDevice()=='desktop')
            {
                ATTL.App.loadAboutCarousel();   
            }
        }
    };
})(jQuery);




(function(w, d, $) {
    $(document).ready(function() {
     ATTL.App.init();
    });
}(window, document, jQuery, undefined));





// window.onload= function(){
// //    ED.App.homepage_section2_image();
// //    ED.App.video_dynamic_height();
// }

// $( window ).resize(function() {
// //   ED.App.homepage_section2_image();
// //   ED.App.video_dynamic_height();
// });
 




//!function ($) {
//    "use strict"; // jshint ;_;
//    var dataModelObj = 'a[data-toggle="my-modal"]',
//            dataModelClose = 'button[data-dismiss="my-modal"]',
//            component = '',
//            videoId = '',
//            videoTitle = '',
//            start = 0;
//
//    var createFrame = function (ob) {
//
//        var Ob = $(ob).find(".model-iframe"),
//                height = Ob.attr("data-height"),
//                id = Ob.find('div').attr('id');
//        videoId = Ob.attr("video-id");
//        videoTitle = Ob.attr("title");
//        component = Ob.attr("data-component");
//        start = 1;
//        var player;
//        player = new YT.Player(id, {
//            videoId: videoId,
//            width: '100%',
//            height: height,
//            playerVars: {
//                autoplay: 1,
//                modestbranding: 1,
//                rel: 0,
//                showInfo: 0
//            },
//            events: {
//                'onStateChange': onPlayerStateChange
//            }
//        });
//
//    };
//
//    var onPlayerStateChange = function (event) {
//        if (event.data === YT.PlayerState.PLAYING) {
//            if (start === 1) {
//                UDM.evq.push(['trackEvent', 'Other', 'Video Plays', videoTitle + '-' + component, 'Task Start']);
//            }
//            start = 0;
//        }
//        if (event.data === YT.PlayerState.ENDED) {
//            UDM.evq.push(['trackEvent', 'Other', 'Video Completes', videoTitle + '-' + component, 'Task Complete']);
//            start = 1;
//        }
//    };
//
//    var Init = function () {
//        $(dataModelObj).on("click", function () {
//            $('<div />', {
//                'class': 'my-modal-backdrop'
//            }).css('height', $(document).height() + 100).appendTo("body");
//
//            var match = $(this).attr("href");
//            createFrame(match);
//            $(match).removeClass("hide");
//        });
//
//        $(dataModelClose).on("click", function () {
//            $(".my-modal-backdrop").hide().remove();
//            $(this).closest('.my-modal').addClass("hide");
//            var iframeID = $(this).closest('.my-modal').find('iframe').attr('id');
//            $(this).closest('.my-modal').find('iframe').remove();
//            $(this).closest('.my-modal').find('.model-iframe').append('<div id=' + iframeID + '></div>');
//        });
//
//
//    };
//
//    return Init();
//
//}(window.jQuery);








 


