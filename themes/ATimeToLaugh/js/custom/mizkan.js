/* ===========================================================
 * custome-lite.js v2.3.2
 * ===========================================================
 * Copyright 2013 Sapient Nitro.
 *
 * limitations under the License.
 * =========================================================== */

 /* Mobile Detection 
 * =========================================================== */
var isMobile = navigator.userAgent.match(/mobile/i);
var whichDevice = function () {
    if (isMobile) {
		var isIpad = navigator.userAgent.match(/ipad/i);
		if (isIpad) {
			return "desktop";
		} else {
			return "mobile";
		}

	} else {
		return "desktop";
	}
}

//setting class selected for first item in storelocator
!function($) {
//  $(".with_img_li:first-child>a").addClass("selected");
//alert("hey");
//    var $Home_carousel_image = $(".hero_carousel_2>.carousel-inner>.item>figure");
//
//
//        var mobileImage = function($Home_carousel_image)
//        {
//            
//                $Home_carousel_image.each(function()
//                {
//                    if (whichDevice()=="mobile") {
//                    var image_path = $(this).children("img.lazy1").attr("data-mob");
//                   $(this).children("img.lazy1").attr("src", image_path);
//                    }
//                    else
//                    {
//                        var image_path_d = $(this).children("img.lazy1").attr("data-desk");
//                        
//                   $(this).children("img.lazy1").attr("src", image_path_d); 
//                    }
//                   
//                });
//
//        }
//        mobileImage($Home_carousel_image);
}(window.jQuery);
/* Setting the mobile menu  */
!function($) {
	if(whichDevice()=="mobile"){
		var $header = $('#header'),
			obj="";
		$header.addClass('mobile-header');
		$header.find('#left-nav-btn').remove();
		//obj=$('#main').find('#left_nav').remove().clone();
		//obj.removeClass("left-nav").find("#second").removeClass("left-nav");
		//obj.appendTo('.menu-header-navigation-container');
		$('#first').find('form').remove().clone().insertBefore('.container nav#first');
	}else{
		var $leftNav = $('#left-nav-btn');
		/* Display left menu button for desktop */
		$(document).ready(function() {
	    	if($(document).innerWidth()<'767') { 
	    		$leftNav.show();
		    }else{
		    	$leftNav.hide();
		    }
		});	
		
		/* Handling left menu after Desktop resize */
		$(window).resize(function() {
	    	if($(document).innerWidth()<'767') { 
	    		$leftNav.show();
		    }else{
		    	$leftNav.hide();
		    }
		});	
	}	
}(window.jQuery);




/*Left Navigation expand/collapse menu
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;

    /* Less codding for toggle */
    /* Hide all expand menu  for none javascript functionality */
    var $objH = $(".left-nav ul ul"),
        $objA = $(".left-nav a.active"),
        expCls = "expand";

    $objH.hide();
    $objH = null; /* Remove from memory */

    /* Show if active */
    $objA.parents(".nav").show().prev().find("span").addClass(expCls);
    $objA.parent().parent().siblings("a").addClass("active");
	$objA.next().show();
	$objA.find("span").addClass(expCls);
    $objA = null; /* Remove from memory */

    /* Toggle Menu and add Class */
    $('span.tree-toggler').on("keypress click", function (e) {
	 if (e.which === 13 || e.type === 'click'){
        $(this).parent().parent().children('ul.tree').slideToggle();
        $(this).toggleClass(expCls);
		//
        $(this).parent().toggleClass("anchor-bold")
        /* Setting nav height  */
        if(whichDevice()=="mobile"){ 
        	$('#first').css('height','100%'); 
        }
		//e.stopPropagation();
		
        e.preventDefault();
		calculateHeight();
		}
    });

	$('#second .tree-toggler').on("keypress click ", function(e){
	if (e.which === 13 || e.type === 'click'){
		$('#second').css('height','auto');
		}
		
	});

	
}(window.jQuery);


/*Search box validation
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;
    //trigger code on search click button
	var $dataFild=$('.form-search').find('input[type="text"]');
	
    $('.btn-search').on('click', function(e){
		if($dataFild.val().length <= 0) {
                    
            $dataFild.addClass("errorInput");
            return false;
        }
    });
	
	$dataFild.on('blur', function() {
		$dataFild.removeClass("errorInput");
	})
	
}(window.jQuery);

/*Extend Carousel from Twitter Bootstrap bootstrap-carousel.js
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;
	var flag = false;
    /* extensionMethods for Slide function*/
    var extensionMethods = {

        slide: function (type, next) {
            if (!$.support.transition && this.$element.hasClass('slide')) {
                this.$element.find('.item').stop(true, true); //Finish animation and jump to end.
            }
            var $active = this.$element.find('.active'),
                $next = next || $active[type](),
                isCycling = this.interval,
                direction = type == 'next' ? 'left' : 'right',
                fallback = type == 'next' ? 'first' : 'last',
                that = this,
                e = $.Event('slide')
                this.sliding = true
                isCycling && this.pause()
                $next = $next.length ? $next : this.$element.find('.item')[fallback]()
                if ($next.hasClass('active')) return
				this.$indicators = this.$element.find('.carousel-indicators')
				 if (this.$indicators.length) {
				this.$indicators.find('.active').removeClass('active')
				this.$element.one('slid', function () {
				  var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
				  $nextIndicator && $nextIndicator.addClass('active')
				})
			  }
                if ($.support.transition && this.$element.hasClass('slide')) {
                    this.$element.trigger(e)
                    if (e.isDefaultPrevented()) return
				    $next.addClass(type)
				    $next[0].offsetWidth // force reflow
				    $active.addClass(direction)
                    $next.addClass(direction)
                    this.$element.one($.support.transition.end, function () {
                        $next.removeClass([type, direction].join(' ')).addClass('active')
                        $active.removeClass(['active', direction].join(' '))
                        that.sliding = false
                        setTimeout(function () {
                            that.$element.trigger('slid')
                        }, 0)
                    })
                } else if (!$.support.transition && this.$element.hasClass('slide')) {
                    this.$element.trigger(e)
                    if (e.isDefaultPrevented()) return
                    $active.animate({
                        left: (direction == 'right' ? '100%' : '-100%')
                    }, 600, function () {
                        $active.removeClass('active')
                        that.sliding = false
                        setTimeout(function () {
                            that.$element.trigger('slid')
                        }, 0)
                    })
                    $next.addClass(type).css({
                        left: (direction == 'right' ? '-100%' : '100%')
                    }).animate({
                        left: '0%'
                    }, 600, function () {
                        $next.removeClass(type).addClass('active')
                    })
                } else {
                    this.$element.trigger(e)
                    if (e.isDefaultPrevented()) return
                    $active.removeClass('active')
                    $next.addClass('active')
                    this.sliding = false
                    this.$element.trigger('slid')
                }
            isCycling && this.cycle()
            return this
        }
    };
	//Swipe event to slide the carousel and set configure for auto and interval value
	var carouselObj = {
		carouselInt: function(selector, interval, auto) {
				var $carouselSel = $(selector);
				// Set time interval for carousel if auto transition is true
				if(auto==true){
					$carouselSel.carousel({
						interval: interval			
					});
				}
				else {
					$carouselSel.carousel('pause')
				}
				
			// play and pause carousel	
			$('.play').unbind('click').on('click', function(){
				if($(this).hasClass('pause')){
					$(this).removeClass('pause');
					$(this).css('background-position','center -60px');
					$('#sub_carousel').carousel('cycle');
					// when flag enable true after click on carouser indicators 
					if(flag == true){
						var carouselInterval = $('#time_interval').val();
						$('#sub_carousel').data('carousel').options.interval = carouselInterval;
						flag = false;
					}
					return false;
				}else{
					$(this).addClass('pause');
					$(this).css('background-position','center -5px');
					$('#sub_carousel').carousel('pause');
					return false;
				}
			});	
				
			// Swipe event to slide the carousel
			$carouselSel.on('swipeleft', function (e) {
				e.stopPropagation();
				 $carouselSel.carousel('next');
			}).on('swiperight', function (e) {
				e.stopPropagation();
				 $carouselSel.carousel('prev');  
			}).on('movestart', function(e) {
				if ((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) {
					e.preventDefault();
					return;
				}
			});	
                        
                        
                        
		},
		
		Init: function() {		
			$('.carousel').each( function (){
				//Need to configure with DEV Given Data <To Be editable>
				/*Set the time interval and auto rotation for carousel  */
				var carouselInterval = $('#time_interval').val();
				var transitionType = $('#transition_type').val();
				carouselObj.carouselInt(this, carouselInterval, transitionType);
				carouselObj.carouselInt('#teser_carousel', false, false);
			});
		}	
	};

	$.extend(true, $["fn"]["carousel"]["Constructor"].prototype, extensionMethods);
	
	$(document).ready(function() { 
		//Initialization Carousel Function with setting
		carouselObj.Init();

		//Video pause on click
		$('a[data-video-pop="video-pop-up"]').click(function() { 
			var obj = $(this).parents('.carousel') 
				obj.carousel(); 
				  var carousel = obj.data('carousel'); 
				  carousel.pause(); 
		   
		 
		});
		
		//Video replace href for china on click	
		$('a[video-href]').each(function() { 
			if (isMobile) { 
				$(this).removeAttr('data-toggle').unbind('click'); 
				$(this).attr('href', $(this).attr('video-href')); 
			}  
		}); 
		
		// added margin-right, if find play class in carousel-inner 
		if($('#sub_carousel').find('.play').length){
			$('.carousel-indicators').css('margin-right','20px');
		}
		
		// when pause icon visible and click on carouser indicators 
		$('.carousel-indicators').on('click', function(){
			if($('#sub_carousel').find('.play').length && $('.play').hasClass('pause')){
				   $('#sub_carousel').data('carousel').options.interval=false;
				   flag = true;
			}
		});

	});


	
	// For carousal on home page lazy Load
    var lazyLoadInit = function (target, dataAttr) {
      
		$(target).lazyload({
			//effect : "fadeIn",
			data_attribute : dataAttr,
			threshold : 200,
			
		});
    }

    var lazyImg="img.lazy",
        keyDesk="desk",
        keyMob="mob";

    if(whichDevice()=="desktop") {
        
        lazyLoadInit(lazyImg, keyDesk);
     } else {
         
        lazyLoadInit(lazyImg, keyMob);
    }
}(window.jQuery);


/*Init Function from bootstrap and Plug - In
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;

    /* popover Init */
	
	// The following code is commented as we customized with our code for popover.
    //$('.nutrition').popover();

	$('.nutrition').on('click', function(e){
		var $this=$(this),
			prooverContent=$this.attr('data-content'),
			xyPosition=$this.position(),
			
			xPos=(xyPosition.left-$this.innerWidth()),
			yPos=(xyPosition.top+$this.height()),
			gProp='div.popover',
            
            nutWidth = $this.width();
           
                  
            		
		if($('.nutrition').width() <= 695 && $('.nutrition').width() >= 248){
			
                       
			//xPos=(xyPosition.left+$this.innerWidth()/2)- 148.5;
                        xPos=xyPosition.left;
				
		}else{
                  
			//xPos=(xyPosition.left-270/2) + nutWidth/2;
                        xPos=xyPosition.left;
                        
			}
		if($this.hasClass("in")) {
			$this.removeClass("in")
			$this.addClass("out");
			$this.next(gProp).remove();
		  
		} else {
			$('.nutrition').removeClass('in');
			$(gProp).remove();
			//$this.addClass('in');
			$this.after('<div class="popover fade bottom in" tabindex="0"><div class="close-btn"></div><div class="arrow"></div><div class="popover-content">'+prooverContent+'</div></div>')
			$this.next(gProp).css({
				'left':xPos,
				'top':yPos,
				'display':"block"
			});
                        $(gProp).focus();
		}

		$('.close-btn').on('click', function(e){
			$(gProp).remove();
		});
		$(gProp).on('blur', function(e){
                    $(gProp).fadeOut( 300, function() {
                        $(gProp).remove();
                    });
		});
		
   

	});
	

	
    // Delete the menu button if left navigation doesn't exist 
    if($(document).find('#second').length == 0) {
        $('#left-nav-btn').remove();
    }

}(window.jQuery);

/* Iframe Model Window
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;
    var dataModelObj = 'a[data-toggle="my-modal"]',
        dataModelClose = 'button[data-dismiss="my-modal"]';

    var createFrame = function (ob,model) {
        var Ob = $(ob).find(".model-iframe"),
            ObjSrc = Ob.attr("data-href"),
            ObHeight = Ob.attr("data-height"),
            ObWidth = Ob.attr("data-width");
        if(model){
			$('<iframe />', {
				frameborder: '0',
				allowTransparency: 'true',
				width: ObWidth,
				height: ObHeight,
				src: ObjSrc
			}).appendTo(Ob);
		} else {
			/*$('<a />',{
				href:ObjSrc,
				
			});*/
		}
    };

    var Init = function () {
        $(dataModelObj).on("click", function () {
            $('<div />', {
                'class': 'my-modal-backdrop'
            }).css('height',$(document).height()+100).appendTo("body");
          
            var match = $(this).attr("href");
            createFrame(match,true);
            $(match).removeClass("hide");
        });

        $(dataModelClose).on("click", function () {
            $(".my-modal-backdrop").hide().remove();
            $(".my-modal").addClass("hide");
            $('.model-iframe iframe').remove();
        });

    };
	 
    return Init();

}(window.jQuery);

/*Detail page Video And Buy It now
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;
    
	var createFrame = function (srcAppend, width, height, src) {
		$('<iframe />', {
			frameborder: '0',
			allowTransparency: 'true',
			width: width,
			height: height,
			src: src+'?wmode=transparent'
		}).appendTo(srcAppend);
		
	}
	
	var addVideo = function (ob,srcAppend) {
		var allVideoWrapper = $('<div/>',{"class":'all-video-wrapper',});
      	$(ob).each(function(){
			
				var isIpad = navigator.userAgent.match(/ipad/i);
                if(isIpad) {
					 $(ob).each(function(){
						
						var videoOutLink = $(this).attr('video-href');
						if(typeof videoOutLink == 'undefined'){

							new addVideo("a.model-video",".detail-page-video");
							
						}
					});
					

					new replaceVideo("a.model-video",".detail-page-video");
			}else {	
			
			      var videoWrap= $('<div />', {"class": 'video-wrapper',}),
				     /*assign value passed by user**/
					  width=$(this).attr("data-width"),
					  height=$(this).attr("data-height");
					  width = parseInt(checkValue(width,560));
					  height= parseInt(checkValue(height,349));
					  
				 if($('.single-article-detail a.model-video').parent().hasClass('videoAppend')){
                  	//for every detail page the video will be rendered wherever the youtube link is added 
                    var ids = $(this).parent().attr('id');
                        videoWrap.appendTo('div#'+ids);

                }else {
					//for only product detail page where video is rendered at the bottom 
                     videoWrap.appendTo(allVideoWrapper);

                }
				
				if($(this).attr("data-overlay")=="true"){
				     //if video has to open in a overlay
					var yid= $(this).attr('data-id'),
						imgSrc = " http://i2.ytimg.com/vi/"+yid+"/0.jpg",
					    ydiv =$('<div class="frame-wrapper" id="frame-wrapper">'),
						yimg = $('<img class="overlay-img" alt="video first frame">'),
						ybutton = $('<a href="#" class="ybutton"/>');
						
					yimg.attr('src', imgSrc);
					yimg.attr('data-href',$(this).attr("href"));
					yimg.css({ "height": height, "width": width});
					videoWrap.css('position','relative');
					ybutton.css({'top':((height/2)-24),'left':((width/2)-32)});
					ybutton.appendTo(ydiv);
					yimg.appendTo(ydiv);
					ydiv.appendTo(videoWrap);
					
				}else{
					new createFrame(videoWrap, width,height, $(this).attr("href"));
				}
                
				$(this).remove();
			}
                
               
			
		});
		
		if($('body.product-detail').length > 0){
			allVideoWrapper.appendTo(srcAppend);
		}
	}
    var checkValue = function (varToCheck,defaultValue){
	
		if(varToCheck==""||varToCheck==undefined){
			varToCheck = defaultValue;
		}
		return varToCheck;
	
	}

	var overlayDisplay =function(ele,mymodal){
		
	$(ele).on("click",function(){
	
		var yimg =$(this).children("img"),
	        
			tempsrc= $(mymodal + " .modal-body");
			
			new createFrame(tempsrc,"100%","349", yimg.attr("data-href"));
			$(mymodal).modal('show');
		  
	})
	
	$("#video-overlay").on("hidden",function(){
				
		$('.modal-body iframe').remove();
				
	})	
	
	}
    var replaceVideo = function (ob,srcAppend) {
        $(ob).each(function(){

            var videoOutLink = $(this).attr('video-href');
            if(typeof videoOutLink !== 'undefined' && videoOutLink !== false){

                $(this).attr("href",videoOutLink);
                
            }
        });

    }
    
	var Init = function () {
		if(isMobile) {
		var isIpad = navigator.userAgent.match(/ipad/i);
			if(isIpad) {
			new addVideo("a.model-video",".detail-page-video");
			}
		 
            new replaceVideo("a.model-video",".detail-page-video");

		} else {
			
			new addVideo("a.model-video",".detail-page-video");
			new overlayDisplay("div.frame-wrapper","#video-overlay"); 
			/*Update Buy in now <a> to Frame*/
			if($("a[data-toggle='modal']").hasClass('buy-it-now-btn')) {
			
			 	//Assign the height and width value passed from USER ()CMS
				var tempSrc=$(".buy-it-now-btn").attr("href");
                    $(".buy-it-now-btn").attr("href","#buy_it_now");
					
                $("#buy_it_now").on("shown",function(){
				 
                    var	height = parseInt($("#bin_modal_height").val()),
						width =  parseInt($("#bin_modal_width").val());
					
                    //First check if the page is from store locator
					if($('body#store-locator').length > 0 || $('body#where_to_buy').length > 0)
					{
						tempSrc = $(".buy-it-now-btn").attr("data-url");
					}					
						
					$(this).css({"width":width,"height":height});
					
					$(".modal-body", this).css({"max-width":width,"max-height":height});
					
					//Create the Iframe with given source and height/width from user
					createFrame("#buy_it_now .modal-body",(width+16),(height+16),tempSrc);
					    
				});
	
				$("#buy_it_now").on("hidden",function(){
				
				    $('.modal-body iframe').remove();
				
				});			  
				
			}
			
			/*popover on single product detail page*/
		
			
		}
		
        
    };
	 
    return Init();

}(window.jQuery);

/*Flyout Menu's
 * =========================================================== */
!function ($) {
    "use strict"; // jshint ;_;
  	
	 /* tab flyout navigation */
	function tabNavigation(){
		 var $a = $( ".navbar .nav-collapse ul li a" );
		 var $li = $( ".navbar .nav-collapse ul li" );
		 var $ul = $( ".navbar .nav-collapse ul" );
		 var $shiftKey = false;
		 $a.focus(function() {
		 		/* added tabin class when next li exit */
		        if($(this).parent().children('.nav').length && $(this).parent().next('li').length) {
		          $(this).parent().children('.nav').show();
		          $(this).parent().addClass('hv_active tabin');
		        }
				/* added tabout class when next li not exit */
		        if($(this).parent().children('.nav').length && $(this).parent().next('li').length == 0){
		          $(this).parent().children('.nav').show();
		          $(this).parent().addClass('hv_active tabout');
		        }
	    });
	    
	    
	    $a.on('focusout' ,function(){
	       if($(this).parent().next('li').length == 0 && $(this).parents('.hv_active').length && $shiftKey != true){
	       		/* get tabin class and next is element null */
		        if($(this).parent().closest('ul.nav').parent().hasClass('tabin') && $(this).next('.nav').length == 0){
		            $(this).parent().closest('ul.nav').hide();
		            $(this).parent().closest('ul.nav').parent().removeClass('hv_active tabin');
		        }
				/* get tabout class and next is element null */
	            if($(this).parent().closest('ul.nav').parent().hasClass('tabout') && $(this).next('.nav').length == 0){
	               $(this).parent().closest('ul.nav').hide();
	               $(this).parent().closest('ul.nav').parent().removeClass('hv_active tabout');
	               /* get tabout class from parents and tabin class did not get */
		           if($(this).parent().parents().find('.tabout').length && $(this).parent().parents('.tabout').find('.tabin').length == 0){
		              $(this).parent().parents('.tabout').children('ul.nav').hide();
		              $(this).parent().parents('.tabout').removeClass('hv_active tabout');
		              $(this).parent().parents('.tabout').children('ul.nav').find('.tabout').removeClass('hv_active tabout');
		           }
		           /* get tabin class and next element is not null */
	               if($(this).parent().closest('.tabin').length && $(this).parent().closest('.tabin').next('li').length){
	               	
	                  if($(this).parent().closest('.tabin').children('.nav').find('.tabout').length){
	                     $(this).parent().closest('.tabout').children('.nav').hide();
	                     $(this).parent().closest('.tabout').removeClass('hv_active tabout');
	                   }
	                   $(this).parent().closest('.tabin').children('.nav').hide();
	                   $(this).parent().closest('.tabin').removeClass('hv_active tabin');
	                   $(this).parent().closest('.tabin').children('.nav').find('.tabin').removeClass('hv_active tabin');
	                }
					/* get tabin class and next element is null */
	                if($(this).parent().closest('.tabin').length && $(this).parent().closest('.tabin').next('li').length == 0){
	                   $(this).parent().closest('.tabin').children('.nav').hide();
	                   $(this).parent().closest('.tabin').removeClass('hv_active tabin');
	                   $(this).parent().closest('.tabin').children('.nav').find('.tabin').removeClass('hv_active tabin');
	                 
	                }
	            }
	   		}
	   		
	   		/* after tab, if we click on anywhere on window */
			$(document).click(function(e) {
			  if(!$(e.target).closest($ul).length) {
				 $ul.find('.hv_active').removeClass('hv_active').children('ul.nav').hide();
		         $ul.find('.tabin').removeClass('tabin');
		         $ul.find('.tabout').removeClass('tabout');
		       }
			}); 
	
	   });
		
	   /* shift tab key */
	   $a.keydown(function (e) {
	      if((e.which == 9 || e.keyCode == 9) && e.shiftKey){
	      	  $shiftKey = e.shiftKey;
	          $(this).parent().children('.nav').hide();
	          /* remove tabin and hv_active class */
	          if($(this).parent().children('.nav').parent('.tabin').length){
	          	$(this).parent().children('.nav').parent('.hv_active').removeClass('hv_active');
	          	$(this).parent().children('.nav').parent('.tabin').removeClass('tabin');
	          }
	          /* remove tabout and hv_active class */
	          if($(this).parent().children('.nav').parent('.tabout').length){
	          	$(this).parent().children('.nav').parent('.hv_active').removeClass('hv_active');
	          	$(this).parent().children('.nav').parent('.tabout').removeClass('tabout');
	          }
	      }else{
	      	$shiftKey = false;
	      }
	   });
   }    
    
    /* flyout navigation for desktop */
    function flyoutNavigation(){
		$('.navbar .nav-collapse ul li').hover(function(){
                if($(this).children('.nav').length) {
                    $(this).children('.nav').show();
                    $(this).addClass('hv_active');
                }
            },function(){
                $(this).children('.nav').blur().hide().removeClass('hv_active');
                $(this).removeClass('hv_active');
				
				/* classes removed which is added for tab key code */
				if($(this).parents().find('.tabin').length || $(this).parents().find('.tabout').length ){
					$(this).parents().find('.tabin').removeClass('tabin');
					$(this).parents().find('.tabout').removeClass('tabout');
					$(this).parents().find('.hv_active').removeClass('hv_active');
				}
            })
	}
	
	/* menu navigation for mobile */
	function menuNavigation(){
		$('a .arrow').on('click', function(){
                $(this).parent().next('.nav').slideToggle(function(){
                		$('#header .nav-collapse.collapse').css('height','100%');
                });
                $(this).toggleClass('hv_active');
                $(this).parent().parent('li').toggleClass('hv_active');
                return false;
            })
	}
	
	/* onload navigation */
    $( document ).ready(function() {
    	if(whichDevice()=="mobile"){
           menuNavigation();
        }
        else {
           flyoutNavigation();
           tabNavigation();
		
        }
	});
	//for input type hidden container tag tracking
	$(document).ready(function() {
        $('[data-mz-ct-identifier]').each(function() {
            var $this = $(this),
                    mzCollection = {
                category: $this.attr('data-ct-category'),
                action: $this.attr('data-ct-action'),
                information: $this.attr('data-ct-information'),
                funnel: $this.attr('data-ct-funnel'),
                id: $this.attr('data-mz-ct-identifier'),
                type: $this.attr('data-ct-attr'),
                enabled: $this.attr('data-ct-enabled') ? $this.attr('data-ct-enabled') : true
            };
            //CT.trackableItems[mzCollection.id] = mzCollection;
        });
    });
	//for fixing side bar
$(document).ready(function(){
    
      calculateHeight();
         
         
     
    
 });

}(window.jQuery);


function calculateHeight(){
	//only for desktop
	var mql = window.matchMedia("screen and (min-width : 768px)");
    if (mql.matches){ 
		//wait for 1 second - delay in toggle
		setTimeout(function(){
			if(jQuery("#product .container_bg").height() + jQuery("#product .carousel-inner").height() <jQuery(".left_nav_component").height()){
				var heightDiff =jQuery(".left_nav_component").height() -(jQuery("#right_side_content_product").height() + jQuery("#product .carousel-inner").height())
				jQuery("#product .container_bg").height(jQuery("#product .container_bg").height() + heightDiff);
			}
			else{jQuery("#product .container_bg").css({height: "auto"})}

		}, 1000);
	}
		
}