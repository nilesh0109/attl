BRANDSETTINGS = {
    expandNavigation : true, // should be set per page
    tabNavigation : true, // should be set per page
    formChangeValidation : true, // should only be set on recipe search page - will run the form no change validation on a page
    hasSortByFilter : true, // should only be set on pages that have a sort by
    hasFAQ:true,
    autoRotate : true,
    rotationTime : 200,
    /* 
      Available carousel types:
      'background-layer' - will absolutely position the image under the carousel image copy text. Essentiallly, acting as a background image.
    */
    carouselType : 'background-layer',
    heightEqualized : true
};