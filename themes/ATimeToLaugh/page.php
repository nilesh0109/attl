<?php
get_header();
?>
<div class="container">
    <article class="single-article-detail">
        <div class="single-article-detail-inner detail-page-video">
            <?php
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
            if (isset($image) && isset($image[0])) {
                ?>
                <figure><img class="pull-right" src="<?php echo $image[0]; ?>" alt="<?php esc_html(the_title()); ?>" title="<?php esc_html(the_title()); ?>" /></figure>
                <?php } ?>
            <h2><?php the_title(); ?></h2>
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                    the_content();
                endwhile;
            endif;

            if (class_exists('RenderSocialMedia')) {
                global $obSocialMedia;
                $obSocialMedia->socialMediaContent();
            }
            ?>
        </div>
    </article>
</div>
<?php
if (get_post_meta($post->ID, RESPONSIVE_IFRAME_ATTRIBUTE, true) == 1) {
    wp_enqueue_script('responsiveiframe', get_template_directory_uri() . '/js/libs/responsiveiframe.js', array('mizkan'), false, true);
}
?>
<?php get_footer(); ?>