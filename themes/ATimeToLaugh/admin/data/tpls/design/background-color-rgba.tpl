<!--rgba-->
<div class="field-sub-container multi">
    <label for="carousel-cap-bg-color">Solid</label>
    <input type="radio"  class="skip" value="bg-color" name="cap-background" id="carousel-cap-bg-color">
    <div rel="solid" class="hide-default">
        <label for="carousel-cap-background-color">Color</label>
        <input type="text" value="" data-css="background-rgba" name="color" id="carousel-cap-background-color" />
    </div>
</div>