<div class="field-sub-container">
    <label for="btn-border-radius">Radius</label>
    <select data-css="border-radius" name="font-size" id="btn-border-radius">
        <option value="0">None</option>
        <option value="4px">4px</option>
        <option value="5px">5px</option>
        <option value="6px">6px</option>
        <option value="7px">7px</option>
        <option value="8px">8px</option>
        <option value="9px">9px</option>
        <option value="10px">10px</option>
        <option value="11px">11px</option>
        <option value="12px">12px</option>
        <option value="13px">13px</option>
        <option value="14px">14px</option>
        <option value="15px">15px</option>
        <option value="16px">16px</option>
        <option value="17px">17px</option>
        <option value="18px">18px</option>
        <option value="19px">19px</option>
        <option value="20px">20px</option>

    </select>
</div>
