<div class="field-sub-container">
    <label>Font Family</label>
    <select  name="font-family" data-css="font-family">
        <optgroup label="Serif Fonts">
            <option value="Georgia, serif">Georgia</option>
            <option value="Palatino Linotype,Book Antiqua, Palatino, serif">Palatino Linotype</option>
            <option value="Times New Roman, Times, serif">Times New Roman</option>
        </optgroup>
        <optgroup label="Sans-Serif Fonts">
            <option value="Arial, Helvetica, sans-serif">Arial</option>
            <option value="Arial Black, Gadget, sans-serif">Arial Black</option>
            <option value="Comic Sans MS, cursive, sans-serif">Comic Sans MS</option>
            <option value="Impact, Charcoal, sans-serif">Impact</option>
            <option value="Lucida Sans Unicode, Lucida Grande, sans-serif">Lucida Sans Unicode</option>
            <option value="Tahoma, Geneva, sans-serif">Tahoma</option>
            <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
            <option value="Verdana, Geneva, sans-serif">Verdana</option>
        </optgroup>
        <optgroup label="Monospace Fonts">
            <option value="Courier New, Courier, monospace">Courier New</option>
            <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
        </optgroup>
    </select>
</div>
