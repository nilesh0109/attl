<div class="field-sub-container">
    <label for="text-decoration">Style</label>
    <select id="text-decoration" name="text-decoration" data-css="text-decoration">
        <option value="none">Normal</option>
        <option value="underline">Underline</option>
    </select>
</div>