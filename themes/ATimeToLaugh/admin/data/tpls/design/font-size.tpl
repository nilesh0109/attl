
<div class="field-sub-container">
    <label>Size</label>
    <select data-css="font-size" name="font-size" >
        <option value="8px">8px</option>
        <option value="9px">9px</option>
        <option value="10px">10px</option>
        <option value="11px">11px</option>
        <option value="12px">12px</option>
        <option value="13px">13px</option>
        <option value="14px">14px</option>
        <option value="15px">15px</option>
        <option value="16px">16px</option>
        <option value="17px">17px</option>
        <option value="18px">18px</option>
        <option value="19px">19px</option>
        <option value="20px">20px</option>
        <option value="21px">21px</option>
        <option value="22px">22px</option>
        <option value="23px">23px</option>
        <option value="24px">24px</option>
        <option value="25px">25px</option>
        <option value="26px">26px</option>
        <option value="27px">27px</option>
        <option value="28px">28px</option>
        <option value="29px">29px</option>
        <option value="30px">30px</option>
        <option value="31px">31px</option>
        <option value="32px">32px</option>
        <option value="33px">33px</option>
        <option value="34px">34px</option>
        <option value="35px">35px</option>
        <option value="36px">36px</option>
        <option value="37px">37px</option>
        <option value="38px">38px</option>
        <option value="39px">39px</option>
        <option value="40px">40px</option>
    </select>
</div>