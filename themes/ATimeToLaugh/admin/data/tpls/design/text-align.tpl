<div class="field-sub-container">
    <label for="carousel-text-align">Text Align</label>
    <select data-css="text-align" name="carousel-text-align" id="carousel-text-align">
        <option value="left">Left</option>
        <option value="center">Center</option>
        <option value="right">Right</option>
    </select>
</div>