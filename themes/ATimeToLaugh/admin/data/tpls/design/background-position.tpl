<div class="field-sub-container">
    <label for="menu-btn-1"><span class="menu-btn-1"> Button 1</span></label>
    <input type="radio" id="menu-btn-1" class="mobile-btn" name="mobile-btn" data-css="background-position" value="0 0" data-default="">

    <label for="menu-btn-2"><span class="menu-btn-2"> Button 2</span></label>
    <input type="radio" id="menu-btn-2" class="mobile-btn" name="mobile-btn" data-css="background-position" value="0 -48px" data-default="">

    <label for="menu-btn-3"><span class="menu-btn-3"> Button 3</span></label>
    <input type="radio" id="menu-btn-3" class="mobile-btn" name="mobile-btn" data-css="background-position" value="0 -96px" data-default="">

    <label for="menu-btn-4"><span class="menu-btn-4"> Button 4</span></label>
    <input type="radio" id="menu-btn-4" class="mobile-btn" name="mobile-btn" data-css="background-position" value="0 -142px" data-default="">
</div>