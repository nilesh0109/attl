<!--rgba-->
<div class="field-sub-container multi">
    <label for="carousel-cap-transback-color">Transparent</label>
    <input type="radio" class="skip" value="transparent-color" name="cap-background" id="carousel-cap-transback-color">
    <div style="display:none" class="hide-default">
        <label for="carousel-cap-trans-color">Color</label>
        <input type="text" value="none transparent" data-css="background" name="color" id="carousel-cap-trans-color" data-default>
    </div>
</div>