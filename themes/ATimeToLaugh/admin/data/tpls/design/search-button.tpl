<div class="field-sub-container">
    <label for="search-btn-1"><span class="search-btn-1"> Button 1</span></label>
    <input type="radio" id="search-btn-1" class="search-btn" name="search-btn" data-css="background-position" value="0 0" data-default="">
    <label for="search-btn-2"><span class="search-btn-2">Button 2</span></label>
    <input type="radio" id="search-btn-2" class="search-btn" name="search-btn" data-css="background-position" value="0 -37px" data-default="">
    <label for="search-btn-3"><span class="search-btn-3">Button 3</span></label>
    <input type="radio" id="search-btn-3" class="search-btn" name="search-btn" data-css="background-position" value="0 -70px" data-default="">
    <label for="search-btn-4"><span class="search-btn-4">Button 4</span></label>
    <input type="radio" id="search-btn-4" class="search-btn" name="search-btn" data-css="background-position" value="0 -104px" data-default="">
</div>