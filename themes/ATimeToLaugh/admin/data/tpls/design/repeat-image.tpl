<div class="field-sub-container">
    <label>Repeat</label>
    <select  name="Repeat" data-css="background-repeat" class="background">

        <option value="repeat-x">Repeat-x</option>
        <option value="repeat-y">Repeat-y</option>
        <option value="repeat">Repeat-X,Repeat-Y</option>
        <option value="no-repeat">No Repeat</option>

    </select>
</div>