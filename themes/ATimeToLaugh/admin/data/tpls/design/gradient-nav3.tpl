
<!-- .field-sub-container -->
<!--gradient-->
<div class="field-sub-container multi">
    <label for="grad-color">Gradient</label>
    <input type="radio" id="grad-color-nav3" name="background4" value="grad-color" class="skip" >

    <div class="hide-default" rel="gradient">
        <label for="top-color">Top</label>
        <input type="text" id="top-color" name="top-color" data-css="top" value="{+startcolor+}" data-default="{+startvalue+}">
        <label for="bottom-color">Bottom</label>
        <input type="text" id="bottom-color" name="bottom-color" data-css="bottom" value="{+endcolor+}" data-default="{+endvalue+}">
    </div>
</div>
<!-- .field-sub-container -->