<div class="field-sub-container">
    <label for="menu-btn-1"><span class="carousel-btn-1"> Button 1</span></label>
    <input type="radio" id="menu-btn-1" class="carousel-btn" name="mobile-btn" data-css="background-position" value="0 -155px" data-default="">
    <label for="menu-btn-2"><span class="carousel-btn-2">Button 2</span></label>
    <input type="radio" id="menu-btn-2" class="carousel-btn" name="mobile-btn" data-css="background-position" value="0 -104px" data-default="">
    <label for="menu-btn-3"><span class="carousel-btn-3">Button 3</span></label>
    <input type="radio" id="menu-btn-3" class="carousel-btn" name="mobile-btn" data-css="background-position" value="0 -56px" data-default="">
    <label for="menu-btn-4"><span class="carousel-btn-4">Button 4</span></label>
    <input type="radio" id="menu-btn-4" class="carousel-btn" name="mobile-btn" data-css="background-position" value="0 -6px" data-default="">
</div>