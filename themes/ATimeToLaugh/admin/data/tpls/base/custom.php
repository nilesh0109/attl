<?php

/**
 * generateCustomUICss:for generating code mirror text box
 * @param type $data
 */
function generateCustomUICss($data) {
    ?>
    <div class="content">
        <div class="content-inner">
            <?php echo "<h2>" . __("Custom Css") . "</h2>"; ?>
            <p> <?php _e("All the styles saved here will reflect directly on site ") ?></p>
            <div class="section">
                <form action ="" method="post" id="form" enctype="multipart/form-data"  class="admin-options">
                    <div class="section-group">
                        <?php
                        preg_match('/(\/\*\*timestamp[\d]+\*\*\/)/', $data, $arr1);
                        $time_stamp = str_replace('/**timestamp', '', $arr1[1]);
                        $time_stamp = str_replace('**/', "", $time_stamp);
                        $data = str_replace('/**timestamp' . $time_stamp . '**/', '', $data);
                        ?>
                        <h3>Custom CSS Section</h3>
                        <div class="field-container-wrapper">
                            <?php if ($data != null) {
                                ?>
                                <textarea name="customCss" id ="customCss" rows ="10" cols ="100" ><?php echo ($data) ?> </textarea>
                            <?php } else {
                                ?>
                                <textarea name="customCss" id ="customCss" rows ="10" cols ="100" > </textarea>
                            <?php } ?>
                            <script>
                                var editor = CodeMirror.fromTextArea(document.getElementById("customCss"), {
                                    lineNumbers: true,
                                    matchBrackets: true
                                });
                            </script>
                            <input type = "hidden" name = "class" value = "customcss"/>
                            <input type = "hidden" name = "timestamp" value = "<?php echo $time_stamp ?>"/>
                        </div>
                    </div>
                    <?php do_action('theme_option_custom_css_form'); ?>
                    <div class = "btn">
                        <?php wp_nonce_field('mz_update_theme_options'); ?>
                        <input type = "hidden" name = "btnVal" id = "btnSubReset" value = ""/>
                        <button type="button" name="mySubmit"><?php _e("Submit") ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
}
?>