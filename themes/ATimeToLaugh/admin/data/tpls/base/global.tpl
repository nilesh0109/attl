<div class="section-group">
    <h3>General Styles</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector="body, p, .teaser3 p, .teaser4 p, .promo2 p, .promo3 p, .teaser5 p, .teaser7 p, .single-product-detail p, .single-article-detail p, .single-article-detail ul li, .single-article-detail ol li, .modern-footer p, .modern-footer .footer-banner figcaption, .help-block, .carousel-caption p">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector="body">
            <fieldset>
                <legend>Background:</legend>
                [+background-color+]
                [+gradient+]
                [+background-color-transparent+]
            </fieldset>
            <fieldset>
                <legend>Background Image:</legend>
                [+file+]
                [+repeat-image+]
            </fieldset>
        </div>
        <!-- .field-container -->

        <div class="field-container" data-selector=".main-wrapper">
            <fieldset>
                <legend>Main Wrapper Background:</legend>
                [+background-color-wrapper+]
                [+gradient-wrapper+]
                [+background-color-transparent-wrapper+]
            </fieldset>

        </div>
    </div>
    <!-- .field-container-wrapper -->
</div>
<!-- .section-group General-->

<div class="section-group">
    <h3>Headings</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector="h2, .carousel h2">
            <fieldset>
                <legend>H2 Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <!-- .field-container -->

        <div class="field-container" data-selector="h3, .teaser3 h3, .teaser4 h3, .promo2 h3, .promo3 h3, .teaser5 h3, legend">
            <fieldset>
                <legend>H3 Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <!-- .field-container -->

        <div class="field-container" data-selector="h4">
            <fieldset>
                <legend>H4 Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <!-- .field-container -->

        <div class="field-container" data-selector="h5">
            <fieldset>
                <legend>H5 Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <!-- .field-container -->
    </div>
    <!-- .field-container-wrapper -->
</div>
<!-- .section-group Heading-->

<div class="section-group">
    <h3>Hyperlinks</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector="a, .btn-link, .modern-footer a">
            <fieldset>
                <legend>Normal Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
                [+text-decoration+]
            </fieldset>
        </div>
        <!-- .field-container -->
        <div class="field-container" data-selector="a:hover, a:focus, .btn-link:hover, .btn-link:focus, .modern-footer a:hover, .modern-footer a:focus">
            <fieldset>
                <legend>Hover Font:</legend>
                [+color+]
                [+text-decoration+]
            </fieldset>
        </div>
        <!-- .field-container -->
    </div>
    <!-- .field-container-wrapper -->
</div>
<!-- .section-group Background-->
<div class="section-group">
    <h3>Button</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".btn, .carousel .carousel-caption .btn, .teaser3 .btn, .teaser4 .btn, .promo2 .btn, .promo3 .btn, .teaser5 .btn, .search-results .btn">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
                [+border-radius+]
            </fieldset>
            <fieldset>
                <legend>Background:</legend>
                [+background-color-nav+]
                [+gradient-nav+]
                [+background-color-transparent-nav+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".btn:hover, .btn:focus, .carousel .carousel-caption .btn:hover, .carousel .carousel-caption .btn:focus, .teaser3 .btn:hover, .teaser3 .btn:focus, .teaser4 .btn:hover, .teaser4 .btn:focus, .promo2 .btn:hover, .promo2 .btn:focus, .promo3 .btn:hover, .promo3 .btn:focus, .teaser5 .btn:hover, .teaser5 .btn:focus, .search-results .btn:hover, .search-results .btn:focus">
            <fieldset>
                <legend>Font Hover:</legend>
                [+color+]
            </fieldset>
            <fieldset>
                <legend>Background Hover:</legend>
                [+background-color-nav1+]
                [+gradient-nav1+]
                [+background-color-transparent-nav1+]
            </fieldset>
        </div>
    </div>
</div>

<!-- .btn -->