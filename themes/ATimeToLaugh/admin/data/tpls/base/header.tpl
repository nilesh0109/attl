<div class="section-group">
    <h3>Header Style</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".modern-header">
            <fieldset>
                <legend>Background:</legend>
                [+background-color+]
                [+gradient+]
                [+background-color-transparent+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-bottom-color+]
            </fieldset>
        </div>
    </div>
</div>
<div class="section-group">
    <h3>Navigation</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".navbar-inner">
            <fieldset>
                <legend>Background:</legend>
                [+background-color-nav+]
                [+gradient-nav+]
                [+background-color-transparent-nav+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".navbar .btn-navbar">
            <fieldset>
                <legend>Mobile Button:</legend>
                [+background-position+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".navbar .nav > li">
            <fieldset>
                <legend>Navigation Right Seprator: In Case of LTR</legend>
                [+border-right+]
                [+border-right-color+]
            </fieldset>
            <fieldset>
                <legend>Navigation Left Seprator: In Case of RTL Enabled</legend>
                [+border-left+]
                [+border-left-color+]
            </fieldset>
        </div>

        <div class="field-container" data-selector=".navbar .btn-navbar:hover, .navbar .btn-navbar:focus" style="display:none">
            <!-- Menu Button Hover -->
            [+background-position-hover+] </div>
        <div class="field-container" data-selector=".navbar .nav > li > a">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".navbar .nav > li > a:hover, .navbar .nav > li > a:focus">
            <fieldset>
                <legend>Font Hover:</legend>
                [+color+]
                [+text-decoration+]
            </fieldset>
            <fieldset>
                <legend>Font Hover Background:</legend>
                [+background-color-nav1+]
                [+gradient-nav1+]
                [+background-color-transparent-nav1+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".navbar .nav > .active > a, .navbar .nav > .active > a:hover, .navbar .nav > .active > a:focus">
            <fieldset>
                <legend>Font Active:</legend>
                [+color+]
            </fieldset>
            <fieldset>
                <legend>Font Active Background:</legend>
                [+background-color-nav2+]
                [+gradient-nav2+]
                [+background-color-transparent-nav2+]
            </fieldset>
        </div>
    </div>
</div>
<div class="section-group">
    <h3>Flyout Navigation</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".navbar .nav-collapse ul.nav ul">
            <fieldset>
                <legend>Background:</legend>
                [+background-color-nav3+]
                [+gradient-nav3+]
                [+background-color-transparent-nav3+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
            </fieldset>
        </div>

        <div class="field-container" data-selector=".navbar .nav-collapse ul.nav ul li">
            <fieldset>
                <legend>Border Separator:</legend>
                [+border-bottom-color+]
            </fieldset>
        </div>

        <div class="field-container" data-selector=".navbar .nav-collapse ul.nav ul li a">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".navbar .nav-collapse ul.nav ul > li > a:hover, .navbar .nav-collapse ul.nav ul > li > a:focus">
            <fieldset>
                <legend>Font Hover:</legend>
                [+color+]
                [+text-decoration+]
            </fieldset>
            <fieldset>
                <legend>Hover Background:</legend>
                [+background-color-nav4+]
                [+gradient-nav4+]
                [+background-color-transparent-nav4+]
            </fieldset>
        </div>
    </div>
</div>
<div class="section-group">
    <h3>Search</h3>
    <div class="field-container-wrapper" >
        <div class="field-container" data-selector=".form-search input[type='text']">
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
                [+border-radius+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".form-search .btn-search">
            <fieldset>
                <legend>Button:</legend>
                [+search-button+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".form-search .btn-search:hover, .form-search .btn-search:focus" style="display:none">[+search-button-hover+]</div>
    </div>
</div>