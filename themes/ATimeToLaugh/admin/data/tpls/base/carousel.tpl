<div class="section-group">
    <h3>Carousels</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".carousel-caption">
            <fieldset>
                <legend>Caption Background:</legend>
                [+background-color-rgba+]
                [+transparent-rgba+]
            </fieldset>

        </div>
        <div class="field-container" data-selector=".carousel h2">
            <fieldset>
                <legend>Heading:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".carousel-caption p">
            <fieldset>
                <legend>Description:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".carousel-control.left">
            <fieldset>
                <legend>Arrows:</legend>
                [+carousel-left+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".carousel-control.right" style="display:none"> [+carousel-right+] </div>
        <div class="field-container" data-selector=".carousel-control.left:hover, .carousel-control.left:focus" style="display:none"> [+carousel-left-hover+] </div>
        <div class="field-container" data-selector=".carousel-control.right:hover, .carousel-control.right:focus" style="display:none"> [+carousel-right-hover+] </div>

        <div class="field-container" data-selector=".carousel-indicators li.active">
            <fieldset>
                <legend>Bullet Active Color:</legend>
                [+color1+]
            </fieldset>

        </div>
        <div class="field-container" data-selector=".carousel-indicators li">
            <fieldset>
                <legend>Bullet Inactive Color:</legend>
                [+color1+]
            </fieldset>

        </div>

    </div>
</div>
<div class="section-group">
    <h3>Button</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".carousel .carousel-caption .btn">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
                [+border-radius+]
            </fieldset>
            <fieldset>
                <legend>Background:</legend>
                [+background-color+]
                [+gradient+]
                [+background-color-transparent+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".carousel .carousel-caption .btn:hover, .carousel .carousel-caption .btn:focus">
            <fieldset>
                <legend>Font Hover:</legend>
                [+color+]
            </fieldset>
            <fieldset>
                <legend>Background Hover:</legend>
                [+background-color-nav+]
                [+gradient-nav+]
                [+background-color-transparent-nav+]
            </fieldset>
        </div>
    </div>
</div>