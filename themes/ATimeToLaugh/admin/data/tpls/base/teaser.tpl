<div class="section-group">
    <h3>Teasers and Promos</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".teaser3, .teaser4, .promo2, .promo3, .article-category .thumbnail, .teaser5, .teaser7, .single-product-detail-inner, .single-article-detail-inner, .teaser7 .thumbnail, article.search-results">
            <fieldset>
                <legend>Background:</legend>
                [+background-color+]
                [+gradient+]
                [+background-color-transparent+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
                [+border-radius+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".teaser3 h3, .teaser4 h3, .promo2 h3, .promo3 h3, .teaser5 h3">
            <fieldset>
                <legend>Heading Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".teaser3 p, .teaser4 p, .promo2 p, .promo3 p, .teaser5 p, .teaser7 p, .single-product-detail p, .single-article-detail p, .single-article-detail ul li, .single-article-detail ol li">
            <fieldset>
                <legend>Description Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
    </div>
</div>
<div class="section-group">
    <h3>Button</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".teaser3 .btn, .teaser4 .btn, .promo2 .btn, .promo3 .btn, .teaser5 .btn">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-color+]
                [+border-radius+]
            </fieldset>
            <fieldset>
                <legend>Background:</legend>
                [+background-color-nav+]
                [+gradient-nav+]
                [+background-color-transparent-nav+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".teaser3 .btn:hover, .teaser3 .btn:focus, .teaser4 .btn:hover, .teaser4 .btn:focus, .promo2 .btn:hover, .promo2 .btn:focus, .promo3 .btn:hover, .promo3 .btn:focus, .teaser5 .btn:hover, .teaser5 .btn:focus">
            <fieldset>
                <legend>Font Hover:</legend>
                [+color+]
            </fieldset>
            <fieldset>
                <legend>Background Hover:</legend>
                [+background-color-nav1+]
                [+gradient-nav1+]
                [+background-color-transparent-nav1+]
            </fieldset>
        </div>
    </div>
</div>