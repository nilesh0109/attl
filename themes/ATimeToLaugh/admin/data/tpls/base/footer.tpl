<div class="section-group">
    <h3>Footer Styles</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".modern-footer">
            <fieldset>
                <legend>Background:</legend>
                [+background-color+]
                [+gradient+]
                [+background-color-transparent+]
            </fieldset>
            <fieldset>
                <legend>Border:</legend>
                [+border-top-color+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".modern-footer p, .modern-footer .footer-banner figcaption">
            <fieldset>
                <legend>Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
    </div>
</div>
<div class="section-group">
    <h3>Links</h3>
    <div class="field-container-wrapper">
        <div class="field-container" data-selector=".modern-footer a">
            <fieldset>
                <legend>Normal Font:</legend>
                [+color+]
                [+font-size+]
                [+font-family+]
                [+font-weight+]
            </fieldset>
        </div>
        <div class="field-container" data-selector=".modern-footer a:hover, .modern-footer a:focus">
            <fieldset>
                <legend>Hover Font:</legend>
                [+color+]
                [+text-decoration+]
            </fieldset>
        </div>
    </div>
</div>