<?php ?>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <script src="../../js/libs/html5shiv.js"></script>

    </head>

    <body class="article-category">
        <style>

        </style>
        <?php if ($_REQUEST['customcsspath']) { ?>
            <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <?php } ?>
        <article class="teaser3 visible-desktop visible-tablet"  style="width:300px; float:left">
            <figure><img title="col-teaser3" alt="col-teaser3" src="../../img/default/image-370x300.png"></figure>
            <div class="caption">
                <h3>Teaser 3</h3>
                <p>Come, Connect, Let's Ignite. Deliver the BEST, We are LITE. </p>
                <a title="Teaser" class="btn btn-small" href="#">Learn More</a>
            </div>
            <!-- /.caption -->
        </article>
        <article class="promo2 visible-desktop visible-tablet" style="width: 170px; float: left; margin: 0px 0px 0px 30px;">
            <figure><img src="../../img/default/image-200x200.png" alt="promo2" title="promo2"/></figure>
            <div class="caption">
                <h3>Promo 2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                <a href="#" class="btn btn-mini" title="Promo2"> Learn More </a>
            </div>
            <!-- /.caption -->
        </article>
        <!-- /.promo2 -->
        <ul class="thumbnails" style="clear:both">
            <li class="span5">
                <div class="thumbnail media">
                    <div class="teaser5">
                        <figure class="pull-left"><img src="../../img/default/image-140x140.png" alt="teaser5" title="teaser5" /></figure>
                        <div class="media-body">
                            <h3 class="media-heading">Teaser 5</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <a title="Teaser5" class="btn btn-small" href="#"> Learn More </a>
                        </div>
                        <!-- .media-body -->
                    </div>
                    <!-- .teaser5 -->
                </div>
                <!-- .thumbnail -->
            </li>
        </ul>
        <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
            <script type="text/javascript">
                (function() {
                    var config = {
                        kitId: '<?php echo $_REQUEST['kitid']; ?>'
                    };
                    var d = false;
                    var tk = document.createElement('script');
                    tk.src = '//use.typekit.net/' + config.kitId + '.js';
                    tk.type = 'text/javascript';
                    tk.async = 'true';
                    tk.onload = tk.onreadystatechange = function() {
                        var rs = this.readyState;
                        if (d || rs && rs != 'complete' && rs != 'loaded')
                            return;
                        d = true;
                        try {
                            Typekit.load(config);
                        } catch (e) {
                        }
                    };
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(tk, s);
                })();
            </script>
        <?php } ?>

        <!-- .thumbnails -->
    </body>
</html>