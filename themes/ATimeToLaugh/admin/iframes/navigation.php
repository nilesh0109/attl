<?php ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
                <link href="../../style.css" rel="stylesheet" type="text/css">
                    <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>" rel="stylesheet" type="text/css">

                        </head>

                        <body>
                            <style>

                            </style>
                            <?php if ($_REQUEST['customcsspath']) { ?>
                                <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
                                <?php } ?>
                                <header class="modal-header" id="header">
                                    <h4> PREVIEW AREA </h4>
                                    <section class="navbar navbar-static-top">
                                        <section class="navbar-inner">
                                            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                                            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" style="display:block"></button>
                                            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                                            <div class="container">
                                                <nav role="navigation" class="nav-collapse">
                                                    <ul class="nav">
                                                        <li class="active"><a title="Home" href="#">Home</a></li>
                                                        <li><a title="Product" href="#">Product</a></li>
                                                        <li><a title="Product Detail" href="#">Product Detail</a></li>
                                                        <li><a title="Article" href="#">Article</a></li>
                                                        <li><a title="Article Category" href="#">Article Category</a></li>
                                                        <li><a title="Article Details" href="#">Article Details</a></li>
                                                    </ul>
                                                    <form class="navbar-form form-search pull-right">
                                                        <input type="text" class="input-medium">
                                                            <button type="submit" class="btn">Search</button>
                                                    </form>
                                                </nav>
                                                <!--/.nav-collapse -->
                                            </div>
                                            <!--/.container -->
                                        </section>
                                        <!-- /.navbar-inner -->
                                    </section>
                                    <!-- /.navbar -->
                                </header>
                                <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
                                    <script type="text/javascript">
                                        (function() {
                                            var config = {
                                                kitId: '<?php echo $_REQUEST['kitid']; ?>'
                                            };
                                            var d = false;
                                            var tk = document.createElement('script');
                                            tk.src = '//use.typekit.net/' + config.kitId + '.js';
                                            tk.type = 'text/javascript';
                                            tk.async = 'true';
                                            tk.onload = tk.onreadystatechange = function() {
                                                var rs = this.readyState;
                                                if (d || rs && rs != 'complete' && rs != 'loaded')
                                                    return;
                                                d = true;
                                                try {
                                                    Typekit.load(config);
                                                } catch (e) {
                                                }
                                            };
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(tk, s);
                                        })();
                                    </script>
                                <?php } ?>

                        </body>
                        </html>