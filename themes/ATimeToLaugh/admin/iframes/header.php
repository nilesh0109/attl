<?php ?>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">

    </head>

    <body>
        <style>

        </style>
        <?php if ($_REQUEST['customcsspath']) { ?>
            <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <?php } ?>
        <header class="modern-header" id="header">
            <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
            <section class="banner clearfix">
                <div class="container">
                    <h1 class="logo pull-left" style="height: 100px"></h1>
                </div>
                <!--/.container -->
            </section>
            <!-- /.banner -->
            <section class="navbar navbar-static-top">
                <section class="navbar-inner">
                    <div class="container" style="width:100% !important">
                        <nav role="navigation" class="nav-collapse">
                            <ul class="nav">
                                <li class="active"><a title="Home" href="#">Home</a></li>
                                <li><a title="Product" href="#">Product</a></li>
                                <li><a title="Product Detail" href="#">Product Detail</a></li>
                                <li><a title="Article" href="#">Article</a></li>
                                <li><a title="Article Category" href="#">Article Category</a></li>
                                <li><a title="Article Details" href="#">Article Details</a></li>
                            </ul>
                            <form class="navbar-form form-search pull-right">
                                <input type="text" class="input-medium">
                                <button type="submit" class="btn-search">Search</button>
                            </form>
                        </nav>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container -->
                </section>
                <!-- /.navbar-inner -->
            </section>
            <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
                <script type="text/javascript">
                    (function() {
                        var config = {
                            kitId: '<?php echo $_REQUEST['kitid']; ?>'
                        };
                        var d = false;
                        var tk = document.createElement('script');
                        tk.src = '//use.typekit.net/' + config.kitId + '.js';
                        tk.type = 'text/javascript';
                        tk.async = 'true';
                        tk.onload = tk.onreadystatechange = function() {
                            var rs = this.readyState;
                            if (d || rs && rs != 'complete' && rs != 'loaded')
                                return;
                            d = true;
                            try {
                                Typekit.load(config);
                            } catch (e) {
                            }
                        };
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(tk, s);
                    })();
                </script>
            <?php } ?>

            <!-- /.navbar -->
        </header>
    </body>
</html>