<?php ?>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <script src="../../js/libs/html5shiv.js"></script>
    </head>

    <body>
        <style>

        </style>
        <?php if ($_REQUEST['customcsspath']) { ?>
            <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <?php } ?>
        <nav role="navigation" class="left-nav visible-desktop visible-tablet" id="left_nav" style="width:400px">
            <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
            <ul class="nav">
                <li class="first">
                    <a title="Prodcut Type" href="#">Link Parent<span class="tree-toggler">Click to expand</span></a>
                    <ul class="nav tree" >
                        <li class="first"><a title="Link" href="#">Link Child 1</a></li>
                        <li class="last">
                            <a title="Link" class="active" href="#">Link Child 2<span class="tree-toggler expand">Click to expand</span></a>
                            <ul class="nav tree" style="display: block;">
                                <li class="first"><a title="Link" href="#">Link Sub-Child 1</a></li>
                                <li class="last">
                                    <a title="Link" href="#">Link Sub-Child 2 <span class="tree-toggler">Click to expand</span></a>
                                </li>
                            </ul>
                            <!-- Level-2 /.nav -->
                        </li>
                        <!-- Level-2 First Link -->
                    </ul>
                    <!-- /.nav -->
                </li>
                <!-- Level-1 First Link -->
            </ul>
            <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
                <script type="text/javascript">
                    (function() {
                        var config = {
                            kitId: '<?php echo $_REQUEST['kitid']; ?>'
                        };
                        var d = false;
                        var tk = document.createElement('script');
                        tk.src = '//use.typekit.net/' + config.kitId + '.js';
                        tk.type = 'text/javascript';
                        tk.async = 'true';
                        tk.onload = tk.onreadystatechange = function() {
                            var rs = this.readyState;
                            if (d || rs && rs != 'complete' && rs != 'loaded')
                                return;
                            d = true;
                            try {
                                Typekit.load(config);
                            } catch (e) {
                            }
                        };
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(tk, s);
                    })();
                </script>
            <?php } ?>

            <!--/.nav -->
        </nav>
    </body>
</html>