<?php ?>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <script src="../../js/libs/html5shiv.js"></script>

    </head>
    <body>
        <style>
        </style>
        <?php if ($_REQUEST['customcsspath']) { ?>
            <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <?php } ?>
        <h2> Heading 2 </h2>
        <h3> Heading 3 </h3>
        <h4> Heading 4 </h4>
        <h5> Heading 5 </h5>
        <p> Paragraph text :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dolor diam, dictum ut massa eu, feugiat ultricies purus. Pellentesque arcu odio, elementum eget justo in, tincidunt gravida lorem</p>
        <a href="#"> Hyperlink </a><br /> <br />
        <a href="#" class="btn" title="Learn More"> Learn More </a>

        <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
            <script type="text/javascript">
                (function() {
                    var config = {
                        kitId: '<?php echo $_REQUEST['kitid']; ?>'
                    };
                    var d = false;
                    var tk = document.createElement('script');
                    tk.src = '//use.typekit.net/' + config.kitId + '.js';
                    tk.type = 'text/javascript';
                    tk.async = 'true';
                    tk.onload = tk.onreadystatechange = function() {
                        var rs = this.readyState;
                        if (d || rs && rs != 'complete' && rs != 'loaded')
                            return;
                        d = true;
                        try {
                            Typekit.load(config);
                        } catch (e) {
                        }
                    };
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(tk, s);
                })();
            </script>
        <?php } ?>

    </body>
</html>