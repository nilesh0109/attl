<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>

        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <script src="../../js/libs/html5shiv.js"></script>

    </head>

    <body>
        <style>

        </style>
        <?php if ($_REQUEST['customcsspath']) { ?>
            <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <?php } ?>
        <section class="carousel slide" id="hero_carousel"  style="width:700px">
            <div class="carousel-inner">
                <div class="item active">
                    <figure><img alt="carousel-1" src="../../img/default/carousel-1-1170x498.png"></figure>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2>Mizkan Slide 1</h2>
                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dolor diam, dictum ut massa eu, feugiat ultricies purus. Pellentesque arcu odio, elementum eget justo in, tincidunt gravida lorem. Phasellus volutpat convallis dui, ut tempus risus bibendum a. Pellentesque porttitor nibh enim, cursus sollicitudin urna pretium eget. Cras molestie commodo facilisis. Vestibulum placerat hendrerit augue eu sagittis. Quisque vel mi aliquam, porta neque ac, tempus turpis. Aliquam sollicitudin aliquet ipsum, sit amet egestas diam varius vel. Etiam sit amet arcu nec nisl congue tincidunt quis sit amet lorem. Quisque leo velit, elementum sed eros id, porta porttitor nunc.</p>
                            <a title="Learn More" class="btn" href="#"> Learn More </a>
                        </div>
                    </div>
                </div>
                <!-- /.item -->
            </div>
            <!-- /.carousel-inner -->

            <a data-slide="prev" href="#hero_carousel" class="left carousel-control"></a>
            <a data-slide="next" href="#hero_carousel" class="right carousel-control"></a>
        </section>
        <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
            <script type="text/javascript">
                (function() {
                    var config = {
                        kitId: '<?php echo $_REQUEST['kitid']; ?>'
                    };
                    var d = false;
                    var tk = document.createElement('script');
                    tk.src = '//use.typekit.net/' + config.kitId + '.js';
                    tk.type = 'text/javascript';
                    tk.async = 'true';
                    tk.onload = tk.onreadystatechange = function() {
                        var rs = this.readyState;
                        if (d || rs && rs != 'complete' && rs != 'loaded')
                            return;
                        d = true;
                        try {
                            Typekit.load(config);
                        } catch (e) {
                        }
                    };
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(tk, s);
                })();
            </script>
        <?php } ?>

    </body>
</html>