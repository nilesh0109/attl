<?php ?>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link href="../../css/framework/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/custom/mz.css" rel="stylesheet" type="text/css">
        <link href="../../style.css" rel="stylesheet" type="text/css">
        <link href= "<?php echo urldecode(( $_REQUEST['csspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
        <script src="../../js/libs/html5shiv.js"></script>

    </head>
    <style>

    </style>
    <?php if ($_REQUEST['customcsspath']) { ?>
        <link href= "<?php echo urldecode(( $_REQUEST['customcsspath'])); ?>?<?php echo $_REQUEST['version'] ?>" rel="stylesheet" type="text/css">
    <?php } ?>
    <body>
        <footer class="modern-footer" id="footer" style="overflow:hidden">
            <div class="container" style="width:100%">
                <nav id="footer_nav">
                    <ul class="inline">
                        <li> <a class="navItemLink" href="#" title="Contact Us">Contact Us</a> </li>
                        <li> <a class="navItemLink" href="#" title="About Us">About Us</a> </li>
                        <li> <a class="navItemLink" href="#" title="Sitemap">Sitemap</a> </li>
                        <li> <a class="navItemLink" target="_blank" href="#" title="Cookies">Cookies</a> </li>
                        <li> <a class="navItemLink" target="_blank" href="#" title="Mizkan">Mizkan</a> </li>
                        <li> <a class="navItemLink" target="_blank" href="#" title="Terms of Service">Terms of Service</a> </li>
                        <li> <a class="navItemLink" target="_blank" href="#" title="Privacy Policy">Privacy Policy</a> </li>
                    </ul>
                </nav>
                <!-- /.footer_nav -->
                <aside class="footer-banner clearfix">

                    <p class="site-footer">This web site is directed only to U.S. consumers for products and services of Mizkan United States.</p>
                </aside>
                <!-- /.footer-banner -->
            </div>
            <!-- /.container -->
        </footer>
        <?php if (isset($_REQUEST['kitid']) && strlen($_REQUEST['kitid']) > 2) { ?>
            <script type="text/javascript">
                (function() {
                    var config = {
                        kitId: '<?php echo $_REQUEST['kitid']; ?>'
                    };
                    var d = false;
                    var tk = document.createElement('script');
                    tk.src = '//use.typekit.net/' + config.kitId + '.js';
                    tk.type = 'text/javascript';
                    tk.async = 'true';
                    tk.onload = tk.onreadystatechange = function() {
                        var rs = this.readyState;
                        if (d || rs && rs != 'complete' && rs != 'loaded')
                            return;
                        d = true;
                        try {
                            Typekit.load(config);
                        } catch (e) {
                        }
                    };
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(tk, s);
                })();
            </script>
        <?php } ?>

    </body>
</html>