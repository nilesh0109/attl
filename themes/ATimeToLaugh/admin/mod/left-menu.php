<div class="left-menu">
    <ul class="parent nav-tab-wrapper">
        <li class="has-children">

            <?php if (isset($_GET["class"]) && ($_GET["class"] != "global")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=global"><?php _e("Global Styles") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active" ><?php _e("Global Styles") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>
        <li class="has-children">
            <?php if (($_GET["class"] != "header")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=header"><?php _e("Header") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active"><?php _e("Header") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>



        <li class="has-children">
            <?php if (($_GET["class"] != "left_navigation")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=left_navigation"><?php _e("Left Navigation") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active"><?php _e("Left Navigation") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>
        <li class="has-children">
            <?php if (($_GET["class"] != "carousel")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=carousel"><?php _e("Carousels") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active"><?php _e("Carousels") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>
        <li class="has-children">
            <?php if (($_GET["class"] != "teaser")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=teaser"><?php _e("Teasers and Promos") ?></a></span>
            <?php } else { ?>

                <span class="list-tile active"><?php _e("Teasers and Promos") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>

        <li class="has-children">
            <?php if (($_GET["class"] != "footer")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=footer"><?php _e("Footer") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active"><?php _e("Footer") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>

        <li class="has-children">
            <?php if (($_GET["class"] != "custom")) {
                ?>
                <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=custom"><?php _e("Custom CSS") ?></a></span>
            <?php } else { ?>
                <span class="list-tile active"><?php _e("Custom CSS") ?></span>
                <span class="arrow"></span>
            <?php } ?>
        </li>
        <?php if (ContactUsWidget_Option::getInstance()->is_contactus_widget_enable()) { ?>
            <li class="has-children">
                <?php if (($_GET["class"] != "contactus")) {
                    ?>
                    <span class="list-tile"><a href="themes.php?page=<?php echo(MZ_EDIT_ADMIN_INTERFACE_SLUG) ?>&amp;class=contactus"><?php _e("Contact Us") ?></a></span>
                <?php } else { ?>
                    <span class="list-tile active"><?php _e("Contact Us") ?></span>
                    <span class="arrow"></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
</div>