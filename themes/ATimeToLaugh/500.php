<?php
/**
 * This document set is the property of Mizkan, and contains confidential and trade secret
 * information.
 * It cannot be transferred from the custody or control of Mizkan except as
 * authorized in writing by an officer of Mizkan. Neither this item nor the information it
 * contains can be used, transferred, reproduced, published, or disclosed, in whole or in part,
 * directly or indirectly, except as expressly authorized by an officer of Mizkan, pursuant to
 * written agreement.
 *
 * Copyright(c) Mizkan 2014
 *
 * @category Page
 * @package Mizkan
 * @author Sanchit Baveja <sbaveja@sapient.com>
 *
 */
global $body_class;
$body_class = 'site-map';
get_header();
header('HTTP/1.0 500 occured');
require_once 'menu_traverse.php';
?>
<section id="main" role="main">
    <div class="container min-height">

        <h2><?php _e('Error 500 Occured', LANGUAGE_DOMAIN_NAME) ?></h2>

        <p> <?php _e("We couldn't find the information you're looking for. Please click on the navigation above for navigating to the main site.", LANGUAGE_DOMAIN_NAME) ?></p>

    </div>
    <!-- .container -->
</section>
<?php get_footer(); ?>